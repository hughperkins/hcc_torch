require "torch"
hctorch = require "libhctorch"

torch.HcStorage.__tostring__ = torch.FloatStorage.__tostring__
torch.HcTensor.__tostring__ = torch.FloatTensor.__tostring__

include('Tensor.lua')
include('FFI.lua')
include('test.lua')

function hctorch.withDevice(newDeviceID, closure)
  local curDeviceID = hctorch.getDevice()
  hctorch.setDevice(newDeviceID)
  local vals = {pcall(closure)}
  hctorch.setDevice(curDeviceID)
  if vals[1] then
    return unpack(vals, 2)
  end
  error(unpack(vals, 2))
end

return hctorch
