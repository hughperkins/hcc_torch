#include "utils.h"
#include "luaT.h"
#include "THHcGeneral.h"
#include "THHcTensorRandom.h"
#include <iostream>

extern void hctorch_HcStorage_init(lua_State* L);
extern void hctorch_HcTensor_init(lua_State* L);
extern void hctorch_HcTensorMath_init(lua_State* L);
extern void hctorch_HcTensorOperator_init(lua_State* L);

static int hctorch_synchronize(lua_State* L) {
  std::vector<hc::accelerator> vecAcc = hc::accelerator::get_all();
  std::vector<hc::accelerator>::iterator it;
  for(it = vecAcc.begin(); it < vecAcc.end(); it++) {
    ((*it).get_default_view()).wait();
  }
  return 0;
}

static int hctorch_getDevice(lua_State* L) {
  int device;
  THHcState* state = hctorch_getstate(L);
  THHcDeviceState* device_state = state->deviceState;
  device = device_state->getCurrentDevID();
  lua_pushnumber(L, device);
  return 1;
}

static int hctorch_deviceReset(lua_State* L) {
#ifndef __TORCH_SPECIFIC__
  // If using Kalmar compiler
  // TODO:
#else
  // FIXME: need to reset ati hc
#endif
  THHcRandom_resetGenerator(hctorch_getstate(L));
  return 0;
}

static int hctorch_getDeviceCount(lua_State* L) {
  int ndevice;
  THHcState* state = hctorch_getstate(L);
  THHcDeviceState* device_state = state->deviceState;
  ndevice = device_state->deviceCount;
  lua_pushnumber(L, ndevice);
  return 1;
}

#define CL_CHECK(_expr)                                                         \
   do {                                                                         \
     cl_int _err = _expr;                                                       \
     if (_err == CL_SUCCESS)                                                    \
       break;                                                                   \
     fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err);   \
     abort();                                                                   \
   } while (0)

#define SET_DEVN_STR_PROP(NAME, VAL) \
lua_pushstring(L, NAME); \
lua_pushstring(L, VAL); \
lua_settable(L, -3);

#define SET_DEVN_NUM_PROP(NAME, VAL) \
lua_pushstring(L, NAME); \
lua_pushnumber(L, VAL); \
lua_settable(L, -3);

// Gets the total and free memory in bytes for the given device ID.
static int hctorch_getMemoryUsage(lua_State* L) {
#ifdef __TORCH_SPECIFIC__
  THHcState* state = hctorch_getstate(L);
  THHcDeviceState* device_state = state->deviceState;
  int deviceID = device_state->getCurrentDevID();

  if (deviceID < 1) {
    std::cout << "Err getMemoryUsag: Invalid device id (device id > 0)" <<
              std::endl;
    return 0;
  }

  if (deviceID > device_state->deviceCount) {
    std::cout << "Err getMemoryUsag: Invalid device id = " << deviceID <<
              " (number of available devices "
              << device_state->deviceCount  << ")" << std::endl;
    return 0;
  }

  // Getting the accelerator corresponding to the device set by user
  cl_device_id devID =
    hc::accelerator::get_all()[deviceID].get_device_id();
  size_t freeBytes[2] = {0};
  size_t totalBytes = 0;
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(totalBytes),
                           &totalBytes, NULL));
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_GLOBAL_FREE_MEMORY_AMD,
                           sizeof(size_t) * 2, freeBytes, NULL));
  int verbose = 1;

  if (verbose) {
    // Verbosely list
    lua_newtable(L);
    SET_DEVN_NUM_PROP("Free memory (Bytes)", freeBytes[0]);
    SET_DEVN_NUM_PROP("The largest free block (Bytes)", freeBytes[1]);
    SET_DEVN_NUM_PROP("Total memory (Bytes)", totalBytes);
    return 1;
  } else {
    lua_pushnumber(L, freeBytes[0]);
    lua_pushnumber(L, freeBytes[1]);
    lua_pushnumber(L, totalBytes);
    return 3;
  }

#endif
  return 1;
}

static int hctorch_setDevice(lua_State* L) {
  THHcState* state = hctorch_getstate(L);
  int deviceID = (int)luaL_checknumber(L, 1);
  THHcDeviceState* device_state = state->deviceState;

  if (deviceID < 1) {
    std::cout << "Err setDevice: Invalid device id (device id > 0)" << std::endl;
    return 0;
  }

  if (deviceID > device_state->deviceCount) {
    std::cout << "Err setDevice: Invalid device id = " << deviceID <<
              " (number of available devices "
              << device_state->deviceCount  << ")" << std::endl;
    return 0;
  }

  device_state->setCurrentDevID(deviceID);
  THHcRNGState* rng_state = state->rngState;
  rng_state->current_gen = &rng_state->gen[deviceID - 1];
  
  return 1;
}

static int hctorch_getDeviceProperties(lua_State* L) {
#ifdef __TORCH_SPECIFIC__
  THHcState* state = hctorch_getstate(L);
  THHcDeviceState* device_state = state->deviceState;
  int deviceID = device_state->getCurrentDevID();

  if (deviceID < 1) {
    std::cout << "Err getDeviceProperties: Invalid device id (device id > 0)" <<
              std::endl;
    return 0;
  }

  if (deviceID > device_state->deviceCount) {
    std::cout << "Err getDeviceProperties: Invalid device id = " << deviceID <<
              " (number of available devices "
              << device_state->deviceCount  << ")" << std::endl;
    return 0;
  }

  // Getting the accelerator corresponding to the device set by user
  cl_device_id devID =
    hc::accelerator::get_all()[deviceID].get_device_id();
  char strBuf[1024] = {0x0};
  cl_uint buf_uint;
  lua_newtable(L);
  SET_DEVN_NUM_PROP("Current Device", deviceID);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_NAME, sizeof(strBuf), strBuf, NULL));
  SET_DEVN_STR_PROP("Device Name", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_VENDOR, sizeof(strBuf), strBuf,
                           NULL));
  SET_DEVN_STR_PROP("Vendor Name", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_VERSION, sizeof(strBuf), strBuf,
                           NULL));
  SET_DEVN_STR_PROP("Device Version", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DRIVER_VERSION, sizeof(strBuf), strBuf,
                           NULL));
  SET_DEVN_STR_PROP("Driver Version", strBuf);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(buf_uint),
                           &buf_uint, NULL));
  SET_DEVN_NUM_PROP("Max Compute Units", buf_uint);
  cl_uint dimensions = 0;
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
                           sizeof(cl_uint), &dimensions, NULL));
  size_t* maxSizes = new size_t[dimensions];
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_WORK_ITEM_SIZES,
                           sizeof(size_t) * dimensions, maxSizes, NULL));
  char str[256] = {0x0};

  for (int i = 0; i < dimensions; i++) {
    int offset = strlen(str);

    if (i != dimensions - 1) {
      sprintf(&str[offset], "%ld,", maxSizes[i]);
    } else {
      sprintf(&str[offset], "%ld", maxSizes[i]);
    }
  }

  SET_DEVN_STR_PROP("Max work items", str);
  delete[] maxSizes;
  size_t szValue;
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(szValue),
                           &szValue, NULL));
  SET_DEVN_NUM_PROP("Max work group size", szValue);
  cl_ulong lvalue;
  cl_uint ivalue;
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(ivalue),
                           &ivalue, NULL));
  SET_DEVN_NUM_PROP("Max clock frequency(MHz)", ivalue);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(lvalue),
                           &lvalue, NULL));
  SET_DEVN_NUM_PROP("Max memory allocation", lvalue);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(lvalue),
                           &lvalue, NULL));
  SET_DEVN_NUM_PROP("Global memory size", lvalue);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
                           sizeof(lvalue), &lvalue, NULL));
  SET_DEVN_NUM_PROP("Constant buffer size", lvalue);
  CL_CHECK(clGetDeviceInfo(devID, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(lvalue),
                           &lvalue, NULL));
  SET_DEVN_NUM_PROP("Local memory size", lvalue);
#endif
  return 1;
}

static int hctorch_seed(lua_State* L) {
  unsigned long seed = THHcRandom_seed(hctorch_getstate(L));
  lua_pushnumber(L, seed);
  return 1;
}

static int hctorch_seedAll(lua_State* L) {
  unsigned long seed = THHcRandom_seedAll(hctorch_getstate(L));
  lua_pushnumber(L, seed);
  return 1;
}

static int hctorch_initialSeed(lua_State* L) {
  unsigned long seed = THHcRandom_initialSeed(hctorch_getstate(L));
  lua_pushnumber(L, seed);
  return 1;
}

static int hctorch_manualSeed(lua_State* L) {
  unsigned long seed = luaL_checknumber(L, 1);
  THHcRandom_manualSeed(hctorch_getstate(L), seed);
  return 0;
}

static int hctorch_manualSeedAll(lua_State* L) {
  unsigned long seed = luaL_checknumber(L, 1);
  THHcRandom_manualSeedAll(hctorch_getstate(L), seed);
  return 0;
}

static int hctorch_getRNGState(lua_State* L) {
  THByteTensor* t = THByteTensor_new();
  THHcRandom_getRNGState(hctorch_getstate(L), t);
  luaT_pushudata(L, t, "torch.ByteTensor");
  return 1;
}

static int hctorch_setRNGState(lua_State* L) {
  THByteTensor* t = (THByteTensor*)luaT_checkudata(L, 1, "torch.ByteTensor");
  THHcRandom_setRNGState(hctorch_getstate(L), t);
  return 0;
}

#define STR(x) #x
#define EXPAND_STR(x) STR(x)
static int hctorch_getState(lua_State* L) {
  lua_getglobal(L, EXPAND_STR(Torch_TARGET));
  lua_getfield(L, -1, "_state");
  lua_remove(L, -2);
  return 1;
}

static const struct luaL_Reg hctorch_stuff__ [] = {
  {"synchronize", hctorch_synchronize},
  {"getDevice", hctorch_getDevice},
  {"deviceReset", hctorch_deviceReset},
  {"getDeviceCount", hctorch_getDeviceCount},
  {"getDeviceProperties", hctorch_getDeviceProperties},
  {"getMemoryUsage", hctorch_getMemoryUsage},
  {"setDevice", hctorch_setDevice},
  {"seed", hctorch_seed},
  {"seedAll", hctorch_seedAll},
  {"initialSeed", hctorch_initialSeed},
  {"manualSeed", hctorch_manualSeed},
  {"manualSeedAll", hctorch_manualSeedAll},
  {"getRNGState", hctorch_getRNGState},
  {"setRNGState", hctorch_setRNGState},
  {"getState", hctorch_getState},
  {NULL, NULL}
};

std::shared_ptr<THHcState> aState;

#define glue2(x,y) x ## y
#define glue(x,y) glue2(x,y)
#define exported_libname(x, y, z) glue(x, y)(z)
LUA_EXTERNC DLL_EXPORT int exported_libname(luaopen_lib, Torch_TARGET,
    lua_State* L);

int exported_libname(luaopen_lib, Torch_TARGET, lua_State* L) {
  lua_newtable(L);
  luaL_register(L, NULL, hctorch_stuff__);
  aState = std::make_shared<THHcState>();
  THHcState* state = aState.get();
  THHcInit(state);
  hctorch_HcStorage_init(L);
  hctorch_HcTensor_init(L);
  hctorch_HcTensorMath_init(L);
  hctorch_HcTensorOperator_init(L);
  /* Store state in hctorch table. */
  lua_pushlightuserdata(L, state);
  lua_setfield(L, -2, "_state");
  return 1;
}
