#include "torch/utils.h"
#include "luaT.h"
#include "THHc.h"

#define Real Torch_BKN_NAME

#define torch_Storage TH_CONCAT_STRING_3(torch., Real, Storage)
#define torch_Tensor TH_CONCAT_STRING_3(torch., Real, Tensor)

static int hctorch_HcTensorOperator___add__(lua_State* L) {
  THHcTensor* tensor1 = (THHcTensor*)luaT_toudata(L, 1, torch_Tensor);
  THHcTensor* tensor2 = (THHcTensor*)luaT_toudata(L, 2, torch_Tensor);
  THHcTensor* r;
  THHcState* state = hctorch_getstate(L);

  if(!tensor1 && !tensor2) {
    luaL_error(L, "expecting two Tensors or one Tensor and one number");
  } else {
    r = THHcTensor_new(state);
    luaT_pushudata(L, r, torch_Tensor);

    if(!tensor1 && tensor2) {
      THHcTensor_resizeAs(state, r, tensor2);
      THHcTensor_copy(state, r, tensor2);
      THHcTensor_add(state, r, r, luaL_checknumber(L, 1));
    } else if(tensor1 && !tensor2) {
      THHcTensor_resizeAs(state, r, tensor1);
      THHcTensor_copy(state, r, tensor1);
      THHcTensor_add(state, r, r, luaL_checknumber(L, 2));
    } else {
      THHcTensor_resizeAs(state, r, tensor1);
      THHcTensor_copy(state, r, tensor1);
      THHcTensor_cadd(state, r, r, 1, tensor2);
    }
  }

  return 1;
}

static int hctorch_HcTensorOperator___sub__(lua_State* L) {
  THHcTensor* tensor1 = (THHcTensor*)luaT_toudata(L, 1, torch_Tensor);
  THHcTensor* tensor2 = (THHcTensor*)luaT_toudata(L, 2, torch_Tensor);
  THHcTensor* r;
  THHcState* state = hctorch_getstate(L);

  if(!tensor1 && !tensor2) {
    luaL_error(L, "expecting two Tensors or one Tensor and one number");
  } else {
    r = THHcTensor_new(state);
    luaT_pushudata(L, r, torch_Tensor);

    if(!tensor1 && tensor2) {
      THHcTensor_resizeAs(state, r, tensor2);
      THHcTensor_fill(state, r, luaL_checknumber(L, 1));
      THHcTensor_cadd(state, r, r, -1, tensor2);
    } else if(tensor1 && !tensor2) {
      THHcTensor_resizeAs(state, r, tensor1);
      THHcTensor_copy(state, r, tensor1);
      THHcTensor_add(state, r, r, -luaL_checknumber(L, 2));
    } else {
      THHcTensor_resizeAs(state, r, tensor1);
      THHcTensor_copy(state, r, tensor1);
      THHcTensor_cadd(state, r, r, -1, tensor2);
    }
  }

  return 1;
}

static int hctorch_HcTensorOperator___unm__(lua_State* L) {
  THHcTensor* tensor = (THHcTensor*)luaT_checkudata(L, 1, torch_Tensor);
  THHcTensor* r;
  THHcState* state = hctorch_getstate(L);
  r = THHcTensor_new(state);
  luaT_pushudata(L, r, torch_Tensor);
  THHcTensor_resizeAs(state, r, tensor);
  THHcTensor_copy(state, r, tensor);
  THHcTensor_mul(state, r, r, -1);
  return 1;
}

static int hctorch_HcTensorOperator___mul__(lua_State* L) {
  THHcTensor* tensor1 = (THHcTensor*)luaT_toudata(L, 1, torch_Tensor);
  THHcTensor* tensor2 = (THHcTensor*)luaT_toudata(L, 2, torch_Tensor);
  THHcTensor* r;
  THHcState* state = hctorch_getstate(L);

  if(!tensor1 && !tensor2) {
    luaL_error(L, "expecting two Tensors or one Tensor and one number");
  } else {
    r = THHcTensor_new(state);
    luaT_pushudata(L, r, torch_Tensor);

    if(!tensor1 && tensor2) {
      THHcTensor_resizeAs(state, r, tensor2);
      THHcTensor_copy(state, r, tensor2);
      THHcTensor_mul(state, r, r, luaL_checknumber(L, 1));
    } else if(tensor1 && !tensor2) {
      THHcTensor_resizeAs(state, r, tensor1);
      THHcTensor_copy(state, r, tensor1);
      THHcTensor_mul(state, r, r, luaL_checknumber(L, 2));
    } else {
      int dimt = tensor1->nDimension;
      int dims = tensor2->nDimension;

      if(dimt == 1 && dims == 1) {
        lua_pushnumber(L, THHcTensor_dot(state, tensor1,
                                          tensor2)); /* ok, we wasted r, but who cares */
      } else if(dimt == 2 && dims == 1) {
        THHcTensor_resize1d(state, r, tensor1->size[0]);
        THHcTensor_zero(state, r);
        THHcTensor_addmv(state, r, 1, r, 1, tensor1, tensor2);
      } else if(dimt == 2 && dims == 2) {
        THHcTensor_resize2d(state, r, tensor1->size[0], tensor2->size[1]);
        THHcTensor_zero(state, r);
        THHcTensor_addmm(state, r, 1, r, 1, tensor1, tensor2);
      } else {
        luaL_error(L, "multiplication between %dD and %dD tensors not yet supported",
                   tensor1->nDimension, tensor2->nDimension);
      }
    }
  }

  return 1;
}

static int hctorch_HcTensorOperator___div__(lua_State* L) {
  THHcTensor* tensor = (THHcTensor*)luaT_checkudata(L, 1, torch_Tensor);
  THHcTensor* r;
  THHcState* state = hctorch_getstate(L);
  luaL_argcheck(L, lua_isnumber(L, 2), 2, "number expected");
  r = THHcTensor_new(state);
  luaT_pushudata(L, r, torch_Tensor);
  THHcTensor_resizeAs(state, r, tensor);
  THHcTensor_copy(state, r, tensor);
  THHcTensor_mul(state, r, r, 1 / lua_tonumber(L, 2));
  return 1;
}

static const struct luaL_Reg hctorch_HcTensorOperator__ [] = {
  {"__add__", hctorch_HcTensorOperator___add__},
  {"__sub__", hctorch_HcTensorOperator___sub__},
  {"__unm__", hctorch_HcTensorOperator___unm__},
  {"__mul__", hctorch_HcTensorOperator___mul__},
  {"__div__", hctorch_HcTensorOperator___div__},
  {NULL, NULL}
};

void hctorch_HcTensorOperator_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaL_register(L, NULL, hctorch_HcTensorOperator__);
  lua_pop(L, 1);
}
#undef Real
