#ifndef TH_Hc_TENSOR_RANDOM_INC
#define TH_Hc_TENSOR_RANDOM_INC

#include "THHcTensor.h"
#include <MTGP/hcrand_mtgp32.h>
#include <random>

/* Generator */
typedef struct _Generator {
  struct HcRandStateMtgp32* gen_states;
  int initf;
  unsigned long initial_seed;
} Generator;

typedef struct THHcRNGState {
  /* One generator per Hc */
  Generator* gen;
  Generator* current_gen;
  int num_devices;
} THHcRNGState;

struct THHcState;

THHC_API void THHcRandom_init(struct THHcState* state, int num_devices, int current_device);
THHC_API void THHcRandom_shutdown(struct THHcState* state);
/* device: the device index starting from 0 */
THHC_API void THHcRandom_setGenerator(struct THHcState* state, int device);
THHC_API void THHcRandom_resetGenerator(struct THHcState* state);
THHC_API unsigned long THHcRandom_seed(struct THHcState* state);
THHC_API unsigned long THHcRandom_seedAll(struct THHcState* state);
THHC_API void THHcRandom_manualSeed(struct THHcState* state, unsigned long the_seed_);
THHC_API void THHcRandom_manualSeedAll(struct THHcState* state, unsigned long the_seed_);
THHC_API unsigned long THHcRandom_initialSeed(struct THHcState* state);
THHC_API void THHcRandom_getRNGState(struct THHcState* state, THByteTensor* rng_state);
THHC_API void THHcRandom_setRNGState(struct THHcState* state, THByteTensor* rng_state);
THHC_API void THHcTensor_geometric(struct THHcState* state, THHcTensor* self, double p);
THHC_API void THHcTensor_bernoulli(struct THHcState* state, THHcTensor* self, double p);
THHC_API void THHcTensor_uniform(struct THHcState* state, THHcTensor* self, double a, double b);
THHC_API void THHcTensor_normal(struct THHcState* state, THHcTensor* self, double mean, double stdv);
THHC_API void THHcTensor_exponential(struct THHcState* state, THHcTensor* self, double lambda);
THHC_API void THHcTensor_cauchy(struct THHcState* state, THHcTensor* self, double median, double sigma);
THHC_API void THHcTensor_logNormal(struct THHcState* state, THHcTensor* self, double mean, double stdv);

#endif

