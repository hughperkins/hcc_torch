#include "THHcTensorMath.h"
#include "THHcGeneral.h"
#include "THHcTensorCopy.h"
#include "THHcTensorRandom.h"
#include "THHcApply.h"

template<class Op>
void THHcTensor_logicalValue(THHcState *state, THHcTensor *self_, THHcTensor *src, Op op)
{
  THHcTensor_resizeAs(state, self_, src);

  if (!THHcTensor_pointwiseApply2(state, self_, src, op)) {
    THArgCheck(false, 2, HCTORCH_DIM_WARNING);
  }
}

struct TensorLTValueOp {
  TensorLTValueOp(float v) __attribute__((hc, cpu)): value(v) {}
  void operator()(float* out, float* in) const __attribute__((hc, cpu)) {
    *out = (*in < value);
  }

  const float value;
};

void THHcTensor_ltValue(THHcState *state, THHcTensor *self_, THHcTensor *src, float value)
{
  THHcTensor_logicalValue(state, self_, src, TensorLTValueOp(value));
}

struct TensorGTValueOp {
  TensorGTValueOp(float v) __attribute__((hc, cpu)) : value(v) {}
  void operator()(float* out, float* in) const __attribute__((hc, cpu)) {
    *out = (*in > value);
  }

  const float value;
};

void THHcTensor_gtValue(THHcState *state, THHcTensor *self_, THHcTensor *src, float value)
{
  THHcTensor_logicalValue(state, self_, src, TensorGTValueOp(value));
}

struct TensorLEValueOp {
  TensorLEValueOp(float v) __attribute__((hc, cpu)) : value(v) {}
  void operator()(float* out, float* in) const __attribute__((hc, cpu)) {
    *out = (*in <= value);
  }

  const float value;
};

void THHcTensor_leValue(THHcState *state, THHcTensor *self_, THHcTensor *src, float value)
{
  THHcTensor_logicalValue(state, self_, src, TensorLEValueOp(value));
}

struct TensorGEValueOp {
  TensorGEValueOp(float v) __attribute__((hc, cpu)): value(v) {}
  void operator()(float* out, float* in) const __attribute__((hc, cpu)) {
    *out = (*in >= value);
  }

  const float value;
};

void THHcTensor_geValue(THHcState *state, THHcTensor *self_, THHcTensor *src, float value)
{
  THHcTensor_logicalValue(state, self_, src, TensorGEValueOp(value));
}

struct TensorEQValueOp {
  TensorEQValueOp(float v) __attribute__((hc, cpu)) : value(v) {}
  void operator()(float* out, float* in) const __attribute__((hc, cpu)) {
    *out = (*in == value);
  }

  const float value;
};

void THHcTensor_eqValue(THHcState *state, THHcTensor *self_, THHcTensor *src, float value)
{
  THHcTensor_logicalValue(state, self_, src, TensorEQValueOp(value));
}

struct TensorNEValueOp {
  TensorNEValueOp(float v) __attribute__((hc, cpu)) : value(v) {}
  void operator()(float* out, float* in) const __attribute__((hc, cpu)) {
    *out = (*in != value);
  }

  const float value;
};

void THHcTensor_neValue(THHcState *state, THHcTensor *self_, THHcTensor *src, float value)
{
  THHcTensor_logicalValue(state, self_, src, TensorNEValueOp(value));
}
