#include "THHcStorageCopy.h"
#include "THHcGeneral.h"

void THHcStorage_copyFloat(THHcState* state, THHcStorage* self, struct THFloatStorage* src) {
  THArgCheck(self->size == src->size, 2, "size does not match");
  THHcCheck(hc::am_copy(self->data, src->data, self->size * sizeof(float)));
}

#define TH_HC_STORAGE_IMPLEMENT_COPY(TYPEC)                                      \
void THHcStorage_copy##TYPEC(THHcState *state, THHcStorage *self, struct TH##TYPEC##Storage *src) { \
  THFloatStorage *buffer;                                                         \
  THArgCheck(self->size == src->size, 2, "size does not match");                  \
  buffer = THFloatStorage_newWithSize(src->size);                                 \
  THFloatStorage_copy##TYPEC(buffer, src);                                        \
  THHcStorage_copyFloat(state, self, buffer);                                    \
  THFloatStorage_free(buffer);                                                    \
}

TH_HC_STORAGE_IMPLEMENT_COPY(Byte)
TH_HC_STORAGE_IMPLEMENT_COPY(Char)
TH_HC_STORAGE_IMPLEMENT_COPY(Short)
TH_HC_STORAGE_IMPLEMENT_COPY(Int)
TH_HC_STORAGE_IMPLEMENT_COPY(Long)
TH_HC_STORAGE_IMPLEMENT_COPY(Double)

void THFloatStorage_copyHc(THHcState* state, THFloatStorage* self, struct THHcStorage* src) {
  THArgCheck(self->size == src->size, 2, "size does not match");
  THHcCheck(hc::am_copy(self->data, src->data, self->size * sizeof(float)));
}

#define TH_HC_STORAGE_IMPLEMENT_COPYTO(TYPEC)                                      \
void TH##TYPEC##Storage_copyHc(THHcState *state, TH##TYPEC##Storage *self, struct THHcStorage *src) { \
  THFloatStorage *buffer;                                                           \
  THArgCheck(self->size == src->size, 2, "size does not match");                    \
  buffer = THFloatStorage_newWithSize(src->size);                                   \
  THFloatStorage_copyHc(state, buffer, src);                                       \
  TH##TYPEC##Storage_copyFloat(self, buffer);                                       \
  THFloatStorage_free(buffer);                                                      \
}

TH_HC_STORAGE_IMPLEMENT_COPYTO(Byte)
TH_HC_STORAGE_IMPLEMENT_COPYTO(Char)
TH_HC_STORAGE_IMPLEMENT_COPYTO(Short)
TH_HC_STORAGE_IMPLEMENT_COPYTO(Int)
TH_HC_STORAGE_IMPLEMENT_COPYTO(Long)
TH_HC_STORAGE_IMPLEMENT_COPYTO(Double)


