#include "THHcApply.h"
#include "hc.hpp"

// Implementation of copyIgnoringOverlaps, defined after pointwiseApply2.
void THHcTensor_copyIgnoringOverlaps(THHcState* state,
                                       THHcTensor* dst,
                                       THHcTensor* src) {
  THHcTensor_pointwiseApply2(state, dst, src, CopyOp<float>(),
                               ReadOnly, // ignore overwrites
                               ReadOnly);
}
