#include "THHcTensorMath.h"
#include "THHcGeneral.h"
#include "THHcTensorCopy.h"
#include "THHcApply.h"

/*
 * Logical Operations on Tensors
 * These functions implement logical comparison operators that take a tensor as input and another tensor 
 * or a number as the comparison target. They return a HcTensor in which each element is 0 or 1 indicating
 * if the comparison for the corresponding element was false or true respectively.
 */
template<class Op>
void THHcTensor_logicalTensor(THHcState *state, THHcTensor *self_, THHcTensor *src1, THHcTensor *src2, Op op)
{
  THHcTensor_resizeAs(state, self_, src1);
  THArgCheck(THHcTensor_nElement(state, src1) == THHcTensor_nElement(state, src2), 3, "sizes do not match");

  if (!THHcTensor_pointwiseApply3(state, self_, src1, src2, op)) {
    THArgCheck(false, 2, HCTORCH_DIM_WARNING);
  }
}

/*
 * torch.lt(a, b)
 * Implements < operator comparing each element in a with b (if b is a number) or each element 
 * in a with corresponding element in b.
 */
struct TensorLTOp {
  TensorLTOp() __attribute__((hc, cpu)) {}
  void operator()(float* out, float* a, float* b) const __attribute__((hc, cpu)) {
    *out = (float) (*a < *b);
  }
};

void THHcTensor_ltTensor(THHcState *state, THHcTensor *self_, THHcTensor *src1, THHcTensor *src2)
{
  THHcTensor_logicalTensor(state, self_, src1, src2, TensorLTOp());
}

/* torch.gt(a, b)
 * Implements > operator comparing each element in a with b (if b is a number) or each element 
 * in a with corresponding element in b.
 */
struct TensorGTOp {
  TensorGTOp() __attribute__((hc, cpu)) {}
  void operator()(float* out, float* a, float* b) const __attribute__((hc, cpu)) {
    *out = (float) (*a > *b);
  }
};

void THHcTensor_gtTensor(THHcState *state, THHcTensor *self_, THHcTensor *src1, THHcTensor *src2)
{
  THHcTensor_logicalTensor(state, self_, src1, src2, TensorGTOp());
}

/* torch.le(a, b)
 * Implements <= operator comparing each element in a with b (if b is a number) or each element 
 * in a with corresponding element in b.
 */
struct TensorLEOp {
  TensorLEOp() __attribute__((hc, cpu)) {}
  void operator()(float* out, float* a, float* b) const __attribute__((hc, cpu)) {
    *out = (float) (*a <= *b);
  }
};
void THHcTensor_leTensor(THHcState *state, THHcTensor *self_, THHcTensor *src1, THHcTensor *src2)
{
  THHcTensor_logicalTensor(state, self_, src1, src2, TensorLEOp());
}

/* torch.ge(a, b)
 * Implements >= operator comparing each element in a with b (if b is a number) or each element 
 * in a with corresponding element in b.
 */
struct TensorGEOp {
  TensorGEOp() __attribute__((hc, cpu)) {}
  void operator()(float* out, float* a, float* b) const __attribute__((hc, cpu)) {
    *out = (float)(*a >= *b);
  }
};

void THHcTensor_geTensor(THHcState *state, THHcTensor *self_, THHcTensor *src1, THHcTensor *src2)
{
  THHcTensor_logicalTensor(state, self_, src1, src2, TensorGEOp());
}

/* torch.eq(a, b)
 * Implements == operator comparing each element in a with b (if b is a number) or each element 
 * in a with corresponding element in b.
 */
struct TensorEQOp {
  TensorEQOp() __attribute__((hc, cpu)) {}
  void operator()(float* out, float* a, float* b) const __attribute__((hc, cpu)) {
    *out = (float) (*a == *b);
  }
};

void THHcTensor_eqTensor(THHcState *state, THHcTensor *self_, THHcTensor *src1, THHcTensor *src2)
{
  THHcTensor_logicalTensor(state, self_, src1, src2, TensorEQOp());
}

/* torch.ne(a, b)
 * Implements != operator comparing each element in a with b (if b is a number) or each element 
 * in a with corresponding element in b.
 */
struct TensorNEOp {
  TensorNEOp() __attribute__((hc, cpu)) {}
  void operator()(float* out, float* a, float* b) const __attribute__((hc, cpu)) {
    *out = (float) (*a != *b);
  }
};

void THHcTensor_neTensor(THHcState *state, THHcTensor *self_, THHcTensor *src1, THHcTensor *src2)
{
  THHcTensor_logicalTensor(state, self_, src1, src2, TensorNEOp());
}
