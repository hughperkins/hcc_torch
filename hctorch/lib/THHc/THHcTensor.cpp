#include "THHcGeneral.h"
#include "THHcTensor.h"
#include "THHcTensorCopy.h"
#include "THAtomic.h"

/**** access methods ****/
THHcStorage* THHcTensor_storage(THHcState* state, const THHcTensor* self) {
  return self->storage;
}

long THHcTensor_storageOffset(THHcState* state, const THHcTensor* self) {
  return self->storageOffset;
}

int THHcTensor_nDimension(THHcState* state, const THHcTensor* self) {
  return self->nDimension;
}

long THHcTensor_size(THHcState* state, const THHcTensor* self, int dim) {
  THArgCheck((dim >= 0) && (dim < self->nDimension), 2, "out of range");
  return self->size[dim];
}

long THHcTensor_stride(THHcState* state, const THHcTensor* self, int dim) {
  THArgCheck((dim >= 0) && (dim < self->nDimension), 2, "out of range");
  return self->stride[dim];
}

THLongStorage* THHcTensor_newSizeOf(THHcState* state, THHcTensor* self) {
  THLongStorage* size = THLongStorage_newWithSize(self->nDimension);
  THLongStorage_rawCopy(size, self->size);
  return size;
}

THLongStorage* THHcTensor_newStrideOf(THHcState* state, THHcTensor* self) {
  THLongStorage* stride = THLongStorage_newWithSize(self->nDimension);
  THLongStorage_rawCopy(stride, self->stride);
  return stride;
}

float* THHcTensor_data(THHcState* state, const THHcTensor* self) {
  if (self->storage) {
    return (self->storage->data + self->storageOffset);
  } else {
    return NULL;
  }
}

void THHcTensor_setFlag(THHcState* state, THHcTensor* self, const char flag) {
  self->flag |= flag;
}

void THHcTensor_clearFlag(THHcState* state, THHcTensor* self, const char flag) {
  self->flag &= ~flag;
}

/**** creation methods ****/

static void THHcTensor_rawInit(THHcState* state, THHcTensor* self);
static void THHcTensor_rawSet(THHcState* state, THHcTensor* self, THHcStorage* storage, long storageOffset, int nDimension, long* size, long* stride);
static void THHcTensor_rawResize(THHcState* state, THHcTensor* self, int nDimension, long* size, long* stride);

/* Empty init */
THHcTensor* THHcTensor_new(THHcState* state) {
  THHcTensor* self = (THHcTensor*)THAlloc(sizeof(THHcTensor));
  THHcTensor_rawInit(state, self);
  return self;
}

/* Pointer-copy init */
THHcTensor* THHcTensor_newWithTensor(THHcState* state, THHcTensor* tensor) {
  THHcTensor* self = (THHcTensor*)THAlloc(sizeof(THHcTensor));
  THHcTensor_rawInit(state, self);
  THHcTensor_rawSet(state, self,
                     tensor->storage,
                     tensor->storageOffset,
                     tensor->nDimension,
                     tensor->size,
                     tensor->stride);
  return self;
}

/* Storage init */
THHcTensor* THHcTensor_newWithStorage(THHcState* state, THHcStorage* storage, long storageOffset, THLongStorage* size, THLongStorage* stride) {
  THHcTensor* self = (THHcTensor*)THAlloc(sizeof(THHcTensor));

  if (size && stride) {
    THArgCheck(size->size == stride->size, 4, "inconsistent size");
  }

  THHcTensor_rawInit(state, self);
  THHcTensor_rawSet(state, self,
                     storage,
                     storageOffset,
                     (size ? size->size : (stride ? stride->size : 0)),
                     (size ? size->data : NULL),
                     (stride ? stride->data : NULL));
  return self;
}
THHcTensor* THHcTensor_newWithStorage1d(THHcState* state, THHcStorage* storage, long storageOffset,
    long size0, long stride0) {
  return THHcTensor_newWithStorage4d(state, storage, storageOffset, size0, stride0, -1, -1,  -1, -1,  -1, -1);
}

THHcTensor* THHcTensor_newWithStorage2d(THHcState* state, THHcStorage* storage, long storageOffset,
    long size0, long stride0,
    long size1, long stride1) {
  return THHcTensor_newWithStorage4d(state, storage, storageOffset, size0, stride0, size1, stride1,  -1, -1,  -1, -1);
}

THHcTensor* THHcTensor_newWithStorage3d(THHcState* state, THHcStorage* storage, long storageOffset,
    long size0, long stride0,
    long size1, long stride1,
    long size2, long stride2) {
  return THHcTensor_newWithStorage4d(state, storage, storageOffset, size0, stride0, size1, stride1,  size2, stride2,  -1, -1);
}

THHcTensor* THHcTensor_newWithStorage4d(THHcState* state, THHcStorage* storage, long storageOffset,
    long size0, long stride0,
    long size1, long stride1,
    long size2, long stride2,
    long size3, long stride3) {
  long size[4] = {size0, size1, size2, size3};
  long stride[4] = {stride0, stride1, stride2, stride3};
  THHcTensor* self = (THHcTensor*)THAlloc(sizeof(THHcTensor));
  THHcTensor_rawInit(state, self);
  THHcTensor_rawSet(state, self, storage, storageOffset, 4, size, stride);
  return self;
}

THHcTensor* THHcTensor_newWithSize(THHcState* state, THLongStorage* size, THLongStorage* stride) {
  return THHcTensor_newWithStorage(state, NULL, 0, size, stride);
}

THHcTensor* THHcTensor_newWithSize1d(THHcState* state, long size0) {
  return THHcTensor_newWithSize4d(state, size0, -1, -1, -1);
}

THHcTensor* THHcTensor_newWithSize2d(THHcState* state, long size0, long size1) {
  return THHcTensor_newWithSize4d(state, size0, size1, -1, -1);
}

THHcTensor* THHcTensor_newWithSize3d(THHcState* state, long size0, long size1, long size2) {
  return THHcTensor_newWithSize4d(state, size0, size1, size2, -1);
}

THHcTensor* THHcTensor_newWithSize4d(THHcState* state, long size0, long size1, long size2, long size3) {
  long size[4] = {size0, size1, size2, size3};
  THHcTensor* self = (THHcTensor*)THAlloc(sizeof(THHcTensor));
  THHcTensor_rawInit(state, self);
  THHcTensor_rawResize(state, self, 4, size, NULL);
  return self;
}

THHcTensor* THHcTensor_newClone(THHcState* state, THHcTensor* self) {
  THHcTensor* tensor = THHcTensor_new(state);
  THHcTensor_resizeAs(state, tensor, self);
  THHcTensor_copy(state, tensor, self);
  return tensor;
}

THHcTensor* THHcTensor_newContiguous(THHcState* state, THHcTensor* self) {
  if (!THHcTensor_isContiguous(state, self)) {
    return THHcTensor_newClone(state, self);
  } else {
    THHcTensor_retain(state, self);
    return self;
  }
}

THHcTensor* THHcTensor_newSelect(THHcState* state, THHcTensor* tensor, int dimension_, long sliceIndex_) {
  THHcTensor* self = THHcTensor_newWithTensor(state, tensor);
  THHcTensor_select(state, self, NULL, dimension_, sliceIndex_);
  return self;
}

THHcTensor* THHcTensor_newNarrow(THHcState* state, THHcTensor* tensor, int dimension_, long firstIndex_, long size_) {
  THHcTensor* self = THHcTensor_newWithTensor(state, tensor);
  THHcTensor_narrow(state, self, NULL, dimension_, firstIndex_, size_);
  return self;
}

THHcTensor* THHcTensor_newTranspose(THHcState* state, THHcTensor* tensor, int dimension1_, int dimension2_) {
  THHcTensor* self = THHcTensor_newWithTensor(state, tensor);
  THHcTensor_transpose(state, self, NULL, dimension1_, dimension2_);
  return self;
}

THHcTensor* THHcTensor_newUnfold(THHcState* state, THHcTensor* tensor, int dimension_, long size_, long step_) {
  THHcTensor* self = THHcTensor_newWithTensor(state, tensor);
  THHcTensor_unfold(state, self, NULL, dimension_, size_, step_);
  return self;
}

/* Resize */
void THHcTensor_resize(THHcState* state, THHcTensor* self, THLongStorage* size, THLongStorage* stride) {
  THArgCheck(size != NULL, 2, "invalid size");

  if (stride) {
    THArgCheck(stride->size == size->size, 3, "invalid stride");
  }

  THHcTensor_rawResize(state, self, size->size, size->data, (stride ? stride->data : NULL));
}

void THHcTensor_resizeAs(THHcState* state, THHcTensor* self, THHcTensor* src) {
  int isSame = 0;
  int d;

  if (self->nDimension == src->nDimension) {
    isSame = 1;

    for (d = 0; d < self->nDimension; d++) {
      if (self->size[d] != src->size[d]) {
        isSame = 0;
        break;
      }
    }
  }

  if (!isSame) {
    THHcTensor_rawResize(state, self, src->nDimension, src->size, NULL);
  }
}

void THHcTensor_resize1d(THHcState* state, THHcTensor* tensor, long size0) {
  THHcTensor_resize4d(state, tensor, size0, -1, -1, -1);
}

void THHcTensor_resize2d(THHcState* state, THHcTensor* tensor, long size0, long size1) {
  THHcTensor_resize4d(state, tensor, size0, size1, -1, -1);
}

void THHcTensor_resize3d(THHcState* state, THHcTensor* tensor, long size0, long size1, long size2) {
  THHcTensor_resize4d(state, tensor, size0, size1, size2, -1);
}

void THHcTensor_resize4d(THHcState* state, THHcTensor* self, long size0, long size1, long size2, long size3) {
  long size[4] = {size0, size1, size2, size3};
  THHcTensor_rawResize(state, self, 4, size, NULL);
}

void THHcTensor_resize5d(THHcState* state, THHcTensor* self, long size0, long size1, long size2, long size3, long size4) {
  long size[5] = {size0, size1, size2, size3, size4};
  THHcTensor_rawResize(state, self, 5, size, NULL);
}

void THHcTensor_set(THHcState* state, THHcTensor* self, THHcTensor* src) {
  if (self != src)
    THHcTensor_rawSet(state, self,
                       src->storage,
                       src->storageOffset,
                       src->nDimension,
                       src->size,
                       src->stride);
}

void THHcTensor_setStorage(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_, THLongStorage* size_, THLongStorage* stride_) {
  if (size_ && stride_) {
    THArgCheck(size_->size == stride_->size, 5, "inconsistent size/stride sizes");
  }

  THHcTensor_rawSet(state, self,
                     storage_,
                     storageOffset_,
                     (size_ ? size_->size : (stride_ ? stride_->size : 0)),
                     (size_ ? size_->data : NULL),
                     (stride_ ? stride_->data : NULL));
}

void THHcTensor_setStorage1d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                              long size0_, long stride0_) {
  THHcTensor_setStorage4d(state, self, storage_, storageOffset_,
                           size0_, stride0_, -1, -1,
                           -1, -1, -1, -1);
}

void THHcTensor_setStorage2d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                              long size0_, long stride0_,
                              long size1_, long stride1_) {
  THHcTensor_setStorage4d(state, self, storage_, storageOffset_,
                           size0_, stride0_, size1_, stride1_,
                           -1, -1, -1, -1);
}

void THHcTensor_setStorage3d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                              long size0_, long stride0_,
                              long size1_, long stride1_,
                              long size2_, long stride2_) {
  THHcTensor_setStorage4d(state, self, storage_, storageOffset_,
                           size0_, stride0_,
                           size1_, stride1_,
                           size2_, stride2_,
                           -1, -1);
}

void THHcTensor_setStorage4d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                              long size0_, long stride0_,
                              long size1_, long stride1_,
                              long size2_, long stride2_,
                              long size3_, long stride3_) {
  long size[4] = {size0_, size1_, size2_, size3_};
  long stride[4] = {stride0_, stride1_, stride2_, stride3_};
  THHcTensor_rawSet(state, self, storage_, storageOffset_, 4, size, stride);
}


void THHcTensor_narrow(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension, long firstIndex, long size) {
  if (!src) {
    src = self;
  }

  THArgCheck((dimension >= 0) && (dimension < src->nDimension), 3, "out of range");
  THArgCheck((firstIndex >= 0) && (firstIndex < src->size[dimension]), 4, "out of range");
  THArgCheck((size > 0) && (firstIndex + size <= src->size[dimension]), 5, "out of range");
  THHcTensor_set(state, self, src);

  if (firstIndex > 0) {
    self->storageOffset += firstIndex * self->stride[dimension];
  }

  self->size[dimension] = size;
}

void THHcTensor_select(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension, long sliceIndex) {
  int d;

  if (!src) {
    src = self;
  }

  THArgCheck(src->nDimension > 1, 1, "cannot select on a vector");
  THArgCheck((dimension >= 0) && (dimension < src->nDimension), 3, "out of range");
  THArgCheck((sliceIndex >= 0) && (sliceIndex < src->size[dimension]), 4, "out of range");
  THHcTensor_set(state, self, src);
  THHcTensor_narrow(state, self, NULL, dimension, sliceIndex, 1);

  for (d = dimension; d < self->nDimension - 1; d++) {
    self->size[d] = self->size[d + 1];
    self->stride[d] = self->stride[d + 1];
  }

  self->nDimension--;
}

void THHcTensor_transpose(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension1, int dimension2) {
  long z;

  if (!src) {
    src = self;
  }

  THArgCheck((dimension1 >= 0) && (dimension1 < src->nDimension), 1, "out of range");
  THArgCheck((dimension2 >= 0) && (dimension2 < src->nDimension), 2, "out of range");
  THHcTensor_set(state, self, src);

  if (dimension1 == dimension2) {
    return;
  }

  z = self->stride[dimension1];
  self->stride[dimension1] = self->stride[dimension2];
  self->stride[dimension2] = z;
  z = self->size[dimension1];
  self->size[dimension1] = self->size[dimension2];
  self->size[dimension2] = z;
}

void THHcTensor_unfold(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension, long size, long step) {
  long* newSize;
  long* newStride;
  int d;

  if (!src) {
    src = self;
  }

  THArgCheck((src->nDimension > 0), 1, "cannot unfold an empty tensor");
  THArgCheck(dimension < src->nDimension, 2, "out of range");
  THArgCheck(size <= src->size[dimension], 3, "out of range");
  THArgCheck(step > 0, 4, "invalid step");
  THHcTensor_set(state, self, src);
  newSize = (long*)THAlloc(sizeof(long) * (self->nDimension + 1));
  newStride = (long*)THAlloc(sizeof(long) * (self->nDimension + 1));
  newSize[self->nDimension] = size;
  newStride[self->nDimension] = self->stride[dimension];

  for (d = 0; d < self->nDimension; d++) {
    if (d == dimension) {
      newSize[d] = (self->size[d] - size) / step + 1;
      newStride[d] = step * self->stride[d];
    } else {
      newSize[d] = self->size[d];
      newStride[d] = self->stride[d];
    }
  }

  THFree(self->size);
  THFree(self->stride);
  self->size = newSize;
  self->stride = newStride;
  self->nDimension++;
}

/* we have to handle the case where the result is a number */
void THHcTensor_squeeze(THHcState* state, THHcTensor* self, THHcTensor* src) {
  int ndim = 0;
  int d;

  if (!src) {
    src = self;
  }

  THHcTensor_set(state, self, src);

  for (d = 0; d < src->nDimension; d++) {
    if (src->size[d] != 1) {
      if (d != ndim) {
        self->size[ndim] = src->size[d];
        self->stride[ndim] = src->stride[d];
      }

      ndim++;
    }
  }

  /* right now, we do not handle 0-dimension tensors */
  if (ndim == 0 && src->nDimension > 0) {
    self->size[0] = 1;
    self->stride[0] = 1;
    ndim = 1;
  }

  self->nDimension = ndim;
}

void THHcTensor_squeeze1d(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension) {
  int d;

  if (!src) {
    src = self;
  }

  THArgCheck(dimension < src->nDimension, 3, "dimension out of range");
  THHcTensor_set(state, self, src);

  if (src->size[dimension] == 1 && src->nDimension > 1) {
    for (d = dimension; d < self->nDimension - 1; d++) {
      self->size[d] = self->size[d + 1];
      self->stride[d] = self->stride[d + 1];
    }

    self->nDimension--;
  }
}

int THHcTensor_isContiguous(THHcState* state, const THHcTensor* self) {
  long z = 1;
  int d;

  for (d = self->nDimension - 1; d >= 0; d--) {
    if (self->size[d] != 1) {
      if (self->stride[d] == z) {
        z *= self->size[d];
      } else {
        return 0;
      }
    }
  }

  return 1;
}

int THHcTensor_isSameSizeAs(THHcState* state, const THHcTensor* self, const THHcTensor* src) {
  int d;

  if (self->nDimension != src->nDimension) {
    return 0;
  }

  for (d = 0; d < self->nDimension; ++d) {
    if (self->size[d] != src->size[d]) {
      return 0;
    }
  }

  return 1;
}

long THHcTensor_nElement(THHcState* state, const THHcTensor* self) {
  if (self->nDimension == 0) {
    return 0;
  } else {
    long nElement = 1;
    int d;

    for (d = 0; d < self->nDimension; d++) {
      nElement *= self->size[d];
    }

    return nElement;
  }
}

void THHcTensor_retain(THHcState* state, THHcTensor* self) {
  if (self->flag & TH_TENSOR_REFCOUNTED) {
    THAtomicIncrementRef(&self->refcount);
  }
}

void THHcTensor_free(THHcState* state, THHcTensor* self) {
  if (!self) {
    return;
  }

  if (self->flag & TH_TENSOR_REFCOUNTED) {
    if (THAtomicDecrementRef(&self->refcount)) {
      THFree(self->size);
      THFree(self->stride);

      if (self->storage) {
        THHcStorage_free(state, self->storage);
      }

      THFree(self);
    }
  }
}

void THHcTensor_freeCopyTo(THHcState* state, THHcTensor* self, THHcTensor* dst) {
  if (self != dst) {
    THHcTensor_copy(state, dst, self);
  }

  THHcTensor_free(state, self);
}

static void THHcTensor_rawInit(THHcState* state, THHcTensor* self) {
  self->refcount = 1;
  self->storage = NULL;
  self->storageOffset = 0;
  self->size = NULL;
  self->stride = NULL;
  self->nDimension = 0;
  self->flag = TH_TENSOR_REFCOUNTED;
}

static void THHcTensor_rawSet(THHcState* state, THHcTensor* self, THHcStorage* storage, long storageOffset, int nDimension, long* size, long* stride) {
  /* storage */
  if (self->storage != storage) {
    if (self->storage) {
      THHcStorage_free(state, self->storage);
    }

    if (storage) {
      self->storage = storage;
      THHcStorage_retain(state, self->storage);
    } else {
      self->storage = NULL;
    }
  }

  /* storageOffset */
  if (storageOffset < 0) {
    THError("Tensor: invalid storage offset");
  }

  self->storageOffset = storageOffset;
  /* size and stride */
  THHcTensor_rawResize(state, self, nDimension, size, stride);
}

static void THHcTensor_rawResize(THHcState* state, THHcTensor* self, int nDimension, long* size, long* stride) {
  int d;
  int nDimension_;
  long totalSize;
  int hascorrectsize = 1;
  nDimension_ = 0;

  for (d = 0; d < nDimension; d++) {
    if (size[d] > 0) {
      nDimension_++;

      if ((self->nDimension > d) && (size[d] != self->size[d])) {
        hascorrectsize = 0;
      }

      if ((self->nDimension > d) && stride && (stride[d] >= 0) && (stride[d] != self->stride[d])) {
        hascorrectsize = 0;
      }
    } else {
      break;
    }
  }

  nDimension = nDimension_;

  if (nDimension != self->nDimension) {
    hascorrectsize = 0;
  }

  if (hascorrectsize) {
    return;
  }

  if (nDimension > 0) {
    if (nDimension != self->nDimension) {
      self->size = (long*)THRealloc(self->size, sizeof(long) * nDimension);
      self->stride = (long*)THRealloc(self->stride, sizeof(long) * nDimension);
      self->nDimension = nDimension;
    }

    totalSize = 1;

    for (d = self->nDimension - 1; d >= 0; d--) {
      self->size[d] = size[d];

      if (stride && (stride[d] >= 0)) {
        self->stride[d] = stride[d];
      } else {
        if (d == self->nDimension - 1) {
          self->stride[d] = 1;
        } else {
          self->stride[d] = self->size[d + 1] * self->stride[d + 1];
        }
      }

      totalSize += (self->size[d] - 1) * self->stride[d];
    }

    if (totalSize + self->storageOffset > 0) {
      if (!self->storage) {
        self->storage = THHcStorage_new(state);
      } else if (self->storage && self->storage->refcount == 0) {
        THAssertMsg((1 == 0), "Try to use a free-ed storage!");
      }

      if (totalSize + self->storageOffset > self->storage->size) {
        THHcStorage_resize(state, self->storage, totalSize + self->storageOffset);
      }
    }
  } else {
    self->nDimension = 0;
  }
}

void THHcTensor_set1d(THHcState* state, THHcTensor* tensor, long x0, float value) {
  THArgCheck(tensor->nDimension == 1, 1, "tensor must have one dimension");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]), 2, "out of range");
  THHcStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0], value);
}

float THHcTensor_get1d(THHcState* state, const THHcTensor* tensor, long x0) {
  THArgCheck(tensor->nDimension == 1, 1, "tensor must have one dimension");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]), 2, "out of range");
  return THHcStorage_get(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0]);
}

void THHcTensor_set2d(THHcState* state, THHcTensor* tensor, long x0, long x1, float value) {
  THArgCheck(tensor->nDimension == 2, 1, "tensor must have two dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]), 2, "out of range");
  THHcStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1], value);
}

float THHcTensor_get2d(THHcState* state, const THHcTensor* tensor, long x0, long x1) {
  THArgCheck(tensor->nDimension == 2, 1, "tensor must have two dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]), 2, "out of range");
  return THHcStorage_get(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1]);
}

void THHcTensor_set3d(THHcState* state, THHcTensor* tensor, long x0, long x1, long x2, float value) {
  THArgCheck(tensor->nDimension == 3, 1, "tensor must have three dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]), 2, "out of range");
  THHcStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2], value);
}

float THHcTensor_get3d(THHcState* state, const THHcTensor* tensor, long x0, long x1, long x2) {
  THArgCheck(tensor->nDimension == 3, 1, "tensor must have three dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]), 2, "out of range");
  return THHcStorage_get(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2]);
}

void THHcTensor_set4d(THHcState* state, THHcTensor* tensor, long x0, long x1, long x2, long x3, float value) {
  THArgCheck(tensor->nDimension == 4, 1, "tensor must have four dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]) && (x3 >= 0) && ( x3 < tensor->size[3]), 2, "out of range");
  THHcStorage_set(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2] + x3 * tensor->stride[3], value);
}

float THHcTensor_get4d(THHcState* state, const THHcTensor* tensor, long x0, long x1, long x2, long x3) {
  THArgCheck(tensor->nDimension == 4, 1, "tensor must have four dimensions");
  THArgCheck((x0 >= 0) && (x0 < tensor->size[0]) && (x1 >= 0) && (x1 < tensor->size[1]) && (x2 >= 0) && (x2 < tensor->size[2]) && (x3 >= 0) && ( x3 < tensor->size[3]), 2, "out of range");
  return THHcStorage_get(state, tensor->storage, tensor->storageOffset + x0 * tensor->stride[0] + x1 * tensor->stride[1] + x2 * tensor->stride[2] + x3 * tensor->stride[3]);
}

int THHcTensor_checkGPU(THHcState *state, unsigned int nTensors, ...)
{
  return 1;
#ifdef DISABLE_CHECK_GPU
  return 1;  // Disable GPU checks.
#else
  int curDev = state->deviceState->getCurrentDevID();
  va_list(args);
  va_start(args, nTensors);
  int valid = 1;
  for (unsigned int i = 0; i < nTensors; i++) {
    THHcTensor* tensor = va_arg(args, THHcTensor*);
    if (tensor == NULL) {
      continue;
    }
    int tensorDev = THHcTensor_getDevice(state, tensor);
    if (tensorDev != -1 && tensorDev != curDev) {
      valid = 0;
      break;
    }
  }
  va_end(args);
  return valid;
#endif
}

