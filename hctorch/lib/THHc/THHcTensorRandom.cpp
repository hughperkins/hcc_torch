#include "THHcTensorRandom.h"
#include "THHcGeneral.h"
#include <hc.hpp>
#include <hc_math.hpp>
#include <MTGP/hcrand_mtgp32.h>

#define MAX_NUM_BLOCKS 64
#define BLOCK_SIZE 256

#ifndef DIVUP
#define DIVUP(x, y) (((x) + (y) - 1) / (y))
#endif

/* Sets up generator. Allocates but does not create the generator states. */
void initializeGenerator(THHcState* state, Generator* gen) {
  assert(gen);
  gen->gen_states = new HcRandStateMtgp32;
  assert(gen->gen_states);
  HcRandStateMtgp32_init(state->deviceState->get_current_accelerator_view(), gen->gen_states);
}

/* Frees memory allocated during setup. */
void destroyGenerator(Generator* gen) {
  if (gen->gen_states) {
    HcRandStateMtgp32_release(gen->gen_states);
    delete gen->gen_states;
  }
}

/* Creates a new generator state given the seed. */
void createGeneratorState(THHcState* state, Generator* gen, unsigned long seed) {
  hc::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();

  if (mtgp32_init_params_kernel(accl_view, mtgp32_params_fast_11213, gen->gen_states)) {
    THError("Creating MTGP constants failed.");
  }

  // Using device API
  if (mtgp32_init_seed_kernel(accl_view, gen->gen_states, seed)) {
    THError("Creating MTGP kernel state failed.");
  }
}

/* Initialize generator array (must be called before any other function) */
void THHcRandom_init(THHcState* state, int devices, int current_device) {
  THHcRNGState* rng_state = state->rngState;
  rng_state->num_devices = devices;
  rng_state->gen = (Generator*)malloc(rng_state->num_devices * sizeof(Generator));

  for (int i = 0; i < rng_state->num_devices; ++i) {
    rng_state->gen[i].initf = 0;
    rng_state->gen[i].initial_seed = 0;
    rng_state->gen[i].gen_states = NULL;
  }

  rng_state->current_gen = &rng_state->gen[current_device];
  // Initialize the generator for the current device. Other generators will be
  // initialized on-demand in THHcRandom_setGenerator.
  initializeGenerator(state, rng_state->current_gen);
  THHcRandom_seed(state);
}

/* Destroy generators and free memory */
void THHcRandom_shutdown(THHcState* state) {
  THHcRNGState* rng_state = state->rngState;

  if (rng_state->gen == NULL) {
    return;
  }

  for (int i = 0; i < rng_state->num_devices; ++i) {
    destroyGenerator(&rng_state->gen[i]);
  }

  free(rng_state->gen);
  rng_state->gen = NULL;
  rng_state->current_gen = NULL;
  // Manually release memory
  free(rng_state);
  rng_state = NULL;
}

/* Set the generator for the current device */
/* device: the device index starting from 0 */
void THHcRandom_setGenerator(THHcState* state, int device) {
  THHcRNGState* rng_state = state->rngState;

  if (device >= rng_state->num_devices) {
    THError("Invalid device index.");
  }

  rng_state->current_gen = &rng_state->gen[device];

  if (rng_state->current_gen->initf == 0) {
    initializeGenerator(state, rng_state->current_gen);
    THHcRandom_seed(state);
  }
}

/* Reset the generator for the current device after a device reset */
void THHcRandom_resetGenerator(THHcState* state) {
  THHcRNGState* rng_state = state->rngState;
  initializeGenerator(state, rng_state->current_gen);
  THHcRandom_manualSeed(state, rng_state->current_gen->initial_seed);
}

/* Random seed */
unsigned long THHcRandom_seed(THHcState* state) {
  unsigned long s = (unsigned long)time(0);
  THHcRandom_manualSeed(state, s);
  return s;
}

unsigned long THHcRandom_seedAll(THHcState* state) {
  unsigned long s = (unsigned long)time(0);
  THHcRandom_manualSeedAll(state, s);
  return s;
}

/* Manually set the seed */
void THHcRandom_manualSeed(THHcState* state, unsigned long seed) {
  THHcRNGState* rng_state = state->rngState;

  if (rng_state->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  rng_state->current_gen->initial_seed = seed;
  createGeneratorState(state, rng_state->current_gen, seed);
  rng_state->current_gen->initf = 1;
}

void THHcRandom_manualSeedAll(THHcState* state, unsigned long seed) {
  THHcRNGState* rng_state = state->rngState;
  int currentDevice = state->deviceState->getCurrentDevID();

  for (int i = 0; i < rng_state->num_devices; ++i) {
    // device ID starts from 1
    state->deviceState->setCurrentDevID(i + 1);
    THHcRandom_setGenerator(state, i);
    THHcRandom_manualSeed(state, seed);
  }

  // Restore current device
  state->deviceState->setCurrentDevID(currentDevice);
  THHcRandom_setGenerator(state, currentDevice - 1);
}

/* Get the initial seed */
unsigned long THHcRandom_initialSeed(THHcState* state) {
  return state->rngState->current_gen->initial_seed;
}

void THHcRandom_getRNGState(THHcState* state, THByteTensor* rng_state) {
  // The RNG state comprises the MTPG32 states and the seed.
  static const size_t states_size = 1 * sizeof(HOSTRandStateMtgp32);
  static const size_t seed_size = sizeof(unsigned long);
  static const size_t total_size = states_size + seed_size;
  THByteTensor_resize1d(rng_state, total_size);
  THArgCheck(THByteTensor_nElement(rng_state) == total_size, 1, "RNG state is wrong size");
  THArgCheck(THByteTensor_isContiguous(rng_state), 1, "RNG state must be contiguous");
  // FIXME: We get array_view pointers from stack, so host copy is fine, unless it is freed ahead
  HcRandStateMtgp32_copy_D2H(state->rngState->current_gen->gen_states,
                              THByteTensor_data(rng_state));
  memcpy(THByteTensor_data(rng_state) + states_size, &state->rngState->current_gen->initial_seed, seed_size);
}

void THHcRandom_setRNGState(THHcState* state, THByteTensor* rng_state) {
  static const size_t states_size = 1 * sizeof(HOSTRandStateMtgp32);
  static const size_t seed_size = sizeof(unsigned long);
  static const size_t total_size = states_size + seed_size;
  THArgCheck(THByteTensor_nElement(rng_state) == total_size, 1, "RNG state is wrong size");
  THArgCheck(THByteTensor_isContiguous(rng_state), 1, "RNG state must be contiguous");
  HcRandStateMtgp32_copy_H2D(THByteTensor_data(rng_state),
                              state->rngState->current_gen->gen_states);
  memcpy(&state->rngState->current_gen->initial_seed, THByteTensor_data(rng_state) + states_size, seed_size);
}

#define GENERATE_KERNEL1(NAME, ARG1, HcRAND_FUNC, FUNCTOR)                                                \
void NAME(THHcState* state, int size, THHcTensor *result, double ARG1) {                                   \
  HcRandStateMtgp32* s = state->rngState->current_gen->gen_states;                                        \
  hc::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();            \
  auto Dresult = result->get_device_data(state);                                                          \
  hc::array_view<float, 1> av_result(result->storage->size, Dresult); \
  HcRAND_FUNC##_kernel(accl_view, s, av_result, FUNCTOR);                                                 \
}
#define GENERATE_KERNEL2(NAME, ARG1, ARG2, HcRAND_FUNC, FUNCTOR)                                          \
void NAME(THHcState* state, int size, THHcTensor *result, double ARG1, double ARG2) {                      \
  HcRandStateMtgp32* s = state->rngState->current_gen->gen_states;                                        \
  hc::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();            \
  auto Dresult = result->get_device_data(state);                                                          \
  hc::array_view<float, 1> av_result(result->storage->size, Dresult); \
  HcRAND_FUNC##_kernel(accl_view, s, av_result, FUNCTOR);                                                 \
}

struct user_uniform_functor {
  double _a;
  double _b;
  user_uniform_functor(double a, double b) __attribute__((hc, cpu)) : _a(a), _b(b) {}
  inline double operator()(const float& x) const __attribute__((hc, cpu)) {
    return x * (_b - _a) + _a;
  }
  // User should provide copy ctor
  user_uniform_functor(const user_uniform_functor&other) __attribute__((hc, cpu)) : _a(other._a), _b(other._b) { }
  // User should provide copy assign ctor
  user_uniform_functor& operator = (const user_uniform_functor&other) __attribute__((hc, cpu)) {
    _a = other._a;
    _b = other._b;
    return *this;
  }
};

GENERATE_KERNEL2(generate_uniform, a, b, user_uniform, user_uniform_functor(a, b))
struct user_bernoulli_functor {
  double _p;
  user_bernoulli_functor(double p) __attribute__((hc, cpu)) : _p(p) {}
  inline double operator()(const float& x) const __attribute__((hc, cpu)) {
    return (double)x <= _p;
  }
  // User should provide copy ctor
  user_bernoulli_functor(const user_bernoulli_functor&other) __attribute__((hc, cpu)) : _p(other._p) { }
  // User should provide copy assign ctor
  user_bernoulli_functor& operator = (const user_bernoulli_functor&other) __attribute__((hc, cpu)) {
    _p = other._p;
    return *this;
  }
};

GENERATE_KERNEL1(generate_bernoulli, p, user_uniform, user_bernoulli_functor(p))
struct user_normal_functor {
  double _stdv;
  double _mean;
  user_normal_functor(double stdv, double mean) __attribute__((hc, cpu)) : _stdv(stdv), _mean(mean) {}
  inline double operator()(const float& x) const __attribute__((hc, cpu)) {
    return (x * _stdv) + _mean;
  }
  // User should provide copy ctor
  user_normal_functor(const user_normal_functor&other) __attribute__((hc, cpu))
    : _stdv(other._stdv), _mean(other._mean) { }
  // User should provide copy assign ctor
  user_normal_functor& operator = (const user_normal_functor&other) __attribute__((hc, cpu)) {
    _stdv = other._stdv;
    _mean = other._mean;
    return *this;
  }
};

GENERATE_KERNEL2(generate_normal, mean, stdv, user_normal, user_normal_functor(stdv, mean))
struct user_geometric_functor {
  double _p;
  user_geometric_functor(double p) __attribute__((hc, cpu)) : _p(p) {}
  inline double operator()(const float& x) const __attribute__((hc, cpu)) {
    return (hc::precise_math::log((double)(1 - x)) / hc::precise_math::log(_p)) + 1;
  }
  // User should provide copy ctor
  user_geometric_functor(const user_geometric_functor&other) __attribute__((hc, cpu)) : _p(other._p) { }
  // User should provide copy assign ctor
  user_geometric_functor& operator = (const user_geometric_functor&other) __attribute__((hc, cpu)) {
    _p = other._p;
    return *this;
  }
};

GENERATE_KERNEL1(generate_geometric, p, user_uniform, user_geometric_functor(p))
struct user_exponential_functor {
  double _lambda;
  user_exponential_functor(double lambda) __attribute__((hc, cpu)) : _lambda(lambda) {}
  inline double operator()(const float& x) const __attribute__((hc, cpu)) {
    return (double)(-1. / _lambda * hc::precise_math::log((double)(1 - x)));
  }
  // User should provide copy ctor
  user_exponential_functor(const user_exponential_functor&other) __attribute__((hc, cpu)) : _lambda(other._lambda) { }
  // User should provide copy assign ctor
  user_exponential_functor& operator = (const user_exponential_functor&other) __attribute__((hc, cpu)) {
    _lambda = other._lambda;
    return *this;
  }
};

GENERATE_KERNEL1(generate_exponential, lambda, user_uniform, user_exponential_functor(lambda))
struct user_cauchy_functor {
  double _median;
  double _sigma;
  user_cauchy_functor(double median, double sigma) __attribute__((hc, cpu)) : _median(median), _sigma(sigma) {}
  inline double operator()(const float& x) const __attribute__((hc, cpu)) {
    return (double)(_median + _sigma * hc::precise_math::tan((double)M_PI * (x - 0.5)));
  }
  // User should provide copy ctor
  user_cauchy_functor(const user_cauchy_functor&other) __attribute__((hc, cpu))
    : _median(other._median), _sigma(other._sigma) { }
  // User should provide copy assign ctor
  user_cauchy_functor& operator = (const user_cauchy_functor&other) __attribute__((hc, cpu)) {
    _median = other._median;
    _sigma = other._sigma;
    return *this;
  }
};

GENERATE_KERNEL2(generate_cauchy, median, sigma, user_uniform, user_cauchy_functor(median, sigma))

#undef GENERATE_KERNEL1
#undef GENERATE_KERNEL2

/* Separate kernel because curand_log_normal gets extra parameters. */
void generate_log_normal(THHcState* state, int size, THHcTensor* self_, double mean, double stddev) {
  HcRandStateMtgp32* s = state->rngState->current_gen->gen_states;
  hc::accelerator_view accl_view = state->deviceState->get_current_accelerator_view();
  auto dresult = self_->get_device_data(state);
  hc::array_view<float, 1> av_result(self_->storage->size, dresult);
  user_log_normal_kernel(accl_view, s, av_result, mean, stddev);
}

#define NUM_BLOCKS min((int)DIVUP(size, BLOCK_SIZE), MAX_NUM_BLOCKS)
void THHcTensor_uniform(THHcState* state, THHcTensor* self_, double a, double b) {
  if (state->rngState->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  generate_uniform(state, size, self, a, b);
  THHcTensor_freeCopyTo(state, self, self_);
};

void THHcTensor_bernoulli(THHcState* state, THHcTensor* self_, double p) {
  if (state->rngState->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  generate_bernoulli(state, size, self, p);
  THHcTensor_freeCopyTo(state, self, self_);
};

void THHcTensor_normal(THHcState* state, THHcTensor* self_, double mean, double stdv) {
  if (state->rngState->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  generate_normal(state, size, self, mean, stdv);
  THHcTensor_freeCopyTo(state, self, self_);
};

void THHcTensor_logNormal(THHcState* state, THHcTensor* self_, double mean, double stdv) {
  if (state->rngState->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  generate_log_normal(state, size, self, mean, stdv);
  THHcTensor_freeCopyTo(state, self, self_);
};

void THHcTensor_geometric(THHcState* state, THHcTensor* self_, double p) {
  if (state->rngState->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  generate_geometric(state, size, self, p);
  THHcTensor_freeCopyTo(state, self, self_);
};

void THHcTensor_exponential(THHcState* state, THHcTensor* self_, double lambda) {
  if (state->rngState->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  generate_exponential(state, size, self, lambda);
  THHcTensor_freeCopyTo(state, self, self_);
};

void THHcTensor_cauchy(THHcState* state, THHcTensor* self_, double median, double sigma) {
  if (state->rngState->current_gen == NULL) {
    THError("Random number generators have not been initialized.");
  }

  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  generate_cauchy(state, size, self, median, sigma);
  THHcTensor_freeCopyTo(state, self, self_);
};
#undef NUM_BLOCKS
