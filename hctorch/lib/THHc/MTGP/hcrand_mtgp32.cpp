#include <hc.hpp>
#include "hcrand_mtgp32.h"

using namespace hc;

//for debug purpose
//#define DEBUG_HcRAND
#ifdef DEBUG_HcRAND
#include <iostream>
template <typename T>
void VerifyData(T expected, T actual, const char* s) {
  if (expected != actual) {
    std::cout << s << " expected=" << expected << "  actual=" << actual << std::endl;
    exit(1);
  }
}
// Host APIs
/**
 * This function initializes the internal state array with a 32-bit
 * integer seed. The allocated memory should be freed by calling
 * mtgp32_free(). \b para should be one of the elements in the
 * parameter table (mtgp32-param-ref.c).
 *
 * @param[out] array MTGP internal status vector.
 * @param[in] para parameter structure
 * @param[in] seed a 32-bit integer used as the seed.
 */
void mtgp32_init_state(uint32_t array[], const mtgp32_params_fast_t* para,
                       uint32_t seed) {
  int i;
  int size = para->mexp / 32 + 1;
  uint32_t hidden_seed;
  uint32_t tmp;
  hidden_seed = para->tbl[4] ^ (para->tbl[8] << 16);
  tmp = hidden_seed;
  tmp += tmp >> 16;
  tmp += tmp >> 8;
  memset(array, tmp & 0xff, sizeof(uint32_t) * size);
  array[0] = seed;
  array[1] = hidden_seed;

  for (i = 1; i < size; i++) {
    array[i] ^= UINT32_C(1812433253) * (array[i - 1]
                                        ^ (array[i - 1] >> 30)) + i;
  }

  for (i = size; i < MTGP32_STATE_SIZE; i++) {
    array[i] = 0;
  }
}
#endif


// C-style
void HcRandStateMtgp32_init(hc::accelerator_view accl_view,
                             HcRandStateMtgp32* s) {
  // kernel params
  auto arrP = hc::array<uint32_t, 2>(hc::extent<2>(HcRAND_GROUP_NUM, MTGP32_TS), accl_view);
  s->param_tbl = new hc::array_view<uint32_t, 2>(arrP);
  auto arrT = hc::array<uint32_t, 2>(hc::extent<2>(HcRAND_GROUP_NUM, MTGP32_TS), accl_view);
  s->temper_tbl = new hc::array_view<uint32_t, 2>(arrT);
  auto arrS = hc::array<uint32_t, 2>(hc::extent<2>(HcRAND_GROUP_NUM, MTGP32_TS), accl_view);
  s->single_temper_tbl = new hc::array_view<uint32_t, 2>(arrS);
  auto arrPos = hc::array<uint32_t, 1>(hc::extent<1>(HcRAND_GROUP_NUM), accl_view);
  s->pos_tbl = new hc::array_view<uint32_t, 1>(arrPos);
  auto arrSh1 = hc::array<uint32_t, 1>(hc::extent<1>(HcRAND_GROUP_NUM), accl_view);
  s->sh1_tbl = new hc::array_view<uint32_t, 1>(arrSh1);
  auto arrSh2 = hc::array<uint32_t, 1>(hc::extent<1>(HcRAND_GROUP_NUM), accl_view);
  s->sh2_tbl = new hc::array_view<uint32_t, 1>(arrSh2);
  auto arrMask = hc::array<uint32_t, 1>(hc::extent<1>(1), accl_view);
  s->mask = new hc::array_view<uint32_t, 1>(arrMask);
  // Redundant member
  auto arrMexp = hc::array<uint32_t, 1>(hc::extent<1>(HcRAND_GROUP_NUM), accl_view);
  s->mexp_tbl = new hc::array_view<uint32_t, 1>(arrMexp);
  // states
  auto arrStatus = hc::array<uint32_t, 2>(hc::extent<2>(USER_GROUP_NUM, MTGP32_STATE_SIZE), accl_view);
  s->d_status = new hc::array_view<uint32_t, 2>(arrStatus);
  auto arrOffset = hc::array<uint32_t, 1>(hc::extent<1>(USER_GROUP_NUM), accl_view);
  s->offset = new hc::array_view<uint32_t, 1>(arrOffset);
  auto arrIndex = hc::array<uint32_t, 1>(hc::extent<1>(USER_GROUP_NUM), accl_view);
  s->index = new hc::array_view<uint32_t, 1>(arrIndex);
}

#define FREE_MEMBER(s, member) \
  if (s->member) {             \
    s->member->~array_view();  \
    delete s->member;          \
  }
// TODO: Big memory leak and cause device buffer allocation fail if exits abnormally
void HcRandStateMtgp32_release(HcRandStateMtgp32* s) {
  FREE_MEMBER(s, param_tbl);
  FREE_MEMBER(s, temper_tbl);
  FREE_MEMBER(s, single_temper_tbl);
  FREE_MEMBER(s, pos_tbl);
  FREE_MEMBER(s, sh1_tbl);
  FREE_MEMBER(s, sh2_tbl);
  FREE_MEMBER(s, mask);
  FREE_MEMBER(s, mexp_tbl);
  FREE_MEMBER(s, d_status);
  FREE_MEMBER(s, offset);
  FREE_MEMBER(s, index);
}

void HcRandStateMtgp32_copy_D2H(void* src, void* dst) {
  HOSTRandStateMtgp32* host = (HOSTRandStateMtgp32*)(dst);
  HcRandStateMtgp32* hc = (HcRandStateMtgp32*)(src);
  hc::copy(*(hc->offset), host->offset);
  hc::copy(*(hc->index), host->index);
  hc::copy(*(hc->d_status), host->d_status);
  hc::copy(*(hc->mexp_tbl), host->mexp_tbl);
  hc::copy(*(hc->param_tbl), host->param_tbl);
  hc::copy(*(hc->temper_tbl), host->temper_tbl);
  hc::copy(*(hc->single_temper_tbl), host->single_temper_tbl);
  hc::copy(*(hc->pos_tbl), host->pos_tbl);
  hc::copy(*(hc->sh1_tbl), host->sh1_tbl);
  hc::copy(*(hc->sh2_tbl), host->sh2_tbl);
  hc::copy(*(hc->mask), host->mask);
}
void HcRandStateMtgp32_copy_H2D(void* src, void* dst) {
  HOSTRandStateMtgp32* host = (HOSTRandStateMtgp32*)(src);
  HcRandStateMtgp32* hc = (HcRandStateMtgp32*)(dst);
  hc::copy(host->offset, *(hc->offset));
  hc::copy(host->index, *(hc->index));
  hc::copy(host->d_status, *(hc->d_status));
  hc::copy(host->param_tbl, *(hc->param_tbl));
  hc::copy(host->temper_tbl, *(hc->temper_tbl));
  hc::copy(host->single_temper_tbl, *(hc->single_temper_tbl));
  hc::copy(host->pos_tbl, *(hc->pos_tbl));
  hc::copy(host->sh1_tbl, *(hc->sh1_tbl));
  hc::copy(host->sh2_tbl, *(hc->sh2_tbl));
  hc::copy(host->mask, *(hc->mask));
}


// The following are device APIs

// Copy param constants onto device
int mtgp32_init_params_kernel(hc::accelerator_view accl_view,
                              mtgp32_params_fast_t* params,
                              HcRandStateMtgp32* s) {
  auto &av_param_tbl = *(s->param_tbl);
  auto &av_temper_tbl = *(s->temper_tbl);
  auto &av_single_temper_tbl = *(s->single_temper_tbl);
  auto &av_pos_tbl = *(s->pos_tbl);
  auto &av_sh1_tbl = *(s->sh1_tbl);
  auto &av_sh2_tbl = *(s->sh2_tbl);
  auto &av_mask = *(s->mask);
  auto &av_mexp_tbl = *(s->mexp_tbl);
  // Prepare data source
  uint32_t vec_param[HcRAND_GROUP_NUM * MTGP32_TS] = {0x0};
  uint32_t vec_temper[HcRAND_GROUP_NUM * MTGP32_TS] = {0x0};
  uint32_t vec_single_temper[HcRAND_GROUP_NUM * MTGP32_TS] = {0x0};
  uint32_t vec_pos[HcRAND_GROUP_NUM] = {0x0};
  uint32_t vec_sh1[HcRAND_GROUP_NUM] = {0x0};
  uint32_t vec_sh2[HcRAND_GROUP_NUM] = {0x0};
  uint32_t vec_mexp[HcRAND_GROUP_NUM] = {0x0};

  for (int i = 0; i < HcRAND_GROUP_NUM; i++) {
    vec_pos[i] = params[i].pos;
    vec_sh1[i] = params[i].sh1;
    vec_sh2[i] = params[i].sh2;
    vec_mexp[i] = params[i].mexp;

    for (int j = 0; j < MTGP32_TS; j++) {
      vec_param[i * MTGP32_TS + j] = params[i].tbl[j];
      vec_temper[i * MTGP32_TS + j] = params[i].tmp_tbl[j];
      vec_single_temper[i * MTGP32_TS + j] = params[i].flt_tmp_tbl[j];
    }
  }

  // Ugly codes to populate array_views
  //av_mask[0] = params[0].mask;
  hc::copy(&params[0].mask, av_mask);
  hc::copy(vec_param, av_param_tbl);
  hc::copy(vec_temper, av_temper_tbl);
  hc::copy(vec_single_temper, av_single_temper_tbl);
  hc::copy(vec_pos, av_pos_tbl);
  hc::copy(vec_sh1, av_sh1_tbl);
  hc::copy(vec_sh2, av_sh2_tbl);
  hc::copy(vec_mexp, av_mexp_tbl);
#ifdef DEBUG_HcRAND
#if 1
  hc::copy(av_mask, av_mask.data());
  hc::copy(av_param_tbl, av_param_tbl.get());
  hc::copy(av_temper_tbl, av_temper_tbl.get());
  hc::copy(av_single_temper_tbl, av_single_temper_tbl.get());
  hc::copy(av_pos_tbl, av_pos_tbl.data());
  hc::copy(av_sh1_tbl, av_sh1_tbl.data());
  hc::copy(av_sh2_tbl, av_sh2_tbl.data());
  hc::copy(av_mexp_tbl, av_mexp_tbl.data());
#endif

  for (int i = 0; i < HcRAND_GROUP_NUM; i++) {
    VerifyData(vec_pos[i], av_pos_tbl[i], "pos table");
    VerifyData(vec_sh1[i], av_sh1_tbl[i], "sh1 table");
    VerifyData(vec_sh2[i], av_sh2_tbl[i], "sh2 table");
    VerifyData(vec_mexp[i], av_mexp_tbl[i], "mexp table");

    for (int j = 0; j < MTGP32_TS; j++) {
      VerifyData(vec_param[i * MTGP32_TS + j], av_param_tbl[i][j], "param table");
      VerifyData(vec_temper[i * MTGP32_TS + j], av_temper_tbl[i][j], "temper table");
      VerifyData(vec_single_temper[i * MTGP32_TS + j], av_single_temper_tbl[i][j], "single_temper table");
    }
  }

  VerifyData(params[0].mask, av_mask[0], "mask");
#endif
  return 0;
}

// Initialize HcRandStateMtgp32 by seed
int mtgp32_init_seed_kernel(hc::accelerator_view accl_view,
                            HcRandStateMtgp32* s, unsigned long seed) {
  seed = seed ^ (seed >> 32);
  int nGroups = USER_GROUP_NUM;
  auto &av_param_tbl = *(s->param_tbl);
  auto &av_offset = *(s->offset);
  auto &av_index = *(s->index);
  auto &av_mexp_tbl = *(s->mexp_tbl);
  auto &av_d_status = *(s->d_status);
  hc::extent<1> ext(nGroups);
  hc::parallel_for_each(accl_view, ext, [ = ] (hc::index<1> idx) __attribute__((hc, cpu)) {
    const int id = idx[0];

    if (id >= nGroups) {
      return;
    }

    uint32_t* status = &av_d_status[id][0];
    uint32_t mexp = av_mexp_tbl[id];
    int size = mexp / 32 + 1;
    // Initialize state
    int i;
    uint32_t hidden_seed;
    uint32_t tmp;
    hidden_seed = av_param_tbl[id][4] ^ (av_param_tbl[id][8] << 16);
    tmp = hidden_seed;
    tmp += tmp >> 16;
    tmp += tmp >> 8;
    tmp = tmp & 0xff;
    tmp |= tmp << 8;
    tmp |= tmp << 16;

    for (i = 0; i < size; i++) {
      status[i] = tmp;
    }

    status[0] = (unsigned int) seed + 1 + id;
    status[1] = hidden_seed;

    for (i = 1; i < size; i++) {
      status[i] ^= UINT32_C(1812433253) * (status[i - 1] ^ (status[i - 1] >> 30)) + i;
    }

#ifdef DEBUG_HcRAND

    for (i = size; i < MTGP32_STATE_SIZE; i++) {
      status[i] = 0;
    }

#endif
    av_offset[id] = 0;
    av_index[id] = id;
  });
#ifdef DEBUG_HcRAND
  {
    av_param_tbl.synchronize();
    av_offset.synchronize();
    av_index.synchronize();
    av_mexp_tbl.synchronize();
    av_d_status.synchronize();

    for (int i = 0; i < USER_GROUP_NUM; i++) {
      VerifyData((uint32_t)0, av_offset[i], "offset table");
      VerifyData((uint32_t)i, av_index[i], "index table");
    }

    uint32_t host_status[USER_GROUP_NUM][MTGP32_STATE_SIZE];

    for (int i = 0; i < USER_GROUP_NUM; i++) {
      mtgp32_init_state(&host_status[i][0], &mtgp32_params_fast_11213[i], seed + i + 1);
    }

    for (int i = 0; i < USER_GROUP_NUM; i++) {
      for (int j = 0; j < MTGP32_STATE_SIZE; j++) {
        VerifyData(host_status[i][j], av_d_status[i][j], "rand status");
      }
    }
  }
#endif
  return 0;
}
