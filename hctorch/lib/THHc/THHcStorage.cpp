#include "THHcStorage.h"
#include "THHcFunctorOpns.h"

void THHcStorage_fill(THHcState* state, THHcStorage* self, float value) {
  // Make sure every changes need to be made to its device data
  fill(state, self->data, self->size, value);
}

void THHcStorage_resize(THHcState* state, THHcStorage* self, long size) {
  THArgCheck(size >= 0, 2, "invalid size");
  
  if (!(self->flag & TH_STORAGE_RESIZABLE)) {
    THError("Trying to resize storage that is not resizable");
  }
  
  if (size == 0) {
    if (self->flag & TH_STORAGE_FREEMEM) {
      THHcCheck(THHcFree(state, self->data));
    }
    self->data = NULL;
    self->size = 0;
  } else {
    float* data = NULL;
    THHcCheck(THHcMalloc(state, (void**)&data, size * sizeof(float)));  
    if (self->data) {
      // Copy the contents to resized buffer
      THHcCheck(hc::am_copy(data, self->data, THMin(self->size, size) * sizeof(float)));
      // Free the old buffer
      THHcCheck(THHcFree(state, self->data));
    }
    self->data = data;
    self->size = size;
  }
}

