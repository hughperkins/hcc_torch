#ifndef THHC_FUNCTOR_OPNS_H
#define THHC_FUNCTOR_OPNS_H


#include "THHcTensor.h"
#include "THHcGeneral.h"

// Common Operators definition
template <class T> struct identity {
  T operator() (const T& x) const __attribute__((hc, cpu)) {return x;}
};

template <class T> struct sum {
  T operator() (const T& x, const T& y) const __attribute__((hc, cpu)) {return x+y;}
};

template <class T> struct multiply {
  T operator() (const T& x, const T& y) const __attribute__((hc, cpu)) {return x * y;}
};

template <class T> struct divide {
  T operator() (const T& x, const T& y) const __attribute__((hc, cpu)) {return x/y;}
};

template <class T> struct maxval {
  T operator() (const T& x, const T& y) const __attribute__((hc, cpu)) {
     if(x > y) return x; else return y;
  }
};

template <class T> struct minval {
  T operator() (const T& x, const T& y) const __attribute__((hc, cpu)) {
     if(x < y) return x; else return y;
  }
};

template <class T> struct is_equal_to {
  bool operator() (const T& x, const T& y) const __attribute__((hc, cpu)) {return x==y;}
};

template <class T> struct is_not_equal_to {
  bool operator() (const T& x, const T& y) const __attribute__((hc, cpu)) {return x!=y;}
};

// Pair structure
template<typename T, typename U>
struct pair_t{
  T first;
  U second;

  // constructor
  inline pair_t<T, U>(T a, U b) __attribute__((hc, cpu)) : first(a), second(b) {}
};




struct addvalue_functor {
  float value;
  addvalue_functor(float value_) __attribute__((hc, cpu)) : value(value_) {}
  addvalue_functor(const addvalue_functor& other) __attribute__((hc, cpu))
    : value(other.value) {}
  addvalue_functor& operator = (const addvalue_functor&other) __attribute__((hc, cpu)) {
    value = other.value;
    return *this;
  }
  float operator()(const float& x) const __attribute__((hc, cpu)) {
    return (x + value);
  }
};

struct mse_functor {
  mse_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    float z = x - y;
    return z * z;
  }
};

struct mulvalue_functor {
  float value;
  mulvalue_functor(float value_)__attribute__((hc, cpu)) : value(value_) {}
  mulvalue_functor(const mulvalue_functor& other) __attribute__((hc, cpu))
    : value(other.value) {}
  mulvalue_functor& operator = (const mulvalue_functor&other) __attribute__((hc, cpu)) {
    value = other.value;
    return *this;
  }
  float operator()(const float& x) const __attribute__((hc, cpu)) {
    return (x * value);
  }
};

struct divvalue_functor {
  float value;
  divvalue_functor(float value_)__attribute__((hc, cpu)) : value(value_) {}
  divvalue_functor(const divvalue_functor& other) __attribute__((hc, cpu))
    : value(other.value) {}
  divvalue_functor& operator = (const divvalue_functor&other) __attribute__((hc, cpu)) {
    value = other.value;
    return *this;
  }
  float operator()(const float& x) const __attribute__((hc, cpu)) {
    return (x / value);
  }
};

struct pow_functor {
  float value;
  pow_functor(float value_) __attribute__((hc, cpu)) : value(value_) {}
  pow_functor(const pow_functor& other) __attribute__((hc, cpu)) : value(other.value) {}
  pow_functor& operator = (const pow_functor&other) __attribute__((hc, cpu)) {
    value = other.value;
    return *this;
  }
  float operator()(const float& x) const __attribute__((hc, cpu)) {
    return hc::fast_math::pow(x, value);
  }
};

struct tpow_functor {
  float value;
  tpow_functor(float value_) __attribute__((hc, cpu)): value(value_) {}
  tpow_functor(const tpow_functor& other) __attribute__((hc, cpu)): value(other.value) {}
  tpow_functor& operator = (const tpow_functor&other) __attribute__((hc, cpu)) {
    value = other.value;
    return *this;
  }
  float operator()(const float& x) const __attribute__((hc, cpu)) {
    return hc::fast_math::pow(value, x);
  }
};


struct cpow_functor {
  cpow_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& a, const float& b) const __attribute__((hc, cpu)) {
    return hc::fast_math::pow(a, b);
  }
};


struct atan2_functor {
  atan2_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    return hc::fast_math::atan2f(x, y);
  }
};

struct clamp_functor {
  float min_value;
  float max_value;
  clamp_functor(float min_value_, float max_value_) __attribute__((hc, cpu)): min_value(min_value_), max_value(max_value_) {}
  clamp_functor(const clamp_functor& other) __attribute__((hc, cpu)): min_value(other.min_value), max_value(other.max_value) {}
  clamp_functor& operator = (const clamp_functor&other) __attribute__((hc, cpu)) {
    min_value = other.min_value;
    max_value = other.max_value;
    return *this;
  }
  float operator()(const float& x) const __attribute__((hc, cpu)) {
    if (x < min_value) {
      return min_value;
    }
    
    if (x > max_value) {
      return max_value;
    }
    
    return x;
  }
};

struct sign_functor {
  sign_functor() __attribute__((hc, cpu)) {}
  float operator()(const float &v) const __attribute__((hc, cpu)) {
    return (v > 0) - (v < 0);
  }
};

struct dist_functor {
  float exponent;
  dist_functor(float exponent_) __attribute__((hc, cpu)) : exponent(exponent_) {}
  dist_functor(const dist_functor& other) __attribute__((hc, cpu)) : exponent(other.exponent) {}
  dist_functor& operator = (const dist_functor&other) __attribute__((hc, cpu)) {
    exponent = other.exponent;
    return *this;
  }
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    return hc::fast_math::pow(hc::fast_math::fabs(x - y), exponent);
  }
};

struct norm_functor {
  float exponent;
  norm_functor(float exponent_)__attribute__((hc, cpu)) : exponent(exponent_) {}
  norm_functor(const norm_functor& other)__attribute__((hc, cpu)) : exponent(other.exponent) {}
  norm_functor& operator = (const norm_functor&other) __attribute__((hc, cpu)) {
    exponent = other.exponent;
    return *this;
  }
  float operator()(const float& x) const __attribute__((hc, cpu)) {
    return hc::fast_math::pow(hc::fast_math::fabs(x), exponent);
  }
};

struct partial_not_equal_functor {
  float rhs;
  partial_not_equal_functor(float rhs) __attribute__((hc, cpu)) : rhs(rhs) {}
  partial_not_equal_functor(const partial_not_equal_functor& other) __attribute__((hc, cpu)) : rhs(other.rhs) {}
  partial_not_equal_functor& operator = (const partial_not_equal_functor&other) __attribute__((hc, cpu)) {
    rhs = other.rhs;
    return *this;
  }
  bool operator()(const float &lhs) const __attribute__((hc, cpu)) {
    return lhs != rhs;
  }
};

struct mse_updateGradInput_functor {
  float norm;
  mse_updateGradInput_functor(float norm_) __attribute__((hc, cpu)) : norm(norm_) {}
  mse_updateGradInput_functor(const mse_updateGradInput_functor &other) __attribute__((hc, cpu)) : norm(other.norm) {}
  mse_updateGradInput_functor& operator = (const mse_updateGradInput_functor&other) __attribute__((hc, cpu)) {
   norm = other.norm;
    return *this;
  }
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    return norm * (x - y);
  }
};

struct binary_abs_functor {
  binary_abs_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    float z = x - y;
    return z >= 0 ? z : -z;
  }
};

struct abs_updateGradInput_functor {
  float norm;
  abs_updateGradInput_functor(float norm_) __attribute__((hc, cpu)): norm(norm_) {}
  abs_updateGradInput_functor(const abs_updateGradInput_functor& other) __attribute__((hc, cpu)): norm(other.norm) {}
  abs_updateGradInput_functor& operator = (const abs_updateGradInput_functor&other) __attribute__((hc, cpu)) {
   norm = other.norm;
    return *this;
  }
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    return (x - y) >= 0 ? norm : -norm;
  }
};

struct kl_functor {
  kl_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    return y > 0 ? y * (hc::fast_math::log(y) - x) : 0;
  }
};

struct kl_updateGradInput_functor {
  float norm;
  kl_updateGradInput_functor(float norm_) __attribute__((hc, cpu)) : norm(norm_) {}
  kl_updateGradInput_functor(const kl_updateGradInput_functor& other) __attribute__((hc, cpu)) : norm(other.norm) {}
  kl_updateGradInput_functor& operator = (const kl_updateGradInput_functor&other) __attribute__((hc, cpu)) {
   norm = other.norm;
    return *this;
  }
  float operator()(const float& x, const float& y) const __attribute__((hc, cpu)) {
    return y > 0 ? norm * (-y) : 0;
  }
};
#define IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(NAME, CFUNC) \
struct NAME##_functor {                                 \
  NAME##_functor() __attribute__((hc, cpu)) {}                \
  float operator()(const float& x) const {              \
    return CFUNC(x);                                    \
  }                                                     \
};

IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(log, hc::fast_math::log)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(log1p, hc::precise_math::log1p)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(exp, hc::fast_math::exp)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(cos, hc::fast_math::cos)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(acos, hc::fast_math::acos)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(cosh, hc::fast_math::cosh)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(sin, hc::fast_math::sin)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(asin, hc::fast_math::asin)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(sinh, hc::fast_math::sinh)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(tan, hc::fast_math::tan)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(atan, hc::fast_math::atan)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(tanh, hc::fast_math::tanh)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(sqrt, hc::fast_math::sqrt)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(ceil, hc::fast_math::ceil)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(floor, hc::fast_math::floor)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(abs, hc::fast_math::fabs)
IMPLEMENT_Hc_TENSOR_BASIC_FUNCTOR(round, hc::fast_math::roundf)


// Define transform functions

// Unary transforms
template <typename UnaryFunction>
void transform(THHcState* state, THHcTensor* first, THHcTensor* result, UnaryFunction op) {
  auto avData_first = first->get_device_data(state);
  auto avData_result = result->get_device_data(state);
  long size = THHcTensor_nElement(state, result);
  unary_transform(state, avData_first, first->storageOffset, avData_result, result->storageOffset, size, op);
}

// Binary transform
template <typename BinaryFunction>
void transform(THHcState* state, THHcTensor* first1, THHcTensor* first2, THHcTensor* result, BinaryFunction op) {
  auto avData_first1 = first1->get_device_data(state);
  auto avData_first2 = first2->get_device_data(state);
  auto avData_result = result->get_device_data(state);
  long size = THHcTensor_nElement(state, result);
  binary_transform(state, avData_first1, first1->storageOffset, avData_first2, first2->storageOffset,
                   avData_result, result->storageOffset, size, op);
}

template <typename BinaryFunction>
void binary_transform(THHcState* state, float*& first1, long first1Offset,
                      float*& first2, long first2Offset,
                      float*& result, long resultOffset, long size,  BinaryFunction f) {
  if (size == 0) {
    return;
  }

  unsigned grdSz = (size + 255) & ~(255);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu)) {
    long index = tidx.global[0];

    if (index < size) {
      result[resultOffset + index] = (float) f(first1[first1Offset + index], first2[first2Offset + index]);
    }
  }).wait();
  return;
}

template <typename UnaryFunction>
void unary_transform(THHcState* state, float*& first, long firstOffset,
                     float*& result, long resultOffset, long size, UnaryFunction f) {
  if (size == 0) {
    return;
  }

  unsigned grdSz = (size + 256) - (size % 256);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu)) {
    long index = tidx.global[0];

    if (index < size) {
      result[resultOffset + index] = f(first[firstOffset + index]);
    }
  }).wait();
  return;
}


// Reduce routines

#define BLOCK_SIZE 256
template <class T, typename BinaryFunction>
inline void reduce_operation(THHcState* state, T *g_idata, T *g_odata, unsigned int n, T val,  BinaryFunction f) {
  
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator accl = state->deviceState->get_current_accelerator();
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  long reduce_num_blocks = (n + (BLOCK_SIZE - 1) & ~(BLOCK_SIZE - 1))/BLOCK_SIZE;

  float* devPartialOut = hc::am_alloc(sizeof(T) * reduce_num_blocks, accl, 0);
 
  hc::extent<1> grdExt(reduce_num_blocks * BLOCK_SIZE);
  hc::tiled_extent<1> t_ext = grdExt.tile(BLOCK_SIZE);
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu))
  {
    tile_static T buf_tmp[BLOCK_SIZE];
    int idx = tidx.global[0];
    int block_idx = idx / BLOCK_SIZE;
    int thread_in_block_idx = idx % BLOCK_SIZE;
    int eidx = idx;
    T res = val;

    while(eidx < n)
    {
      res = f(res, g_idata[eidx]);
      eidx += reduce_num_blocks * BLOCK_SIZE;
    }
    buf_tmp[thread_in_block_idx] = res;
    tidx.barrier.wait();

    // Seqential part
    if (tidx.local[0] == 0)
    {
      res = val;
      for (uint i = 0; i < BLOCK_SIZE; i++)
      {
        res = f(res, buf_tmp[i]);
      }
      devPartialOut[block_idx] = res;
    }
  }).wait();

  hc::extent<1> grdExt1(1);
  hc::tiled_extent<1> t_ext1 = grdExt1.tile(1);
  hc::parallel_for_each(accl_view, t_ext1, [=] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu))
  {
    T res = val;
    for (uint i = 0; i < reduce_num_blocks; i++)
    {
      res = f(res, devPartialOut[i]);
    }
    g_odata[0] = res;
  }).wait(); 

  hc::am_free(devPartialOut); 
}

template<class T, typename BinaryFunction>
inline T reduce(THHcState* state, THHcTensor* input, T init, BinaryFunction f) {
  T hRes, *dRes = NULL;
  auto dv_input_data = input->get_device_data(state);
  THHcCheck(THHcMalloc(state, (void**)&dRes, 1 * sizeof(T)));
  reduce_operation(state, dv_input_data + input->storageOffset, dRes, THHcTensor_nElement(state, input), init, f);
  hc::am_copy(&hRes, dRes, 1*sizeof(T));
  THHcFree(state, dRes);
  return hRes;
}



// Innerproduct
template <class T, typename BinaryFunction1, typename BinaryFunction2>
inline T inner_product(THHcState* state, THHcTensor* first1, THHcTensor* first2, T init, BinaryFunction1 op1, BinaryFunction2 op2) {
  // Create temp contiguous array to store intermediate transform results  
  THHcTensor* temp = THHcTensor_newContiguous(state, first1);
  transform(state, first1, first2, temp, op2);
  return reduce<T>(state, temp, init, op1);
}

float InnerProduct_plus_mse(THHcState* state, THHcTensor* input, THHcTensor* target);
float InnerProduct_plus_abs(THHcState* state, THHcTensor* input, THHcTensor* target);
float InnerProduct_plus_kl(THHcState* state, THHcTensor* input, THHcTensor* target);
float InnerProduct_plus_dist(THHcState* state, THHcTensor* self, THHcTensor* src, float value);

float transform_var_all(THHcState* state, THHcTensor* self, float mean);
void transform_clamp(THHcState* state, THHcTensor* src, THHcTensor* self, float min_value, float max_value);
void transform_mse(THHcState* state, THHcTensor* input, THHcTensor* target, THHcTensor* gradInput, float norm);
void transform_abs(THHcState* state, THHcTensor* input, THHcTensor* target, THHcTensor* gradInput, float norm);
void transform_kl(THHcState* state, THHcTensor* input, THHcTensor* target, THHcTensor* gradInput, float norm);
void transform_addvalue(THHcState* state, THHcTensor* src, THHcTensor* self, float value);
void transform_mulvalue(THHcState* state, THHcTensor* src, THHcTensor* self, float value);
void transform_divvalue(THHcState* state, THHcTensor* src, THHcTensor* self, float value);
void transform_pow(THHcState* state, THHcTensor* src, THHcTensor* self, float value);
void transform_tpow(THHcState* state, THHcTensor* src, THHcTensor* self, float value);
void transformBinary_cpow(THHcState* state, THHcTensor* src1, THHcTensor* src2, THHcTensor* self);
void transform_log(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_log1p(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_exp(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_cos(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_acos(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_cosh(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_sin(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_asin(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_sinh(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_tan(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_atan(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_tanh(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_sqrt(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_ceil(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_floor(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_abs(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_round(THHcState* state, THHcTensor* src, THHcTensor* self);
void transform_sign(THHcState* state, THHcTensor* src, THHcTensor* self);

void transformBinary_multiply(THHcState* state, THHcTensor* src1, THHcTensor* src2, THHcTensor* self);
void transformBinary_divide(THHcState* state, THHcTensor* src1, THHcTensor* src2, THHcTensor* self);
void transformBinary_atan2(THHcState* state, THHcTensor* src1, THHcTensor* src2, THHcTensor* self);

float Reduce_minimum(THHcState* state, THHcTensor* self);
float Reduce_maximum(THHcState* state, THHcTensor* self);
float Reduce_plus(THHcState* state, THHcTensor* self);
float Reduce_multiply(THHcState* state, THHcTensor* self);
float InnerPdt(THHcState* state, THHcTensor* self, THHcTensor* src);
void fill(THHcState* state, float*& dest, long size, float  val);
#endif
