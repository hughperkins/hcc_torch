#ifndef THHC_TENSOR_INC
#define THHC_TENSOR_INC

#include "THTensor.h"
#include "THHcStorage.h"
#include "THHcGeneral.h"

#define TH_TENSOR_REFCOUNTED 1

//#define TRAP_STORAGE_MEMORY_LEAK

typedef struct THHcTensor {
  long* size;
  long* stride;
  int nDimension;
  THHcStorage* storage;
  long storageOffset;
  int refcount;
  char flag;
  
  // Function to return device pointer associated with Tensor
  inline float* get_device_data(THHcState* state) {
  
    float* devPtr = NULL;
    
    if (this->storage && this->storage->refcount) {
      devPtr = static_cast<float*>(this->storage->data);
    } else {
      // TODO: Memory leak here
      // Avoid using free-ed storage. Create a new one with the size determined by Tensor
      {
        long totalSize = 1;
        
        for (int d = this->nDimension - 1; d >= 0; d--) {
          totalSize += (this->size[d] - 1) * this->stride[d];
        }
        
        if (totalSize + this->storageOffset > 0) {
          if (!this->storage) {
#ifdef TRAP_STORAGE_MEMORY_LEAK
            THAssertMsg((1 == 0), "Try to use a null storage!");
#endif
            this->storage = THHcStorage_new(state);
          } else if (this->storage && this->storage->refcount == 0) {
#ifdef TRAP_STORAGE_MEMORY_LEAK
            THAssertMsg((1 == 0), "Try to use a free-ed storage!");
#endif
            // FIXME: Though there is leak, we use a new one to mitigate the crash
            THHcStorage_resize(state, this->storage, totalSize + this->storageOffset);
          }
        }
      }
      devPtr = static_cast<float*>(this->storage->data);
    }
    
    THAssertMsg((devPtr), "null pointer");
    return devPtr;
  }
  
  // Function to get ID of the device on which this Tensor resides
  int getDevice(hc::accelerator current_accl) {
    for (int i = 0; i < hc::accelerator::get_all().size(); i ++) {
      if (current_accl == hc::accelerator::get_all()[i]) {
        int deviceIdx = i;
        return deviceIdx;
      }
    }
    
    assert(1 && "not valid device ID");
    return 0;
  }
  
  // Function to return the source accelerator view. This will be used by get_device_data(THHcState *state, )
  // method of  THHcTensor to provide the array_view in the appropriate accelerator.
  inline hc::accelerator_view get_accelerator_view() {
    hc::array_view<float, 1> avPtr(this->storage->size, this->storage->data);
    return avPtr.get_source_accelerator_view();
  }

} THHcTensor;

/**** access methods ****/
THHC_API THHcStorage* THHcTensor_storage(THHcState* state, const THHcTensor* self);
THHC_API long THHcTensor_storageOffset(THHcState* state, const THHcTensor* self);
THHC_API int THHcTensor_nDimension(THHcState* state, const THHcTensor* self);
THHC_API long THHcTensor_size(THHcState* state, const THHcTensor* self, int dim);
THHC_API long THHcTensor_stride(THHcState* state, const THHcTensor* self, int dim);
THHC_API THLongStorage* THHcTensor_newSizeOf(THHcState* state, THHcTensor* self);
THHC_API THLongStorage* THHcTensor_newStrideOf(THHcState* state, THHcTensor* self);
THHC_API float* THHcTensor_data(THHcState* state, const THHcTensor* self);

THHC_API void THHcTensor_setFlag(THHcState* state, THHcTensor* self, const char flag);
THHC_API void THHcTensor_clearFlag(THHcState* state, THHcTensor* self, const char flag);


/**** creation methods ****/
THHC_API THHcTensor* THHcTensor_new(THHcState* state);
THHC_API THHcTensor* THHcTensor_newWithTensor(THHcState* state, THHcTensor* tensor);
/* stride might be NULL */
THHC_API THHcTensor* THHcTensor_newWithStorage(THHcState* state, THHcStorage* storage_, long storageOffset_,
    THLongStorage* size_, THLongStorage* stride_);
THHC_API THHcTensor* THHcTensor_newWithStorage1d(THHcState* state, THHcStorage* storage_, long storageOffset_,
    long size0_, long stride0_);
THHC_API THHcTensor* THHcTensor_newWithStorage2d(THHcState* state, THHcStorage* storage_, long storageOffset_,
    long size0_, long stride0_,
    long size1_, long stride1_);
THHC_API THHcTensor* THHcTensor_newWithStorage3d(THHcState* state, THHcStorage* storage_, long storageOffset_,
    long size0_, long stride0_,
    long size1_, long stride1_,
    long size2_, long stride2_);
THHC_API THHcTensor* THHcTensor_newWithStorage4d(THHcState* state, THHcStorage* storage_, long storageOffset_,
    long size0_, long stride0_,
    long size1_, long stride1_,
    long size2_, long stride2_,
    long size3_, long stride3_);

/* stride might be NULL */
THHC_API THHcTensor* THHcTensor_newWithSize(THHcState* state, THLongStorage* size_, THLongStorage* stride_);
THHC_API THHcTensor* THHcTensor_newWithSize1d(THHcState* state, long size0_);
THHC_API THHcTensor* THHcTensor_newWithSize2d(THHcState* state, long size0_, long size1_);
THHC_API THHcTensor* THHcTensor_newWithSize3d(THHcState* state, long size0_, long size1_, long size2_);
THHC_API THHcTensor* THHcTensor_newWithSize4d(THHcState* state, long size0_, long size1_, long size2_, long size3_);

THHC_API THHcTensor* THHcTensor_newClone(THHcState* state, THHcTensor* self);
THHC_API THHcTensor* THHcTensor_newContiguous(THHcState* state, THHcTensor* tensor);
THHC_API THHcTensor* THHcTensor_newSelect(THHcState* state, THHcTensor* tensor, int dimension_, long sliceIndex_);
THHC_API THHcTensor* THHcTensor_newNarrow(THHcState* state, THHcTensor* tensor, int dimension_, long firstIndex_, long size_);
THHC_API THHcTensor* THHcTensor_newTranspose(THHcState* state, THHcTensor* tensor, int dimension1_, int dimension2_);
THHC_API THHcTensor* THHcTensor_newUnfold(THHcState* state, THHcTensor* tensor, int dimension_, long size_, long step_);

THHC_API void THHcTensor_resize(THHcState* state, THHcTensor* tensor, THLongStorage* size, THLongStorage* stride);
THHC_API void THHcTensor_resizeAs(THHcState* state, THHcTensor* tensor, THHcTensor* src);
THHC_API void THHcTensor_resize1d(THHcState* state, THHcTensor* tensor, long size0_);
THHC_API void THHcTensor_resize2d(THHcState* state, THHcTensor* tensor, long size0_, long size1_);
THHC_API void THHcTensor_resize3d(THHcState* state, THHcTensor* tensor, long size0_, long size1_, long size2_);
THHC_API void THHcTensor_resize4d(THHcState* state, THHcTensor* tensor, long size0_, long size1_, long size2_, long size3_);
THHC_API void THHcTensor_resize5d(THHcState* state, THHcTensor* tensor, long size0_, long size1_, long size2_, long size3_, long size4_);

THHC_API void THHcTensor_set(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_setStorage(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                                    THLongStorage* size_, THLongStorage* stride_);
THHC_API void THHcTensor_setStorage1d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                                      long size0_, long stride0_);
THHC_API void THHcTensor_setStorage2d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                                      long size0_, long stride0_,
                                      long size1_, long stride1_);
THHC_API void THHcTensor_setStorage3d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                                      long size0_, long stride0_,
                                      long size1_, long stride1_,
                                      long size2_, long stride2_);
THHC_API void THHcTensor_setStorage4d(THHcState* state, THHcTensor* self, THHcStorage* storage_, long storageOffset_,
                                      long size0_, long stride0_,
                                      long size1_, long stride1_,
                                      long size2_, long stride2_,
                                      long size3_, long stride3_);

THHC_API void THHcTensor_narrow(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension_, long firstIndex_, long size_);
THHC_API void THHcTensor_select(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension_, long sliceIndex_);
THHC_API void THHcTensor_transpose(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension1_, int dimension2_);
THHC_API void THHcTensor_unfold(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension_, long size_, long step_);

THHC_API void THHcTensor_squeeze(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_squeeze1d(THHcState* state, THHcTensor* self, THHcTensor* src, int dimension_);

THHC_API int THHcTensor_isContiguous(THHcState* state, const THHcTensor* self);
THHC_API int THHcTensor_isSameSizeAs(THHcState* state, const THHcTensor* self, const THHcTensor* src);
THHC_API long THHcTensor_nElement(THHcState* state, const THHcTensor* self);

THHC_API void THHcTensor_retain(THHcState* state, THHcTensor* self);
THHC_API void THHcTensor_free(THHcState* state, THHcTensor* self);
THHC_API void THHcTensor_freeCopyTo(THHcState* state, THHcTensor* self, THHcTensor* dst);

/* Slow access methods [check everything] */
THHC_API void THHcTensor_set1d(THHcState* state, THHcTensor* tensor, long x0, float value);
THHC_API void THHcTensor_set2d(THHcState* state, THHcTensor* tensor, long x0, long x1, float value);
THHC_API void THHcTensor_set3d(THHcState* state, THHcTensor* tensor, long x0, long x1, long x2, float value);
THHC_API void THHcTensor_set4d(THHcState* state, THHcTensor* tensor, long x0, long x1, long x2, long x3, float value);

THHC_API float THHcTensor_get1d(THHcState* state, const THHcTensor* tensor, long x0);
THHC_API float THHcTensor_get2d(THHcState* state, const THHcTensor* tensor, long x0, long x1);
THHC_API float THHcTensor_get3d(THHcState* state, const THHcTensor* tensor, long x0, long x1, long x2);
THHC_API float THHcTensor_get4d(THHcState* state, const THHcTensor* tensor, long x0, long x1, long x2, long x3);

THHC_API int THHcTensor_getDevice(THHcState* state, THHcTensor* self);

#endif

