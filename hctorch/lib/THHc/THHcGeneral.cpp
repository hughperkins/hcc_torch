#include "THHcGeneral.h"
#include "TH.h"
#include "THHcTensorRandom.h"

/* Initialize device count, default device and rand generator */
void THHcInit(THHcState* state) {
  state->deviceState = (THHcDeviceState*)malloc(sizeof(THHcDeviceState));
  THHcDeviceState* device_state = state->deviceState;
  // Get all the available accelerators
  std::vector<hc::accelerator> accs = hc::accelerator::get_all();
  // Logic to get rid of CPU from the list of accelerators
  device_state->deviceCount = accs.size() - 1;
  //Choose the first accelerator as default.
  device_state->currentDeviceID = 1;
  state->rngState = (THHcRNGState*)malloc(sizeof(THHcRNGState));
  // Default device index is 0
  THHcRandom_init(state, device_state->deviceCount, 0);
}

/* Free device state and random generator */
void THHcShutdown(THHcState* state) {
  THHcRandom_shutdown(state);
  free(state->deviceState);
  state->deviceState = NULL;
}

void __THHcCheck(int err, const char* file, const int line) {
  if (err != 0) {
    THError("%s(%i) : Hc runtime error : %s",
            file, line, "");
  }
}

/* Get gridsize based on given size */
void THHcGetGridSize(int* nBlockPerColumn_, int* nBlockPerRow_, int* nThreadPerBlock_, long size) {
  const int nThreadPerBlock = 256;
  long nBlockPerGrid = size / nThreadPerBlock;
  long nBlockPerColumn = 0L;
  long nBlockPerRow = 0L;
  
  if (size % nThreadPerBlock) {
    nBlockPerGrid++;
  }
  
  if (nBlockPerGrid <= 65535) {
    nBlockPerRow = nBlockPerGrid;
    nBlockPerColumn = 1;
  } else if (nBlockPerGrid <= (65355L * 65355L)) {
    unsigned int uiSqrt = (unsigned int)(sqrt((float)nBlockPerGrid));
    nBlockPerRow = uiSqrt;
    nBlockPerColumn = uiSqrt;
    
    while ((nBlockPerRow * nBlockPerColumn) < nBlockPerGrid) {
      nBlockPerRow++;
    }
  } else {
    THError("too large vector for Hc, sorry");
  }
  
  *nBlockPerColumn_ = (int)nBlockPerColumn;
  *nBlockPerRow_ = (int)nBlockPerRow;
  *nThreadPerBlock_ = (int)nThreadPerBlock;
}

hcError_t THHcMalloc(THHcState *state, void **ptr, size_t size) {
  accelerator accl = state->deviceState->get_current_accelerator();
  *ptr = hc::am_alloc(size, accl, 0);   
  if(*ptr == 0)
     return -1;
  else {
     return 0;
  }
}

hcError_t THHcFree(THHcState *state, void *ptr) {
  if(hc::am_free(ptr) == AM_SUCCESS)
    return 0;
  else
    return -1;
}
