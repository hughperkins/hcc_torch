#ifndef THHC_GENERAL_INC
#define THHC_GENERAL_INC

#include "THGeneral.h"
#include "amp.h"
#include "hc.hpp"
#include "hc_am.hpp"
#undef log1p

#ifdef __cplusplus
# define THHC_EXTERNC extern "C"
#else
# define THHC_EXTERNC extern
#endif

#ifdef WIN32
# ifdef THHC_EXPORTS
#  define THHC_API THHC_EXTERNC __declspec(dllexport)
# else
#  define THHC_API THHC_EXTERNC __declspec(dllimport)
# endif
#else
# define THHC_API THHC_EXTERNC
#endif

typedef short int hcError_t;

struct THHcRNGState;  /* Random number generator state. */
struct THHcDeviceState;
/* Global state to be held in hctorch table*/
typedef struct THHcDeviceState {
  long deviceCount; // Total num of accelerator
  int currentDeviceID; // Current active device
  
  // Routine to set the current device ID
  inline void setCurrentDevID(int devID) {
    this->currentDeviceID = devID;
  }
  
  // Routine to return the current device ID
  inline int getCurrentDevID() {
    return this->currentDeviceID;
  }
  
  // get Current accelerator
  inline hc::accelerator get_current_accelerator() {
    hc::accelerator currentAcc = hc::accelerator::get_all()[this->currentDeviceID];
    return currentAcc;
  }
  
  // get Current accelerator
  inline Concurrency::accelerator get_current_amp_accelerator() {
    Concurrency::accelerator currentAcc = Concurrency::accelerator::get_all()[this->currentDeviceID];
    return currentAcc;
  }
  // get current accelerator_view
  inline hc::accelerator_view get_current_accelerator_view() {
    hc::accelerator currentAcc = hc::accelerator::get_all()[this->currentDeviceID];
    return currentAcc.get_default_view();
  }
  
} THHcDeviceState;

/* Global state to be held in hctorch table*/
typedef struct THHcState {
  struct THHcRNGState* rngState;
  struct THHcDeviceState* deviceState;
} THHcState;


THHC_API void THHcInit(THHcState* state);
THHC_API void THHcShutdown(THHcState* state);

#define THHcCheck(err) __THHcCheck(err, __FILE__, __LINE__)

THHC_API void __THHcCheck(int err, const char* file, const int line);

THHC_API void THHcGetGridSize(int* nBlockPerColumn_, int* nBlockPerRow_, int* nThreadPerBlock_, long size);

THHC_API hcError_t THHcMalloc(THHcState *state, void **ptr, size_t size);

THHC_API hcError_t THHcFree(THHcState *state, void *ptr);

#endif

