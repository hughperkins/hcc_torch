#include "THHcFunctorOpns.h"

void fill(THHcState* state, float*& dest, long size, float  val) {
  unsigned grdSz = (size + 256) - (size % 256);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu)) {
    int globalId = tidx.global[0];

    if ( globalId >= size) {
      return;
    }

    dest[globalId] = val;
  }).wait();
}

float InnerProduct_plus_mse(THHcState* state, THHcTensor* input, THHcTensor* target) {
  return inner_product<float>(state, input, target, 0.0, sum<float>(), mse_functor());
}

float InnerProduct_plus_abs(THHcState* state, THHcTensor* input, THHcTensor* target) {
  return inner_product<float>(state, input, target, 0.0, sum<float>(), binary_abs_functor());
}

float InnerProduct_plus_dist(THHcState* state, THHcTensor* self, THHcTensor* src, float value) {
  return inner_product<float>(state, self, src, 0.0, sum<float>(), dist_functor(value));
}

float InnerProduct_plus_kl(THHcState* state, THHcTensor* input, THHcTensor* target) {
  return inner_product<float>(state, input, target, 0.0, sum<float>(), kl_functor());
}

void transform_mse(THHcState* state, THHcTensor* input, THHcTensor* target, THHcTensor* gradInput, float norm) {
  transform(state, input, target, gradInput, mse_updateGradInput_functor(norm));
}

void transform_abs(THHcState* state, THHcTensor* input, THHcTensor* target, THHcTensor* gradInput, float norm) {
  transform(state, input, target, gradInput, abs_updateGradInput_functor(norm));
}

void transform_kl(THHcState* state, THHcTensor* input, THHcTensor* target, THHcTensor* gradInput, float norm) {
  transform(state, input, target, gradInput, kl_updateGradInput_functor(norm));
}

float transform_var_all(THHcState* state, THHcTensor* self, float mean) {
  THHcTensor* diff = THHcTensor_newContiguous(state, self);
  transform(state, self, diff, std::bind2nd(std::minus<float>(), mean));
  float result = inner_product<float>(state, diff, diff, 0.0, sum<float>(), multiply<float>());
  return result;
}

void transform_addvalue(THHcState* state, THHcTensor* src, THHcTensor* self, float value) {
  transform(state, src, self, addvalue_functor(value));
}

void transform_mulvalue(THHcState* state, THHcTensor* src, THHcTensor* self, float value) {
  transform(state, src, self, mulvalue_functor(value));
}

void transform_divvalue(THHcState* state, THHcTensor* src, THHcTensor* self, float value) {
  transform(state, src, self, divvalue_functor(value));
}

void transform_pow(THHcState* state, THHcTensor* src, THHcTensor* self, float value) {
  transform(state, src, self, pow_functor(value));
}

void transform_tpow(THHcState* state, THHcTensor* src, THHcTensor* self, float value) {
  transform(state, src, self, tpow_functor(value));
}
void transform_clamp(THHcState* state, THHcTensor* src, THHcTensor* self, float min_value, float max_value) {
  transform(state, src, self, clamp_functor(min_value, max_value));
}

void transform_log(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, log_functor());
}

void transform_log1p(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, log1p_functor());
}

void transform_exp(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, exp_functor());
}

void transform_cos(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, cos_functor());
}

void transform_acos(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, acos_functor());
}

void transform_cosh(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, cosh_functor());
}

void transform_sin(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, sin_functor());
}

void transform_asin(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, asin_functor());
}

void transform_sinh(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, sinh_functor());
}

void transform_tan(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, tan_functor());
}

void transform_atan(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, atan_functor());
}

void transform_tanh(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, tanh_functor());
}

void transform_sqrt(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, sqrt_functor());
}

void transform_ceil(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, ceil_functor());
}

void transform_floor(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, floor_functor());
}

void transform_abs(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, abs_functor());
}

void transform_round(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, round_functor());
}

void transform_sign(THHcState* state, THHcTensor* src, THHcTensor* self) {
  transform(state, src, self, sign_functor());
}

void transformBinary_multiply(THHcState* state, THHcTensor* src1, THHcTensor* src2, THHcTensor* self) {
  transform(state, src1, src2, self, multiply<float>());
}
void transformBinary_divide(THHcState* state, THHcTensor* src1, THHcTensor* src2, THHcTensor* self) {
  transform(state, src1, src2, self,  divide<float>());
}

void transformBinary_cpow(THHcState* state, THHcTensor* src1, THHcTensor* src2, THHcTensor* self) {
  transform(state, src1, src2, self, cpow_functor());
}

void transformBinary_atan2(THHcState* state, THHcTensor* tx, THHcTensor* ty, THHcTensor* self) {
  transform(state, tx, ty, self, atan2_functor());
}

float Reduce_minimum(THHcState* state, THHcTensor* self) {
  return reduce<float>(state, self,(float)(THInf), minval<float>());
}

float Reduce_maximum(THHcState* state, THHcTensor* self) {
  return reduce<float>(state, self,(float)(-THInf), maxval<float>());
}

float Reduce_plus(THHcState* state, THHcTensor* self) {
  return reduce<float>(state, self,(float)(0.0), sum<float>());
}

float Reduce_multiply(THHcState* state, THHcTensor* self) {
  return reduce<float>(state, self,(float)(1.0), multiply<float>());
}

float InnerPdt(THHcState* state, THHcTensor* self, THHcTensor* src) {
  return 0.0;
}
