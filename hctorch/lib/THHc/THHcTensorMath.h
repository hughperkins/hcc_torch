#ifndef TH_Hc_TENSOR_MATH_INC
#define TH_Hc_TENSOR_MATH_INC

#include "THHcTensor.h"
#include "THHcTensorRandom.h"

THHC_API void THHcTensor_fill(THHcState* state, THHcTensor* self, float value);
THHC_API void THHcTensor_zero(THHcState* state, THHcTensor* self);

THHC_API void THHcTensor_zeros(THHcState* state, THHcTensor* r_, THLongStorage* size);
THHC_API void THHcTensor_ones(THHcState* state, THHcTensor* r_, THLongStorage* size);
THHC_API void THHcTensor_reshape(THHcState* state, THHcTensor* r_, THHcTensor* t, THLongStorage* size);
THHC_API long THHcTensor_numel(THHcState* state, THHcTensor* t);

THHC_API void THHcTensor_add(THHcState* state, THHcTensor* self, THHcTensor* src, float value);
THHC_API void THHcTensor_mul(THHcState* state, THHcTensor* self, THHcTensor* src, float value);
THHC_API void THHcTensor_div(THHcState* state, THHcTensor* self, THHcTensor* src, float value);


THHC_API void THHcTensor_cadd(THHcState* state, THHcTensor* self, THHcTensor* src1, float value, THHcTensor* src2);
THHC_API void THHcTensor_cadd_tst(THHcState* state, THHcTensor* self, THHcTensor* src1, float value, THHcTensor* src2);
THHC_API void THHcTensor_cmul(THHcState* state, THHcTensor* self, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_cdiv(THHcState* state, THHcTensor* self, THHcTensor* src1, THHcTensor* src2);

THHC_API void THHcTensor_addcmul(THHcState* state, THHcTensor* self, THHcTensor* t, float value, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_addcdiv(THHcState* state, THHcTensor* self, THHcTensor* t, float value, THHcTensor* src1, THHcTensor* src2);

THHC_API float THHcTensor_dot(THHcState* state, THHcTensor* self, THHcTensor* src);

THHC_API float THHcTensor_minall(THHcState* state, THHcTensor* self);
THHC_API float THHcTensor_maxall(THHcState* state, THHcTensor* self);
THHC_API float THHcTensor_sumall(THHcState* state, THHcTensor* self);
THHC_API float THHcTensor_prodall(THHcState* state, THHcTensor* self);
THHC_API void THHcTensor_min(THHcState* state, THHcTensor* values, THHcTensor* indices, THHcTensor* src, long dim);
THHC_API void THHcTensor_max(THHcState* state, THHcTensor* values, THHcTensor* indices, THHcTensor* src, long dim);
THHC_API void THHcTensor_sum(THHcState* state, THHcTensor* self, THHcTensor* src, long dim);
THHC_API void THHcTensor_prod(THHcState* state, THHcTensor* self, THHcTensor* src, long dim);

THHC_API void THHcTensor_cumsum(THHcState* state, THHcTensor* self, THHcTensor* src, long dim);
THHC_API void THHcTensor_cumprod(THHcState* state, THHcTensor* self, THHcTensor* src, long dim);

THHC_API void THHcTensor_addmv(THHcState* state, THHcTensor* self, float beta, THHcTensor* t, float alpha, THHcTensor* mat, THHcTensor* vec);
THHC_API void THHcTensor_addmm(THHcState* state, THHcTensor* self, float beta, THHcTensor* t, float alpha, THHcTensor* mat1, THHcTensor* mat2);
THHC_API void THHcTensor_addr(THHcState* state, THHcTensor* self, float beta, THHcTensor* t, float alpha, THHcTensor* vec1, THHcTensor* vec2);
THHC_API void THHcTensor_baddbmm(THHcState *state, THHcTensor *result, float beta, THHcTensor *t,
                                  float alpha, THHcTensor *batch1, THHcTensor *batch2);

THHC_API void THHcTensor_log(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_log1p(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_exp(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_cos(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_acos(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_cosh(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_sin(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_asin(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_sinh(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_tan(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_atan(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_tanh(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_pow(THHcState* state, THHcTensor* self, THHcTensor* src, float value);
void THHcTensor_tpow(THHcState* state, THHcTensor* self_, float value, THHcTensor* src);
void THHcTensor_cpow(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_clamp(THHcState* state, THHcTensor* self, THHcTensor* src, float min_value, float max_value);
THHC_API void THHcTensor_sqrt(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_ceil(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_floor(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_abs(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_sign(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_round(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_atan2(THHcState* state, THHcTensor* r_, THHcTensor* tx, THHcTensor* ty);

THHC_API void THHcTensor_ltValue(THHcState* state, THHcTensor* self_, THHcTensor* src, float value);
THHC_API void THHcTensor_gtValue(THHcState* state, THHcTensor* self_, THHcTensor* src, float value);
THHC_API void THHcTensor_leValue(THHcState* state, THHcTensor* self_, THHcTensor* src, float value);
THHC_API void THHcTensor_geValue(THHcState* state, THHcTensor* self_, THHcTensor* src, float value);
THHC_API void THHcTensor_eqValue(THHcState* state, THHcTensor* self_, THHcTensor* src, float value);
THHC_API void THHcTensor_neValue(THHcState* state, THHcTensor* self_, THHcTensor* src, float value);

THHC_API void THHcTensor_ltTensor(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_gtTensor(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_leTensor(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_geTensor(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_eqTensor(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2);
THHC_API void THHcTensor_neTensor(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2);

THHC_API float THHcTensor_meanall(THHcState* state, THHcTensor* self);
THHC_API void  THHcTensor_mean(THHcState* state, THHcTensor* self, THHcTensor* src, long dim);
THHC_API float THHcTensor_varall(THHcState* state, THHcTensor* self);

void THHcTensor_var(THHcState* state, THHcTensor* self_, THHcTensor* src, long dimension, int flag);
THHC_API float THHcTensor_stdall(THHcState* state, THHcTensor* self);
void THHcTensor_std(THHcState* state, THHcTensor* self_, THHcTensor* src, long dimension, int flag);
THHC_API float THHcTensor_normall(THHcState* state, THHcTensor* self, float value);
THHC_API void  THHcTensor_norm(THHcState* state, THHcTensor* self, THHcTensor* src, float value, long dimension);
THHC_API void  THHcTensor_renorm(THHcState* state, THHcTensor* self, THHcTensor* src, float value, long dimension, float max_norm);
THHC_API float THHcTensor_dist(THHcState* state, THHcTensor* self, THHcTensor* src, float value);

THHC_API void THHcTensor_rand(THHcState* state, THHcTensor* r_, THLongStorage* size);
THHC_API void THHcTensor_randn(THHcState* state, THHcTensor* r_, THLongStorage* size);

THHC_API void THHcTensor_indexCopy(THHcState* state, THHcTensor* res_, int dim, THLongTensor* indices, THHcTensor* src);
THHC_API void THHcTensor_indexFill(THHcState* state, THHcTensor* tensor, int dim, THLongTensor* index, float val);
THHC_API void THHcTensor_indexSelect(THHcState* state, THHcTensor* tensor, THHcTensor* src, int dim, THLongTensor* index);

// API used in torch to invoke matrix vector multiplication
void THHcBlas_gemv(THHcState* state, char TransA, long M, long N, float alpha,
                    float* A, long aOffset,
                    float* X, long xOffset, long incX, float beta,
                    float* Y, long yOffset, long incY,
                    float* temp_buf); 

#endif
