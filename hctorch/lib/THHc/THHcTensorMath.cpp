#include "THHcTensorMath.h"
#include "THHcGeneral.h"
#include "THHcTensorCopy.h"
#include "THHcTensorRandom.h"
#include "hc_math.hpp"
#include <algorithm>
#include <utility>
#include <numeric>
#include "THHcFunctorOpns.h"
#include "hcblaslib.h"
#include "THHcReduce.h"

#define NB_THREADS_PER_BLOCK 256

/* Description: Fill given HcTensor(self_) with the specified float value */
void THHcTensor_fill(THHcState* state, THHcTensor* self_, float value) {
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  THHcStorage_fill(state, self->storage, value);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Fill the given HcTensor(self_) with zero */
void THHcTensor_zero(THHcState* state, THHcTensor* self_) {
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  THHcStorage_fill(state, self->storage, 0);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Resize the given Hctensor(r_) to the size specified and fill it with zero */
void THHcTensor_zeros(THHcState* state, THHcTensor* r_, THLongStorage* size) {
  THHcTensor_resize(state, r_, size, NULL);
  THHcTensor_zero(state, r_);
}

/* Description: Resize the given HcTensor to the size specified and fill it with one */
void THHcTensor_ones(THHcState* state, THHcTensor* r_, THLongStorage* size) {
  THHcTensor_resize(state, r_, size, NULL);
  THHcTensor_fill(state, r_, 1);
}

/* Description: Resize the given HcTensor(r_) to the size specified and and copy the elements of the source tensor (t)*/
void THHcTensor_reshape(THHcState* state, THHcTensor* r_, THHcTensor* t, THLongStorage* size) {
  THHcTensor_resize(state, r_, size, NULL);
  THHcTensor_copy(state, r_, t);
}
/* Description: Returns nember of elements present in the given tensor(t)*/
long THHcTensor_numel(THHcState* state, THHcTensor* t) {
  return THHcTensor_nElement(state, t);
}

/* Description: Add every element of the src HcTensor(src_) with the specified value and store
 *              it in the resultant HcTensor(self_)
 *
 *Parameters:
 *     state: A structure that holds device state information
 *     self_: Output tensor
 *     src_ : Input tensor
 *     value: Scalar value
 *
 * Operation:
 *     self_[i] = src_[i] + value, where i = number of elements of src_
 */
void THHcTensor_add(THHcState* state, THHcTensor* self_, THHcTensor* src_, float value) {
  THHcTensor_resizeAs(state, self_, src_);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  THHcTensor* src = THHcTensor_newContiguous(state, src_);
  transform_addvalue(state, src, self, value);
  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Multiply every element of the src HcTensor(src_) with the specified value and
 *              store it in the resultant HcTensor(self_)
 *
 *Parameters:
 *     state: A structure that holds device state information
 *     self_: Output tensor
 *     src_ : Input tensor
 *     value: Scalar value
 *
 * Operation:
 *     self_[i] = src_[i] * value, where i = number of elements of src_
 */
void THHcTensor_mul(THHcState* state, THHcTensor* self_, THHcTensor* src_, float value) {
  THHcTensor_resizeAs(state, self_, src_);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  THHcTensor* src = THHcTensor_newContiguous(state, src_);
  transform_mulvalue(state, src, self, value);
  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Divide every element of the src HcTensor(src_) with the specified value and
 *              store it in the resultant HcTensor(self_)
 *
 *Parameters:
 *     state: A structure that holds device state information
 *     self_: Output tensor
 *     src_ : Input tensor
 *     value: Scalar value
 *
 * Operation:
 *     self_[i] = src_[i] / value, where i = number of elements of src_
 */
void THHcTensor_div(THHcState* state, THHcTensor* self_, THHcTensor* src_, float value) {
  THHcTensor_resizeAs(state, self_, src_);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  THHcTensor* src = THHcTensor_newContiguous(state, src_);
  transform_divvalue(state, src, self, value);
  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Multiply every element of src2 HcTensor with the specified value
                and add it to corresponding self_ Hctensor by overwriting it.
 *
 * Parameters:
 *     state: A structure that holds device state information
 *     self_: Output HcTensor.
 *     src1: Refence tensor form where the values of self_ are copied.
 *     src2: Source tensor
 *
 * Note : Number of elements in src1 and src2 must be equal
 * Operation:
 *     self_[i] = self_[i] + src2[i] * value, where i = number of elements in src2.
*/
void THHcTensor_cadd(THHcState* state, THHcTensor* self_, THHcTensor* src1, float value, THHcTensor* src2) {
  THHcTensor_resizeAs(state, self_, src1);
  THArgCheck(THHcTensor_nElement(state, src1) == THHcTensor_nElement(state, src2), 3, "size do not match");
  THHcTensor* self = THHcTensor_newContiguous(state, self_);

  if (self_ != src1) {
    src1 = THHcTensor_newContiguous(state, src1);
    THHcTensor_copy(state, self, src1);
    THHcTensor_free(state, src1);
  }
 
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;
  src2 = THHcTensor_newContiguous(state, src2);
  auto avData_src2 = src2->get_device_data(state);
  auto avData_self = self->get_device_data(state);
  hcBlas.hcblas_saxpy(accl_view, self->storage->size, value,
                 avData_src2, 1,
                 avData_self, 1, src2->storageOffset, self->storageOffset);
  THHcTensor_free(state, src2);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Multipy every element of src1 with corresponding element in src2
 *             and store the result in self_, where src1, src2 and self_ are HcTensors.
 *
 * Parameters:
 *     state: A structure that holds device state information
 *     self_: Destination HcTensor
 *     src1, src2: Source HcTensors.
 *
 * Note : Number of elements in src1 and src2 must be equal
 * Operation:
       self_[i] = src1[i] * src2[i], where i = number of elements in src1/src2
*/
void THHcTensor_cmul(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2) {
  THHcTensor_resizeAs(state, self_, src1);
  THArgCheck(THHcTensor_nElement(state, src1) == THHcTensor_nElement(state, src2), 3, "size do not match");
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src1 = THHcTensor_newContiguous(state, src1);
  src2 = THHcTensor_newContiguous(state, src2);
  transformBinary_multiply(state, src1, src2, self);
  THHcTensor_free(state, src1);
  THHcTensor_free(state, src2);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Element-wise divide operation, taking the elements of src1 by elements of src2
 *              and save back results in self_.
 *
 * Parameters:
 *     state: A structure that holds device state information
 *     self_: Destination HcTensor
 *     src1, src2: Source HcTensors.
 *
 * Note : Number of elements in src1 and src2 must be equal
 * Operation:
       self_[i] = src1[i] ^ src2[i], where i = number of elements in src1/src2
*/
void THHcTensor_cpow(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2) {
  THHcTensor_resizeAs(state, self_, src1);
  THArgCheck(THHcTensor_nElement(state, src1) == THHcTensor_nElement(state, src2), 3, "size does not match");
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src1 = THHcTensor_newContiguous(state, src1);
  src2 = THHcTensor_newContiguous(state, src2);
  transformBinary_cpow(state, src1, src2, self);
  THHcTensor_free(state, src1);
  THHcTensor_free(state, src2);
  THHcTensor_freeCopyTo(state, self, self_);
}


/* Description: Divide every element of src1 by corresponding element in src2
*               and save back results in self_.
*
* Parameters:
*     state: A structure that holds device state information
*     self_: Destination HcTensor
*     src1, src2: Source HcTensors.
*
* Note : Number of elements in src1 and src2 must be equal
* Operation:
      self_[i] = src1[i] / src2[i], where i = number of elements in src1/src2
*/
void THHcTensor_cdiv(THHcState* state, THHcTensor* self_, THHcTensor* src1, THHcTensor* src2) {
  THHcTensor_resizeAs(state, self_, src1);
  THArgCheck(THHcTensor_nElement(state, src1) == THHcTensor_nElement(state, src2), 3, "size does not match");
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src1 = THHcTensor_newContiguous(state, src1);
  src2 = THHcTensor_newContiguous(state, src2);
  transformBinary_divide(state, src1, src2, self);
  THHcTensor_free(state, src1);
  THHcTensor_free(state, src2);
  THHcTensor_freeCopyTo(state, self, self_);
}

/*Kenel function for addcmul */
void THHcTensor_kernel_addcmul(THHcState* state, float* &Data,
                                long dataOffset,
                                float value, float*&src1Data,
                                long src1Offset,
                                float*&src2Data,
                                long src2Offset, long size) {
  const int nthreads = 256;
  int sz = size / 8;
  sz = (sz + (nthreads - 1)) & ~(nthreads - 1);
  hc::extent<2> gridExt(8, sz);
  hc::tiled_extent<2> t_ext = gridExt.tile(1, nthreads);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    long k = tidx.tile[0] * gridExt[1] + tidx.global[1];

    if (k < size) {
      Data[dataOffset + k] += value * src1Data[src1Offset + k] * src2Data[src2Offset + k];
    }
  }).wait();
}

/* Description: Performs the element-wise multiplication of src1 by src2,
 *              multiply the result by the scalar value (1 if not present) and add it to self_.
 *
 * Parameters:
 *     state: A structure that holds device state information
 *     value: A scalar value.
 *     self_: Destination HcTensor
 *     src1, src2: Source HcTensors.
 *
 * Note : Number of elements in src1 and src2 must be equal
 * Operation:
       self_[i] += src1[i] * src2[i] * Value, where i = number of elements in src1/src2
 */
void THHcTensor_addcmul(THHcState* state, THHcTensor* self_, THHcTensor* t, float value, THHcTensor* src1, THHcTensor* src2) {
  if (self_ != t) {
    THHcTensor_resizeAs(state, self_, t);
    THHcTensor_copy(state, self_, t);
  }

  THHcTensor_resizeAs(state, self_, src1);
  THArgCheck(THHcTensor_nElement(state, src1) == THHcTensor_nElement(state, src2), 3, "size do not match");
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  src1 = THHcTensor_newContiguous(state, src1);
  src2 = THHcTensor_newContiguous(state, src2);
  auto avData = self->get_device_data(state);
  auto avSrc1 = src1->get_device_data(state);
  auto avSrc2 = src2->get_device_data(state);
  THHcTensor_kernel_addcmul(state, avData, self->storageOffset, value,
                             avSrc1, src1->storageOffset,
                             avSrc2, src2->storageOffset, size);
  THHcTensor_free(state, src1);
  THHcTensor_free(state, src2);
  THHcTensor_freeCopyTo(state, self, self_);
}
/* Kernel Function for adcdiv */
void THHcTensor_kernel_addcdiv(THHcState* state, float* &Data, long dataOffset,
                                float value, float* &src1Data,
                                long src1Offset, float* &src2Data,
                                long src2Offset, long size) {
  const int nthreads = 256;
  int sz = fmax(size / 8, 1);
  sz = (sz + (nthreads - 1)) & ~(nthreads - 1);
  hc::extent<2> gridExt(8, sz);
  hc::tiled_extent<2> t_ext = gridExt.tile(1, nthreads);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    long k = tidx.tile[0] * gridExt[1] + tidx.global[1];

    if(k + dataOffset < size) {
      Data[dataOffset + k] += value *  src1Data[src1Offset + k] / src2Data[src2Offset + k];;
    }
  }).wait();
}

/* Description: Performs the element-wise division of src1 by src2,
 *              multiply the result by the scalar value (1 if not present) and add it to self_.
 *
 * Parameters:
 *     state: A structure that holds device state information
 *     value: A scalar value.
 *     self_: Destination HcTensor
 *     src1, src2: Source HcTensors.
 *
 * Note : Number of elements in src1 and src2 must be equal
 * Operation:
       self_[i] += src1[i] / src2[i] * Value, where i = number of elements in src1/src2
 */
void THHcTensor_addcdiv(THHcState* state, THHcTensor* self_, THHcTensor* t, float value, THHcTensor* src1, THHcTensor* src2) {
  if (self_ != t) {
    THHcTensor_resizeAs(state, self_, t);
    THHcTensor_copy(state, self_, t);
  }

  THHcTensor_resizeAs(state, self_, src1);
  THArgCheck(THHcTensor_nElement(state, src1) == THHcTensor_nElement(state, src2), 3, "size does not match");
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  long size = THHcTensor_nElement(state, self);
  src1 = THHcTensor_newContiguous(state, src1);
  src2 = THHcTensor_newContiguous(state, src2);
  auto avData = self->get_device_data(state);
  auto avSrc1 = src1->get_device_data(state);
  auto avSrc2 = src2->get_device_data(state);
  // TODO: need to figure out inaccuracy of native divide in OpenCL
  THHcTensor_kernel_addcdiv(state, avData, self->storageOffset, value,
                             avSrc1, src1->storageOffset,
                             avSrc2, src2->storageOffset, size);
  THHcTensor_free(state, src1);
  THHcTensor_free(state, src2);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* Description: Returns the output of dot product between self and src1.
 *
 * Parameters:
 *     state: A structure that holds device state information
 *     src1, self: Source HcTensors.
 *
 * Note : Number of elements in src1 and self must be equal
 * Operation:
       output += self[i] * src1[i]  where i = number of elements in src1/self
 */
float THHcTensor_dot(THHcState* state, THHcTensor* self, THHcTensor* src) {
  THArgCheck(THHcTensor_nElement(state, self) == THHcTensor_nElement(state, src), 2, "size do not match");
  self = THHcTensor_newContiguous(state, self);
  src = THHcTensor_newContiguous(state, src);
  float result = InnerPdt(state, self, src);
  THHcTensor_free(state, src);
  THHcTensor_free(state, self);
  return result;
}

/* Description: Returns the minimum value in HcTensor self */
float THHcTensor_minall(THHcState* state, THHcTensor* self) {
  self = THHcTensor_newContiguous(state, self);
  float result = Reduce_minimum(state, self);
  THHcTensor_free(state, self);
  return result;
}

/* Description: Returns the maximum value in HcTensor self */
float THHcTensor_maxall(THHcState* state, THHcTensor* self) {
  self = THHcTensor_newContiguous(state, self);
  float result = Reduce_maximum(state, self);
  THHcTensor_free(state, self);
  return result;
}

/* Description: Returns the sum of all elements in HcTensor self */
float THHcTensor_sumall(THHcState* state, THHcTensor* self) {
  self = THHcTensor_newContiguous(state, self);
  float result = Reduce_plus(state, self);
  THHcTensor_free(state, self);
  return result;
}

/* Description: Returns the product of all elements in HcTensor self */
float THHcTensor_prodall(THHcState* state, THHcTensor* self) {
  self = THHcTensor_newContiguous(state, self);
  float result = Reduce_multiply(state, self);
  THHcTensor_free(state, self);
  return result;
}

struct dim4 {
  unsigned arr[4];

  dim4(unsigned init = 0) {
    for (unsigned i = 0; i < 4; i++) {
      arr[i] = init;
    }
  }

  unsigned& operator[](const unsigned& idx) {
    return arr[idx];
  }
};

void THHcTensor_sum(THHcState* state, THHcTensor* self, THHcTensor* src, long dimension) {
  return THHcTensor_reduceDim(state, self, src, dimension, 0.0f, sum<float>());
}

void THHcTensor_prod(THHcState* state, THHcTensor* self, THHcTensor* src, long dimension) {
  return THHcTensor_reduceDim(state, self, src, dimension, 1.0f, multiplies<float>());
}

/* Perform an inclusive scan along an outer dimension of a tensor.
*
* - num_orows is the size of the flattened outer dimensions;
* - num_irows is the size of the flattened inner dimensions;
* - row_size is the size of the dimension along which to compute the variance;
*
* The dimensions to the outside and inside of the specified dimension are considered as flattened.
* Thread blocks with the same blockIdx.y process an "outer row" (i.e. an element of the flattened
* outer dimensions, which contains several "inner rows").
* Each thread processes a single inner row at a time.
*/
template <class BinaryOp>
void THHcTensor_kernel_scanOuterDim(THHcState* state, float* &avTgt, float* &avSrc,
                                     unsigned num_orows, unsigned num_irows, unsigned row_size,
                                     float init, BinaryOp binary_op, unsigned int gridConf[]) {
  gridConf[0] = (gridConf[0] + 255) & ~255;
  hc::extent<2> grdExt(gridConf[1], gridConf[0]);
  hc::tiled_extent<2> t_ext = grdExt.tile(1, 256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    for (unsigned orow = tidx.tile[1]; orow < num_orows; orow += grdExt[1] / tidx.tile_dim[1]) {
      for (unsigned irow = tidx.tile[0] * tidx.tile_dim[1] + tidx.local[1]; irow < num_irows; irow += grdExt[0] / tidx.tile_dim[0] * tidx.tile_dim[1]) {
        long index = orow * row_size * num_irows + irow;
        float acc = init;

        for (unsigned col = 0; col < row_size; ++col) {
          acc = binary_op(acc, avSrc[index]);
          avTgt[index] = acc;
          index += num_irows;
        }
      }
    }
  }).wait();
}

template <class BinaryOp>
void THHcTensor_scanOuterDim(THHcState* state, THHcTensor* tgt, THHcTensor* src, long dimension,
                              float init, BinaryOp binary_op) {
  unsigned ndim = THHcTensor_nDimension(state, src);
  // Treat all outer dimensions (i.e. dim < dimension) as one.
  unsigned num_orows = 1;

  for (unsigned dim = 0; dim < dimension; dim++) {
    num_orows *= THHcTensor_size(state, src, dim);
  }

  unsigned row_size = THHcTensor_size(state, src, dimension);
  // Treat all inner dimensions (i.e. dim > dimension) as one.
  unsigned num_irows = 1;

  for (unsigned dim = dimension + 1; dim < ndim; dim++) {
    num_irows *= THHcTensor_size(state, src, dim);
  }

  unsigned int threads = hc::fast_math::fmin(512, num_irows);
  unsigned int maxGridDim = 1024;
  unsigned int gridConfig[2];
  gridConfig[0] = hc::fast_math::fmin(maxGridDim, num_orows);
  gridConfig[1] = hc::fast_math::fmin(maxGridDim, ((num_irows + threads - 1) / threads));
  auto avTgt = tgt->get_device_data(state);
  auto avSrc = src->get_device_data(state);
  THHcTensor_kernel_scanOuterDim(state, avTgt, avSrc, num_orows, num_irows, row_size, init, binary_op, gridConfig);
}

/* Perform an inclusive scan along the innermost dimension of a tensor.
*
* - num_rows is the size of the flattened outer dimensions;
* - row_size is the size of the innermost dimension;
*
* The outer dimensions of the tensor are considered as a single dimension, i.e. the tensor is
* considered as having 'num_rows' rows of size 'row_size'.
* Each thread block processes one or more sets of contiguous rows (processing multiple rows
* per thread block is quicker than processing a single row, especially for short rows).
*/
template <int num_threads_x, int num_threads_y, class BinaryFunction>
void THHcTensor_kernel_scanInnermostDim(THHcState* state, float* &avTgt, float* &avSrc,
    unsigned num_rows, unsigned row_size, float init, BinaryFunction binary_op) {
  unsigned int grid = hc::fast_math::fmin(256, ((num_rows + 32 - 1) / 32));
  hc::extent<2> grdExt(1 * 32, grid * 8);
  hc::tiled_extent<2> t_ext = grdExt.tile(32, 8);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    tile_static float sbuf[num_threads_y][2 * num_threads_x];
    float* row_buf = sbuf[tidx.local[0]];

    for (unsigned block_row = tidx.tile[1] * tidx.tile_dim[0]; block_row < num_rows; block_row += tidx.tile_dim[0] * (grdExt[1] / tidx.tile_dim[1])) {
      unsigned row = block_row + tidx.local[0];
      float block_total = init;
      long row_src = row * row_size;
      long row_tgt = row * row_size;

      // Perform scan on one block at a time, keeping track of the total value of
      // all blocks processed so far.
      for (unsigned block_col = 0; block_col < row_size; block_col += 2 * num_threads_x) {
        // Load data into shared memory (two values per thread).
        unsigned col1 = block_col + tidx.local[1];
        unsigned col2 = block_col + num_threads_x + tidx.local[1];

        if (row < num_rows) {
          if (col1 < row_size) {
            row_buf[tidx.local[1]] = avSrc[row_src + col1];
          } else {
            row_buf[tidx.local[1]] = init;
          }

          if (col2 < row_size) {
            row_buf[num_threads_x + tidx.local[1]] = avSrc[row_src + col2];
          } else {
            row_buf[num_threads_x + tidx.local[1]] = init;
          }

          // Add the total value of all previous blocks to the first value of this block.
          if (tidx.local[1] == 0) {
            row_buf[0] = binary_op(row_buf[0], block_total);
          }
        }

        tidx.barrier.wait();

        // Parallel reduction (up-sweep).
        for (unsigned s = num_threads_x, d = 1; s >= 1; s >>= 1, d <<= 1) {
          if (row < num_rows && tidx.local[1] < s) {
            unsigned offset = (2 * tidx.local[1] + 1) * d - 1;
            row_buf[offset + d] = binary_op(row_buf[offset], row_buf[offset + d]);
          }

          tidx.barrier.wait();
        }

        // Down-sweep.
        for (unsigned s = 2, d = num_threads_x / 2; d >= 1; s <<= 1, d >>= 1) {
          if (row < num_rows && tidx.local[1] < s - 1) {
            unsigned offset = 2 * (tidx.local[1] + 1) * d - 1;
            row_buf[offset + d] = binary_op(row_buf[offset], row_buf[offset + d]);
          }

          tidx.barrier.wait();
        }

        // Write back to output.
        if (row < num_rows) {
          if (col1 < row_size) {
            avTgt[row_tgt + col1] = row_buf[tidx.local[1]];
          }

          if (col2 < row_size) {
            avTgt[row_tgt + col2] = row_buf[num_threads_x + tidx.local[1]];
          }
        }

        block_total = row_buf[2 * num_threads_x - 1];
        tidx.barrier.wait();
      }
    }
  }).wait();
}

template <class BinaryFunction>
void THHcTensor_scanInnermostDim(THHcState* state, THHcTensor* tgt, THHcTensor* src, float init, BinaryFunction binary_op) {
  unsigned ndim = THHcTensor_nDimension(state, src);
  // Treat all outer dimensions as a single dimension.
  unsigned num_rows = 1;

  for (unsigned dim = 0; dim < ndim - 1; dim++) {
    num_rows *= THHcTensor_size(state, src, dim);
  }

  unsigned row_size = THHcTensor_size(state, src, ndim - 1);
  auto avTgt = tgt->get_device_data(state);
  auto avSrc = src->get_device_data(state);
  THHcTensor_kernel_scanInnermostDim<8, 32>(state, avTgt, avSrc, num_rows, row_size, init, binary_op);
}

template <class BinaryFunction>
void THHcTensor_scanDim(THHcState* state, THHcTensor* self_, THHcTensor* src, long dimension, float init, BinaryFunction binary_op) {
  THHcTensor_resizeAs(state, self_, src);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);

  if (dimension == THHcTensor_nDimension(state, src) - 1) {
    THHcTensor_scanInnermostDim(state, self, src, init, binary_op);
  } else {
    THHcTensor_scanOuterDim(state, self, src, dimension, init, binary_op);
  }

  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

/* cumsum:
 * This routine returns the cumulative sum of the elements of src along specified dimension.
 */
void THHcTensor_cumsum(THHcState* state, THHcTensor* self, THHcTensor* src, long dimension) {
  return THHcTensor_scanDim(state, self, src, dimension, 0.0f, sum<float>());
}


/* cumprod:
 * This routine returns the cumulative product of the elements of src along specified dimension.
 */
void THHcTensor_cumprod(THHcState* state, THHcTensor* self, THHcTensor* src, long dimension) {
  return THHcTensor_scanDim(state, self, src, dimension, 1.0f, multiplies<float>());
}

// Make_Pair definition
pair_t<float, float> Make_Pair(float src, float col) __attribute__((hc, cpu)) {
  return (pair_t<float, float>(src, col));
}

void THHcTensor_max(THHcState* state, THHcTensor* values, THHcTensor* indices, THHcTensor* src, long dimension) {
  const float minfloat32 = -3.402823466e+38f;
  return THHcTensor_reduceDim(state, values, src, dimension, minfloat32, maxval<float>());
}

void THHcTensor_min(THHcState* state, THHcTensor* values, THHcTensor* indices, THHcTensor* src, long dimension) {
  const float maxfloat32 = 3.402823466e+38f;
  return THHcTensor_reduceDim(state, values, src, dimension, maxfloat32, minval<float>());
}

// Gemv kernel
// Matrix Vector Multiplication where the Matrix A is transposed
static void gemv_TransA(THHcState* state, float* A_mat, int aOffset,
                        float* X_vec, long xOffset,
                        float* Y_vec, long yOffset,
                        float alpha, float beta, int lenX, int lenY,
                        float* tempBuf) {
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  
  // Case where Y vector's length (lenY) is very small compared to lenX
  // TO DO: Need to represent this case in a better way
  // The parameter tempBuf is used in this case to make a global sychronization across threadblocks
  if ((lenX - lenY) > 5000) {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    int len_X = (lenX + (BLOCK_SIZE - 1)) & ~(BLOCK_SIZE - 1);
    int num_blocks = len_X / BLOCK_SIZE;
    hc::extent<1> grdExt(len_X);
    hc::tiled_extent<1> t_ext = grdExt.tile(BLOCK_SIZE);
    hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
      tile_static float t[BLOCK_SIZE];
      
      for (int Col = 0; Col < lenY; Col++) {
        int blockIdx = tidx.tile[0];
        int threadIdx = tidx.local[0];
        tempBuf[Col * num_blocks + blockIdx] = 0;
        t[threadIdx] = 0;
        
        if (Col < lenY && blockIdx * BLOCK_SIZE + threadIdx < lenX) {
          t[threadIdx] = X_vec[xOffset + blockIdx * BLOCK_SIZE + threadIdx] * A_mat[aOffset + Col * lenX + blockIdx * BLOCK_SIZE + threadIdx];
        }
        
        tidx.barrier.wait();
        
        for (int stride = BLOCK_SIZE / 2; stride >= 1; stride /= 2) {
          if (threadIdx < stride) {
            t[threadIdx] += t[threadIdx + stride];
          }
        }
        
        tempBuf[Col * num_blocks + blockIdx] = t[0];
        tidx.barrier.wait();
      }
      
      if (tidx.tile[0] == 0) {
        for (int Col = 0; Col < lenY; Col++) {
          tile_static float sh[BLOCK_SIZE];
          int threadId = tidx.local[0];
          sh[tidx.local[0]] = 0;
          
          for (int i = threadId; i < num_blocks; i += tidx.tile_dim[0]) {
            sh[threadId] += tempBuf[Col * num_blocks + i];
          }
          
          tidx.barrier.wait();
          
          for (int stride = BLOCK_SIZE / 2; stride >= 1; stride /= 2) {
            if (threadId < stride) {
              sh[threadId] += sh[threadId + stride];
            }
          }
          
          tidx.barrier.wait();
          Y_vec[yOffset + Col] = (isnan(Y_vec[yOffset + Col]) ||
                                  isinf(Y_vec[yOffset + Col])) ? 0 :
                                 Y_vec[yOffset + Col];
          Y_vec[yOffset + Col] *= beta;
          Y_vec[yOffset + Col] += alpha * sh[0];
        }
      }
    }).wait();
  } else {
    // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
    hc::extent<1> grdExt(lenY * BLOCK_SIZE);
    hc::tiled_extent<1> t_ext = grdExt.tile(BLOCK_SIZE);
    hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
      int threadIdx = tidx.local[0];
      int blockIdx = tidx.tile[0];
      int Col = blockIdx;
      tile_static float sh[BLOCK_SIZE];
      sh[threadIdx] = 0;
      
      for (int tileId = 0; tileId < ((lenX + BLOCK_SIZE - 1) & ~(BLOCK_SIZE - 1)) / BLOCK_SIZE; tileId++) {
        if (tileId * BLOCK_SIZE + threadIdx < lenX && Col < lenY) {
          sh[threadIdx] += X_vec[xOffset + tileId * BLOCK_SIZE + threadIdx] * A_mat[aOffset + Col * lenX + tileId * BLOCK_SIZE + threadIdx];
        }
      }
      
      tidx.barrier.wait();
      
      for (int stride = BLOCK_SIZE / 2; stride >= 1; stride /= 2) {
        if (threadIdx < stride) {
          sh[threadIdx] += sh[threadIdx + stride];
        }
        
        tidx.barrier.wait();
      }
      
      if (threadIdx == 0 && Col < lenY) {
        Y_vec[yOffset + Col] = (isnan(Y_vec[yOffset + Col]) ||
                                isinf(Y_vec[yOffset + Col])) ? 0 :
                               Y_vec[yOffset + Col];
        Y_vec[yOffset + Col] *= beta;
        Y_vec[yOffset + Col] += alpha * sh[0];
      }
    }).wait();
  }
}

static void gemv_NoTransA(THHcState* state, float* A, long aOffset,
                          float* X, long xOffset,
                          float* Y, long yOffset,
                          float alpha, float beta, int lenX, int lenY) {
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  // Make grid size in each dimension an exact multiple of threadblock size in the corresponding dimension
  long size = (lenY + 255) & ~255;
  hc::extent<1> compute_domain(size);
  hc::parallel_for_each(accl_view, compute_domain.tile(BLOCK_SIZE), [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    int bx = tidx.tile[0];
    int tx = tidx.local[0];
    tile_static float Xds[BLOCK_SIZE];
    int Col = bx * BLOCK_SIZE + tx;
    float Pvalue = 0;
    
    for (int m = 0; m < (lenX - 1) / BLOCK_SIZE + 1; ++m) {
      if (m * BLOCK_SIZE + tx < lenX) {
        Xds[tx] = X[xOffset + m * BLOCK_SIZE + tx];
      } else {
        Xds[tx] = 0;
      }
      
      tidx.barrier.wait();
      
      for (int k = 0; k < BLOCK_SIZE; k++)
        if (Col < lenY && m * BLOCK_SIZE + k < lenX) {
          Pvalue += Xds[k] * A[aOffset + Col + (m * BLOCK_SIZE + k) * lenY];
        }
        
      tidx.barrier.wait();
    }
    
    if (Col < lenY) {
      Y[yOffset + Col] = (isnan(Y[yOffset + Col]) ||
                          isinf(Y[yOffset + Col])) ? 0 :
                         Y[yOffset + Col];
      Y[yOffset + Col] *= beta;
      Y[yOffset + Col] += alpha * Pvalue;
    }
  }).wait();
}

// API used in torch to invoke matrix vector multiplication
void THHcBlas_gemv(THHcState* state, char TransA, long M, long N, float alpha,
                    float* A, long aOffset,
                    float* X, long xOffset, long incX, float beta,
                    float* Y, long yOffset, long incY,
                    float* temp_buf) {
  if (alpha == 0.0) {
    return;
  }
  
  int lenX, lenY;
  
  if (M == 0 || N == 0) {
    return;
  }
  
  if (alpha == 0.0 && beta == 1.0) {
    return;
  }
  
  if (TransA == 'n') {
    lenX = N;
    lenY = M;
  } else {
    lenX = M;
    lenY = N;
  }
  
  if (TransA == 't') {
    gemv_TransA(state, A, aOffset, X, xOffset, Y, yOffset, alpha, beta, lenX, lenY, temp_buf);
  } else if (TransA == 'n') {
    gemv_NoTransA(state, A, aOffset, X, xOffset, Y, yOffset, alpha, beta, lenX, lenY);
  }
}

/* addmv
 * Performs a matrix-vector multiplication between mat (2D tensor) and vec (1D tensor) and provde result it to r_.
 * alpha and beta are scalars used for scaling purpose
 */
void THHcTensor_addmv(THHcState* state, THHcTensor* r_, float beta, THHcTensor* t, float alpha, THHcTensor* mat, THHcTensor* vec) {
  if ((mat->nDimension != 2) || (vec->nDimension != 1)) {
    THError("matrix and vector expected");
  }

  if (mat->size[1] != vec->size[0]) {
    THError("size mismatch");
  }

  if (t->nDimension != 1) {
    THError("size mismatch");
  }

  if (t->size[0] != mat->size[0]) {
    THError("size mismatch");
  }

  if (r_ != t) {
    THHcTensor_resizeAs(state, r_, t);
    THHcTensor_copy(state, r_, t);
  }

  auto avR_ = r_->get_device_data(state);
  auto avVec = vec->get_device_data(state);
  int lenX;
  int lenY;

  if (mat->stride[0] == 1) {
    lenX = mat->size[0];
    lenY = mat->size[1];
  } else {
    lenX = mat->size[1];
    lenY = mat->size[0];
  }

  int len_X = (lenX + 255) & ~255;
  int numBlocks = len_X / 256;
  float* tempBuf;
  THHcCheck(THHcMalloc(state, (void**) &tempBuf, numBlocks * lenY * sizeof(float)));
  hc::extent<1> ext(numBlocks * lenY);

  if (mat->stride[0] == 1) {
    auto avMat = mat->get_device_data(state);
    THHcBlas_gemv(state, 'n', mat->size[0], mat->size[1], alpha,
                   avMat, mat->storageOffset + 0,
                   avVec, vec->storageOffset, vec->stride[0], beta,
                   avR_, r_->storageOffset, r_->stride[0], tempBuf);
  } else if (mat->stride[1] == 1) {
    auto avMat = mat->get_device_data(state);
    THHcBlas_gemv(state, 't', mat->size[1], mat->size[0], alpha,
                   avMat, mat->storageOffset + 0,
                   avVec, vec->storageOffset, vec->stride[0], beta,
                   avR_, r_->storageOffset, r_->stride[0], tempBuf);
  } else {
    THHcTensor* cmat = THHcTensor_newContiguous(state, mat);
    auto avCMat = cmat->get_device_data(state);
    THHcBlas_gemv(state, 't', mat->size[1], mat->size[0], alpha,
                   avCMat, cmat->storageOffset + 0,
                   avVec, vec->storageOffset, vec->stride[0], beta,
                   avR_, r_->storageOffset, r_->stride[0], tempBuf);
    THHcTensor_free(state, cmat);
  }
  THHcFree(state, tempBuf);
}

/* Description : Performs a matrix-matrix multiplication between m1 (2D tensor)
 *               and m2 (2D tensor) and multiply it with scalar alpha.
 *               Optional value beta is a scalar that scales the result tensor,
 *               before accumulating the result into the tensor. Defaults to 1.0.
 *
 * In other words,
 *
 * res = res * beta + alpha * m1*m2
 */
void THHcTensor_addmm(THHcState* state, THHcTensor* r_, float beta, THHcTensor* t, float alpha, THHcTensor* m1, THHcTensor* m2) {
  char transpose_r, transpose_m1, transpose_m2;
  THHcTensor* r__, *m1_, *m2_;

  if ((m1->nDimension != 2) || (m2->nDimension != 2)) {
    THError("matrix and matrix expected");
  }

  if (t->nDimension != 2) {
    THError("size mismatch");
  }

  if ((t->size[0] != m1->size[0]) || (t->size[1] != m2->size[1]) || (m1->size[1] != m2->size[0])) {
    THError("size mismatch");
  }

  if (t != r_) {
    THHcTensor_resizeAs(state, r_, t);
    THHcTensor_copy(state, r_, t);
  }

  /* r_ */
  if (r_->stride[0] == 1) {
    transpose_r = 'n';
    r__ = r_;
  } else if (r_->stride[1] == 1) {
    THHcTensor* swap = m2;
    m2 = m1;
    m1 = swap;
    transpose_r = 't';
    r__ = r_;
  } else {
    transpose_r = 'n';
    r__ = THHcTensor_newWithSize2d(state, r_->size[1], r_->size[0]);
    THHcTensor_copy(state, r__, r_);
    THHcTensor_transpose(state, r__, NULL, 0, 1);
  }

  /* m1 */
  if (m1->stride[(transpose_r == 'n' ? 0 : 1)] == 1) {
    transpose_m1 = 'n';
    m1_ = m1;
  } else if (m1->stride[(transpose_r == 'n' ? 1 : 0)] == 1) {
    transpose_m1 = 't';
    m1_ = m1;
  } else {
    transpose_m1 = (transpose_r == 'n' ? 't' : 'n');
    m1_ = THHcTensor_newContiguous(state, m1);
  }

  /* m2 */
  if (m2->stride[(transpose_r == 'n' ? 0 : 1)] == 1) {
    transpose_m2 = 'n';
    m2_ = m2;
  } else if (m2->stride[(transpose_r == 'n' ? 1 : 0)] == 1) {
    transpose_m2 = 't';
    m2_ = m2;
  } else {
    transpose_m2 = (transpose_r == 'n' ? 't' : 'n');
    m2_ = THHcTensor_newContiguous(state, m2);
  }

  auto avM1Mat = m1_->get_device_data(state);
  auto avM2Mat = m2_->get_device_data(state);
  auto avRMat = r_->get_device_data(state);
  /* do the operation */
  int n = r__->size[(transpose_r == 'n' ? 0 : 1)];
  int m = r__->size[(transpose_r == 'n' ? 1 : 0)];
  int k = m1_->size[(transpose_r == 'n' ? 1 : 0)];
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;
  hcBlas.hcblas_sgemm(accl_view, ColMajor,
                      transpose_m1 == 'n' ? NoTrans : Trans,
                      transpose_m2 == 'n' ? NoTrans : Trans,
                      n,
                      m,
                      k,
                      alpha,
                      avM1Mat,
                      (transpose_m1 == 'n' ? m1_->stride[(transpose_r == 'n' ? 1 : 0)] : m1_->stride[(transpose_r == 'n' ? 0 : 1)]),
                      avM2Mat,
                      (transpose_m2 == 'n' ? m2_->stride[(transpose_r == 'n' ? 1 : 0)] : m2_->stride[(transpose_r == 'n' ? 0 : 1)]),
                      beta,
                      avRMat,
                      r__->stride[(transpose_r == 'n' ? 1 : 0)],  m1_->storageOffset,  m2_->storageOffset,  r_->storageOffset);

  /* free intermediate variables */
  if (m1_ != m1) {
    THHcTensor_free(state, m1_);
  }

  if (m2_ != m2) {
    THHcTensor_free(state, m2_);
  }

  if (r__ != r_) {
    THHcTensor_freeCopyTo(state, r__, r_);
  }
}

/* Description: Performs the outer-product between vec1 (1D tensor) and vec2 (1D tensor).
 *              Optional values alpha and beta are scalars that get multipled with result
 *              and product of vectors respectively.
 *
 * In other words,
 *
 * res[i, j] = beta * res[i, j] + alpha * vec1[i] * vec2[j]
 *
 * If vec1 is a vector of size n and vec2 is a vector of size m, then mat must be a matrix of size n x m.
 */
void THHcTensor_addr(THHcState* state, THHcTensor* r_, float beta, THHcTensor* t, float alpha, THHcTensor* vec1, THHcTensor* vec2) {
  if ((vec1->nDimension != 1) || (vec2->nDimension != 1)) {
    THError("vector and vector expected");
  }

  if (t->nDimension != 2) {
    THError("size mismatch");
  }

  if ((t->size[0] != vec1->size[0]) || (t->size[1] != vec2->size[0])) {
    THError("size mismatch");
  }

  if (r_ != t) {
    THHcTensor_resizeAs(state, r_, t);
    THHcTensor_copy(state, r_, t);
  }

  if (beta != 1) {
    THHcTensor_mul(state, r_, r_, beta);
  }

  auto avData_vec1 = vec1->get_device_data(state);
  auto avData_vec2 = vec2->get_device_data(state);
  auto avData_r_ = r_->get_device_data(state);

  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;

  if (r_->stride[0] == 1) {
    hcBlas.hcblas_sger(accl_view, ColMajor, vec1->size[0], vec2->size[0], alpha,
                  avData_vec1, vec1->storageOffset, vec1->stride[0],
                  avData_vec2, vec2->storageOffset, vec2->stride[0],
                  avData_r_, r_->storageOffset, r_->stride[1]);
  } else if (r_->stride[1] == 1) {
    hcBlas.hcblas_sger(accl_view, ColMajor, vec2->size[0], vec1->size[0], alpha,
                  avData_vec2, vec2->storageOffset, vec2->stride[0],
                  avData_vec1, vec1->storageOffset, vec1->stride[0],
                  avData_r_, r_->storageOffset, r_->stride[0]);
  } else {
    THHcTensor* cr = THHcTensor_newClone(state, r_);
    auto avData_cr = cr->get_device_data(state);
    hcBlas.hcblas_sger(accl_view, ColMajor,  vec2->size[0], vec1->size[0], alpha,
                  avData_vec2, vec2->storageOffset, vec2->stride[0],
                  avData_vec1, vec1->storageOffset, vec1->stride[0],
                  avData_cr, cr->storageOffset, cr->stride[0]);
    THHcTensor_freeCopyTo(state, cr, r_);
  }
}

void THHcTensor_baddbmm(THHcState* state, THHcTensor* result, float beta, THHcTensor* t,
                         float alpha, THHcTensor* batch1, THHcTensor* batch2) {
  // TODO: Implementation
}

#define IMPLEMENT_HC_TENSOR_BASIC_FUNC(NAME, CFUNC)                             \
void THHcTensor_##NAME(THHcState *state, THHcTensor *self_, THHcTensor *src) { \
  THHcTensor_resizeAs(state, self_, src);                                       \
  THHcTensor *self = THHcTensor_newContiguous(state, self_);                   \
  src = THHcTensor_newContiguous(state, src);                                   \
  transform_##NAME(state, src, self);                                        \
                                                                                 \
  THHcTensor_free(state, src);                                                  \
  THHcTensor_freeCopyTo(state, self, self_);                                    \
}

IMPLEMENT_HC_TENSOR_BASIC_FUNC(log, hc::fast_math::log)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(log1p, hc::precise_math::log1p)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(exp, hc::fast_math::exp)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(cos, hc::fast_math::cos)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(acos, hc::fast_math::acos)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(cosh, hc::fast_math::cosh)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(sin, hc::fast_math::sin)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(asin, hc::fast_math::asin)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(sinh, hc::fast_math::sinh)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(tan, hc::fast_math::tan)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(atan, hc::fast_math::atan)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(tanh, hc::fast_math::tanh)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(sqrt, hc::fast_math::sqrt)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(ceil, hc::fast_math::ceil)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(floor, hc::fast_math::floor)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(abs, hc::fast_math::fabs)
IMPLEMENT_HC_TENSOR_BASIC_FUNC(round, hc::fast_math::roundf)

void THHcTensor_pow(THHcState* state, THHcTensor* self_, THHcTensor* src, float value) {
  THHcTensor_resizeAs(state, self_, src);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);
  transform_pow(state, src, self, value);
  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

void THHcTensor_tpow(THHcState* state, THHcTensor* self_, float value, THHcTensor* src) {
  THHcTensor_resizeAs(state, self_, src);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);
  transform_tpow(state, src, self, value);
  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}


void THHcTensor_atan2(THHcState* state, THHcTensor* self_, THHcTensor* tx, THHcTensor* ty) {
  THHcTensor_resizeAs(state, self_, tx);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  tx = THHcTensor_newContiguous(state, tx);
  ty = THHcTensor_newContiguous(state, ty);
  transformBinary_atan2(state, tx, ty, self);
  THHcTensor_free(state, tx);
  THHcTensor_free(state, ty);
  THHcTensor_freeCopyTo(state, self, self_);
}

void THHcTensor_clamp(THHcState* state, THHcTensor* self_, THHcTensor* src, float min_value,
                       float max_value) {
  THArgCheck(THHcTensor_nElement(state, self_) == THHcTensor_nElement(state, src), 2, "sizes do not match");
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);
  transform_clamp(state, src, self, min_value, max_value);
  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

void THHcTensor_sign(THHcState* state, THHcTensor* self_, THHcTensor* src) {
  THHcTensor_resizeAs(state, self_, src);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);
  transform_sign(state, src, self);
  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

float THHcTensor_meanall(THHcState* state, THHcTensor* self) {
  THArgCheck(self->nDimension > 0, 1, "empty Tensor");
  return THHcTensor_sumall(state, self) / THHcTensor_nElement(state, self);
}

void THHcTensor_mean(THHcState* state, THHcTensor* self, THHcTensor* src, long dim) {
  THHcTensor_sum(state, self, src, dim);
  THHcTensor_div(state, self, self, THHcTensor_size(state, src, dim));
}

float THHcTensor_varall(THHcState* state, THHcTensor* self) {
  self = THHcTensor_newContiguous(state, self);
  float mean = THHcTensor_meanall(state, self);
  float result = transform_var_all(state, self, mean);
  result = result / (THHcTensor_nElement(state, self) - 1);
  THHcTensor_free(state, self);
  return result;
  return 0;
}

float THHcTensor_stdall(THHcState* state, THHcTensor* self) {
  return std::sqrt(THHcTensor_varall(state, self));
}

/* Compute the variance (or standard deviation) along an outer dimension of a tensor.
 *
 * - num_orows is the size of the flattened outer dimensions;
 * - num_irows is the size of the flattened inner dimensions;
 * - row_size is the size of the dimension along which to compute the variance;
 * - if flag is set, normalize by `row_size` instead of `row_size - 1`
 * - if apply_sqrt is set, compute the standard deviation instead of variance
 *
 * The dimensions to the outside and inside of the specified dimension are considered as flattened.
 * Thread blocks with the same blockIdx.y process an "outer row" (i.e. an element of the flattened
 * outer dimensions, which contains several "inner rows").
 * Each thread processes a single inner row at a time.
 */

template <bool flag, bool apply_sqrt>
void THHcTensor_kernel_varOuterDim(THHcState* state, float* &avTgt,
                                    float* &avSrc, unsigned num_orows,
                                    unsigned num_irows, unsigned row_size) {
  hc::extent<2> grdExt(num_orows, num_irows * 256);
  hc::tiled_extent<2> t_ext = grdExt.tile(1, 256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    unsigned orow = tidx.tile[0];

    if (orow < num_orows) {
      unsigned irow = tidx.global[1];

      if (irow < num_irows) {
        float sum = 0, sum2 = 0;

        for (unsigned col = 0; col < row_size; ++col) {
          float val = avSrc[orow * row_size * num_irows + irow + num_irows * col];
          sum += val;
          sum2 += val * val;
        }

        if (flag) {
          sum /= row_size;
          sum2 /= row_size;
          sum2 -= sum * sum;
          sum2 = (sum2 < 0 ? 0 : sum2);
        } else {
          sum /= row_size;
          sum2 /= row_size - 1;
          sum2 -= ((float)row_size) / ((float)(row_size - 1)) * sum * sum;
          sum2 = (sum2 < 0 ? 0 : sum2);
        }

        if (apply_sqrt) {
          avTgt[orow * num_irows + irow] = hc::fast_math::sqrt(sum2);
        } else {
          avTgt[orow * num_irows + irow] = sum2;
        }
      }
    }
  }).wait();
}

template <bool apply_sqrt>
void THHcTensor_varOuterDim(THHcState* state, THHcTensor* tgt, THHcTensor* src, long dimension, int flag) {
  unsigned ndim = THHcTensor_nDimension(state, src);
  // Treat all outer dimensions (i.e. dim < dimension) as one.
  unsigned num_orows = 1;

  for (unsigned dim = 0; dim < dimension; dim++) {
    num_orows *= THHcTensor_size(state, src, dim);
  }

  unsigned row_size = THHcTensor_size(state, src, dimension);
  // Treat all inner dimensions (i.e. dim > dimension) as one.
  unsigned num_irows = 1;

  for (unsigned dim = dimension + 1; dim < ndim; dim++) {
    num_irows *= THHcTensor_size(state, src, dim);
  }

  auto avTgt = tgt->get_device_data(state);
  auto avSrc = src->get_device_data(state);

  if (flag) {
    THHcTensor_kernel_varOuterDim<true, apply_sqrt>(state,
        avTgt, avSrc, num_orows, num_irows, row_size);
  } else {
    THHcTensor_kernel_varOuterDim<false, apply_sqrt>(state,
        avTgt, avSrc, num_orows, num_irows, row_size);
  }
}

/* Compute the variance (or standard deviation) of the innermost dimension of a tensor.
 *
 * - num_rows is the size of the flattened outer dimensions;
 * - row_size is the size of the innermost dimension;
 * - if flag is set, normalize by `row_size` instead of `row_size - 1`
 * - if apply_sqrt is set, compute the standard deviation instead of variance
 *
 * The outer dimensions of the tensor are considered as a single dimension, i.e. the tensor is
 * considered as having 'num_rows' rows of size 'row_size'.
 * Each thread block processes one or more sets of contiguous rows (processing multiple rows
 * per thread block is quicker than processing a single row, especially for short rows).
 */

template <bool flag, bool apply_sqrt>
void THHcTensor_kernel_varInnermostDim(THHcState* state, float* &avTgt,
                                        float* &avSrc, unsigned num_rows,
                                        unsigned row_size) {
  hc::extent<2> grdExt(num_rows * 16, 16);
  hc::tiled_extent<2> t_ext = grdExt.tile(16, 16);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    tile_static float ssum[16][16];
    tile_static float ssum2[16][16];
    unsigned row = tidx.global[0];
    float sum = 0, sum2 = 0;

    if (row < num_rows) {
      for (unsigned col = tidx.local[1]; col < row_size; col += 16) {
        float val = avSrc[col + row * row_size];
        sum += val;
        sum2 += val * val;
      }

      ssum[tidx.local[0]][tidx.local[1]] = sum;
      ssum2[tidx.local[0]][tidx.local[1]] = sum2;
      tidx.barrier.wait();

      for (unsigned s = 8; s > 1; s >>= 1) {
        if (row < num_rows && tidx.local[1] < s) {
          ssum[tidx.local[0]][tidx.local[1]] += ssum[tidx.local[0]][tidx.local[1] + s];
          ssum2[tidx.local[0]][tidx.local[1]] += ssum2[tidx.local[0]][tidx.local[1] + s];
        }

        tidx.barrier.wait();
      }

      if (row < num_rows && tidx.local[1] == 0) {
        sum = ssum[tidx.local[0]][0] + ssum[tidx.local[0]][1];
        sum2 = ssum2[tidx.local[0]][0] + ssum2[tidx.local[0]][1];

        if (flag) {
          sum /= row_size;
          sum2 /= row_size;
          sum2 -= sum * sum;
          sum2 = (sum2 < 0 ? 0 : sum2);
        } else {
          sum /= row_size;
          sum2 /= row_size - 1;
          sum2 -= ((float)row_size) / ((float)(row_size - 1)) * sum * sum;
          sum2 = (sum2 < 0 ? 0 : sum2);
        }

        if (apply_sqrt) {
          avTgt[row] = hc::fast_math::sqrt(sum2);
        } else {
          avTgt[row] = sum2;
        }
      }

      tidx.barrier.wait();
    }
  }).wait();
}

template <bool apply_sqrt>
void THHcTensor_varInnermostDim(THHcState* state, THHcTensor* tgt, THHcTensor* src, int flag) {
  unsigned ndim = THHcTensor_nDimension(state, src);
  unsigned num_rows = 1;

  for (unsigned dim = 0; dim < ndim - 1; dim++) {
    num_rows *= THHcTensor_size(state, src, dim);
  }

  unsigned row_size = THHcTensor_size(state, src, ndim - 1);
  auto avTgt = tgt->get_device_data(state);
  auto avSrc = src->get_device_data(state);

  if (flag) {
    THHcTensor_kernel_varInnermostDim<true, apply_sqrt>(state, avTgt, avSrc, num_rows, row_size);
  } else {
    THHcTensor_kernel_varInnermostDim<false, apply_sqrt>(state,
        avTgt, avSrc, num_rows, row_size);
  }
}

void THHcTensor_var(THHcState* state, THHcTensor* self_, THHcTensor* src, long dimension, int flag) {
  THLongStorage* dim = THHcTensor_newSizeOf(state, src);
  THLongStorage_set(dim, dimension, 1);
  THHcTensor_resize(state, self_, dim, NULL);
  THLongStorage_free(dim);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);

  if (dimension == THHcTensor_nDimension(state, src) - 1) {
    THHcTensor_varInnermostDim<false>(state, self, src, flag);
  } else {
    THHcTensor_varOuterDim<false>(state, self, src, dimension, flag);
  }

  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

void THHcTensor_std(THHcState* state, THHcTensor* self_, THHcTensor* src, long dimension, int flag) {
  THLongStorage* dim = THHcTensor_newSizeOf(state, src);
  THLongStorage_set(dim, dimension, 1);
  THHcTensor_resize(state, self_, dim, NULL);
  THLongStorage_free(dim);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);

  if (dimension == THHcTensor_nDimension(state, src) - 1) {
    THHcTensor_varInnermostDim<true>(state, self, src, flag);
  } else {
    THHcTensor_varOuterDim<true>(state, self, src, dimension, flag);
  }

  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}


float THHcTensor_normall(THHcState* state, THHcTensor* self, float value) {
  self = THHcTensor_newContiguous(state, self);
  float result;
  if (value == 0.0f) {
    THHcTensor* temp = THHcTensor_newContiguous(state, self);
    transform(state, self, temp, partial_not_equal_functor(0.0f));
    //result = reduce<float>(state, temp, 0.0, multiply<float>());
    result = 0.0;
    
  } else {
    THHcTensor* temp = THHcTensor_newContiguous(state, self);
    transform(state, self, temp, norm_functor(value));
    result = 0.0;
    //result = reduce<float>(state, temp, 0.0, minus<float>());
    result = pow(result, (float)1.0 / value);
  }

  THHcTensor_free(state, self);
  return result;
}

void THHcTensor_norm(THHcState* state, THHcTensor* self,
                      THHcTensor* src, float value, long dimension) {
  if (value == 0.0f) {
    THHcTensor_transformReduceDim(state, self, src, dimension,
                                   partial_not_equal_functor(0.0f),
                                   (float)0, sum<float>());
  } else {
    THHcTensor_transformReduceDim(state, self, src, dimension,
                                   norm_functor(value), (float)0,
                                   sum<float>());
    THHcTensor_pow(state, self, self, 1 / value);
  }
}

/*
 * Renormalizes the sub-tensors along dimension dim such that they do not exceed norm maxnorm.
 * y=torch.renorm(x,p,dim,maxnorm) returns a version of x with p-norms lower than maxnorm over non-dim dimensions.
 * The dim argument is not to be confused with the argument of the same name in function norm.
 * In this case, the p-norm is measured for each i-th sub-tensor x:select(dim, i).
 * This function is equivalent to (but faster than) the following:
 */
void THHcTensor_kernel_renorm(THHcState* state, float* &avData,
                               long dataOffset, const float value, const long size,
                               const float maxnorm, long gridSz) {
  hc::extent<1> grdExt(gridSz * 32);
  hc::tiled_extent<1> t_ext = grdExt.tile(32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[32];
    unsigned long tx = tidx.local[0];
    long bx = tidx.tile[0];
    long step = tidx.tile_dim[0];
    float* dat = avData + dataOffset;
    float* row = dat + size * bx;
    buffer[tx] = 0;

    // get norm of axis
    for (long i = tx; i < size; i += step) {
      buffer[tx] += hc::fast_math::pow(hc::fast_math::fabs(row[i]), value);
    }

    // add (reduce)
    for (unsigned int stride = tidx.tile_dim[0] >> 1; stride > 0; stride >>= 1) {
      tidx.barrier.wait();

      if (tx < stride) {
        buffer[tx] += buffer[tx + stride];
      }
    }

    // clip norms
    tidx.barrier.wait();
    float norm = hc::fast_math::pow(buffer[0], 1.0 / value);

    if (norm > maxnorm) {
      norm = maxnorm / (norm + 1e-7);

      // renormalize
      for (long i = tx; i < size; i += step) {
        row[i] *= norm;
      }
    }
  }).wait();
}

void THHcTensor_renorm(THHcState* state, THHcTensor* self, THHcTensor* src, float value, long dimension, float maxnorm) {
  THHcTensor* self_;
  THHcTensor* src_ = THHcTensor_newTranspose(state, src, dimension, 0);
  THHcTensor* data = THHcTensor_newClone(state, src_);
  long size = THHcTensor_nElement(state, data) / data->size[0];
  THArgCheck(dimension >= 0 && dimension < THHcTensor_nDimension(state, src), 3, "invalid dimension");
  THArgCheck(value > 0, 2, "non-positive-norm not supported");
  THArgCheck(THHcTensor_nDimension(state, src) > 1, 1, "need at least 2 dimensions");
  long gridSize = data->size[0] * 32;
  auto avData = data->get_device_data(state);
  THHcTensor_kernel_renorm(state, avData, data->storageOffset, value, size, maxnorm, gridSize);
  THHcTensor_free(state, src_);
  self_ = THHcTensor_newTranspose(state, data, dimension, 0);
  THHcTensor_resizeAs(state, self, self_);
  THHcTensor_freeCopyTo(state, self_, self);
  THHcTensor_free(state, data);
}

float THHcTensor_dist(THHcState* state, THHcTensor* self, THHcTensor* src, float value) {
  self = THHcTensor_newContiguous(state, self);
  src = THHcTensor_newContiguous(state, src);
  float result = InnerProduct_plus_dist(state, self, src, value);
  THHcTensor_free(state, src);
  THHcTensor_free(state, self);
  return pow(result, (float)1.0 / value);
}

/* rand
 * Routine to returns a one-dimensional tensor of size n filled with random numbers from a uniform distribution
 *  on the interval (0,1).
 */
void THHcTensor_rand(THHcState* state, THHcTensor* r_, THLongStorage* size) {
  THHcTensor_resize(state, r_, size, NULL);
  THHcTensor_uniform(state, r_, 0, 1);
}

/* randn
 * Routine to returns a one-dimensional tensor of size n filled with random numbers from a normal distribution
 *  with mean 0 and variance 1.
 */
void THHcTensor_randn(THHcState* state, THHcTensor* r_, THLongStorage* size) {
  THHcTensor_resize(state, r_, size, NULL);
  THHcTensor_normal(state, r_, 0, 1);
}

/*
 * Fills the elements of the original Tensor with value val by selecting the indices in the order given in indx.
 */
void THHcTensor_kernel_indexFill(THHcState* state, float* &srcTensor, long srcOffset,
                                  hc::array_view<long> &srcStride,
                                  hc::array_view<long, 1> &indx,
                                  long src_nDim, int dim, long idx_size,
                                  long tensor_size, long size_dim, float val, long nblockx) {
  hc::extent<2> gridExt(16, nblockx * 16);
  hc::tiled_extent<2> t_ext = gridExt.tile(16, 16);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    int thread_idx = tidx.tile[1] * tidx.tile_dim[1] * tidx.tile_dim[0] + tidx.local[0] * tidx.tile_dim[1] + tidx.local[1];
    long flat_size = tensor_size / idx_size;

    if (thread_idx < flat_size) {
      long coeff = 0;
      int leftover = thread_idx;
      int srcIdx = 0;

      for (int d = 0; d < src_nDim; d++) {
        if (d < dim) {
          coeff = leftover / (srcStride[hc::index<1>(d)] / size_dim);
          leftover -= coeff * (srcStride[hc::index<1>(d)] / size_dim);
          srcIdx += coeff * srcStride[hc::index<1>(d)];
        } else if (d > dim) {
          coeff = leftover / srcStride[hc::index<1>(d)];
          leftover -= coeff * srcStride[hc::index<1>(d)];
          srcIdx += coeff * srcStride[hc::index<1>(d)];
        }
      }

      for (int i = 0; i < idx_size; i++) {
        srcTensor[srcOffset + (srcIdx + (int)((indx[i]) - 1)*srcStride[dim])] = val;
      }
    }
  }).wait();
}

/*
 * Copies the elements of tensor into the original tensor by selecting the indices in the order given in index.
 * The shape of tensor must exactly match the elements indexed or an error will be thrown.
 */
void THHcTensor_kernel_indexCopy(THHcState* state, float* &resTensor, long resOffset,
                                  float* &srcTensor, long srcOffset,
                                  hc::array_view<long, 1> &resStride,
                                  hc::array_view<long, 1> &indx,
                                  long res_size, long res_nDim, int dim,
                                  long idx_size, long src_size, long size_dim, long nblockx) {
  hc::extent<2> gridExt(16, (nblockx + 15) & ~15);
  hc::tiled_extent<2> t_ext = gridExt.tile(16, 16);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    int thread_idx = tidx.tile[1] * tidx.tile_dim[1] * tidx.tile_dim[0] + tidx.local[0] * tidx.tile_dim[1] + tidx.local[1];
    long flat_size = src_size / idx_size;

    if (thread_idx < flat_size) {
      long coeff = 0;
      int leftover = thread_idx;
      int targetIdx = 0;
      int resIdx = 0;

      for (int d = 0; d < res_nDim; d++) {
        if (d < dim) {
          long stride_d = (resStride[hc::index<1>(d)]) / size_dim;
          coeff = leftover / stride_d;
          leftover -= coeff * stride_d;
          targetIdx += coeff * stride_d * idx_size;
          resIdx += coeff * (resStride[hc::index<1>(d)]);
        } else if (d > dim) {
          coeff = leftover / (resStride[hc::index<1>(d)]);
          leftover -= coeff * (resStride[hc::index<1>(d)]);
          targetIdx += coeff * (resStride[hc::index<1>(d)]);
          resIdx += coeff * (resStride[hc::index<1>(d)]);
        }
      }

      for (int i = 0; i < idx_size; i++) {
        resTensor[resOffset + resIdx + ((int)(indx[hc::index<1>(i)]) - 1) * (resStride[hc::index<1>(dim)])] = srcTensor[srcOffset + targetIdx + (int) i * (resStride[hc::index<1>(dim)])];
      }
    }
  }).wait();
}

void THHcTensor_indexCopy(THHcState* state, THHcTensor* res_, int dim, THLongTensor* indices, THHcTensor* src) {
  hc::array_view<long, 1>* stride_;
  long nIndex = indices->size[0];
  long nRes;
  THArgCheck(indices->nDimension == 1, 3, "expecting vector of indices");
  THArgCheck(dim < src->nDimension, 4, "Indexing dim is out of bounds");
  THArgCheck(src->nDimension > 0, 2, "Source tensor is empty");
  THArgCheck(nIndex == src->size[dim], 4, "length of src.size[dim] is not equal to length of indices");
  src = THHcTensor_newContiguous(state, src);
  nRes = THHcTensor_nElement(state, res_);
  long nblockx = (long)(ceil((float)nRes / nIndex / (16 * 16)));
  hc::array<long, 1> arrstride_ = hc::array<long, 1>(hc::extent<1>(res_->nDimension),
      res_->stride, state->deviceState->get_current_accelerator_view());
  stride_ =  new hc::array_view<long, 1>(arrstride_);
  auto avRes = res_->get_device_data(state);
  auto avSrc = src->get_device_data(state);
  hc::array_view<long, 1> pavInd(indices->storage->size, indices->storage->data);
  THHcTensor_kernel_indexCopy(state, avRes, res_->storageOffset,
                               avSrc, src->storageOffset,
                               *stride_, pavInd, nRes,
                               res_->nDimension, dim, nIndex,
                               THHcTensor_nElement(state, src), res_->size[dim], nblockx);
  delete stride_;
  THHcTensor_free(state, src);
}

void THHcTensor_indexFill(THHcState* state, THHcTensor* res_, int dim, THLongTensor* indices, float val) {
  hc::array_view<long, 1>* stride_;
  long nIndex = indices->size[0];
  long nRes;
  THArgCheck(indices->nDimension == 1, 3, "Index is supposed to be a vector");
  THArgCheck(dim < res_->nDimension, 4, "Indexing dim is out of bounds");
  THArgCheck(res_->nDimension > 0, 2, "Source tensor is empty");
  nRes = THHcTensor_nElement(state, res_) / res_->size[dim] * nIndex;
  long nblockx = (long)(ceil((float)nRes / nIndex / (16 * 16)));
  hc::array<long, 1> arrstride_ = hc::array<long, 1>(hc::extent<1>(res_->nDimension),
      res_->stride, state->deviceState->get_current_accelerator_view());
  stride_ =  new hc::array_view<long, 1>(arrstride_);
  auto avRes = res_->get_device_data(state);
  hc::array_view<long, 1> pavInd(indices->storage->size, indices->storage->data);
  THHcTensor_kernel_indexFill(state, avRes, res_->storageOffset, *stride_, pavInd, res_->nDimension,
                               dim, nIndex, nRes, res_->size[dim], val, nblockx);
  delete stride_;
}

void THHcTensor_kernel_indexSelect(THHcState* state, float* &resTensor, long resOffset,
                                    float* &srcTensor, long srcOffset,
                                    hc::array_view<long, 1> &srcStride, THLongTensor* indices,
                                    long src_nDim, int dim, long idx_size,
                                    long tensor_size, long src_size,
                                    long size_dim, long nblockx) {
  hc::array<long, 1> arrindx(hc::extent<1>(indices->storage->size), indices->storage->data, state->deviceState->get_current_accelerator_view());
  hc::array_view<long, 1> indx(arrindx);
  hc::extent<2> gridExt(16, nblockx * 16);
  hc::tiled_extent<2> t_ext = gridExt.tile(16, 16);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    int thread_idx = tidx.tile[1] * tidx.tile_dim[1] * tidx.tile_dim[0] + tidx.local[0] * tidx.tile_dim[1] + tidx.local[1];
    long flat_size = tensor_size / idx_size;

    if (thread_idx < flat_size) {
      long coeff = 0;
      int leftover = thread_idx;
      int targetIdx = 0;
      int srcIdx = 0;

      for (int d = 0; d < src_nDim; d++) {
        if (d < dim) {
          long stride_d = srcStride[hc::index<1>(d)] / size_dim;
          coeff = leftover / stride_d;
          leftover -= coeff * stride_d;
          targetIdx += coeff * stride_d * idx_size;
          srcIdx += coeff * srcStride[hc::index<1>(d)];
        } else if (d > dim) {
          coeff = leftover / srcStride[hc::index<1>(d)];
          leftover -= coeff * srcStride[hc::index<1>(d)];
          targetIdx += coeff * srcStride[hc::index<1>(d)];
          srcIdx += coeff * srcStride[hc::index<1>(d)];
        }
      }

      for (int i = 0; i < idx_size; i++) {
        resTensor[resOffset + targetIdx + i * srcStride[dim]] = srcTensor[srcOffset + srcIdx + ((int)(indx[i]) - 1) * srcStride[dim]];
      }
    }
  }).wait();
}

void THHcTensor_indexSelect(THHcState* state, THHcTensor* res_, THHcTensor* src, int dim, THLongTensor* indices) {
  THLongStorage* newSize;
  hc::array_view<long>* stride_;
  long nIndex = indices->size[0];
  long nRes;
  THArgCheck(indices->nDimension == 1, 3, "expecting vector of indices");
  THArgCheck(dim < src->nDimension, 4, "Indexing dim is out of bounds");
  THArgCheck(src->nDimension > 0, 2, "Source tensor is empty");
  newSize = THLongStorage_newWithSize(src->nDimension);
  THLongStorage_rawCopy(newSize, src->size);
  newSize->data[dim] = nIndex;
  THHcTensor_resize(state, res_, newSize, NULL);
  THLongStorage_free(newSize);
  nRes = THHcTensor_nElement(state, res_);
  long nblockx = (long)(ceil((float)nRes / nIndex / (16 * 16)));
  hc::array<long, 1> arrstride_ = hc::array<long, 1>(hc::extent<1>(src->nDimension), src->stride,
      state->deviceState->get_current_accelerator_view());
  stride_ =  new hc::array_view<long, 1>(arrstride_);
  auto avRes = res_->get_device_data(state);
  auto avSrc = src->get_device_data(state);
  THHcTensor_kernel_indexSelect(state, avRes, res_->storageOffset,
                                 avSrc, src->storageOffset,
                                 *stride_, indices, src->nDimension, dim,
                                 indices->size[0], nRes,
                                 THHcTensor_nElement(state, src), src->size[dim], nblockx);
  delete stride_;
}
