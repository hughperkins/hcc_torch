#include "THHcTensor.h"

THHC_API int THHcTensor_getDevice(THHcState* state, THHcTensor* thc) {
  if (!thc->storage) {
    return 0;
  }

  return thc->getDevice(state->deviceState->get_current_accelerator());
}
