#ifndef THHc_STORAGE_INC
#define THHc_STORAGE_INC

#include "THStorage.h"
#include "THHcGeneral.h"
#include "hc.hpp"

#define TH_STORAGE_REFCOUNTED 1
#define TH_STORAGE_RESIZABLE  2
#define TH_STORAGE_FREEMEM    4


typedef struct THHcStorage {
  float* data;
  long size;
  int refcount;
  char flag;
  float** allocatorContext;
  Concurrency::array_view<float>* ampAllocatorContext;
} THHcStorage;


THHC_API float* THHcStorage_data(THHcState* state, const THHcStorage*);
THHC_API long THHcStorage_size(THHcState* state, const THHcStorage*);

/* slow access -- checks everything */
THHC_API void THHcStorage_set(THHcState* state, THHcStorage*, long, float);
THHC_API float THHcStorage_get(THHcState* state, const THHcStorage*, long);

THHC_API THHcStorage* THHcStorage_new(THHcState* state);
THHC_API THHcStorage* THHcStorage_newWithSize(THHcState* state, long size);
THHC_API THHcStorage* THHcStorage_newWithSize1(THHcState* state, float);
THHC_API THHcStorage* THHcStorage_newWithSize2(THHcState* state, float, float);
THHC_API THHcStorage* THHcStorage_newWithSize3(THHcState* state, float, float, float);
THHC_API THHcStorage* THHcStorage_newWithSize4(THHcState* state, float, float, float, float);
THHC_API THHcStorage* THHcStorage_newWithMapping(THHcState* state, const char* filename, long size, int shared);

/* takes ownership of data */
THHC_API THHcStorage* THHcStorage_newWithData(THHcState* state, float* data, long size);

THHC_API THHcStorage* THHcStorage_newWithAllocator(THHcState* state, long size,
    THAllocator* allocator,
    void* allocatorContext);
THHC_API THHcStorage* THHcStorage_newWithDataAndAllocator(
  THHcState* state, float* data, long size, THAllocator* allocator, void* allocatorContext);

THHC_API void THHcStorage_setFlag(THHcState* state, THHcStorage* storage, const char flag);
THHC_API void THHcStorage_clearFlag(THHcState* state, THHcStorage* storage, const char flag);
THHC_API void THHcStorage_retain(THHcState* state, THHcStorage* storage);

THHC_API void THHcStorage_free(THHcState* state, THHcStorage* storage);
THHC_API void THHcStorage_resize(THHcState* state, THHcStorage* storage, long size);
THHC_API void THHcStorage_fill(THHcState* state, THHcStorage* storage, float value);

#endif
