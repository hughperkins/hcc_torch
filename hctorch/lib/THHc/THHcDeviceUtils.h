#ifndef THHC_DEVICE_UTILS_INC
#define THHC_DEVICE_UTILS_INC

/* The largest consecutive integer representable in float32 (2^24) */
#define FLOAT32_MAX_CONSECUTIVE_INT 16777216.0f

/**
   Computes ceil(a / b)
*/
template <typename T>
T THHcCeilDiv(T a, T b) __attribute__((hc, cpu)) {
  return (a + b - 1) / b;
}

/**
   Computes ceil(a / b) * b; i.e., rounds up `a` to the next highest
   multiple of b
*/
template <typename T>
T THHcRoundUp(T a, T b) __attribute__((hc, cpu)) {
  return THHcCeilDiv(a, b) * b;
}

/**
 * For CC 3.5+, perform a load using __ldg
 */
template <typename T>
T doLdg(const T* p) __attribute__((hc, cpu)) {
  return *p;
}

#endif // THHC_DEVICE_UTILS_INC

