#ifndef THHc_INC
#define THHc_INC

#include "THHcGeneral.h"
#include "THHcStorage.h"
#include "THHcStorageCopy.h"
#include "THHcTensor.h"
#include "THHcTensorCopy.h"
#include "THHcTensorRandom.h"
#include "THHcTensorMath.h"
#include "THHcTensorConv.h"

#endif
