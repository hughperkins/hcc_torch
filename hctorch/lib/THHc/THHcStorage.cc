#include "THHcStorage.h"
#include "THHcGeneral.h"
#include "THAtomic.h"

void THHcStorage_set(THHcState* state, THHcStorage* self, long index, float value) {
  THArgCheck((index >= 0) && (index < self->size), 2, "index out of bounds");
  THHcCheck(hc::am_copy(self->data + index, &value, sizeof(float)));
}

float THHcStorage_get(THHcState* state, const THHcStorage* self, long index) {
  float value;
  THArgCheck((index >= 0) && (index < self->size), 2, "index out of bounds");
  THHcCheck(hc::am_copy(&value, self->data + index, sizeof(float)));
  return value;
}

THHcStorage* THHcStorage_new(THHcState* state) {
  THHcStorage* storage = (THHcStorage*)THAlloc(sizeof(THHcStorage));
  storage->data = NULL;
  storage->size = 0;
  storage->refcount = 1;
  storage->flag = TH_STORAGE_REFCOUNTED | TH_STORAGE_RESIZABLE | TH_STORAGE_FREEMEM;
  return storage;
}

THHcStorage* THHcStorage_newWithSize(THHcState* state, long size) {
  THArgCheck(size >= 0, 2, "invalid size");
  
  if (size > 0) {
    THHcStorage* storage = (THHcStorage*)THAlloc(sizeof(THHcStorage));
    // Allocating device array of given size
    THHcCheck(THHcMalloc(state, (void **)&storage->data, size * sizeof(float)));
    storage->size = size;
    storage->refcount = 1;
    storage->flag = TH_STORAGE_REFCOUNTED | TH_STORAGE_RESIZABLE | TH_STORAGE_FREEMEM;
    return storage;
  } else {
    return THHcStorage_new(state);
  }
}

THHcStorage* THHcStorage_newWithSize1(THHcState* state, float data0) {
  THHcStorage* self = THHcStorage_newWithSize(state, 1);
  THHcStorage_set(state, self, 0, data0);
  return self;
}

THHcStorage* THHcStorage_newWithSize2(THHcState* state, float data0, float data1) {
  THHcStorage* self = THHcStorage_newWithSize(state, 2);
  THHcStorage_set(state, self, 0, data0);
  THHcStorage_set(state, self, 1, data1);
  return self;
}

THHcStorage* THHcStorage_newWithSize3(THHcState* state, float data0, float data1, float data2) {
  THHcStorage* self = THHcStorage_newWithSize(state, 3);
  THHcStorage_set(state, self, 0, data0);
  THHcStorage_set(state, self, 1, data1);
  THHcStorage_set(state, self, 2, data2);
  return self;
}

THHcStorage* THHcStorage_newWithSize4(THHcState* state, float data0, float data1, float data2, float data3) {
  THHcStorage* self = THHcStorage_newWithSize(state, 4);
  THHcStorage_set(state, self, 0, data0);
  THHcStorage_set(state, self, 1, data1);
  THHcStorage_set(state, self, 2, data2);
  THHcStorage_set(state, self, 3, data3);
  return self;
}

THHcStorage* THHcStorage_newWithMapping(THHcState* state, const char* fileName, long size, int isShared) {
  THError("not available yet for THHcStorage");
  return NULL;
}

// Note that 'data' is on host
THHcStorage* THHcStorage_newWithData(THHcState* state, float* data, long size) {
  THHcStorage* storage = (THHcStorage*)THAlloc(sizeof(THHcStorage));
  storage->data = data;
  storage->size = size;
  storage->refcount = 1;
  storage->flag = TH_STORAGE_REFCOUNTED | TH_STORAGE_RESIZABLE | TH_STORAGE_FREEMEM;
  return storage;
}

void THHcStorage_retain(THHcState* state, THHcStorage* self) {
  if (self && (self->flag & TH_STORAGE_REFCOUNTED)) {
    THAtomicIncrementRef(&self->refcount);
  }
}

void THHcStorage_free(THHcState* state, THHcStorage* self) {
  if (!(self->flag & TH_STORAGE_REFCOUNTED)) {
    return;
  }
  
  if (THAtomicDecrementRef(&self->refcount)) {
    if (self->flag & TH_STORAGE_FREEMEM) {
      THHcCheck(THHcFree(state,self->data)); 
      self->data = NULL;
      self->size = 0;
      self->refcount = 0;
      self->flag = 0;
    }
    THFree(self);
  }
}


