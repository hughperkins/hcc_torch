#ifndef TH_Hc_TENSOR_COPY_INC
#define TH_Hc_TENSOR_COPY_INC

#include "THHcTensor.h"
#include "THHcGeneral.h"

THHC_API void THHcTensor_copy(THHcState* state, THHcTensor* self, THHcTensor* src);
THHC_API void THHcTensor_copyByte(THHcState* state, THHcTensor* self, THByteTensor* src);
THHC_API void THHcTensor_copyChar(THHcState* state, THHcTensor* self, THCharTensor* src);
THHC_API void THHcTensor_copyShort(THHcState* state, THHcTensor* self, THShortTensor* src);
THHC_API void THHcTensor_copyInt(THHcState* state, THHcTensor* self, THIntTensor* src);
THHC_API void THHcTensor_copyLong(THHcState* state, THHcTensor* self, THLongTensor* src);
THHC_API void THHcTensor_copyFloat(THHcState* state, THHcTensor* self, THFloatTensor* src);
THHC_API void THHcTensor_copyDouble(THHcState* state, THHcTensor* self, THDoubleTensor* src);

THHC_API void THByteTensor_copyHc(THHcState* state, THByteTensor* self, THHcTensor* src);
THHC_API void THCharTensor_copyHc(THHcState* state, THCharTensor* self, THHcTensor* src);
THHC_API void THShortTensor_copyHc(THHcState* state, THShortTensor* self, THHcTensor* src);
THHC_API void THIntTensor_copyHc(THHcState* state, THIntTensor* self, THHcTensor* src);
THHC_API void THLongTensor_copyHc(THHcState* state, THLongTensor* self, THHcTensor* src);
THHC_API void THFloatTensor_copyHc(THHcState* state, THFloatTensor* self, THHcTensor* src);
THHC_API void THDoubleTensor_copyHc(THHcState* state, THDoubleTensor* self, THHcTensor* src);
THHC_API void THHcTensor_copyHc(THHcState* state, THHcTensor* self, THHcTensor* src);

#endif
