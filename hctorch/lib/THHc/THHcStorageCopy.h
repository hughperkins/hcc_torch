#ifndef THHc_STORAGE_COPY_INC
#define THHc_STORAGE_COPY_INC

#include "THHcStorage.h"
#include "THHcGeneral.h"

/* Support for copy between different Storage types */

THHC_API void THHcStorage_copy(THHcState* state, THHcStorage* storage, THHcStorage* src);
THHC_API void THHcStorage_copyByte(THHcState* state, THHcStorage* storage, struct THByteStorage* src);
THHC_API void THHcStorage_copyChar(THHcState* state, THHcStorage* storage, struct THCharStorage* src);
THHC_API void THHcStorage_copyShort(THHcState* state, THHcStorage* storage, struct THShortStorage* src);
THHC_API void THHcStorage_copyInt(THHcState* state, THHcStorage* storage, struct THIntStorage* src);
THHC_API void THHcStorage_copyLong(THHcState* state, THHcStorage* storage, struct THLongStorage* src);
THHC_API void THHcStorage_copyFloat(THHcState* state, THHcStorage* storage, struct THFloatStorage* src);
THHC_API void THHcStorage_copyDouble(THHcState* state, THHcStorage* storage, struct THDoubleStorage* src);

THHC_API void THByteStorage_copyHc(THHcState* state, THByteStorage* self, struct THHcStorage* src);
THHC_API void THCharStorage_copyHc(THHcState* state, THCharStorage* self, struct THHcStorage* src);
THHC_API void THShortStorage_copyHc(THHcState* state, THShortStorage* self, struct THHcStorage* src);
THHC_API void THIntStorage_copyHc(THHcState* state, THIntStorage* self, struct THHcStorage* src);
THHC_API void THLongStorage_copyHc(THHcState* state, THLongStorage* self, struct THHcStorage* src);
THHC_API void THFloatStorage_copyHc(THHcState* state, THFloatStorage* self, struct THHcStorage* src);
THHC_API void THDoubleStorage_copyHc(THHcState* state, THDoubleStorage* self, struct THHcStorage* src);
THHC_API void THHcStorage_copyHc(THHcState* state, THHcStorage* self, THHcStorage* src);

#endif
