#ifndef TH_Hc_TENSOR_CONV_INC
#define TH_Hc_TENSOR_CONV_INC

#include "THHcTensor.h"

THHC_API void THHcTensor_conv2Dmv(THHcState* state, THHcTensor* output, float beta, THHcTensor* input,
                                  THHcTensor* kernel, long srow, long scol, const char* type);

THHC_API void THHcTensor_conv2Dmm(THHcState* state, THHcTensor* output, float beta, THHcTensor* input,
                                  THHcTensor* kernel, long srow, long scol, const char* type);

THHC_API void THHcTensor_conv2DRevger(THHcState* state, THHcTensor* output, float beta, float alpha,
                                      THHcTensor* input, THHcTensor* kernel,
                                      long srow, long scol);

THHC_API void THHcTensor_conv2DRevgerm(THHcState* state, THHcTensor* output, float beta, float alpha,
                                       THHcTensor* input, THHcTensor* kernel,
                                       long srow, long scol);

THHC_API void THHcTensor_conv2Dmap(THHcState* state, THHcTensor* output, THHcTensor* input,
                                   THHcTensor* kernel, long stride_x, long stride_y,
                                   THHcTensor* table, long fanin);

#endif
