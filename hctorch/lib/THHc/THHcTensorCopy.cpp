#include "THHcTensorCopy.h"
#include "THHcGeneral.h"
#include "THHcTensor.h"

// Maximum number of dimensions allowed for hctorch
#define MAX_DIMS 25

#ifndef DIVUP
#define DIVUP(x, y) (((x) + (y) - 1) / (y))
#endif

// Copy self->size to device and remove all dims of size=1
static void THHcTensor_computesz(THHcState* state, THHcTensor* self, long*& sz_,
                                  long*& st_, int* dim_, long* innermostdim) {
  long* szh, *sth;
  int i, j, dim;
  long last_sz;
  dim = 0;

  // how many dims with size > 1 ?
  for (i = self->nDimension - 1; i >= 0; i--) {
    if (self->size[i] != 1) {
      dim++;
    }
  }

  if (dim == 0) {
    THError("Error: using non-contiguous code-path for tensor with all singleton dimensions");
  }

  THHcCheck(THHcMalloc(state, (void**)&sz_, sizeof(long) * dim));
  THHcCheck(THHcMalloc(state, (void**)&st_, sizeof(long) * dim));
  szh = (long*)THAlloc(sizeof(long) * dim);
  sth = (long*)THAlloc(sizeof(long) * dim);
  j = dim - 1;

  for (i = self->nDimension - 1; i >= 0; i--) {
    // ignore dimensions of size 1 to prevent copy bug
    if (self->size[i] != 1) {
      sth[j] = self->stride[i];

      if (j == dim - 1) {
        szh[j] = 1;
        *innermostdim = self->size[i];
      } else {
        szh[j] = szh[j + 1] * last_sz;
      }

      j--;
      last_sz = self->size[i];
    }
  }

  THHcCheck(hc::am_copy(sz_, szh, sizeof(long) * dim));
  THHcCheck(hc::am_copy(st_, sth, sizeof(long) * dim));
  THFree(szh);
  THFree(sth);
  *dim_ = dim;
}

void THHcTensor_kernel_copy(THHcState* state, float* av_dst, long dstOffset,
                             float* av_src, long srcOffset,
                             long* av_dst_sz,
                             long* av_dst_st, int dst_dim,
                             long* av_src_sz,
                             long* av_src_st,
                             int src_dim, long n_elem, long innerdim, int nblockx,
                             int nblocky, int nblockz) {
  hc::extent<3> copyExt(nblockz, nblocky * 16, nblockx * 16);
  hc::tiled_extent<3> t_ext = copyExt.tile(1, 16, 16);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  //Copy Kernel
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<3> tidx) __attribute__((hc, cpu)) {
    long k = (tidx.tile[0] * (copyExt[2] / tidx.tile_dim[2]) * (copyExt[1] / tidx.tile_dim[1]) + tidx.tile[1] * (copyExt[2] / tidx.tile_dim[2]) + tidx.tile[2] ) * tidx.tile_dim[1] + tidx.local[1];
    long i_start = tidx.local[2] * av_src_st[src_dim - 1];
    long i_step = tidx.tile_dim[2] * av_src_st[src_dim - 1];
    long o_start = tidx.local[2] * av_dst_st[src_dim - 1];
    long o_step = tidx.tile_dim[2] * av_dst_st[src_dim - 1];
    long o_end = innerdim * av_dst_st[src_dim - 1];

    if (((k + 1) * innerdim) <= n_elem) {
      long dst_idx = 0;
      long dst_rest = k * innerdim;

      for (int dim = 0; dim < dst_dim; dim++) {
        dst_idx += (dst_rest / av_dst_sz[dim]) * av_dst_st[dim];
        dst_rest = dst_rest % av_dst_sz[dim];
      }

      long src_idx = 0;
      long src_rest = k * innerdim;

      for (int dim = 0; dim < src_dim; dim++) {
        src_idx += (src_rest / av_src_sz[dim]) * av_src_st[dim];
        src_rest = src_rest % av_src_sz[dim];
      }

      for (int i = i_start, o = o_start; o < o_end; i += i_step, o += o_step) {
        av_dst[dstOffset + dst_idx + o] = av_src[srcOffset + src_idx + i];
      }
    }
  }).wait();
}

THHC_API void THHcTensor_copy(THHcState* state, THHcTensor* self, THHcTensor* src) {
  // Avoid unnecessary copy
  if (self == src) {
    return;
  }

  long totalElements = THHcTensor_nElement(state, self);
  THArgCheck(totalElements == THHcTensor_nElement(state, src), 2, "sizes do not match");
  THArgCheck(THHcTensor_nDimension(state, self) <= MAX_DIMS, 2, "Copy only supported for <= 25 dimensions");
  THArgCheck(THHcTensor_nDimension(state, src) <= MAX_DIMS, 3, "Copy only supported for <= 25 dimensions");

  if (THHcTensor_nDimension(state, self) == 0) {
    // Zero-dim tensor; copy nothing
    return;
  }

  if ((THHcTensor_isContiguous(state, self) && THHcTensor_isContiguous(state, src)) || (totalElements == 1)) {
    auto Src = src->get_device_data(state);
    auto Dst = self->get_device_data(state);
    THHcCheck(hc::am_copy(Dst + self->storageOffset, Src + src->storageOffset, THHcTensor_nElement(state, src) * sizeof(float)));
  } else {
    long* d_self_sz = NULL, *d_self_st = NULL, *d_src_sz = NULL, *d_src_st = NULL;
    int self_dim, src_dim;
    long size = THHcTensor_nElement(state, self);
    long innermostdim;
    // Data is valid only in device side of d_src_sz, d_src_st, d_self_sz, d_self_st
    THHcTensor_computesz(state, src, d_src_sz, d_src_st, &src_dim, &innermostdim);
    THHcTensor_computesz(state, self, d_self_sz, d_self_st, &self_dim, &innermostdim);
    int nblocks = ceil((float)size / (16 * innermostdim ));
    // if nblocks greater than 65535 then we need to open a second dimension
#define __MAX_NUM_BLOCKS_PER_GRID_DIM__ 65535
    // The configuration below can deal with Tensors
    // of size up to 65535 * 65535 * 65535 * 16 elements.
    int nblocks_x = (nblocks > __MAX_NUM_BLOCKS_PER_GRID_DIM__) ? __MAX_NUM_BLOCKS_PER_GRID_DIM__ : nblocks;
    int number_blocks_dim_x = DIVUP(nblocks, nblocks_x);
    int nblocks_y = (number_blocks_dim_x > __MAX_NUM_BLOCKS_PER_GRID_DIM__) ? __MAX_NUM_BLOCKS_PER_GRID_DIM__ : number_blocks_dim_x;
    int number_blocks_dim_y = DIVUP(nblocks, nblocks_x * nblocks_y);
    int nblocks_z = number_blocks_dim_y;
    auto avSelf = self->get_device_data(state);
    auto avSrc = src->get_device_data(state);
    THHcTensor_kernel_copy(state, avSelf, self->storageOffset, avSrc, src->storageOffset,
                            d_self_sz, d_self_st, self_dim,
                            d_src_sz, d_src_st, src_dim,
                            size, innermostdim, nblocks_x, nblocks_y, nblocks_z);

    THHcFree(state, d_self_st);
    THHcFree(state, d_self_sz);
    THHcFree(state, d_src_st);
    THHcFree(state, d_src_sz);
  }
}
