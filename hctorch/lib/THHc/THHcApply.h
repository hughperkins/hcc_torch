#ifndef THHC_APPLY_INC
#define THHC_APPLY_INC

#include "THHcTensorCopy.h"
#include "THHcReduceApplyUtils.h"

//
// This file contains pointwise operation functions and kernels that
// work on both contiguous and non-contiguous tensor arguments of
// arbitrary (up to MAX_HCTORCH_DIMS) dimensioned arguments without
// copying or temporary storage.
//

// Threads per block for our apply kernel
#define THHC_APPLY_THREADS_PER_BLOCK 16 * 16

// Called when we are copying into an overlapping index `dst`, but
// we don't care which writer wins. Hacky but it works.
THHC_API void THHcTensor_copyIgnoringOverlaps(THHcState* state,
                                       THHcTensor* dst,
                                       THHcTensor* src);

template <typename Op, typename IndexType, int ADims>
void THHcTensor_pointwiseApply1(THHcState* state, TensorInfo<IndexType> a,
                             IndexType totalElements,
                             Op op) {

  int sz = (totalElements + (THHC_APPLY_THREADS_PER_BLOCK - 1)) & ~(THHC_APPLY_THREADS_PER_BLOCK - 1);
  hc::extent<1> gridExt(sz);
  hc::tiled_extent<1> t_ext = gridExt.tile(THHC_APPLY_THREADS_PER_BLOCK);
  THHcDeviceState* device_state = state->deviceState;
  float* aData = (a.tensor)->get_device_data(state);
  IndexType* Astrides = a.dStrides;
  IndexType* Asizes = a.dSizes;
  long AstorageOffset = (a.tensor)->storageOffset;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
                            
    IndexType linearIndex = tidx.global[0];
         
    if (linearIndex < totalElements)
    {
      // Convert `linearIndex` into an offset of `a`
      const IndexType aOffset = 
      IndexToOffset<IndexType, ADims>::get(linearIndex, Astrides, Asizes, ADims);
      op(&aData[aOffset + AstorageOffset]);
    }
  }).wait();
}


template <typename Op, typename IndexType, int ADims, int BDims>
void
THHcTensor_pointwiseApply2(THHcState *state, TensorInfo<IndexType> a,
                             TensorInfo<IndexType> b,
                             IndexType totalElements,
                             Op op) {
  int sz = (totalElements + (THHC_APPLY_THREADS_PER_BLOCK - 1)) & ~(THHC_APPLY_THREADS_PER_BLOCK - 1);
  hc::extent<1> gridExt(sz);
  hc::tiled_extent<1> t_ext = gridExt.tile(THHC_APPLY_THREADS_PER_BLOCK);
  THHcDeviceState* device_state = state->deviceState;
  float* adata = (a.tensor)->get_device_data(state);
  float* bdata = (b.tensor)->get_device_data(state);
  IndexType *Astrides = a.dStrides, *Asizes = a.dSizes;
  IndexType *Bstrides = b.dStrides, *Bsizes = b.dSizes;
  long AstorageOffset = (a.tensor)->storageOffset;
  long BstorageOffset = (b.tensor)->storageOffset;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
                            
    IndexType linearIndex = tidx.global[0];
         
    if (linearIndex < totalElements)
    {
      const IndexType aOffset =
      IndexToOffset<IndexType, ADims>::get(linearIndex, Astrides, Asizes, ADims);
      // Convert `linearIndex` into an offset of `b`
      const IndexType bOffset =
      IndexToOffset<IndexType, BDims>::get(linearIndex, Bstrides, Bsizes, BDims);

      op(&adata[aOffset + AstorageOffset], &bdata[bOffset + BstorageOffset]);
    }
  }).wait();
}

template <typename Op, typename IndexType, int ADims, int BDims, int CDims>
void
THHcTensor_pointwiseApply3(THHcState *state, TensorInfo<IndexType> a,
                             TensorInfo<IndexType> b,
                             TensorInfo<IndexType> c,
                             IndexType totalElements,
                             Op op) {

  int sz = (totalElements + (THHC_APPLY_THREADS_PER_BLOCK - 1)) & ~(THHC_APPLY_THREADS_PER_BLOCK - 1);
  hc::extent<1> gridExt(sz);
  hc::tiled_extent<1> t_ext = gridExt.tile(THHC_APPLY_THREADS_PER_BLOCK);
  THHcDeviceState* device_state = state->deviceState;
  float* adata = (a.tensor)->get_device_data(state);
  float* bdata =  (b.tensor)->get_device_data(state);
  float* cdata = (c.tensor)->get_device_data(state);
  IndexType *Astrides = a.dStrides, *Asizes = a.dSizes;
  IndexType *Bstrides = b.dStrides, *Bsizes = b.dSizes;
  IndexType *Cstrides = c.dStrides, *Csizes = c.dSizes;
  long AstorageOffset = (a.tensor)->storageOffset;
  long BstorageOffset = (b.tensor)->storageOffset;
  long CstorageOffset = (c.tensor)->storageOffset;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
                            
    IndexType linearIndex = tidx.global[0];
         
    if (linearIndex < totalElements)
    {
      // Convert `linearIndex` into an offset of `a`
      const IndexType aOffset =
      IndexToOffset<IndexType, ADims>::get(linearIndex, Astrides, Asizes, ADims);

      // Convert `linearIndex` into an offset of `b`
      const IndexType bOffset =
      IndexToOffset<IndexType, BDims>::get(linearIndex, Bstrides, Bsizes, BDims);

      // Convert `linearIndex` into an offset of `c`
      const IndexType cOffset =
      IndexToOffset<IndexType, CDims>::get(linearIndex, Cstrides, Csizes, CDims);

      op(&adata[aOffset + AstorageOffset], &bdata[bOffset + BstorageOffset], &cdata[cOffset + CstorageOffset]);
    }
  }).wait();
}



template <typename Op>
bool THHcTensor_pointwiseApply1(THHcState* state,
                                  THHcTensor* a,
                                  const Op& op,
                                  TensorArgType aType = ReadWrite) {
  long totalElements = THHcTensor_nElement(state, a);

  if (THHcTensor_nDimension(state, a) > MAX_HCTORCH_DIMS) {
    return false;
  }

  if (THHcTensor_nDimension(state, a) == 0) {
    // Zero-dim tensor; do nothing
    return true;
  }

  // If tensor args have overlapping indices and are read/write, then
  // we must expand the tensor to a contiguous form first, since
  // otherwise there are conflicting writes. Upon copying back to the
  // non-contiguous form, there will be conflicting writes, but at
  // least with copy, one of the updaters will win atomically. This is
  // a sketchy property of the old system as well (writing into all
  // indices of a tensor with overlapping indices should probably be
  // an error, since it is unclear which one should win), but we will
  // preserve this last-writer-wins (in arbitrary copy order) behavior.
  THHcTensor* oldA = NULL;

  if (aType == ReadWrite && THHc_overlappingIndices(state, a)) {
    // Must perform in contiguous space
    oldA = a;
    a = THHcTensor_newContiguous(state, a);
  }

  // It is possible that the tensor dimensions are able to be collapsed,
  // and thus we can reduce the actual code complexity of the copy by
  // exploiting this knowledge statically, since the div/mod is the
  // most expensive part of the operation, more so than memory accesses.
  // For instance, when copying a non-contiguous to a contiguous tensor
  // (or vice versa), the contiguous tensor can be collapsed to one
  // dimension, and the loop to translate the linear index to the array
  // index can be similarly collapsed. That is what this unrolling is for.
#define HANDLE_CASE(TYPE, A)                                   \
  THHcTensor_pointwiseApply1<Op, TYPE, A>(state,    \
      aInfo, (TYPE) totalElements, op);

#define HANDLE_A_CASE(TYPE, A)                      \
  {                                                 \
    if (aInfo.isContiguous()) {                     \
      HANDLE_CASE(TYPE, -2);                        \
    } else {                                        \
      switch (A) {                                  \
        case 1:                                     \
          HANDLE_CASE(TYPE, 1);                     \
          break;                                    \
        case 2:                                     \
          HANDLE_CASE(TYPE, 2);                     \
          break;                                    \
        case 3:                                     \
          HANDLE_CASE(TYPE, 3);                     \
          break;                                    \
        default:                                    \
          HANDLE_CASE(TYPE, -1);                    \
          break;                                    \
      }                                             \
    }                                               \
  }

  // Can we use 32-bit integer math in the kernel (the linear ID for the copy
  // and the resulting non-linear offset is all computable using 32-bit math?)
  // We also use unsigned index math in the kernel, as signed div/mod has
  // additional overhead.
  if (THHc_canUse32BitIndexMath(state, a)) {
    TensorInfo<unsigned int> aInfo(state, a);
    aInfo.collapseDims();

    HANDLE_A_CASE(unsigned int, aInfo.dims);
  } else {
    TensorInfo<unsigned long> aInfo(state, a);
    aInfo.collapseDims();

    // For large tensors, we only compile the completely contiguous
    // version and the completely generic version, to reduce
    // compilation time.
    if (aInfo.isContiguous()) {
      THHcTensor_pointwiseApply1<Op, unsigned long, -2>(state,
          aInfo, (unsigned long) totalElements, op);
    } else {
      THHcTensor_pointwiseApply1<Op, unsigned long, -1>(state,
          aInfo, (unsigned long) totalElements, op);
    }
  }
#undef HANDLE_CASE
#undef HANDLE_A_CASE

  if (oldA) {
    // Ignore overlaps when copying back; if we use THHcTensor_copy
    // instead, it will recursively try and invoke ourselves to make
    // oldA contiguous.
    THHcTensor_copyIgnoringOverlaps(state, oldA, a);
    THHcTensor_free(state, a);
    a = oldA;
  }

  return true;
}


template <typename Op>
bool THHcTensor_pointwiseApply2(THHcState* state,
                                  THHcTensor* a,
                                  THHcTensor* b,
                                  const Op& op,
                                  TensorArgType aType = ReadWrite,
                                  TensorArgType bType = ReadOnly) {
  long totalElements = THHcTensor_nElement(state, a);

  if (totalElements != THHcTensor_nElement(state, b)) {
    return false;
  }

  if (THHcTensor_nDimension(state, a) > MAX_HCTORCH_DIMS ||
      THHcTensor_nDimension(state, b) > MAX_HCTORCH_DIMS) {
    return false;
  }

  if (THHcTensor_nDimension(state, a) == 0) {
    // Zero-dim tensor; do nothing
    return true;
  }


  // If tensor args have overlapping indices and are read/write, then
  // we must expand the tensor to a contiguous form first, since
  // otherwise there are conflicting writes. Upon copying back to the
  // non-contiguous form, there will be conflicting writes, but at
  // least with copy, one of the updaters will win atomically. This is
  // a sketchy property of the old system as well (writing into all
  // indices of a tensor with overlapping indices should probably be
  // an error, since it is unclear which one should win), but we will
  // preserve this last-writer-wins (in arbitrary copy order) behavior.
  THHcTensor* oldA = NULL;
  THHcTensor* oldB = NULL;

  if (aType == ReadWrite && THHc_overlappingIndices(state, a)) {
    // Must perform in contiguous space
    oldA = a;
    a = THHcTensor_newContiguous(state, a);
  }
  if (bType == ReadWrite && THHc_overlappingIndices(state, b)) {
    // Must perform in contiguous space
    oldB = b;
    b = THHcTensor_newContiguous(state, b);
  }

  // It is possible that the tensor dimensions are able to be collapsed,
  // and thus we can reduce the actual code complexity of the copy by
  // exploiting this knowledge statically, since the div/mod is the
  // most expensive part of the operation, more so than memory accesses.
  // For instance, when copying a non-contiguous to a contiguous tensor
  // (or vice versa), the contiguous tensor can be collapsed to one
  // dimension, and the loop to translate the linear index to the array
  // index can be similarly collapsed. That is what this unrolling is for.
#define HANDLE_CASE(TYPE, A, B)                                \
  THHcTensor_pointwiseApply2<Op, TYPE, A, B>                 \
    (state,   \
      aInfo, bInfo, (TYPE) totalElements, op);

#define HANDLE_B_CASE(TYPE, A, B)                   \
  {                                                 \
    if (bInfo.isContiguous()) {                     \
      HANDLE_CASE(TYPE, A, -2);                     \
    } else {                                        \
      switch (B) {                                  \
        case 1:                                     \
          HANDLE_CASE(TYPE, A, 1);                  \
          break;                                    \
        case 2:                                     \
          HANDLE_CASE(TYPE, A, 2);                  \
          break;                                    \
        case 3:                                     \
          HANDLE_CASE(TYPE, A, 3);                  \
          break;                                    \
        default:                                    \
          HANDLE_CASE(TYPE, A, -1);                 \
          break;                                    \
      }                                             \
    }                                               \
  }

#define HANDLE_A_CASE(TYPE, A, B)                   \
  {                                                 \
    if (aInfo.isContiguous()) {                     \
      HANDLE_B_CASE(TYPE, -2, B);                   \
    } else {                                        \
      switch (A) {                                  \
        case 1:                                     \
          HANDLE_B_CASE(TYPE, 1, B);                \
          break;                                    \
        case 2:                                     \
          HANDLE_B_CASE(TYPE, 2, B);                \
          break;                                    \
        case 3:                                     \
          HANDLE_B_CASE(TYPE, 3, B);                \
          break;                                    \
        default:                                    \
          HANDLE_B_CASE(TYPE, -1, B);               \
          break;                                    \
      }                                             \
    }                                               \
  }

  if (THHc_canUse32BitIndexMath(state, a) &&
      THHc_canUse32BitIndexMath(state, b)) {
    TensorInfo<unsigned int> aInfo(state, a);
    aInfo.collapseDims();
    TensorInfo<unsigned int> bInfo(state, b);
    bInfo.collapseDims();

    HANDLE_A_CASE(unsigned int, aInfo.dims, bInfo.dims);
  } else {
    TensorInfo<unsigned long> aInfo(state, a);
    aInfo.collapseDims();
    TensorInfo<unsigned long> bInfo(state, b);
    bInfo.collapseDims();

    // For large tensors, we only compile the completely contiguous
    // version and the completely generic version, to reduce
    // compilation time.
    if (aInfo.isContiguous() && bInfo.isContiguous()) {
      THHcTensor_pointwiseApply2<Op, unsigned long, -2, -2>
        (state,
          aInfo, bInfo, (unsigned long) totalElements, op);
    } else {
      THHcTensor_pointwiseApply2<Op, unsigned long, -1, -1>
        (state,
          aInfo, bInfo, (unsigned long) totalElements, op);
    }
  }
#undef HANDLE_CASE
#undef HANDLE_B_CASE
#undef HANDLE_A_CASE

  if (oldA) {
    // Ignore overlaps when copying back; if we use THHcTensor_copy
    // instead, it will recursively try and invoke ourselves to make
    // oldA contiguous.
    THHcTensor_copyIgnoringOverlaps(state, oldA, a);
    THHcTensor_free(state, a);
    a = oldA;
  }

  if (oldB) {
    // Ignore overlaps when copying back; if we use THHcTensor_copy
    // instead, it will recursively try and invoke ourselves to make
    // oldB contiguous.
    THHcTensor_copyIgnoringOverlaps(state, oldB, b);
    THHcTensor_free(state, b);
    b = oldB;
  }

  return true;
}

template <typename Op>
bool THHcTensor_pointwiseApply3(THHcState* state,
                                  THHcTensor* a,
                                  THHcTensor* b,
                                  THHcTensor* c,
                                  const Op& op,
                                  TensorArgType aType = ReadWrite,
                                  TensorArgType bType = ReadOnly,
                                  TensorArgType cType = ReadOnly) {
  long totalElements = THHcTensor_nElement(state, a);

  if (totalElements != THHcTensor_nElement(state, b) ||
      totalElements != THHcTensor_nElement(state, c)) {
    return false;
  }

  if (THHcTensor_nDimension(state, a) > MAX_HCTORCH_DIMS ||
      THHcTensor_nDimension(state, b) > MAX_HCTORCH_DIMS ||
      THHcTensor_nDimension(state, c) > MAX_HCTORCH_DIMS) {
    return false;
  }

  if (THHcTensor_nDimension(state, a) == 0) {
    // Zero-dim tensor; do nothing
    return true;
  }

  // If tensor args have overlapping indices and are read/write, then
  // we must expand the tensor to a contiguous form first, since
  // otherwise there are conflicting writes. Upon copying back to the
  // non-contiguous form, there will be conflicting writes, but at
  // least with copy, one of the updaters will win atomically. This is
  // a sketchy property of the old system as well (writing into all
  // indices of a tensor with overlapping indices should probably be
  // an error, since it is unclear which one should win), but we will
  // preserve this last-writer-wins (in arbitrary copy order) behavior.
  THHcTensor* oldA = NULL;
  THHcTensor* oldB = NULL;
  THHcTensor* oldC = NULL;

  if (aType == ReadWrite && THHc_overlappingIndices(state, a)) {
    // Must perform in contiguous space
    oldA = a;
    a = THHcTensor_newContiguous(state, a);
  }

  if (bType == ReadWrite && THHc_overlappingIndices(state, b)) {
    // Must perform in contiguous space
    oldB = b;
    b = THHcTensor_newContiguous(state, b);
  }

  if (cType == ReadWrite && THHc_overlappingIndices(state, c)) {
    // Must perform in contiguous space
    oldC = c;
    c = THHcTensor_newContiguous(state, c);
  }

#define HANDLE_CASE(TYPE, A, B, C)                                      \
  THHcTensor_pointwiseApply3<Op, TYPE, A, B, C>                       \
    (state,             \
      aInfo, bInfo, cInfo, (TYPE) totalElements, op);

#define HANDLE_C_CASE(TYPE, A, B, C)             \
  {                                              \
    if (cInfo.isContiguous()) {                  \
      HANDLE_CASE(TYPE, A, B, -2);               \
    } else {                                     \
      switch (C) {                               \
        case 1:                                  \
          HANDLE_CASE(TYPE, A, B, 1);            \
          break;                                 \
        case 2:                                  \
          HANDLE_CASE(TYPE, A, B, 2);            \
          break;                                 \
        case 3:                                  \
          HANDLE_CASE(TYPE, A, B, 3);            \
          break;                                 \
        default:                                 \
          HANDLE_CASE(TYPE, A, B, -1);           \
          break;                                 \
      }                                          \
    }                                            \
  }

#define HANDLE_B_CASE(TYPE, A, B, C)                 \
  {                                                  \
    if (bInfo.isContiguous()) {                      \
      HANDLE_C_CASE(TYPE, A, -2, C);                 \
    } else {                                         \
      switch (B) {                                   \
        case 1:                                      \
          HANDLE_C_CASE(TYPE, A, 1, C);              \
          break;                                     \
        case 2:                                      \
          HANDLE_C_CASE(TYPE, A, 2, C);              \
          break;                                     \
        case 3:                                      \
          HANDLE_C_CASE(TYPE, A, 3, C);              \
          break;                                     \
        default:                                     \
          HANDLE_C_CASE(TYPE, A, -1, C);             \
          break;                                     \
      }                                              \
    }                                                \
  }

#define HANDLE_A_CASE(TYPE, A, B, C)                 \
  {                                                  \
    if (aInfo.isContiguous()) {                      \
      HANDLE_B_CASE(TYPE, -2, B, C);                 \
    } else {                                         \
      switch (A) {                                   \
        case 1:                                      \
          HANDLE_B_CASE(TYPE, 1, B, C);              \
          break;                                     \
        case 2:                                      \
          HANDLE_B_CASE(TYPE, 2, B, C);              \
          break;                                     \
        case 3:                                      \
          HANDLE_B_CASE(TYPE, 3, B, C);              \
          break;                                     \
        default:                                     \
          HANDLE_B_CASE(TYPE, -1, B, C);             \
          break;                                     \
      }                                              \
    }                                                \
  }

  if (THHc_canUse32BitIndexMath(state, a) &&
      THHc_canUse32BitIndexMath(state, b) &&
      THHc_canUse32BitIndexMath(state, c)) {
    TensorInfo<unsigned int> aInfo(state, a);
    aInfo.collapseDims();
    TensorInfo<unsigned int> bInfo(state, b);
    bInfo.collapseDims();
    TensorInfo<unsigned int> cInfo(state, c);
    cInfo.collapseDims();

    HANDLE_A_CASE(unsigned int, aInfo.dims, bInfo.dims, cInfo.dims);
  } else {
    TensorInfo<unsigned long> aInfo(state, a);
    aInfo.collapseDims();
    TensorInfo<unsigned long> bInfo(state, b);
    bInfo.collapseDims();
    TensorInfo<unsigned long> cInfo(state, c);
    cInfo.collapseDims();

    // For large tensors, we only compile the completely contiguous
    // version and the completely generic version, to reduce
    // compilation time.
    if (aInfo.isContiguous() && bInfo.isContiguous() && cInfo.isContiguous()) {
      THHcTensor_pointwiseApply3<Op, unsigned long, -2, -2, -2>
        (state,
          aInfo, bInfo, cInfo, (unsigned long) totalElements, op);
    } else {
      THHcTensor_pointwiseApply3<Op, unsigned long, -1, -1, -1>
        (state,
          aInfo, bInfo, cInfo, (unsigned long) totalElements, op);
    }
  }
#undef HANDLE_CASE
#undef HANDLE_C_CASE
#undef HANDLE_B_CASE
#undef HANDLE_A_CASE

  if (oldA) {
    // Ignore overlaps when copying back; if we use THHcTensor_copy
    // instead, it will recursively try and invoke ourselves to make
    // oldA contiguous.
    THHcTensor_copyIgnoringOverlaps(state, oldA, a);
    THHcTensor_free(state, a);
    a = oldA;
  }

  if (oldB) {
    // Ignore overlaps when copying back; if we use THHcTensor_copy
    // instead, it will recursively try and invoke ourselves to make
    // oldB contiguous.
    THHcTensor_copyIgnoringOverlaps(state, oldB, b);
    THHcTensor_free(state, b);
    b = oldB;
  }

  if (oldC) {
    // Ignore overlaps when copying back; if we use THHcTensor_copy
    // instead, it will recursively try and invoke ourselves to make
    // oldC contiguous.
    THHcTensor_copyIgnoringOverlaps(state, oldC, c);
    THHcTensor_free(state, c);
    c = oldC;
  }

  return true;
}

#undef THHC_APPLY_THREADS_PER_BLOCK

#endif // THC_APPLY_INC

