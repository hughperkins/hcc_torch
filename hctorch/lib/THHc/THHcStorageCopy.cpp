#include "THHcStorageCopy.h"
#include "THHcGeneral.h"


void THHcStorage_copy(THHcState* state, THHcStorage* self, THHcStorage* src) {
  THArgCheck(self->size == src->size, 2, "size does not match");
  THHcCheck(hc::am_copy(self->data, src->data, self->size * sizeof(float)));
}

void THHcStorage_copyHc(THHcState* state, THHcStorage* self, THHcStorage* src) {
  THHcStorage_copy(state, self, src);
}

