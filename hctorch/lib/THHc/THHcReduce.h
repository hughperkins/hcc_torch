#ifndef THHC_REDUCE_INC
#define THHC_REDUCE_INC
#include "THHcFunctorOpns.h"

/* Reduce one of the outer dimensions of a tensor
 *
 * For an n-d tensor (n <= 4) where the reduction is *not* along the innermost
 * dimension:
 *
 * - block.x and grid.x make up the innermost dimension;
 * - The reduced dimension is looped over inside a block; and
 * - grid.y and grid.z are the remaining two dimensions (if any).
 * - block.y and block.z are not used as we're limited to 512 or 1024 threads
 *   in the block.
 *
 * For sizes/strides, index 3 is the reduced dimension, while the remaining
 * indices are for the remaining dimensions with index 0 the innermost dimension.
 *
 * Reduction along the innermost dimension is handled in a separate kernel.
 */

template <class UnaryFunction, class BinaryFunction>
void THHcTensor_kernel_transformReduceOuterDim(THHcState* state, float* &avTgt, long tgtOffset,
    float* &avSrc, long srcOffset,
    unsigned int tgtSz, unsigned int srcSz,
    unsigned int* &avSrc_stride,
    unsigned int* &avTgt_stride,
    unsigned int* &avSize,
    UnaryFunction unary_op, float init,
    BinaryFunction binary_op, unsigned int gridConf[]) {
  const size_t reduce = 3;
  gridConf[0] = (gridConf[0] + 255) & ~255;
  hc::extent<3> grdExt(gridConf[2], gridConf[1], gridConf[0]);
  hc::tiled_extent<3> t_ext = grdExt.tile(1, 1, 256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<3> tidx) __attribute__((hc, cpu)) {
    for (unsigned z = tidx.tile[0]; z < avSize[2] ; z += grdExt[0] / tidx.tile_dim[0]) {
      for (unsigned y = tidx.tile[1]; y < avSize[1] ; y += grdExt[1] / tidx.tile_dim[1]) {
        for (unsigned col = tidx.global[2]; col < avSize[0]; col += grdExt[2]) {
          float acc = init;
          unsigned idx = z * avSrc_stride[2] + y * avSrc_stride[1] + col;  //moved the loop independent expression outside the loop
          unsigned i = 0;

          if (avSize[reduce] >= 8) { //Do loop unrolling if size in reduction dimension is above 8
            for (i = 0; i < avSize[reduce] / 8; i += 8) {
              acc = binary_op(acc, (avSrc[srcOffset + idx]));  //removed unary_op as it returns the passesd parameter itself
              idx += avSrc_stride[reduce];                     //replaced multiplication with addition
              acc = binary_op(acc, (avSrc[srcOffset + idx]));
              idx += avSrc_stride[reduce];
              acc = binary_op(acc, (avSrc[srcOffset + idx]));
              idx += avSrc_stride[reduce];
              acc = binary_op(acc, (avSrc[srcOffset + idx]));
              idx += avSrc_stride[reduce];
              acc = binary_op(acc, (avSrc[srcOffset + idx]));
              idx += avSrc_stride[reduce];
              acc = binary_op(acc, (avSrc[srcOffset + idx]));
              idx += avSrc_stride[reduce];
              acc = binary_op(acc, (avSrc[srcOffset + idx]));
              idx += avSrc_stride[reduce];
              acc = binary_op(acc, (avSrc[srcOffset + idx]));
              idx += avSrc_stride[reduce];
            }
          }

          //remaining iterations
          for (; i < avSize[reduce]; i++) {
            acc = binary_op(acc, (avSrc[srcOffset + idx]));  //adding all elemets of reduced outer dimension
            idx += avSrc_stride[reduce];
          }

          avTgt[tgtOffset + z * avTgt_stride[2] + y * avTgt_stride[1] + col] = float(acc);  //store shrunk value in reduced dimension
        }
      }
    }
  }).wait();
}

template <class UnaryFunction, class BinaryFunction>
void THHcTensor_transformReduceOuterDim(THHcState* state, THHcTensor* tgt, THHcTensor* src,
    long rdim, UnaryFunction unary_op,
    float init, BinaryFunction binary_op) {
  const size_t reduce = 3;
  hc::accelerator accl = state->deviceState->get_current_accelerator();
  unsigned int* src_stride = (unsigned int *) hc::am_alloc(4 * sizeof(unsigned int), accl, 1); 
  unsigned int* tgt_stride = (unsigned int *) hc::am_alloc(4 * sizeof(unsigned int), accl, 1); 
  unsigned int* size = (unsigned int *) hc::am_alloc(4 * sizeof(unsigned int), accl, 1);
  for (int i =0; i < 4; i++)
  {
     src_stride[i] = tgt_stride[i] = 0;
     size[i] = 1;
  } 
  unsigned int gridConfig[3];
  unsigned ndim = THHcTensor_nDimension(state, src);
  auto avTgt = tgt->get_device_data(state);
  auto avSrc = src->get_device_data(state);

  for (unsigned idim = 0, o = ndim - 2; idim < ndim; idim++) {
    unsigned odim = idim == rdim ? reduce : o--;
    src_stride[odim] = THHcTensor_stride(state, src, idim);
    tgt_stride[odim] = THHcTensor_stride(state, tgt, idim);
    size[odim] = THHcTensor_size(state, src, idim);
  }

  const unsigned nThreadPerBlock = 256;
  unsigned nBlockPerColumn = (size[0] + nThreadPerBlock - 1) / nThreadPerBlock;
  unsigned maxGridDim = 1024; // anything < 64k is fine. The choice has no impact on performance.
  gridConfig[0] = THMin(maxGridDim, nBlockPerColumn);
  gridConfig[1] = THMin(maxGridDim, size[1]);
  gridConfig[2] = THMin(maxGridDim, size[2]);
  THHcTensor_kernel_transformReduceOuterDim(state, avTgt, tgt->storageOffset,
      avSrc, src->storageOffset,
      THHcTensor_nElement(state, src), THHcTensor_nElement(state, tgt),
      src_stride, tgt_stride, size, unary_op, init, binary_op, gridConfig);
}

/* Reduce the innermost dimension of a tensor
 *
 * For an n-d tensor (n <= 4) where the reduction is along the innermost dimension:
 *
 * - block.x is the innermost dimension, i.e. dimension 0;
 * - block.y and grid.y make up dimension 1; and
 * - grid.x and grid z are the remaining two outer dimensions (if any)
 *
 * Reduction along other dimensions is handled in a separate kernel.
 */

template <class UnaryFunction, class BinaryFunction>
void THHcTensor_kernel_transformReduceInnermostDim(THHcState* state, float* &avTgt, long tgtOffset,
    float* &avSrc, long srcOffset,
    unsigned int tgtSz, unsigned int srcSz,
    unsigned int* &avSrc_stride,
    unsigned int* &avTgt_stride,
    unsigned int* &avSize,
    UnaryFunction unary_op, float init,
    BinaryFunction binary_op, unsigned int gridConf[]) {
  hc::extent<3> grdExt(gridConf[2], gridConf[1] * 8, gridConf[0] * 32);
  hc::tiled_extent<3> t_ext = grdExt.tile(1, 8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<3> tidx) __attribute__((hc, cpu)) {
    tile_static float sbuf[16][32]; // 8kB

    for (unsigned z = tidx.tile[0]; z < avSize[3] ; z += grdExt[0] / tidx.tile_dim[0]) {
      for (unsigned x = tidx.tile[2]; x < avSize[2] ; x += grdExt[2] / tidx.tile_dim[2]) {
        for (unsigned bRow = tidx.tile[1] * tidx.tile_dim[1]; bRow < avSize[1]; bRow += grdExt[1]) {
          float acc = init;
          unsigned row = bRow + tidx.local[1];
          bool reducing = tidx.local[2] < tidx.tile_dim[1] && bRow + tidx.local[2] < avSize[1] && tidx.local[1] == 0;

          for (unsigned bCol = 0; bCol < avSize[0]; bCol += tidx.tile_dim[2]) {
            sbuf[tidx.local[1]][tidx.local[2]] = init;
            unsigned col = bCol + tidx.local[2];

            if (row < avSize[1] && col < avSize[0]) {
              sbuf[tidx.local[1]][tidx.local[2]] = unary_op(avSrc[srcOffset + z * avSrc_stride[3] + x * avSrc_stride[2] + row * avSrc_stride[1] + col]);
            }

            tidx.barrier.wait();
            float* line = &sbuf[tidx.local[1]][0];

            for (unsigned s = 16; s > 1; s >>= 1) {
              if (row < avSize[1] && tidx.local[2] < s) {
                line[tidx.local[2]] = binary_op(line[tidx.local[2]], line[tidx.local[2] + s]);
              }

              tidx.barrier.wait();
            }

            if (reducing) {
              sbuf[tidx.local[2]][0] = binary_op(sbuf[tidx.local[2]][0], sbuf[tidx.local[2]][1]);
              acc = binary_op(acc, sbuf[tidx.local[2]][0]);
            }

            tidx.barrier.wait();
          }

          if (reducing) {
            unsigned row = bRow + tidx.local[2];
            unsigned tgt_offset = z * avTgt_stride[3] + x * avTgt_stride[2];
            avTgt[tgtOffset + tgt_offset + row] = acc;
          }
        }
      }
    }
  }).wait();
}

template <class UnaryFunction, class BinaryFunction>
void THHcTensor_transformReduceInnermostDim(THHcState* state, THHcTensor* tgt, THHcTensor* src,
    UnaryFunction unary_op, float init,
    BinaryFunction binary_op) {
  unsigned int gridConfig[3];
  hc::accelerator accl = state->deviceState->get_current_accelerator();
  unsigned int* src_stride = (unsigned int *) hc::am_alloc(4 * sizeof(unsigned int), accl, 1); 
  unsigned int* tgt_stride = (unsigned int *) hc::am_alloc(4 * sizeof(unsigned int), accl, 1); 
  unsigned int* size = (unsigned int *) hc::am_alloc(4 * sizeof(unsigned int), accl, 1);
  for (int i =0; i < 4; i++)
  {
     src_stride[i] = tgt_stride[i] = 0;
     size[i] = 1;
  } 
  unsigned ndim = THHcTensor_nDimension(state, src);
  auto avTgt = tgt->get_device_data(state);
  auto avSrc = src->get_device_data(state);

  for (unsigned dim = 0; dim < ndim; dim++) {
    unsigned odim = ndim - 1 - dim;
    src_stride[odim] = THHcTensor_stride(state, src, dim);
    tgt_stride[odim] = THHcTensor_stride(state, tgt, dim);
    size[odim] = THHcTensor_size(state, src, dim);
  }

  unsigned nBlockPerRow = (size[1] + 16 - 1) / 16;
  unsigned maxGridDim = 1024; // anything < 64k is fine. The choice has no impact on performance.
  gridConfig[0] = std::min(maxGridDim, size[2]);
  gridConfig[1] = std::min(maxGridDim, nBlockPerRow);
  gridConfig[2] = std::min(maxGridDim, size[3]);

  THHcTensor_kernel_transformReduceInnermostDim(state, avTgt, tgt->storageOffset,
      avSrc, src->storageOffset,
      THHcTensor_nElement(state, tgt), THHcTensor_nElement(state, src),
      src_stride, tgt_stride, size, unary_op, init,
      binary_op, gridConfig);
}

template <class UnaryFunction, class BinaryFunction>
void THHcTensor_transformReduceDim(THHcState* state, THHcTensor* self_, THHcTensor* src,
                                    long dimension, UnaryFunction unary_op,
                                    float init, BinaryFunction binary_op) {
  THArgCheck(dimension >= 0 && dimension < THHcTensor_nDimension(state, src), 3, "dimension out of range");
  // hcnn specific
  THArgCheck(THHcTensor_nDimension(state, src) <= 4, 2, "too many dimensions (>4)");
  // hcnn specific end
  THLongStorage* dim = THHcTensor_newSizeOf(state, src);
  THLongStorage_set(dim, dimension, 1);
  THHcTensor_resize(state, self_, dim, NULL);
  THLongStorage_free(dim);
  THHcTensor* self = THHcTensor_newContiguous(state, self_);
  src = THHcTensor_newContiguous(state, src);

  if (dimension == THHcTensor_nDimension(state, src) - 1) {
    THHcTensor_transformReduceInnermostDim(state, self, src, unary_op, init, binary_op);
  } else {
    THHcTensor_transformReduceOuterDim(state, self, src, dimension, unary_op, init, binary_op);
  }

  THHcTensor_free(state, src);
  THHcTensor_freeCopyTo(state, self, self_);
}

template <class BinaryFunction>
void THHcTensor_reduceDim(THHcState* state, THHcTensor* self_, THHcTensor* src, long dimension, float init, BinaryFunction binary_op) {
  THHcTensor_transformReduceDim(state, self_, src, dimension, identity<float>(), init, binary_op);
}




#endif

