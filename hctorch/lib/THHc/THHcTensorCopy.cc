#include "THHcTensorCopy.h"
#include "THHcGeneral.h"
#include "THHcTensor.h"

/* specific methods */
void THHcTensor_copyFloat(THHcState* state, THHcTensor* self, struct THFloatTensor* src) {
  THArgCheck(THHcTensor_nElement(state, self) == THFloatTensor_nElement(src), 2, "sizes do not match");
  THHcTensor* selfc = THHcTensor_newContiguous(state, self);
  src = THFloatTensor_newContiguous(src);
  THHcCheck(hc::am_copy(THHcTensor_data(state, selfc), THFloatTensor_data(src), THFloatTensor_nElement(src) * sizeof(float)));
  THFloatTensor_free(src);
  THHcTensor_freeCopyTo(state, selfc, self);
}

/* everything comes down to copy to a tensor of floats */
#define IMPLEMENT_TH_HC_TENSOR_COPY(TYPEC)                                                                  \
void THHcTensor_copy##TYPEC(THHcState *state, THHcTensor *self, struct TH##TYPEC##Tensor *src) {            \
  THArgCheck(THHcTensor_nElement(state, self) == TH##TYPEC##Tensor_nElement(src), 2, "sizes do not match"); \
  THLongStorage *size = TH##TYPEC##Tensor_newSizeOf(src);                                                    \
  THFloatTensor *srcf = THFloatTensor_newWithSize(size, NULL);                                               \
                                                                                                             \
  THFloatTensor_copy##TYPEC(srcf, src);                                                                      \
  THHcTensor_copyFloat(state, self, srcf);                                                                  \
                                                                                                             \
  THLongStorage_free(size);                                                                                  \
  THFloatTensor_free(srcf);                                                                                  \
}

IMPLEMENT_TH_HC_TENSOR_COPY(Byte)
IMPLEMENT_TH_HC_TENSOR_COPY(Char)
IMPLEMENT_TH_HC_TENSOR_COPY(Short)
IMPLEMENT_TH_HC_TENSOR_COPY(Int)
IMPLEMENT_TH_HC_TENSOR_COPY(Long)
IMPLEMENT_TH_HC_TENSOR_COPY(Double)

/* copyHc */
void THFloatTensor_copyHc(THHcState* state, THFloatTensor* self, struct THHcTensor* src) {
  THArgCheck(THFloatTensor_nElement(self) == THHcTensor_nElement(state, src), 2, "sizes do not match");
  THFloatTensor* selfc = THFloatTensor_newContiguous(self);
  src = THHcTensor_newContiguous(state, src);
  THHcCheck(hc::am_copy(THFloatTensor_data(selfc), THHcTensor_data(state, src), THHcTensor_nElement(state, src) * sizeof(float)));
  THHcTensor_free(state, src);
  THFloatTensor_freeCopyTo(selfc, self);
}

#define IMPLEMENT_TH_HC_TENSOR_COPY_TO(TYPEC)                                                               \
void TH##TYPEC##Tensor_copyHc(THHcState *state, TH##TYPEC##Tensor *self, struct THHcTensor *src) {          \
  THArgCheck(TH##TYPEC##Tensor_nElement(self) == THHcTensor_nElement(state, src), 2, "sizes do not match"); \
  THLongStorage *size = THHcTensor_newSizeOf(state, src);                                                   \
  THFloatTensor *srcf = THFloatTensor_newWithSize(size, NULL);                                               \
                                                                                                             \
  THFloatTensor_copyHc(state, srcf, src);                                                                   \
  TH##TYPEC##Tensor_copyFloat(self, srcf);                                                                   \
                                                                                                             \
  THLongStorage_free(size);                                                                                  \
  THFloatTensor_free(srcf);                                                                                  \
}

IMPLEMENT_TH_HC_TENSOR_COPY_TO(Byte)
IMPLEMENT_TH_HC_TENSOR_COPY_TO(Char)
IMPLEMENT_TH_HC_TENSOR_COPY_TO(Short)
IMPLEMENT_TH_HC_TENSOR_COPY_TO(Int)
IMPLEMENT_TH_HC_TENSOR_COPY_TO(Long)
IMPLEMENT_TH_HC_TENSOR_COPY_TO(Double)

void THHcTensor_copyHc(THHcState* state, THHcTensor* self, THHcTensor* src) {
  THHcTensor_copy(state, self, src);
}


