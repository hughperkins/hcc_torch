#include "torch/utils.h"
#include "THHc.h"
#include "THFile.h"
#include "luaT.h"

/* everything is as the generic Storage.c, except few things (see below) */

#define real float
#define Real Hc
#define TH_GENERIC_FILE "generic/Storage.c"

#define torch_Storage_(NAME) TH_CONCAT_4(torch_,Real,Storage_,NAME)
// Note that 'fdata' is on host
#define THFile_readRealRaw(file, data, size) {                                                                            \
  float *fdata = (float*)THAlloc(sizeof(float) * size);                                                                   \
  THFile_readFloatRaw(file, fdata, size);                                                                                 \
  THHcCheck(hc::am_copy(data, fdata, size * sizeof(float)));                                                              \
  THFree(fdata);                                                                                                          \
}

#define THFile_writeRealRaw(file, data, size) {                                                                           \
  float *fdata = (float *)THAlloc(sizeof(float) * size);                                                                  \
  THHcCheck(hc::am_copy(fdata, data, size * sizeof(float)));                                                             \
  THFile_writeFloatRaw(file, fdata, size);                                                                                \
  THFree(fdata);                                                                                                          \
}

#define torch_Storage TH_CONCAT_STRING_3(torch.,Real,Storage)

#include "generic/Storage.c"

#undef real
#undef Real
#undef TH_GENERIC_FILE

/* now we overwrite some methods specific to HcStorage */

static int hctorch_HcStorage_copy(lua_State* L) {
  THHcState* state = hctorch_getstate(L);
  THHcStorage* storage = (THHcStorage*)luaT_checkudata(L, 1, "torch.HcStorage");
  void* src;
  
  if ( (src = (THHcStorage*)luaT_toudata(L, 2, "torch.HcStorage")) ) {
    THHcStorage_copy(state, storage, (THHcStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.ByteStorage")) ) {
    THHcStorage_copyByte(state, storage, (THByteStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.CharStorage")) ) {
    THHcStorage_copyChar(state, storage, (THCharStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.ShortStorage")) ) {
    THHcStorage_copyShort(state, storage, (THShortStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.IntStorage")) ) {
    THHcStorage_copyInt(state, storage, (THIntStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.LongStorage")) ) {
    THHcStorage_copyLong(state, storage, (THLongStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.FloatStorage")) ) {
    THHcStorage_copyFloat(state, storage, (THFloatStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.DoubleStorage")) ) {
    THHcStorage_copyDouble(state, storage, (THDoubleStorage*)src);
  } else if ( (src = luaT_toudata(L, 2, "torch.HcStorage")) ) {
    THHcStorage_copyHc(state, storage, (THHcStorage*)src);
  } else {
    luaL_typerror(L, 2, "torch.*Storage");
  }
  
  lua_settop(L, 1);
  return 1;
}

#define HC_IMPLEMENT_STORAGE_COPY(TYPEC)                                                               \
  static int hctorch_##TYPEC##Storage_copy(lua_State *L) {                                             \
  TH##TYPEC##Storage *storage = (TH##TYPEC##Storage *)luaT_checkudata(L, 1, "torch." #TYPEC "Storage"); \
  void *src;                                                                                            \
  if ( (src = luaT_toudata(L, 2, "torch." #TYPEC "Storage")) )                                          \
    TH##TYPEC##Storage_copy(storage, (TH##TYPEC##Storage *)src);                                        \
  else if ( (src = luaT_toudata(L, 2, "torch.ByteStorage")) )                                           \
    TH##TYPEC##Storage_copyByte(storage, (THByteStorage *)src);                                         \
  else if ( (src = luaT_toudata(L, 2, "torch.CharStorage")) )                                           \
    TH##TYPEC##Storage_copyChar(storage, (THCharStorage *)src);                                         \
  else if ( (src = luaT_toudata(L, 2, "torch.ShortStorage")) )                                          \
    TH##TYPEC##Storage_copyShort(storage, (THShortStorage *)src);                                       \
  else if ( (src = luaT_toudata(L, 2, "torch.IntStorage")) )                                            \
    TH##TYPEC##Storage_copyInt(storage, (THIntStorage *)src);                                           \
  else if ( (src = luaT_toudata(L, 2, "torch.LongStorage")) )                                           \
    TH##TYPEC##Storage_copyLong(storage, (THLongStorage *)src);                                         \
  else if ( (src = luaT_toudata(L, 2, "torch.FloatStorage")) )                                          \
    TH##TYPEC##Storage_copyFloat(storage, (THFloatStorage *)src);                                       \
  else if ( (src = luaT_toudata(L, 2, "torch.DoubleStorage")) )                                         \
    TH##TYPEC##Storage_copyDouble(storage, (THDoubleStorage*)src);                                      \
  else if ( (src = luaT_toudata(L, 2, "torch.HcStorage")) )                                            \
    TH##TYPEC##Storage_copyHc(hctorch_getstate(L), storage, (THHcStorage *)src);                     \
  else                                                                                                  \
    luaL_typerror(L, 2, "torch.*Storage");                                                              \
                                                                                                        \
  lua_settop(L, 1);                                                                                     \
  return 1;                                                                                             \
}

HC_IMPLEMENT_STORAGE_COPY(Byte)
HC_IMPLEMENT_STORAGE_COPY(Char)
HC_IMPLEMENT_STORAGE_COPY(Short)
HC_IMPLEMENT_STORAGE_COPY(Int)
HC_IMPLEMENT_STORAGE_COPY(Long)
HC_IMPLEMENT_STORAGE_COPY(Float)
HC_IMPLEMENT_STORAGE_COPY(Double)

void hctorch_HcStorage_init(lua_State* L) {
  /* the standard stuff */
  torch_HcStorage_init(L);
  /* the copy methods */
  int i;
  const char* tnames[8] = {"torch.ByteStorage",
                           "torch.CharStorage",
                           "torch.ShortStorage",
                           "torch.IntStorage",
                           "torch.LongStorage",
                           "torch.FloatStorage",
                           "torch.DoubleStorage",
                           "torch.HcStorage"
                          };
  static int (*funcs[8])(lua_State*) = {hctorch_ByteStorage_copy,
                                        hctorch_CharStorage_copy,
                                        hctorch_ShortStorage_copy,
                                        hctorch_IntStorage_copy,
                                        hctorch_LongStorage_copy,
                                        hctorch_FloatStorage_copy,
                                        hctorch_DoubleStorage_copy,
                                        hctorch_HcStorage_copy
                                       };
                                       
  for(i = 0; i < 8; i++) {
    luaT_pushmetatable(L, tnames[i]);
    lua_pushcfunction(L, funcs[i]);
    lua_setfield(L, -2, "copy");
    lua_pop(L, 1);
  }
}

