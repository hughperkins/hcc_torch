# ** HCC backend Implementation for Torch7 ** #

##Introduction: ##

This repository hosts the HCC backend implementation project for  [torch7](http://torch.ch/). To know what HCC is please refer [here](https://bitbucket.org/multicoreware/hcc/wiki/Home). Torch7 framework currently has a CUDA backend support in the form of [cutorch](https://github.com/torch/cutorch) and [cunn](https://github.com/torch/cunn) packages. The goal of this project is to develop  hctorch and hcnn packages that would functionally behave as  HCC counterparts for existing cutorch and cunn packages. This project mainly targets the linux platform and makes use of the linux-based HCC compiler implementation hosted [here](https://bitbucket.org/multicoreware/hcc/wiki/Home). 

##Prerequisites: ##

**Hardware Requirements:**

* CPU: mainstream brand, Better if with >=4 Cores Intel Haswell based CPU 
* System Memory >= 4GB (Better if >10GB for NN application over multiple GPUs)
* Hard Drive > 200GB (Better if SSD or NVMe driver  for NN application over multiple GPUs)
* Minimum GPU Memory (Global) > 2GB

**GPU SDK and driver Requirements:**

* dGPUs: AMD R9 Fury X, R9 Fury, R9 Nano
* APUs: AMD APU Kaveri or Carrizo

**System software requirements:**

* Ubuntu 14.04 trusty
* GCC 4.6 and later
* CPP 4.6 and later (come with GCC package)
* python 2.7 and later
* HCC 0.9 from [here](https://bitbucket.org/multicoreware/hcc/downloads/hcc-0.9.16041-0be508d-ff03947-5a1009a-Linux.deb)


**Tools and Misc Requirements:**

* git 1.9 and later
* cmake 2.6 and later (2.6 and 2.8 are tested)
* firewall off
* root privilege or user account in sudo group


**Ubuntu Packages requirements:**

* libc6-dev-i386
* liblapack-dev
* graphicsmagick
* libboost-all-dev
* lua5.1


## Tested Environment so far: 

This section enumerates the list of tested combinations of Hardware and system software

**GPU Cards tested:**

* Radeon R9 Nano
* Radeon R9 FuryX 
* Radeon R9 Fury 
* Kaveri and Carizo APU

**Driver versions tested**  

* Boltzmann Early Release Driver for dGPU

   ROCM 1.0 Release : https://github.com/RadeonOpenCompute/ROCm/blob/master/README.md
     
* Traditional HSA driver for APU (Kaveri)

**Desktop System Tested**

* Supermicro SYS-7048GR-TR  Tower 4 R9 Nano
* ASUS X99-E WS motherboard with 4 R9 Nano
* Gigabyte GA-X79S 2 AMD R9 Nano

**Server System Tested**

* Supermicro SYS 2028GR-THT  6 R9 NANO
* Supermicro SYS-1028GQ-TRT 4 R9 NANO
* Supermicro SYS-7048GR-TR Tower 4 R9 NANO
 

## Installation Steps: 

A. ROCM 1.0 Installation (If not done so far)

B. HCBLAS Library Installation 

C. HCC_TORCH Installation (involving hctorch and hcnn packages)


## Installation Steps in detail:

### A. ROCM 1.0 Installation: 

  To Know more about ROCM  refer https://github.com/RadeonOpenCompute/ROCm/blob/master/README.md

  a. Installing Debian ROCM repositories
     
  Before proceeding, make sure to completely uninstall any pre-release ROCm packages
     
  Refer https://github.com/RadeonOpenCompute/ROCm#removing-pre-release-packages for instructions to remove pre-release ROCM packages
     
  Steps to install rocm package are 
     
      * wget -qO - http://packages.amd.com/rocm/apt/debian/rocm.gpg.key | sudo apt-key add -
      
      * sudo sh -c 'echo deb [arch=amd64] http://packages.amd.com/rocm/apt/debian/ trusty main > /etc/apt/sources.list.d/rocm.list'
     
      * sudo apt-get update
      
      * sudo apt-get install rocm
      
      * Reboot the system
      
  b. Once Reboot, verify the installation
    
  To verify that the ROCm stack completed successfully you can execute to HSA vector_copy sample application:

       * cd /opt/rocm/hsa/sample
        
       * make
       
       * ./vector_copy

### B. HCBLAS library Installation

Install using Prebuilt debian

        * wget https://bitbucket.org/multicoreware/hcblas/downloads/hcblas-master-db04c54-Linux.deb
                
        * sudo dpkg -i hcblas-master-db04c54-Linux.deb

      
### C. HCC_TORCH Installation (includes hctorch and hcnn package installation)

(i) Install Prerequisites for Torch: 

Need sudo access for this step

       sudo curl -sk https://raw.githubusercontent.com/torch/ezinstall/master/install-deps | bash

(ii) Clone MCW Torch7 source codes

      * cd ~/

      * git clone https://bitbucket.org/multicoreware/hcc_torch.git

      * cd ~/hcc_torch

      * git checkout origin/master


(iii) Run one time build script

      * cd ~/hcc_torch

      * export CLAMP_NOTILECHECK=ON   

      * sudo apt-get install lua5.1

      * chmod 777 ./install.sh

      *./install.sh

(iv) Make sure to restart the machine or use another terminal so as to reflect the environment variable settings.

(v) Run th command to invoke Torch IDE. You should see a prompt as below

      torchtest@mcw:~$ th
 
       ______             __   |  Torch7                                         
      /_  __/__  ________/ /   |  Scientific computing for Lua. 
       / / / _ \/ __/ __/ _ \  |  Type ? for help                                
      /_/  \___/_/  \__/_//_/  |  https://github.com/torch         
                               |  http://torch.ch                  
	
      th> 



## C. Unit Testing ##

Run the following commands to perform unit testing of different rocks.

(a) hctorch: 

       th -lhctorch -e "hctorch.test()"        

(b) hcnn:

       th -lhcnn -e "nn.testhc()"



(c) mcw_nn:

       th -lnn -e "nn.test()"