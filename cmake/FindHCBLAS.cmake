# http://www.cmake.org/Wiki/CMake:How_To_Find_Libraries

# - Try to find HCBLAS library
# Once done this will define
#  HCBLAS_FOUND - System has HCBLAS
#  HCBLAS_HEADER_PATH= path to header files
#  HCBLAS_LIBRARY_PATH= path to library files


if( MSVC OR APPLE)
  message(FATAL_ERROR "Unsupported platform.")
endif()

if(EXISTS "/opt/rocm/hcblas")
  find_path(HCBLAS_HEADER_PATH hcblas.h
           HINTS /opt/rocm/hcblas/include)
  find_path(HCBLAS_LIBRARY_PATH libhcblas.so
           HINTS /opt/rocm/hcblas/lib)
  include(FindPackageHandleStandardArgs)
  # handle the QUIETLY and REQUIRED arguments and set HCBLAS_FOUND to TRUE
  # if all listed variables are TRUE
  find_package_handle_standard_args(HCBLAS DEFAULT_MSG
                                    HCBLAS_LIBRARY_PATH HCBLAS_HEADER_PATH)
  mark_as_advanced(HCBLAS_HEADER_PATH HCBLAS_LIBRARY_PATH)
  if (HCBLAS_FOUND)
    message(STATUS "HCBLAS library found in ${HCBLAS_LIBRARY_PATH}/..")
  elseif()
    message(FATAL_ERROR "HCBLAS not found.")
  endif()

elseif(EXISTS "${CMAKE_SOURCE_DIR}/../extern/opt/rocm/hcblas")
  set(hcblas_path "${CMAKE_SOURCE_DIR}/../extern/opt/rocm/hcblas")
  find_path(HCBLAS_HEADER_PATH hcblas.h
           HINTS ${hcblas_path}/include)
  find_path(HCBLAS_LIBRARY_PATH libhcblas.so
           HINTS ${hcblas_path}/lib)
  include(FindPackageHandleStandardArgs)
  # handle the QUIETLY and REQUIRED arguments and set HCBLAS_FOUND to TRUE
  # if all listed variables are TRUE
  find_package_handle_standard_args(HCBLAS DEFAULT_MSG
                                    HCBLAS_LIBRARY_PATH HCBLAS_HEADER_PATH)
  mark_as_advanced(HCBLAS_HEADER_PATH HCBLAS_LIBRARY_PATH)
  if (HCBLAS_FOUND)
    message(STATUS "HCBLAS library found in ${HCBLAS_LIBRARY_PATH}")
  elseif()
    message(FATAL_ERROR "HCBLAS not found.")
  endif()

else()
    message(FATAL_ERROR "HCBLAS not found.")
endif()




