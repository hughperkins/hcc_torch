/************************************************************************************************
 * AbsCriterion
 *
 * criterion = nn.AbsCriterion()
 * Functionality : Measures the mean absolute value of the element-wise difference between input;
 *                 Creates a criterion that measures the mean absolute value of the element-wise
 *                 difference between input x and target y:
 *                 loss(x, y)  = 1/n \sum |x_i - y_i|
 *
 * If x and y are d-dimensional Tensors with a total of n elements,
 * the sum operation still operates over all the elements, and divides by n.
 *
 **************************************************************************************************/

#include <iostream>
#include <vector>
#include <numeric>
#include "utils.h"
#include "amp_math.h"
#include "THHcFunctorOpns.h"

static int hcnn_AbsCriterion_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  float sum;
  long size = THHcTensor_nElement(state, input);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  sum = InnerProduct_plus_abs(state, input, target);
  
  if(sizeAverage) {
    sum /= size;
  }
  
  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  lua_pushnumber(L, sum);
  lua_setfield(L, 1, "output");
  lua_pushnumber(L, sum);
  return 1;
}

static int hcnn_AbsCriterion_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  long size = THHcTensor_nElement(state, input);
  float norm = (sizeAverage ? 1. / size : 1.);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  THHcTensor_resizeAs(state, gradInput, input);
  transform_abs(state, input, target, gradInput, norm);
  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  return 1;
}

static const struct luaL_Reg hcnn_AbsCriterion__ [] = {
  {"AbsCriterion_updateOutput", hcnn_AbsCriterion_updateOutput},
  {"AbsCriterion_updateGradInput", hcnn_AbsCriterion_updateGradInput},
  {NULL, NULL}
};

/* Binding the AbsCriterion layer routines with lua stack */
void hcnn_AbsCriterion_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_AbsCriterion__, "nn");
  lua_pop(L, 1);
}
