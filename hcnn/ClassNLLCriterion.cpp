/************************************************************************************************
 * ClassNLLCriterion
 *
 * criterion = nn.ClassNLLCriterion([weights])
 * Description : The negative log likelihood criterion. It is useful to train a classication
 *               problem with n classes. If provided, the optional argument weights should be
 *               a 1D Tensor assigning weight to each of the classes. This is particularly useful
 *               when you have an unbalanced training set.
 *
 * The input given through a forward() is expected to contain log-probabilities of
 * each class: input has to be a 1D Tensor of size n. Obtaining log-probabilities
 * in a neural network is easily achieved by adding a LogSoftMax layer in the last layer
 * & of your neural network. You may use CrossEntropyCriterion instead, if you prefer not
 * to add an extra layer to your network. This criterion expect a class index
 * (1 to the number of class) as target when calling forward(input, target) and
 * backward(input, target)
 *
 * The loss can be described as:
 *               loss(x, class) = -x[class]
 * or in the case of the weights argument being specified:
 *               loss(x, class) = -weights[class] * x[class]
 *
 **************************************************************************************************/
#include <stdio.h>
#include<assert.h>
#include "utils.h"
static const int NTHREADS = 32;

void hcnn_ClassNLLCriterion_updateOutput_kernel1(THHcState* state, float* &avOutput, long outOffset,
    float* &avInput, long inOffset,
    float* &avTarget, long targetOffset,
    int ntarget) {
  hc::extent<1> grdExt(1);
  hc::tiled_extent<1> t_ext = grdExt.tile(1);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu)) {
    // TODO: T4951791 Reuse code between updateOutput_kernel1 and
    // updateOutput_kernel.
    // Verify whether `register` does anything here.
    register int i, t;

    for (i = 0; i < ntarget; i++) {
      t = avTarget[targetOffset + i] - 1;

      if (t >= 0) {
        avOutput[outOffset + 0] = -avInput[inOffset + t];
      }
    }
  }).wait();
}

void hcnn_ClassNLLCriterion_updateOutput_kernel(THHcState* state, float* &avOutput, long outOffset,
    float* &avInput, long inOffset,
    float* &avTarget, long targetOffset,
    int nframe, int ndim, int sizeAverage, int ntarget) {
  hc::extent<1> grdExt(1 * 32);
  hc::tiled_extent<1> t_ext = grdExt.tile(32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu)) {
    tile_static float shInputs[NTHREADS];
    // Verify whether `register` does anything here.
    register int i, j, t;
    shInputs[tidx.local[0]] = .0;

    for (i = tidx.local[0]; i < nframe; i += NTHREADS) {
      for (j = 0; j < ntarget; ++j) {
        t = (int)avTarget[targetOffset + i * ntarget + j] - 1;

        if (t >= 0) {
          shInputs[tidx.local[0]] += avInput[inOffset + i * ndim + t];
        }
      }
    }

    tidx.barrier.wait();

    // TODO: T4951791 Reuse code between updateOutput_kernel1 and
    // updateOutput_kernel
    if (tidx.local[0] == 0) {
      avOutput[outOffset] = .0;

      for (i = 0; i < NTHREADS; ++i) {
        avOutput[outOffset] += shInputs[i];
      }

      if (sizeAverage) {
        avOutput[outOffset] /= nframe;
      }

      avOutput[outOffset] = -(avOutput[outOffset]);
    }
  }).wait();
}

void hcnn_ClassNLLCriterion_updateGradInput_kernel(THHcState* state, float* &avGradInput, long gradInOffset,
    float* &avTarget, long targetOffset,
    int nframe, int ndim, float grad, int ntarget) {
  hc::extent<1> grdExt(1 * 32);
  hc::tiled_extent<1> t_ext = grdExt.tile(32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu)) {
    register int i, j, t;

    for (i = tidx.local[0]; i < nframe; i += NTHREADS) {
      for (j = 0; j < ntarget; ++j) {
        t = (int)avTarget[targetOffset + i * ntarget + j] - 1;

        if (t >= 0) {
          avGradInput[gradInOffset + i * ndim + t] = grad;
        }
      }
    }
  }).wait();
}

static int hcnn_ClassNLLCriterion_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  target = THHcTensor_newContiguous(state, target);
  int ntarget = 1;

  if (target->nDimension > 1) {
    ntarget = target->size[1];
  }

  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "outputTensor", torch_Tensor);
  output = THHcTensor_newContiguous(state, output);
  auto avOutput = output->get_device_data(state);
  auto avInput = input->get_device_data(state);
  auto avTarget = target->get_device_data(state);

  if (input->nDimension == 1) {
    hcnn_ClassNLLCriterion_updateOutput_kernel1(state, avOutput, output->storageOffset,
        avInput, input->storageOffset,
        avTarget, target->storageOffset, ntarget);
  } else if (input->nDimension == 2) {
    int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
    hcnn_ClassNLLCriterion_updateOutput_kernel (state, avOutput, output->storageOffset,
        avInput, input->storageOffset,
        avTarget, target->storageOffset,
        input->size[0], input->size[1],
        sizeAverage, ntarget);
  } else {
    THArgCheck(0, 2, "vector or matrix expected");
  }

  THHcTensor_free(state, output);
  THHcTensor_free(state, target);
  THHcTensor_free(state, input);
  return 1;
}

static int hcnn_ClassNLLCriterion_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  target = THHcTensor_newContiguous(state, target);
  int ntarget = 1;

  if (target->nDimension > 1) {
    ntarget = target->size[1];
  }

  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata( L, 1, "gradInput", torch_Tensor);
  gradInput = THHcTensor_newContiguous(state, gradInput);
  auto avGradInput = gradInput->get_device_data(state);
  auto avTarget = target->get_device_data(state);
  float grad = -1.0;

  if (input->nDimension == 1) {
    if (ntarget > 1) {
      THArgCheck(0, 2, "multi-target not implemented");
    }

    float tid;
    auto avSrc = target->get_device_data(state);
    auto Src = avSrc + target->storageOffset;
    THHcCheck(hc::am_copy(&tid, Src, sizeof(float)));
    auto avDst = gradInput->get_device_data(state);
    auto Dst = avDst + gradInput->storageOffset + (int)tid - 1;
    THHcCheck(hc::am_copy(Dst, &grad, sizeof(float)));
  } else if (input->nDimension == 2) {
    int nframe = input->size[0];
    int ndim = input->size[1];
    int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");

    if (sizeAverage) {
      grad /= nframe;
    }

    hcnn_ClassNLLCriterion_updateGradInput_kernel(state, avGradInput, gradInput->storageOffset,
        avTarget, target->storageOffset,
        nframe, ndim, grad, ntarget);
  } else {
    THArgCheck(0, 2, "vector or matrix expected");
  }

  THHcTensor_free(state, gradInput);
  THHcTensor_free(state, target);
  THHcTensor_free(state, input);
  return 1;
}

static const struct luaL_Reg hcnn_ClassNLLCriterion__[] = {
  {"ClassNLLCriterion_updateOutput", hcnn_ClassNLLCriterion_updateOutput},
  {"ClassNLLCriterion_updateGradInput", hcnn_ClassNLLCriterion_updateGradInput},
  {NULL, NULL}
};

/* Binding the ClassNLLCriterion layer routines with lua stack */
void hcnn_ClassNLLCriterion_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_ClassNLLCriterion__, "nn");
  lua_pop(L, 1);
}
