/*************************************************************************************************************************************
 *SpatialConvolution Matrix Multiplication Based
 *
 * module = nn.SpatialConvolutionMM(nInputPlane, nOutputPlane, kW, kH, [dW], [dH], [padW], [padH])
 *
 * Functionality: Applies a 2D convolution over an input image composed of several input planes.
 *                The input tensor in forward(input) is expected to be a 3D tensor (nInputPlane x height x width).
 *
 * The parameters are the following:
 *                nInputPlane: The number of expected input planes in the image given into forward().
 *                nOutputPlane: The number of output planes the convolution layer will produce.
 *                kW: The kernel width of the convolution
 *                kH: The kernel height of the convolution
 *                dW: The step of the convolution in the width dimension. Default is 1.
 *                dH: The step of the convolution in the height dimension. Default is 1.
 *                padW: The additional zeros added per width to the input planes. Default is 0, a good number is (kW-1)/2.
 *                padH: The additional zeros added per height to the input planes. Default is padW, a good number is (kH-1)/2.
 *
 * Note that depending of the size of your kernel, several (of the last) columns or rows of the input image might be lost.
 * It is up to the user to add proper padding in images.
 *
 * If the input image is a 3D tensor nInputPlane x height x width, the output image size will be nOutputPlane x oheight x owidth where
 *                owidth  = floor((width  + 2*padW - kW) / dW + 1)
 *                oheight = floor((height + 2*padH - kH) / dH + 1)
 *
 * The parameters of the convolution can be found in self.weight (Tensor of size nOutputPlane x nInputPlane x kH x kW)
 * and self.bias (Tensor of size nOutputPlane). The corresponding gradients can be found in self.gradWeight and self.gradBias.
 * The output value of the layer can be precisely described as:
 * output[i][j][k] = bias[k] + sum_l sum_{s=1}^kW sum_{t=1}^kH weight[s][t][l][k] * input[dW*(i-1)+s)][dH*(j-1)+t][l]
 *
 *************************************************************************************************************************************/
#include "utils.h"
#include "THHcGeneral.h"
#include "THHcTensorMath.h"
#include "THHcTensor.h"
#include "hcblaslib.h"

#define NUMTHREADS 256

/* Function that decides the application of  batch optimization  depending on
 * available device memory
 */
static inline bool can_apply_opt(THHcState* state, unsigned long size) {
  THHcDeviceState* device_state = state->deviceState;
  auto acc = device_state->get_current_accelerator();
  return ((acc.get_dedicated_memory() * 1024) > size);
}

/* im2col Kernel */
// Kernel for fast unfold+copy
// (borrowed from Caffe: https://github.com/BVLC/caffe/blob/master/src/caffe/layers/conv_layer.cu)

/* Functionality: Rearrange blocks from matrix (here image) into columns.
 *
 * Input Parameters:
 *       state: Encapsulates target device accelerator info
 *       avData_im : Input Image
 *       inOffset: Input Image offset from wherein the data shall be transformed to column
 *       height & width: Dimensions of the input image
 *       channels: Numbers of input image chanels. For eg In case of RGB, channels=3
 *       ksize_w & ksize_h : Width and Height of the block.
 *       pad_w & pad_h: Padding width or Padding height.
 *                      Utilized when transformation is done for distinct ksize_w x ksize_h blocks of input image.
 *       stride_w & stride_h: Dimensions  to control the sliding of ksize_w x ksize_h block within input image
 *       colOffset: Column offset from wherein the output column vectors are written out.
 *
 * Output Parameter:
 * avData_col: Output Column matrix
 */
void im2col(THHcState* state, float* &avData_im, long imOffset,
            int channels, int height, int width, int ksize_h,
            int ksize_w, int pad_h, int pad_w, int stride_h, int stride_w,
            float* &avData_col, long colOffset) {
  int height_col = (height + 2 * pad_h - ksize_h) / stride_h + 1;
  int width_col = (width + 2 * pad_w - ksize_w) / stride_w + 1;
  int n = channels * height_col * width_col;
  unsigned grdSz = (n + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1);
  hc::extent<2> grdExt(grdSz, ksize_h);
  hc::tiled_extent<2> t_ext = grdExt.tile(NUMTHREADS, 1);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    long dataCol = colOffset;
    long dataIm = imOffset;
    int i = tidx.global[0];
    int p = tidx.global[1];

    if(i < n && p < ksize_h) {
      int w_out = i % width_col;
      i /= width_col;
      int h_out = i % height_col;
      int channel_in = i / height_col;
      int channel_out = channel_in * ksize_h * ksize_w;
      int h_in = h_out * stride_h - pad_h;
      int w_in = w_out * stride_w - pad_w;
      dataCol += (channel_out * height_col + h_out) * width_col + w_out;
      dataIm += (channel_in * height + h_in) * width + w_in ;
      dataCol += height_col * width_col * ksize_w * p;

      for (int j = 0; j < ksize_w; ++j) {
        int h = h_in + p;
        int w = w_in + j;
        avData_col[dataCol] = (h >= 0 && w >= 0 && h < height && w < width) ? avData_im[ dataIm + p * width + j] : 0;
        dataCol += height_col * width_col;
      }
    }
  }).wait();
}

/* im2col_batch kernel */
/*
 * Functionality same as im2col. This kernel is an optimization attempt to avoid loop invocation of im2col kernels. Rather
 * can use a single batch kernel with the loop iteration managed by the third grid dimension

 * Input Parameters:
 *       state: Encapsulates target device accelerator info
 *       avData_im : Input Image
 *       height & width: Dimensions of the input image
 *       inOffset: Input Image offset from wherein the data shall be transformed to column
 *       channels: Numbers of input image chanels. For eg In case of RGB, channels=3
 *       ksize_w & ksize_h : Width and Height of the block.
 *       pad_w & pasy_h: Padding width or Padding height.
 *                       Utilized when transformation is done for distinct ksize_w x ksize_h blocks of input image.
 *       stride_w & stride_h: Dimensions  to control the sliding of ksize_w x ksize_h block within input image
 *       colOffset: Column offset from wherein the output column vectors are written out.
 *       im_batchOffset and col_batchOffset: Offset values while dealing the input image and output columns in batches
 *       batchSize = size of the batch
 *
 * Output Parameter:
 *       avData_col: Output Column matrix
 */
void im2col_batch(THHcState* state, float* &avData_im, long imOffset, long im_batchOffset,
                  int channels, int height, int width, int ksize_h,
                  int ksize_w, int pad_h, int pad_w, int stride_h, int stride_w,
                  float* &avData_col, long colOffset, long col_batchOffset, int batchSize) {
  int height_col = (height + 2 * pad_h - ksize_h) / stride_h + 1;
  int width_col = (width + 2 * pad_w - ksize_w) / stride_w + 1;
  int n = channels * height_col * width_col;
  unsigned grdSz = (n + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1);
  hc::extent<3> grdExt(batchSize, grdSz, ksize_h);
  hc::tiled_extent<3> t_ext = grdExt.tile(1, NUMTHREADS, 1);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<3> tidx) __attribute__((hc, cpu)) {
    int elt = tidx.tile[0];
    long dataCol = colOffset + col_batchOffset * elt;
    long dataIm = imOffset + im_batchOffset * elt;
    int i = tidx.global[1];
    int p = tidx.global[2];

    if(i < n && p < ksize_h) {
      int w_out = i % width_col;
      i /= width_col;
      int h_out = i % height_col;
      int channel_in = i / height_col;
      int channel_out = channel_in * ksize_h * ksize_w;
      int h_in = h_out * stride_h - pad_h;
      int w_in = w_out * stride_w - pad_w;
      dataCol += (channel_out * height_col + h_out) * width_col + w_out;
      dataIm += (channel_in * height + h_in) * width + w_in ;
      dataCol += height_col * width_col * ksize_w * p;

      for (int j = 0; j < ksize_w; ++j) {
        int h = h_in + p;
        int w = w_in + j;
        avData_col[dataCol] = (h >= 0 && w >= 0 && h < height && w < width) ? avData_im[ dataIm + p * width + j] : 0;
        dataCol += height_col * width_col;
      }
    }
  }).wait();
}

/* col2im kernel */
/* Functionality: Rearrange block columns back into matrix (image).
 *
 * Rearranges columns of the matrix B, representing blocks of size (patch_h x patch_w) from a matrix of
   size (height x width), back into its original size, usually close to (height x width).
 * This function is most useful as reverse operation to im2col.
 *
 * Input Parameters:
 *       state: Encapsulates target device accelerator info
 *       avData_col : Input Column matrix
 *       height & width: Dimensions of the output image that got transformed to input column matrix by im2col
 *       colOffset: Input column offset from wherein the data shall be transformed back to image
 *       channels: Numbers of output image chanels. For eg In case of RGB, channels=3
 *       patch_w & patch_h : Width and Height of the block.
 *       pad_w & pad_h: Padding width or Padding height.
 *                      Utilized when transformation is done for distinct ksize_w x ksize_h blocks of input image.
 *       stride_w & stride_h: Dimensions  to control the sliding of ksize_w x ksize_h block within input image
 *       colOffset: Column offset from wherein the output column vectors are written out.
 *       height_col & width_col: Dimensions of the input column matrix.
 *       elt: Batch iterator
 *       inp_stride: block starting offset
 *
 * Output Parameter
 *       avData_im : Output restored image
 */
void col2im_kernel(THHcState* state, int n, float* &avData_col, long colOffset,
                   int height, int width, int channels,
                   int patch_h, int patch_w, int pad_h, int pad_w, int stride_h,
                   int stride_w, int height_col, int width_col,
                   float* &avData_im, long imOffset,
                   int inp_stride, int elt) {
  unsigned grdSz = (n + NUMTHREADS) - (n % NUMTHREADS);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(NUMTHREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    for (int i = tidx.global[0]; i < (n); i += grdExt[0]) {
      float val = 0.0;
      int w = i % width + pad_w;
      int h = (i / width) % height + pad_h;
      int c = i / (width * height);
      // compute the start and end of the output
      int w_col_start = (w < patch_w) ? 0 : (w - patch_w) / stride_w + 1;
      int w_col_end = THMin((int)(w / stride_w + 1), width_col);
      int h_col_start = (h < patch_h) ? 0 : (h - patch_h) / stride_h + 1;
      int h_col_end = THMin((int)(h / stride_h + 1), height_col);
      // equivalent implementation
      long offset = (c * patch_h * patch_w + h * patch_w + w) * height_col * width_col;
      long coeff_h_col = (1 - stride_h * patch_w * height_col) * width_col;
      long coeff_w_col = (1 - stride_w * height_col * width_col);

      for (int h_col = h_col_start; h_col < h_col_end; ++h_col) {
        for (int w_col = w_col_start; w_col < w_col_end; ++w_col) {
          val += avData_col[colOffset + offset + h_col * coeff_h_col + w_col * coeff_w_col];
        }
      }

      avData_im[imOffset + i + elt * inp_stride] = val;
    }
  }).wait();
}

/* col2im_batch kernel */
/*
 * Functionality same as col2im. This kernel is an optimization attempt to avoid loop invocation of col2im kernels. Rather
 * can use a single batch kernel with the loop iteration managed by the third grid dimension
 *
 * Input Parameters:
 *        state: Encapsulates target device accelerator info
 *        avData_col : Input Column matrix
 *        height & width: Dimensions of the output image that got transformed to input column matrix by im2col
 *        colOffset: Input column offset from wherein the data shall be transformed back to image
 *        channels: Numbers of output image chanels. For eg In case of RGB, channels=3
 *        patch_w & patch_h : Width and Height of the block.
 *        pad_w & pasy_h: Padding width or Padding height.
 *                        Utilized when transformation is done for distinct ksize_w x ksize_h blocks of input image.
 *        stride_w & stride_h: Dimensions  to control the sliding of ksize_w x ksize_h block within input image
 *        colOffset: Column offset from wherein the output column vectors are written out.
 *        height_col & width_col: Dimensions of the input column matrix.
 *        elt: Batch iterator
 *        inp_stride: block starting offset
 *        batch_size: size of the batch.
 *
 * Output Parameter
 *        avData_im : Output restored image
 */
void col2im_kernel_batch(THHcState* state, int n, float* &avData_col, long colOffset, long col_batchOffset,
                         int height, int width, int channels,
                         int patch_h, int patch_w, int pad_h, int pad_w, int stride_h,
                         int stride_w, int height_col, int width_col,
                         float* &avData_im, long imOffset,
                         int inp_stride, int batchSize) {
  unsigned grdSz = (n + NUMTHREADS) - (n % NUMTHREADS);
  hc::extent<2> grdExt(batchSize, grdSz);
  hc::tiled_extent<2> t_ext = grdExt.tile(1, NUMTHREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    int elt = tidx.tile[0];

    for (int i = tidx.global[1]; i < (n); i += grdExt[1]) {
      float val = 0.0;
      int w = i % width + pad_w;
      int h = (i / width) % height + pad_h;
      int c = i / (width * height);
      // compute the start and end of the output
      int w_col_start = (w < patch_w) ? 0 : (w - patch_w) / stride_w + 1;
      int w_col_end = THMin((int)(w / stride_w + 1), width_col);
      int h_col_start = (h < patch_h) ? 0 : (h - patch_h) / stride_h + 1;
      int h_col_end = THMin((int)(h / stride_h + 1), height_col);
      // equivalent implementation
      long offset = (c * patch_h * patch_w + h * patch_w + w) * height_col * width_col;
      long coeff_h_col = (1 - stride_h * patch_w * height_col) * width_col;
      long coeff_w_col = (1 - stride_w * height_col * width_col);

      for (int h_col = h_col_start; h_col < h_col_end; ++h_col) {
        for (int w_col = w_col_start; w_col < w_col_end; ++w_col) {
          val += avData_col[colOffset + col_batchOffset * elt + offset + h_col * coeff_h_col + w_col * coeff_w_col];
        }
      }

      avData_im[imOffset + i + elt * inp_stride] = val;
    }
  }).wait();
}

/* col2im kernel wrapper routine */
void col2im(THHcState* state, float* &avData_col, long colOffset,
            int channels, int height, int width,
            int patch_h, int patch_w, int pad_h, int pad_w,
            int stride_h, int stride_w,
            float* &avData_im, long imOffset,
            int inp_stride, int elt) {
  int height_col = (height + 2 * pad_h - patch_h) / stride_h + 1;
  int width_col = (width + 2 * pad_w - patch_w) / stride_w + 1;
  int num_kernels = channels * height * width;
  // To avoid involving atomic operations, we will launch one kernel per
  // bottom dimension, and then in the kernel add up the top dimensions.
  col2im_kernel(state, num_kernels, avData_col, colOffset, height, width, channels, patch_h, patch_w,
                pad_h, pad_w, stride_h, stride_w, height_col, width_col, avData_im, imOffset, inp_stride, elt);
}

/* col2im_batch kernel wrapper routine */
void col2im_batch(THHcState* state, float* &avData_col, long colOffset, long col_batchOffset,
                  int channels, int height, int width,
                  int patch_h, int patch_w, int pad_h, int pad_w,
                  int stride_h, int stride_w,
                  float* &avData_im, long imOffset,
                  int inp_stride, int batchSize) {
  int height_col = (height + 2 * pad_h - patch_h) / stride_h + 1;
  int width_col = (width + 2 * pad_w - patch_w) / stride_w + 1;
  int num_kernels = channels * height * width;
  // To avoid involving atomic operations, we will launch one kernel per
  // bottom dimension, and then in the kernel add up the top dimensions.
  col2im_kernel_batch(state, num_kernels, avData_col, colOffset, col_batchOffset, height, width, channels, patch_h, patch_w,
                      pad_h, pad_w, stride_h, stride_w, height_col, width_col, avData_im, imOffset, inp_stride, batchSize);
}

/* UpdateOutput routine invoked during Forward Propogation of convolution layer */
/* Computes the output using the current parameter set of the class (SpatialConvolutionMM) and input.
 * This function returns the result which is stored in the output field.
 */
static int hcnn_SpatialConvolutionMM_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  // Input
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  // Params:
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  int nOutputPlane = luaT_getfieldcheckint(L, 1, "nOutputPlane");
  int padW = luaT_getfieldcheckint(L, 1, "padW");
  int padH = luaT_getfieldcheckint(L, 1, "padH");
  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", torch_Tensor);
  THHcTensor* bias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "bias", torch_Tensor);
  THHcTensor* columns = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "finput", torch_Tensor);
  THHcTensor* ones = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "fgradInput", torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 3 || input->nDimension == 4, 2, "3D or 4D (batch mode) tensor is expected");
  int batch = 1;

  if (input->nDimension == 3) {
    luaL_argcheck(L, input->size[0] == nInputPlane, 2, "input channels and nInputPlane dont match");
    // Force batch
    batch = 0;
    THHcTensor_resize4d(state, input, 1, input->size[0], input->size[1], input->size[2]);
  } else {
    luaL_argcheck(L, input->size[1] == nInputPlane, 2, "input channels and nInputPlane dont match");
  }

  long inputWidth   = input->size[3];
  long inputHeight  = input->size[2];
  long outputWidth  = (inputWidth + 2 * padW - kW) / dW + 1;
  long outputHeight = (inputHeight + 2 * padH - kH) / dH + 1;

  if (outputWidth < 1 || outputHeight < 1)
    THError("Given input size: (%dx%dx%d). Calculated output size: (%dx%dx%d). Output size is too small",
            nInputPlane, inputHeight, inputWidth, nOutputPlane, outputHeight, outputWidth);

  // Batch size + input planes
  long batchSize = input->size[0];
  // Resize output
  THHcTensor_resize4d(state, output, batchSize, nOutputPlane, outputHeight, outputWidth);
  // Resize temporary columns, 4d
  bool apply_opt = can_apply_opt(state, batchSize * nInputPlane * kW * kH * outputHeight * outputWidth * 4);

  if (apply_opt) {
    THHcTensor_resize2d(state, columns, batchSize * nInputPlane * kW * kH, outputHeight * outputWidth);
  } else {
    THHcTensor_resize2d(state, columns, nInputPlane * kW * kH, outputHeight * outputWidth);
  }

  // Define a buffer of ones, for bias accumulation
  // Note: this buffer can be shared with other modules, it only ever gets increased,
  // and always contains ones.
  if (ones->nDimension != 2 || ones->size[0]*ones->size[1] < outputHeight * outputWidth) {
    // Resize plane and fill with ones...
    THHcTensor_resize2d(state, ones, outputHeight, outputWidth);
    THHcTensor_fill(state, ones, 1);
  }

  // Helpers
  auto avData_col = columns->get_device_data(state);
  auto avData_im = input->get_device_data(state);
  auto avData_ones = ones->get_device_data(state);
  auto avData_bias = bias->get_device_data(state);
  auto avData_output = output->get_device_data(state);
  auto avData_weight = weight->get_device_data(state);
  long m_ = nOutputPlane;
  long n_ = outputHeight * outputWidth;
  long k_ = 1;
  long m = weight->size[0];
  long n = columns->size[1];
  long k = weight->size[1];
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;

  if (apply_opt) {
    // Matrix mulitply per output:
    // Do Bias first:
    // M,N,K are dims of matrix A and B
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, Trans, NoTrans , n_, m_, k_, 1,
                        avData_ones, k_, 0,
                        avData_bias, k_, 0, 0,
                        avData_output, n_, output->stride[0], ones->storageOffset, bias->storageOffset, output->storageOffset, batchSize);
    // Extract columns:
    im2col_batch(state, avData_im, input->storageOffset, input->stride[0],
                 nInputPlane, inputHeight, inputWidth, kH, kW, padH,
                 padW, dH, dW, avData_col, columns->storageOffset,
                 columns->stride[0] * columns->size[0] / batchSize, batchSize);
    // M,N,K are dims of matrix A and B
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, NoTrans, NoTrans, n, m, k, 1,
                        avData_col, n, columns->stride[0] * columns->size[0] / batchSize,
                        avData_weight, k, 0, 1,
                        avData_output, n, output->stride[0], columns->storageOffset, weight->storageOffset, output->storageOffset, batchSize);
  } else {
    // For each elt in batch, do:
    for (int elt = 0; elt < batchSize; elt ++) {
      // Matrix mulitply per output:
      // Do Bias first:
      // M,N,K are dims of matrix A and B
      // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
      hcBlas.hcblas_sgemm(accl_view, ColMajor, Trans, NoTrans, n_, m_, k_, 1,
                          avData_ones, k_,
                          avData_bias, k_, 0,
                          avData_output, n_, ones->storageOffset, bias->storageOffset, output->storageOffset + output->stride[0] * elt);
      // Extract columns:
      im2col(state, avData_im, input->storageOffset + input->stride[0] * elt,
             nInputPlane, inputHeight, inputWidth, kH, kW, padH,
             padW, dH, dW, avData_col, columns->storageOffset);
      // M,N,K are dims of matrix A and B
      // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
      hcBlas.hcblas_sgemm(accl_view, ColMajor, NoTrans, NoTrans, n, m, k, 1,
                          avData_col, n,
                          avData_weight, k, 1,
                          avData_output, n, columns->storageOffset, weight->storageOffset, output->storageOffset + output->stride[0] * elt);
    }
  }

  // Resize output
  if (batch == 0) {
    THHcTensor_resize3d(state, output, nOutputPlane, outputHeight, outputWidth);
    THHcTensor_resize3d(state, input, nInputPlane, inputHeight, inputWidth);
  }

  return 1;
}

/* UpdateGradInput routine invoked during Backward Propogation of convolution layer */
/*
 * Computing the gradient of the SpatialConvolutionMM module with respect to its own input.
 * This is returned in gradInput. Also, the gradInput state variable is updated accordingly
 */
static int hcnn_SpatialConvolutionMM_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  // Inputs
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  // Params
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  int nOutputPlane = luaT_getfieldcheckint(L, 1, "nOutputPlane");
  int padW = luaT_getfieldcheckint(L, 1, "padW");
  int padH = luaT_getfieldcheckint(L, 1, "padH");
  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", torch_Tensor);
  THHcTensor* gradColumns = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "finput", torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 3 || input->nDimension == 4, 2, "3D or 4D (batch mode) tensor is expected");
  int batch = 1;

  if (input->nDimension == 3) {
    // Force batch
    batch = 0;
    THHcTensor_resize4d(state, input, 1, input->size[0], input->size[1], input->size[2]);
    THHcTensor_resize4d(state, gradOutput, 1, gradOutput->size[0], gradOutput->size[1], gradOutput->size[2]);
  }

  long inputWidth   = input->size[3];
  long inputHeight  = input->size[2];
  long outputWidth  = (inputWidth + 2 * padW - kW) / dW + 1;
  long outputHeight = (inputHeight + 2 * padH - kH) / dH + 1;
  // Batch size + input planes
  long batchSize = input->size[0];
  // Resize output
  THHcTensor_resize4d(state, gradInput, batchSize, nInputPlane, inputHeight, inputWidth);
  // Resize temporary columns
  bool apply_opt = can_apply_opt(state, batchSize * nInputPlane * kW * kH * outputHeight * outputWidth * 4);

  if (apply_opt) {
    THHcTensor_resize2d(state, gradColumns, batchSize * nInputPlane * kW * kH, outputHeight * outputWidth);
  } else {
    THHcTensor_resize2d(state, gradColumns, nInputPlane * kW * kH, outputHeight * outputWidth);
  }

  auto avData_col = gradColumns->get_device_data(state);
  auto avData_im = gradInput->get_device_data(state);
  auto avData_gradOutput = gradOutput->get_device_data(state);
  auto avData_weight = weight->get_device_data(state);
  long m = weight->size[1];
  long n = gradColumns->size[1];
  long k = weight->size[0];
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;

  if (apply_opt) {
    // Matrix mulitply per sample:
    // M,N,K are dims of matrix A and B
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, NoTrans, Trans, n, m, k, 1,
                        avData_gradOutput, n, gradOutput->stride[0],
                        avData_weight, m, 0, 0,
                        avData_col, n, gradColumns->stride[0] * gradColumns->size[0] / batchSize, gradOutput->storageOffset, weight->storageOffset, gradColumns->storageOffset, batchSize);
    // Unpack columns back into input:
    col2im_batch(state, avData_col, gradColumns->storageOffset, gradColumns->stride[0] * gradColumns->size[0] / batchSize,  nInputPlane,
                 inputHeight, inputWidth, kH, kW, padH, padW, dH, dW,
                 avData_im, gradInput->storageOffset, gradInput->stride[0], batchSize);
  } else {
    // For each elt in batch, do:
    for (int elt = 0; elt < batchSize; elt ++) {
      // Matrix mulitply per sample:
      // M,N,K are dims of matrix A and B
      // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
      hcBlas.hcblas_sgemm(accl_view, ColMajor, NoTrans, Trans, n, m, k, 1,
                          avData_gradOutput, n,
                          avData_weight, m, 0,
                          avData_col, n, gradOutput->storageOffset + gradOutput->stride[0] * elt, weight->storageOffset, gradColumns->storageOffset);
      // Unpack columns back into input:
      col2im(state, avData_col, gradColumns->storageOffset,  nInputPlane,
             inputHeight, inputWidth, kH, kW, padH, padW, dH, dW,
             avData_im, gradInput->storageOffset, gradInput->stride[0], elt);
    }
  }

  // Resize output
  if (batch == 0) {
    THHcTensor_resize3d(state, gradOutput, nOutputPlane, outputHeight, outputWidth);
    THHcTensor_resize3d(state, input, nInputPlane, inputHeight, inputWidth);
    THHcTensor_resize3d(state, gradInput, nInputPlane, inputHeight, inputWidth);
  }

  return 1;
}

/* AccGradParameters routine invoked at the end of Back Propogation of convolution layer*/
/*
 * Computing the gradient of the SpatialSubSampling module with respect to its ownparameters.
 * Many modules do not perform this step as they do not have any parameters.
 * The state variable name for the parameters is module dependent.
 * The SpatialConvolutionMM module is expected to accumulate the gradients with respect to the parameters
 * in some variable.

 * scale is a scale factor that is multiplied with the gradParameters before being accumulated.
*/
static int hcnn_SpatialConvolutionMM_accGradParameters(lua_State* L) {
  THHcState* state = getGputorchState(L);
  // Inputs
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  // Params
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  int nOutputPlane = luaT_getfieldcheckint(L, 1, "nOutputPlane");
  int padW = luaT_getfieldcheckint(L, 1, "padW");
  int padH = luaT_getfieldcheckint(L, 1, "padH");
  float scale = luaL_optnumber(L, 4, 1);
  THHcTensor* gradWeight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradWeight", torch_Tensor);
  THHcTensor* gradBias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradBias", torch_Tensor);
  THHcTensor* columns = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "finput", torch_Tensor);
  THHcTensor* ones = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "fgradInput", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 3 || input->nDimension == 4, 2, "3D or 4D (batch mode) tensor is expected");
  int batch = 1;

  if (input->nDimension == 3) {
    // Force batch
    batch = 0;
    THHcTensor_resize4d(state, input, 1, input->size[0], input->size[1], input->size[2]);
    THHcTensor_resize4d(state, gradOutput, 1, gradOutput->size[0], gradOutput->size[1], gradOutput->size[2]);
  }

  long inputWidth   = input->size[3];
  long inputHeight  = input->size[2];
  long outputWidth  = (inputWidth + 2 * padW - kW) / dW + 1;
  long outputHeight = (inputHeight + 2 * padH - kH) / dH + 1;
  // Batch size + input planes
  long batchSize = input->size[0];

  // Define a buffer of ones, for bias accumulation
  if (ones->nDimension != 2 || ones->size[0]*ones->size[1] < outputHeight * outputWidth) {
    // Resize plane and fill with ones...
    THHcTensor_resize2d(state, ones, outputHeight, outputWidth);
    THHcTensor_fill(state, ones, 1);
  }

  // Resize temporary columns
  bool apply_opt = false;//can_apply_opt(state, batchSize*nInputPlane*kW*kH*outputHeight*outputWidth*4);

  if (apply_opt) {
    THHcTensor_resize2d(state, columns, batchSize * nInputPlane * kW * kH, outputHeight * outputWidth);
  } else {
    THHcTensor_resize2d(state, columns, nInputPlane * kW * kH, outputHeight * outputWidth);
  }

  long m = gradWeight->size[0];
  long n = gradWeight->size[1];
  long k = columns->size[1];
  long m_ = nOutputPlane;
  long k_ = outputHeight * outputWidth;
  auto avData_col = columns->get_device_data(state);
  auto avData_im = input->get_device_data(state);
  auto avData_gradOutput = gradOutput->get_device_data(state);
  auto avData_gradWeight = gradWeight->get_device_data(state);
  auto avData_ones = ones->get_device_data(state);
  auto avData_gradBias = gradBias->get_device_data(state);
  int lenX = k_;
  int lenY = m_;
  int len_X = (lenX + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1);
  int numBlocks = len_X / NUMTHREADS;
  float* temp_buf;
  THHcCheck(THHcMalloc(state, (void**) &temp_buf, numBlocks * lenY * sizeof(float)));
  hc::extent<1> ext(numBlocks * lenY);
  numBlocks = ((k + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1)) / NUMTHREADS;
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;

  // For each elt in batch, do:
  if (apply_opt) {
    // Extract columns:
    im2col_batch(state, avData_im, input->storageOffset, input->stride[0],
                 nInputPlane, inputHeight, inputWidth, kH, kW, padH, padW,
                 dH, dW, avData_col, columns->storageOffset, columns->stride[0] * columns->size[0] / batchSize, batchSize);
    // M,N,K are dims of matrix A and B
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, Trans, NoTrans, n, m, k, scale,
                        avData_col, k, columns->stride[0] * columns->size[0] / batchSize,
                        avData_gradOutput, k, gradOutput->stride[0], 1,
                        avData_gradWeight, n, 0, columns->storageOffset, gradOutput->storageOffset, gradWeight->storageOffset, batchSize);

    for (int elt = 0; elt < batchSize; elt ++) {
      THHcBlas_gemv(state, 't', k_, m_, scale,
                     avData_gradOutput,
                     gradOutput->storageOffset + gradOutput->stride[0] * elt,
                     avData_ones, ones->storageOffset, 1, 1,
                     avData_gradBias, gradBias->storageOffset, 1, temp_buf);
    }
  } else {
    // For each elt in batch, do:
    for (int elt = 0; elt < batchSize; elt ++) {
      // Extract columns:
      im2col(state, avData_im, input->storageOffset + input->stride[0] * elt,
             nInputPlane, inputHeight, inputWidth, kH, kW, padH, padW,
             dH, dW, avData_col, columns->storageOffset);
      // M,N,K are dims of matrix A and B
      // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
      hcBlas.hcblas_sgemm(accl_view, ColMajor, Trans, NoTrans, n, m, k, scale,
                          avData_col, k,
                          avData_gradOutput, k, 1,
                          avData_gradWeight, n, columns->storageOffset, gradOutput->storageOffset + gradOutput->stride[0] * elt, gradWeight->storageOffset);
      THHcBlas_gemv(state, 't', k_, m_, scale,
                     avData_gradOutput,
                     gradOutput->storageOffset + gradOutput->stride[0] * elt,
                     avData_ones, ones->storageOffset, 1, 1,
                     avData_gradBias, gradBias->storageOffset, 1, temp_buf);
    }
  }

  // Resize
  if (batch == 0) {
    THHcTensor_resize3d(state, gradOutput, nOutputPlane, outputHeight, outputWidth);
    THHcTensor_resize3d(state, input, nInputPlane, inputHeight, inputWidth);
  }

  THHcCheck(THHcFree(state, temp_buf));
  return 0;
}

static const struct luaL_Reg hcnn_SpatialConvolutionMM__ [] = {
  {"SpatialConvolutionMM_updateOutput", hcnn_SpatialConvolutionMM_updateOutput},
  {"SpatialConvolutionMM_updateGradInput", hcnn_SpatialConvolutionMM_updateGradInput},
  {"SpatialConvolutionMM_accGradParameters", hcnn_SpatialConvolutionMM_accGradParameters},
  {NULL, NULL}
};

/* Binding the Convolution layer routines with lua stack */
void hcnn_SpatialConvolutionMM_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_SpatialConvolutionMM__, "nn");
  lua_pop(L, 1);
}
