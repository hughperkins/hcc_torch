/********************************************************************************************
 * SpatialMaxPooling
 *
 * module = nn.SpatialMaxPooling(kW, kH [, dW, dH, padW, padH])
 *
 * Functionality: Applies 2D max-pooling operation in kWxkH regions by step size dWxdH steps.
 *                The number of output features is equal to the number of input planes.
 *
 * If the input image is a 3D tensor nInputPlane x height x width,
 * the output image size will be nOutputPlane x oheight x owidth where
 *                owidth  = op((width  + 2*padW - kW) / dW + 1)
 *                oheight = op((height + 2*padH - kH) / dH + 1)
 *
 *Note: op is a rounding operator. By default, it is floor.
 *      It can be changed by calling :ceil() or :floor() methods.
 *
*********************************************************************************************/
#include "utils.h"
#include "amp_math.h"
#define NUMTHREADS 256
using namespace hc::fast_math;

void MaxPoolForward(THHcState* state, const int nthreads,
                    float* &avBottom_data, long bottom_offset,
                    const int num, const int channels, const int height,
                    const int width, const int pooled_height, const int pooled_width,
                    const int kernel_h, const int kernel_w, const int stride_h,
                    const int stride_w, const int pad_h, const int pad_w,
                    float* &avTop_data, long top_offset,
                    float* &avTop_mask, long mask_offset) {
  unsigned grdSz = (nthreads + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(NUMTHREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    int index = tidx.global[0];

    if (index < nthreads) {
      long bottomOffset = bottom_offset;
      int pw = index % pooled_width;
      int ph = (index / pooled_width) % pooled_height;
      int c = (index / pooled_width / pooled_height) % channels;
      int n = index / pooled_width / pooled_height / channels;
      int hstart = ph * stride_h - pad_h;
      int wstart = pw * stride_w - pad_w;
      int hend = hc::fast_math::fmin(hstart + kernel_h, height);
      int wend = hc::fast_math::fmin(wstart + kernel_w, width);
      hstart = hc::fast_math::fmax(hstart, 0);
      wstart = hc::fast_math::fmax(wstart, 0);
      float maxval = -FLT_MAX;
      int maxidx = -1;
      bottomOffset += (n * channels + c) * height * width;

      for (int h = hstart; h < hend; ++h) {
        for (int w = wstart; w < wend; ++w) {
          if (avBottom_data[bottomOffset + h * width + w] > maxval) {
            maxidx = h * width + w;
            maxval = avBottom_data[bottomOffset + maxidx];
          }
        }
      }

      avTop_data[top_offset + index] = maxval;
      avTop_mask[mask_offset + index] = maxidx + 1;
    }
  }).wait();
}

void MaxPoolBackward(THHcState* state, const int nthreads,
                     float* &top_diff, long top_offset,
                     float* &top_mask, long mask_offset, const int num,
                     const int channels, const int height, const int width, const int pooled_height,
                     const int pooled_width, const int kernel_h, const int kernel_w,
                     const int stride_h, const int stride_w, const int pad_h, const int pad_w,
                     float* &bottom_diff, long bottom_offset) {
  unsigned grdSz = (nthreads + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(NUMTHREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    int index = tidx.global[0];

    if (index < nthreads) {
      int w = index % width;
      int h = (index / width) % height;
      int c = (index / width / height) % channels;
      int n = index / width / height / channels;
      int phstart =
        (h + pad_h < kernel_h) ? 0 : (h + pad_h - kernel_h) / stride_h + 1;
      int phend = hc::fast_math::fmin((h + pad_h) / stride_h + 1, pooled_height);
      int pwstart =
        (w + pad_w < kernel_w) ? 0 : (w + pad_w - kernel_w) / stride_w + 1;
      int pwend = hc::fast_math::fmin((w + pad_w) / stride_w + 1, pooled_width);
      float gradient = 0.0;
      int offset = (n * channels + c) * pooled_height * pooled_width;
      long topOffset = top_offset + offset;
      long maskOffset = mask_offset + offset;

      for (int ph = phstart; ph < phend; ++ph) {
        for (int pw = pwstart; pw < pwend; ++pw) {
          if (top_mask[maskOffset + ph * pooled_width + pw] - 1 == h * width + w) {
            gradient += top_diff[topOffset + ph * pooled_width + pw];
          }
        }
      }

      bottom_diff[bottom_offset + index] = gradient;
    }
  }).wait();
}

static int hcnn_SpatialMaxPooling_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int padW = luaT_getfieldcheckint(L, 1, "padW");
  int padH = luaT_getfieldcheckint(L, 1, "padH");
  bool ceil_mode = luaT_getfieldcheckboolean(L, 1, "ceil_mode");
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* indices = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "indices", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 3 || input->nDimension == 4, 2, "3D or 4D (batch) tensor expected");
  long nInputCols, nInputRows, nInputPlane, batchSize;
  long nOutputCols, nOutputRows;

  if (input->nDimension == 3) {
    nInputCols = input->size[2];
    nInputRows = input->size[1];
    nInputPlane = input->size[0];
    batchSize = 1;
  } else {
    nInputCols = input->size[3];
    nInputRows = input->size[2];
    nInputPlane = input->size[1];
    batchSize = input->size[0];
  }

  luaL_argcheck(L, nInputCols >= kW - padW && nInputRows >= kH - padH, 2, "input image smaller than kernel size");
  luaL_argcheck(L, kW / 2 >= padW && kH / 2 >= padH, 2, "pad should be smaller than half of kernel size");

  if(ceil_mode) {
    nOutputCols = std::ceil(float(nInputCols - kW + 2 * padW) / float(dW)) + 1;
    nOutputRows = std::ceil(float(nInputRows - kH + 2 * padH) / float(dH)) + 1;
  } else {
    nOutputCols = std::floor(float(nInputCols - kW + 2 * padW) / float(dW)) + 1;
    nOutputRows = std::floor(float(nInputRows - kH + 2 * padH) / float(dH)) + 1;
  }

  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resize4d(state, output, batchSize, nInputPlane, nOutputRows, nOutputCols);
  THHcTensor_resizeAs(state, indices, output);
  int count = THHcTensor_nElement(state, output);
  auto avInput = input->get_device_data(state);
  auto avIndices = indices->get_device_data(state);
  auto avOutput = output->get_device_data(state);
  MaxPoolForward(state, count, avInput, input->storageOffset,
                 batchSize, nInputPlane, nInputRows, nInputCols, nOutputRows,
                 nOutputCols, kH, kW, dH, dW, padH, padW, avOutput,
                 output->storageOffset, avIndices, indices->storageOffset);

  if(input->nDimension == 3) {
    THHcTensor_resize3d(state, output, nInputPlane, nOutputRows, nOutputCols);
  }

  THHcTensor_free(state, input);
  return 1;
}

static int hcnn_SpatialMaxPooling_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int padW = luaT_getfieldcheckint(L, 1, "padW");
  int padH = luaT_getfieldcheckint(L, 1, "padH");
  bool ceil_mode = luaT_getfieldcheckboolean(L, 1, "ceil_mode");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  THHcTensor* indices = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "indices", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  long nInputCols, nInputRows, nInputPlane, batchSize;
  long nOutputCols, nOutputRows;

  if (input->nDimension == 3) {
    nInputCols = input->size[2];
    nInputRows = input->size[1];
    nInputPlane = input->size[0];
    batchSize = 1;
  } else {
    nInputCols = input->size[3];
    nInputRows = input->size[2];
    nInputPlane = input->size[1];
    batchSize = input->size[0];
  }

  if(ceil_mode) {
    nOutputCols = std::ceil(float(nInputCols - kW + 2 * padW) / float(dW)) + 1;
    nOutputRows = std::ceil(float(nInputRows - kH + 2 * padH) / float(dH)) + 1;
  } else {
    nOutputCols = std::floor(float(nInputCols - kW + 2 * padW) / float(dW)) + 1;
    nOutputRows = std::floor(float(nInputRows - kH + 2 * padH) / float(dH)) + 1;
  }

  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, input);
  int count = THHcTensor_nElement(state, input);
  auto avGradOutput = gradOutput->get_device_data(state);
  auto avIndices = indices->get_device_data(state);
  auto avGradInput = gradInput->get_device_data(state);
  MaxPoolBackward (state, count, avGradOutput, gradOutput->storageOffset,
                   avIndices, indices->storageOffset,
                   batchSize, nInputPlane, nInputRows, nInputCols,
                   nOutputRows, nOutputCols, kH, kW, dH, dW, padH, padW,
                   avGradInput, gradInput->storageOffset);
  THHcTensor_free(state, gradOutput);
  // clean
  THHcTensor_free(state, input);
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_SpatialMaxPooling__ [] = {
  {"SpatialMaxPooling_updateOutput", hcnn_SpatialMaxPooling_updateOutput},
  {"SpatialMaxPooling_updateGradInput", hcnn_SpatialMaxPooling_updateGradInput},
  {NULL, NULL}
};

void hcnn_SpatialMaxPooling_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_SpatialMaxPooling__, "nn");
  lua_pop(L, 1);
}

#undef NUMTHREADS
