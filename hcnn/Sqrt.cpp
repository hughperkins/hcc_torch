/***********************************************************************************
 * Sqrt
 *
 * module = nn.Sqrt()
 * Functionality : Takes the square root of each element for adapting Tensor methods
 *                 and providing affine transformations.
 *
 ***********************************************************************************/
#include "utils.h"
#include "amp_math.h"
struct sqrtupdateOutput_functor {
  double bias;
  sqrtupdateOutput_functor(double bias_) __attribute__((hc, cpu)): bias(bias_) {}
  sqrtupdateOutput_functor(const sqrtupdateOutput_functor& other) __attribute__((hc, cpu))
    : bias(other.bias) {}
  float operator()(const float& input) const __attribute__((hc, cpu)) {
    return hc::fast_math::sqrt(input + bias);
  }
  sqrtupdateOutput_functor& operator = (const sqrtupdateOutput_functor&other) __attribute__((hc, cpu)) {
    bias = other.bias;
    return *this;
  }
};

static int hcnn_Sqrt_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  double bias = luaT_getfieldchecknumber(L, 1, "eps");
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, sqrtupdateOutput_functor(bias));
  THHcTensor_free(state, input);
  return 1;
}

struct sqrtupdateGradInput_functor {
  double bias;
  sqrtupdateGradInput_functor(double bias_) __attribute__((hc, cpu)) : bias(bias_) {}
  sqrtupdateGradInput_functor(const sqrtupdateGradInput_functor &other) __attribute__((hc, cpu))
    : bias(other.bias) {}
  float operator()(const float& output, const float& gradOutput) const __attribute__((hc, cpu)) {
    return 0.5 * gradOutput / output;
  }
  sqrtupdateGradInput_functor& operator = (const sqrtupdateGradInput_functor&other) __attribute__((hc, cpu)) {
    bias = other.bias;
    return *this;
  }
};

static int hcnn_Sqrt_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  double bias = luaT_getfieldchecknumber(L, 1, "eps");
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, output);
  transform(state, output, gradOutput, gradInput, sqrtupdateGradInput_functor(bias));
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_Sqrt__ [] = {
  {"Sqrt_updateOutput", hcnn_Sqrt_updateOutput},
  {"Sqrt_updateGradInput", hcnn_Sqrt_updateGradInput},
  {NULL, NULL}
};

/* Binding the Sqrt layer routines with lua stack */
void hcnn_Sqrt_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Sqrt__, "nn");
  lua_pop(L, 1);
}
