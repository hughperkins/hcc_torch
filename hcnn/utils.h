#ifndef HcNN_UTILS_H
#define HcNN_UTILS_H

extern "C"
{
#include <lua.h>
}
#include <luaT.h>
#include <THHc/THHc.h>
#include "THHcGeneral.h"
#include "THHcFunctorOpns.h"

#define torch_Storage TH_CONCAT_STRING_3(torch., Torch_BKN_NAME, Storage)
#define torch_Tensor TH_CONCAT_STRING_3(torch., Torch_BKN_NAME, Tensor)

THHcState* getGputorchState(lua_State* L);

void hcnn_ClassNLLCriterion_init(lua_State *L);
void hcnn_Tanh_init(lua_State *L);
void hcnn_Sigmoid_init(lua_State *L);
void hcnn_HardTanh_init(lua_State *L);
void hcnn_L1Cost_init(lua_State *L);
void hcnn_LogSoftMax_init(lua_State *L);
void hcnn_SoftMax_init(lua_State *L);
void hcnn_TemporalConvolution_init(lua_State *L);
void hcnn_TemporalMaxPooling_init(lua_State *L);
void hcnn_SpatialConvolutionMM_init(lua_State *L);
void hcnn_SpatialMaxPooling_init(lua_State *L);
void hcnn_SpatialAdaptiveMaxPooling_init(lua_State *L);
void hcnn_SpatialSubSampling_init(lua_State *L);
void hcnn_SpatialAveragePooling_init(lua_State *L);
void hcnn_MultiMarginCriterion_init(lua_State *L);
void hcnn_Square_init(lua_State *L);
void hcnn_Sqrt_init(lua_State *L);
void hcnn_Threshold_init(lua_State *L);
void hcnn_MSECriterion_init(lua_State *L);
void hcnn_AbsCriterion_init(lua_State *L);
void hcnn_DistKLDivCriterion_init(lua_State *L);
void hcnn_Abs_init(lua_State *L);
void hcnn_SoftPlus_init(lua_State *L);
void hcnn_SpatialUpSamplingNearest_init(lua_State *L);
void hcnn_VolumetricConvolution_init(lua_State *L);
void hcnn_LogSigmoid_init(lua_State *L);
void hcnn_PReLU_init(lua_State *L);
void hcnn_LookupTable_init(lua_State *L);
void hcnn_Max_init(lua_State *L);
void hcnn_Min_init(lua_State *L);
void hcnn_Exp_init(lua_State *L);
void hcnn_Tanh_init(lua_State *L);
#endif
