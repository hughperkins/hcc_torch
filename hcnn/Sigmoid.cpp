/****************************************************************
 * Sigmoid
 *
 * Applies the Sigmoid function element-wise to the input Tensor,
 * thus outputting a Tensor of the same dimension.
 *
 * Sigmoid is defined as
 * f(x) = 1/(1+exp(-x)).
 *
 ****************************************************************/
#include "utils.h"
#include "amp_math.h"

struct sigmoidupdateOutput_functor {
  sigmoidupdateOutput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input) const __attribute__((hc, cpu)) {
    return 1. / (1. + hc::fast_math::exp(-input));
  }
};

static int hcnn_Sigmoid_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, sigmoidupdateOutput_functor());
  THHcTensor_free(state, input);
  return 1;
}

struct sigmoidupdateGradInput_functor {
  sigmoidupdateGradInput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& output, const float& gradOutput) const __attribute__((hc, cpu)) {
    return gradOutput * (1. - output) * output;
  }
};

static int hcnn_Sigmoid_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, output);
  transform(state, output, gradOutput, gradInput, sigmoidupdateGradInput_functor());
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_Sigmoid__ [] = {
  {"Sigmoid_updateOutput", hcnn_Sigmoid_updateOutput},
  {"Sigmoid_updateGradInput", hcnn_Sigmoid_updateGradInput},
  {NULL, NULL}
};

/* Binding the Sigmoid layer routines with lua stack */
void hcnn_Sigmoid_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Sigmoid__, "nn");
  lua_pop(L, 1);
}
