/**********************************************************************************************************
 * MultiMarginCriterion
 *
 * criterion = nn.MarginCriterion([margin])
 * Functionality : MultiMarginCriterion Creates a criterion that optimizes a multi-class classification
 *                 hinge loss (margin-based loss) between input x (a Tensor of dimension 1) and output y
 *                 (which is a target class index, 1 <= y <= x:size(1)):
 *
 * loss(x, y) = sum_i(max(0, 1 - (x[y] - x[i]))^p) / x:size(1)
 * where i == 1 to x:size(1) and i ~= y. Note that this criterion also works with 2D inputs and 1D targets.
 *
 **********************************************************************************************************/
#include "utils.h"
#define MULTIMARGIN_THREADS 128

template <int P>
void hcnn_MultiMarginCriterion_updateOutput_kernel(THHcState* state, float* &avOutput, long outOffset,
    float* &avInp, long inpOffset,
    float* &avTarget, long targetOffset,
    int nframe, int dim, int sizeaverage) {
  hc::extent<1> grdExt(MULTIMARGIN_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(MULTIMARGIN_THREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[MULTIMARGIN_THREADS];
    int k = tidx.tile[0];
    float* input_k = avInp + inpOffset;
    float* output_k = avOutput + outOffset;
    int target_k = ((int)avTarget[targetOffset + k]) - 1;
    float input_target_k = input_k[target_k];
    int i_start = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];
    buffer[i_start] = 0;

    for (int i = i_start; i < i_end; i += i_step) {
      float z = 1 - input_target_k + input_k[i];

      if (i == target_k) {
        continue;
      }

      if (z > 0) {
        buffer[i_start] += (P == 1) ? z : z * z;
      }
    }

    tidx.barrier.wait();

    // reduce
    if (i_start == 0) {
      float sum = 0;

      for (int i = 0; i < i_step; i++) {
        sum += buffer[i];
      }

      if (sizeaverage) {
        *output_k = sum / dim;
      } else {
        *output_k = sum;
      }
    }
  }).wait();
}

template <int P>
void hcnn_MultiMarginCriterion_updateGradInput_kernel(THHcState* state, float* &avgradInput, long gradInOffset,
    float* &avInp, long inpOffset,
    float* &avTarget, long targetOffset,
    int nframe, int dim, int sizeaverage) {
  hc::extent<1> grdExt(MULTIMARGIN_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(MULTIMARGIN_THREADS);
  float g = (float)(sizeaverage ? 1.0 / ((float)dim) : 1.0);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[MULTIMARGIN_THREADS];
    int k = tidx.tile[0];
    float* input_k = avInp + inpOffset;
    float* gradInput_k = avgradInput + gradInOffset;
    int target_k = ((int)avTarget[targetOffset + k]) - 1;
    float input_target_k = input_k[target_k];
    int i_start = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];
    buffer[i_start] = 0;

    for (int i = i_start; i < i_end; i += i_step) {
      float z = 1 - input_target_k + input_k[i];

      if (i == target_k) {
        continue;
      }

      if (z > 0) {
        float h = (P == 1) ? g : 2 * g * z;
        buffer[i_start] -= h;
        gradInput_k[i] = h;
      } else {
        gradInput_k[i] = 0;
      }
    }

    tidx.barrier.wait();

    // reduce
    if (i_start == 0) {
      float gradInput_target_k = 0;

      for (int i = 0; i < i_step; i++) {
        gradInput_target_k += buffer[i];
      }

      gradInput_k[target_k] = gradInput_target_k;
    }
  }).wait();
}

static int hcnn_MultiMarginCriterion_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  int p = luaT_getfieldchecknumber(L, 1, "p");
  int sizeaverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  input = THHcTensor_newContiguous(state, input);
  auto avInput = input->get_device_data(state);

  if (input->nDimension == 1) {
    float target_ = luaL_checknumber(L, 3);
    THHcStorage* target = THHcStorage_newWithSize(state, 1);
    THHcStorage* output = THHcStorage_newWithSize(state, 1);
    THHcStorage_fill(state, target, target_);
    float** pavTarget = target->allocatorContext;
    float** pavOutput = output->allocatorContext;

    if(p == 1)
      hcnn_MultiMarginCriterion_updateOutput_kernel<1>(state, *pavOutput, 0,
          avInput, input->storageOffset,
          *pavTarget, 0,
          1, input->size[0],
          sizeaverage);
    else if(p == 2)
      hcnn_MultiMarginCriterion_updateOutput_kernel<2>(state, *pavOutput, 0,
          avInput, input->storageOffset,
          *pavTarget, 0,
          1, input->size[0],
          sizeaverage);

    lua_pushnumber(L, THHcStorage_get(state, output, 0));
    THHcStorage_free(state, output);
    THHcStorage_free(state, target);
  } else if (input->nDimension == 2) {
    THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
    THHcTensor* output = THHcTensor_newWithSize1d(state, input->size[0]);
    auto avTarget = target->get_device_data(state);
    auto avOutput = output->get_device_data(state);

    if (p == 1)
      hcnn_MultiMarginCriterion_updateOutput_kernel<1>(state, avOutput, output->storageOffset,
          avInput, input->storageOffset,
          avTarget, target->storageOffset,
          input->size[0], input->size[1],
          sizeaverage);
    else if(p == 2)
      hcnn_MultiMarginCriterion_updateOutput_kernel<2>(state, avOutput, output->storageOffset,
          avInput, input->storageOffset,
          avTarget, target->storageOffset,
          input->size[0], input->size[1],
          sizeaverage);

    lua_pushnumber(L, THHcTensor_sumall(state, output));
    THHcTensor_free(state, output);
  } else {
    THError("vector or matrix expected");
  }

  lua_pushstring(L, "output");
  lua_pushvalue(L, -2);
  lua_rawset(L, 1);
  THHcTensor_free(state, input);
  return 1;
}

static int hcnn_MultiMarginCriterion_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  int sizeaverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  int p = luaT_getfieldchecknumber(L, 1, "p");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  THHcTensor_resizeAs(state, gradInput, input);
  auto avGradInput = gradInput->get_device_data(state);
  auto avInput = input->get_device_data(state);

  if (gradInput->nDimension == 1) {
    float target_ = luaL_checknumber(L, 3);
    THHcTensor* target = THHcTensor_newWithSize1d(state, 1);
    THHcTensor_fill(state, target, target_);
    auto avTarget = target->get_device_data(state);

    if (p == 1)
      hcnn_MultiMarginCriterion_updateGradInput_kernel<1>(state, avGradInput, gradInput->storageOffset,
          avInput, input->storageOffset,
          avTarget, target->storageOffset,
          1, gradInput->size[0], sizeaverage);
    else if(p == 2)
      hcnn_MultiMarginCriterion_updateGradInput_kernel<2>(state, avGradInput, gradInput->storageOffset,
          avInput, input->storageOffset,
          avTarget, target->storageOffset,
          1, gradInput->size[0], sizeaverage);

    THHcTensor_free(state, target);
  } else if (gradInput->nDimension == 2) {
    THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
    auto avTarget = target->get_device_data(state);

    if (p == 1)
      hcnn_MultiMarginCriterion_updateGradInput_kernel<1>(state, avGradInput, gradInput->storageOffset,
          avInput, input->storageOffset,
          avTarget, target->storageOffset,
          gradInput->size[0], gradInput->size[1], sizeaverage);
    else if(p == 2)
      hcnn_MultiMarginCriterion_updateGradInput_kernel<2>(state, avGradInput, gradInput->storageOffset,
          avInput, input->storageOffset,
          avTarget, target->storageOffset,
          gradInput->size[0], gradInput->size[1], sizeaverage);
  } else {
    THError("vector or matrix expected");
  }

  return 1;
}

static const struct luaL_Reg hcnn_MultiMarginCriterion__ [] = {
  {"MultiMarginCriterion_updateOutput", hcnn_MultiMarginCriterion_updateOutput},
  {"MultiMarginCriterion_updateGradInput", hcnn_MultiMarginCriterion_updateGradInput},
  {NULL, NULL}
};

/* Binding the MultiMarginCriterion layer routines with lua stack */
void hcnn_MultiMarginCriterion_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_MultiMarginCriterion__, "nn");
  lua_pop(L, 1);
}
