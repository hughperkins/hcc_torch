/*******************************************************************************
 * Square
 *
 * module = nn.Square()
 * Functionality : Takes the square of each element for adapting Tensor methods
 *                 and providing affine transformations.
 *
 *******************************************************************************/
#include "utils.h"
#include "amp_math.h"

struct squareupdateOutput_functor {
  squareupdateOutput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input) const __attribute__((hc, cpu)) {
    return input * input;
  }
};

static int hcnn_Square_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, squareupdateOutput_functor());
  THHcTensor_free(state, input);
  return 1;
}

struct squareupdateGradInput_functor {
  squareupdateGradInput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input, const float& gradOutput) const __attribute__((hc, cpu)) {
    return 2.0 * gradOutput * input;
  }
};

static int hcnn_Square_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, gradInput, input);
  transform(state, input, gradOutput, gradInput, squareupdateGradInput_functor());
  THHcTensor_free(state, input);
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_Square__ [] = {
  {"Square_updateOutput", hcnn_Square_updateOutput},
  {"Square_updateGradInput", hcnn_Square_updateGradInput},
  {NULL, NULL}
};

/* Binding the Square layer routines with lua stack */
void hcnn_Square_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Square__, "nn");
  lua_pop(L, 1);
}
