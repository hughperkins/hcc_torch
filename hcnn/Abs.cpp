/**********************************************************************************
 * Abs
 *
 * module = Abs()
 * Functionality: Applies an element-wise abs operation for adapting Tensor methods
 *                and providing affine transformations.
 *
 **********************************************************************************/

#include <iostream>
#include <vector>
#include "utils.h"
#include "amp_math.h"

struct absupdateOutput_functor {
  absupdateOutput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input) const  __attribute__((hc, cpu)) {
    return hc::fast_math::fabs(input);
  }
};

static int hcnn_Abs_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, absupdateOutput_functor());
  THHcTensor_free(state, input);
  return 1;
}

struct absupdateGradInput_functor {
  absupdateGradInput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input, const float& gradOutput) const __attribute__((hc, cpu)) {
    if(input < 0) {
      return -gradOutput;
    } else {
      return gradOutput;
    }
  }
};

static int hcnn_Abs_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, input);
  transform(state, input, gradOutput, gradInput, absupdateGradInput_functor());
  THHcTensor_free(state, gradOutput);
  THHcTensor_free(state, input);
  return 1;
}

static const struct luaL_Reg hcnn_Abs__ [] = {
  {"Abs_updateOutput", hcnn_Abs_updateOutput},
  {"Abs_updateGradInput", hcnn_Abs_updateGradInput},
  {NULL, NULL}
};

/* Binding the Abs layer routines with lua stack */
void hcnn_Abs_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Abs__, "nn");
  lua_pop(L, 1);
}
