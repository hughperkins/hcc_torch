/************************************************************************************************
 * SpatialUpSamplingNearest
 *
 * module = nn.SpatialUpSamplingNearest(scale)
 * Functionality: Applies a 2D up-sampling over an input image composed of several input planes.
 *                The input tensor in forward(input) is expected to be a 3D or 4D
 *                tensor (i.e. for 4D: nBatchPlane x nInputPlane x height x width).
 *                The number of output planes will be the same.
 *                The v dimension is assumed to be the second last
 *                dimension (i.e. for 4D it will be the 3rd dim),
 *                and the u dimension is assumed to be the last dimension.

 * The parameters are the following:
 *                scale: The upscale ratio. Must be a positive integer
 *                The up-scaling method is simple nearest neighbor, ie:
 *
 * output(u, v) = input(floor((u-1)/scale)+1, floor((v-1)/scale)+1)
 *                Where u and v are index from 1 (as per lua convention).
 *                There are no learnable parameters.
 *
 ***************************************************************************************************/

#include "luaT.h"
#include "THHc.h"
#include "utils.h"

int translate_idx(int ii, int d1, int d2, int d3, int scale_factor) __attribute__((hc, cpu)) {
  int x, y, z, w;
  w = ii % d3;
  ii = ii / d3;
  z = ii % d2;
  ii = ii / d2;
  y = ii % d1;
  ii = ii / d1;
  x = ii;
  w = w / scale_factor;
  z = z / scale_factor;
  d2 /= scale_factor;
  d3 /= scale_factor;
  return (((x * d1 + y) * d2) + z) * d3 + w;
}

int translate_idx_inv(int ii, int d1, int d2, int d3, int scale_factor, int off_x, int off_y) __attribute__((hc, cpu)) {
  int x, y, z, w;
  w = ii % d3;
  ii = ii / d3;
  z = ii % d2;
  ii = ii / d2;
  y = ii % d1;
  ii = ii / d1;
  x = ii;
  w = w * scale_factor + off_x;
  z = z * scale_factor + off_y;
  d2 *= scale_factor;
  d3 *= scale_factor;
  return (((x * d1 + y) * d2) + z) * d3 + w;
}

void upscale(THHcState* state, float* &avInp, long inpOffset,
             float* &avOut, long outOffset,
             unsigned int inpSz, unsigned int outSz, long no_elements,
             int scale_factor, int d1, int d2, int d3, unsigned int grdConf[]) {
  hc::extent<2> grdExt(grdConf[1], grdConf[0] * 256);
  hc::tiled_extent<2> t_ext = grdExt.tile(1, 256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    long ii = tidx.global[1];
    ii += tidx.local[0] + tidx.tile_dim[0] * (tidx.tile_dim[1] * grdExt[1]) * tidx.tile[0];
    
    if (ii >= no_elements) {
      return;
    }
    
    int ipidx = translate_idx(ii, d1, d2, d3, scale_factor);
    avOut[outOffset + ii] = avInp[inpOffset + ipidx];
  }).wait();
}

static int hcnn_SpatialUpSamplingNearest_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor_zero(state, output);
  int scale_factor = luaT_getfieldcheckint(L, 1, "scale_factor");
  input = THHcTensor_newContiguous(state, input);
  // This is for allocating output Tensor
  long no_elements = 1;
  
  for (int i = 0; i < input->nDimension; i++) {
    no_elements *= input->size[i];
  }
  
  no_elements *= scale_factor * scale_factor;
  int d1;
  int d2;
  int d3;
  
  if (input->nDimension == 3) {
    d1 = output->size[0];
    d2 = output->size[1];
    d3 = output->size[2];
  } else {
    d1 = output->size[1];
    d2 = output->size[2];
    d3 = output->size[3];
  }
  
  // blocks & threads:
  long nthreads = 256;
  // Max number of blocks: http://en.wikipedia.org/wiki/Hc
  // 65535 for SM 2.x, 2^32 -1 for >= 3.0
  // TODO: When we move to SM 3.5 we should update this
  long n_xblocks = std::min(std::max((int)ceil((float)no_elements / nthreads), 1), 65535);
  long n_yblocks = (long)ceil((float)no_elements / (float)(n_xblocks * nthreads));
  
  if (n_yblocks > 65535) {
    THError("Input size is too large!  aborting");
  }
  
  unsigned int grdConf[2];
  grdConf[0] = n_xblocks;
  grdConf[1] = n_yblocks;
  unsigned int inpSz = THHcTensor_nElement(state, input);
  unsigned int outSz = THHcTensor_nElement(state, output);
  auto avInput = input->get_device_data(state);
  auto avOutput = output->get_device_data(state);
  // kernel:
  upscale(state, avInput, input->storageOffset,
          avOutput, output->storageOffset,
          inpSz, outSz, no_elements, scale_factor, d1, d2, d3, grdConf);
  THHcTensor_free(state, input);
  return 1;
}

void downscale(THHcState* state, float* &avInp, long inpOffset,
               float* &avOut, long outOffset,
               unsigned int gradInpSz, unsigned int gradOutSz, long no_elements,
               int scale_factor, int d1, int d2, int d3, unsigned int gridConf[]) {
  hc::extent<2> grdExt(gridConf[1], gridConf[0] * 256);
  hc::tiled_extent<2> t_ext = grdExt.tile(1, 256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    long ii = tidx.global[1];
    ii += tidx.local[0] + tidx.tile_dim[0] * (tidx.tile_dim[1] * grdExt[1]) * tidx.tile[0];
    
    if (ii >= no_elements) {
      return;
    }
    
    for (int i = 0; i < scale_factor; i++) {
      for (int j = 0; j < scale_factor; j++) {
        int ipidx = translate_idx_inv(ii, d1, d2, d3, scale_factor, i, j);
        avInp[inpOffset + ii] += avOut[outOffset + ipidx];
      }
    }
  }).wait();
}


static int hcnn_SpatialUpSamplingNearest_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput  = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  int scale_factor = luaT_getfieldcheckint(L, 1, "scale_factor");
  THHcTensor_zero(state, gradInput);
  long no_elements = 1;
  
  for (int i = 0; i < gradInput->nDimension; i++) {
    no_elements *= gradInput->size[i];
  }
  
  int d1;
  int d2;
  int d3;
  
  if (gradInput->nDimension == 3) {
    d1 = gradInput->size[0];
    d2 = gradInput->size[1];
    d3 = gradInput->size[2];
  } else {
    d1 = gradInput->size[1];
    d2 = gradInput->size[2];
    d3 = gradInput->size[3];
  }
  
  // blocks & threads:
  long nthreads = 256;
  // Max number of blocks: http://en.wikipedia.org/wiki/Hc
  // 65535 for SM 2.x, 2^32 -1 for >= 3.0
  // TODO: When we move to SM 3.5 we should update this
  long n_xblocks = std::min(std::max((int)ceil((float)no_elements / nthreads), 1), 65535);
  long n_yblocks = (long)ceil((float)no_elements / (float)(n_xblocks * nthreads));
  
  if (n_yblocks > 65535) {
    THError("Input size is too large!  aborting");
  }
  
  unsigned int gradConf[2];
  gradConf[0] = n_xblocks;
  gradConf[1] = n_yblocks;
  auto avGradInput = gradInput->get_device_data(state);
  auto avGradOutput = gradOutput->get_device_data(state);
  // kernel:
  downscale(state, avGradInput, gradInput->storageOffset,
            avGradOutput, gradOutput->storageOffset,
            THHcTensor_nElement(state, gradInput), THHcTensor_nElement(state, gradOutput),
            no_elements, scale_factor, d1, d2, d3, gradConf);
  return 1;
}

static const struct luaL_Reg hcnn_SpatialUpSamplingNearest__ [] = {
  {"SpatialUpSamplingNearest_updateOutput", hcnn_SpatialUpSamplingNearest_updateOutput},
  {"SpatialUpSamplingNearest_updateGradInput", hcnn_SpatialUpSamplingNearest_updateGradInput},
  {NULL, NULL}
};

/* Binding the SpatialUpSamplingNearest layer routines with lua stack */
void hcnn_SpatialUpSamplingNearest_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_SpatialUpSamplingNearest__, "nn");
  lua_pop(L, 1);
}
