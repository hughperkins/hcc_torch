/**********************************************************************************************************
 * The Kullback�Leibler divergence criterion.
 *
 * criterion = nn.DistKLDivCriterion()
 * Functionality : KL divergence is a useful distance measure for continuous distributions and
 *                 is often useful when performing direct regression over the space of (discretely sampled)
 *                 continuous output distributions. As with ClassNLLCriterion, the input given through a
 *                 forward() is expected to contain log-probabilities, however unlike ClassNLLCriterion,
 *                 input is not restricted to a 1D or 2D vector (as the criterion is applied element-wise).
 *
 * This criterion expect a target Tensor of the same size as the input Tensor
 * when calling forward(input, target) and backward(input, target).
 *
 * The loss can be described as:
 * loss(x, target) = \sum(target_i * (log(target_i) - x_i))
 *
 *************************************************************************************************************/

#include <numeric>
#include "utils.h"
#include "amp_math.h"
#include "THHcFunctorOpns.h"

static int hcnn_DistKLDivCriterion_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  luaL_argcheck(L, THHcTensor_nElement(state, input) == THHcTensor_nElement(state, target), 2,
                "input and target need to have the same number of elements");
  float sum;
  long size = THHcTensor_nElement(state, input);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  sum = InnerProduct_plus_kl(state, input, target);
  
  if (sizeAverage) {
    sum /= size;
  }
  
  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  lua_pushnumber(L, sum);
  lua_setfield(L, 1, "output");
  lua_pushnumber(L, sum);
  return 1;
}

static int hcnn_DistKLDivCriterion_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  luaL_argcheck(L, THHcTensor_nElement(state, input) == THHcTensor_nElement(state, target), 2,
                "input and target need to have the same number of elements");
  long size = THHcTensor_nElement(state, input);
  float norm = (sizeAverage ? 2. / size : 2.);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  THHcTensor_resizeAs(state, gradInput, input);
  transform_kl(state, input, target, gradInput, norm);
  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  return 1;
}

static const struct luaL_Reg hcnn_DistKLDivCriterion__ [] = {
  {"DistKLDivCriterion_updateOutput", hcnn_DistKLDivCriterion_updateOutput},
  {"DistKLDivCriterion_updateGradInput", hcnn_DistKLDivCriterion_updateGradInput},
  {NULL, NULL}
};

/* Binding the DistKLDivCriterion layer routines with lua stack */
void hcnn_DistKLDivCriterion_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_DistKLDivCriterion__, "nn");
  lua_pop(L, 1);
}
