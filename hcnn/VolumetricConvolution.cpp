/************************************************************************************************************************
 *VolumetricConvolution : a 3D convolution over an input video (a sequence of images)
 *
 * module = nn.VolumetricConvolution(nInputPlane, nOutputPlane, kT, kW, kH [, dT, dW, dH])
 *
 * Functionality: Applies a 3D convolution over an input image composed of several input planes.
 *                The input tensor in forward(input) is expected to be a 4D tensor (nInputPlane x time x height x width).
 *
 * The parameters are the following:
 *                nInputPlane: The number of expected input planes in the image given into forward().
 *                nOutputPlane: The number of output planes the convolution layer will produce.
 *                kT: The kernel size of the convolution in time
 *                kW: The kernel width of the convolution
 *                kH: The kernel height of the convolution
 *                dT: The step of the convolution in the time dimension. Default is 1.
 *                dW: The step of the convolution in the width dimension. Default is 1.
 *                dH: The step of the convolution in the height dimension. Default is 1.
 *
 * Note that depending of the size of your kernel, several (of the last) columns
 * or rows of the input image might be lost. It is up to the user to add proper padding in images.
 *
 * If the input image is a 4D tensor nInputPlane x time x height x width, the output image size
 * will be nOutputPlane x otime x owidth x oheight where
 *                otime   = (time  - kT)  / dT + 1
 *                owidth  = (width  - kW) / dW + 1
 *                oheight = (height - kH) / dH + 1 .
 *
 * The parameters of the convolution can be found in self.weight
 * (Tensor of size nOutputPlane x nInputPlane x kT x kH x kW) and self.bias (Tensor of size nOutputPlane).
 * The corresponding gradients can be found in self.gradWeight and self.gradBias.
 *
 *************************************************************************************************************************/
#include "utils.h"
#include "amp_math.h"
#include "THHcGeneral.h"
#include "THHcTensor.h"
#include "hcblaslib.h"

#define NUMTHREADS 256

/* im3d2col Kernel */
/* Functionality: Rearrange blocks from matrix (here 3D image sequence or video) into columns.
 *
 * Input Parameters:
 *       state: Encapsulates target device accelerator info
 *       avData_im : Input Image sequence
 *       inOffset: Input Image offset from wherein the data shall be transformed to column
 *       height, width & depth: Dimensions of the 3D input image sequence
 *       channels: Numbers of input image chanels. For eg In case of RGB, channels=3
 *       ksize_w, ksize_h ksize_d : Width, Height and depth of the 3D block.
 *       pad_w, pad_h & pad_w: Padding width, Padding height and Padding width.
 *                             Utilized when transformation is done for distinct ksize_w x ksize_h blocks of input image.
 *       stride_w, stride_h and stride_d: Dimensions to control the sliding of ksize_w x ksize_h x ksize_d 3D block within input image
 *       colOffset: Column offset from wherein the output column vectors are written out.
 *
 * Output Parameter:
 *       avData_col: Output Column matrix
 */
void im3d2col(THHcState* state, float* &avData_im, long imOffset,
              const int channels, const int height, const int width, const int depth,
              const int kernel_h, const int kernel_w, const int kernel_d,
              const int pad_h, const int pad_w, const int pad_d,
              const int stride_h, const int stride_w, const int stride_d,
              float* &avData_col, long colOffset) {
  // We are going to launch channels * height_col * width_col * depth_col kernels, each
  // kernel responsible for copying a single-channel grid.
  int height_col = (height + 2 * pad_h - kernel_h) / stride_h + 1;
  int width_col = (width + 2 * pad_w - kernel_w) / stride_w + 1;
  int depth_col = (depth + 2 * pad_d - kernel_d) / stride_d + 1;
  int num_kernels = channels * height_col * width_col * depth_col;
  unsigned grdSz = (num_kernels + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(NUMTHREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    long dataCol = colOffset;
    long dataIm = imOffset;
    int index = tidx.global[0];

    if(index < num_kernels) {
      int d_out = index % depth_col;
      int w_index = index / depth_col;
      int w_out = w_index % width_col;
      int h_index = w_index / width_col;
      int h_out = h_index % height_col;
      int channel_in = h_index / height_col;
      //channel_in = 1;
      int channel_out = channel_in * kernel_h * kernel_w * kernel_d;
      int h_in = h_out * stride_h - pad_h;
      int w_in = w_out * stride_w - pad_w;
      int d_in = d_out * stride_d - pad_d;
      dataCol += channel_out * (height_col * width_col * depth_col) +
                 h_out * (width_col * depth_col) + w_out * depth_col + d_out;
      dataIm += channel_in * (height * width * depth) +
                h_in * (width * depth) + w_in * depth + d_in;

      for (int i = 0; i < kernel_h; ++i) {
        int h = h_in + i;

        for (int j = 0; j < kernel_w; ++j) {
          int w = w_in + j;

          for (int k = 0; k < kernel_d; ++k) {
            int d = d_in + k;
            avData_col[dataCol] = (h >= 0 && w >= 0 && d >= 0 &&
                                   h < height && w < width && d < depth) ?
                                  avData_im[dataIm + i * (width * depth) + j * depth + k] : 0;
            dataCol += height_col * width_col * depth_col;
          }
        }
      }
    }
  }).wait();
}

/* col3d2im kernel */
/* Functionality: Rearrange 3D block columns back into 3D matrix (image sequence / video).
 *
 * Rearranges columns of the matrix B, representing blocks of size (patch_h x patch_w x patch_d) from a matrix of
   size (height x width x depth), back into its original size, usually close to (height x width x depth).
 * This function is most useful as reverse operation to im3d2col.
 *
 * Input Parameters:
 *       state: Encapsulates target device accelerator info
 *       avData_col : Input Column matrix
 *       height & width: Dimensions of the output image that got transformed to input column matrix by im2col
 *       colOffset: Input column offset from wherein the data shall be transformed back to image
 *       channels: Numbers of output image chanels. For eg In case of RGB, channels=3
 *       patch_w, patch_h & patch_d : Width, Height and Depth of the block.
 *       pad_w, pad_h & pad_d: Padding width, Padding height and Padding width.
 *                             Utilized when transformation is done for distinct ksize_w x ksize_h blocks of input image.
 *       stride_w, stride_h & stride_d: Dimensions  to control the sliding of ksize_w x ksize_h x ksize_d block within input image
 *       colOffset: Column offset from wherein the output column vectors are written out.
 *       height_col,  width_col & depth_col: Dimensions of the input column matrix.
 *
 * Output Parameter
 *       avData_im : Output restored 3D image
 */
void col2im3d(THHcState* state, float* &avData_columns, long columnOffset, const int channels,
              const int height, const int width, const int depth,
              const int patch_h, const int patch_w, const int patch_d,
              const int pad_h, const int pad_w, const int pad_d,
              const int stride_h, const int stride_w, const int stride_d,
              float* &avData_input, long inputOffset) {
  int height_col = (height + 2 * pad_h - patch_h) / stride_h + 1;
  int width_col = (width + 2 * pad_w - patch_w) / stride_w + 1;
  int depth_col = (depth + 2 * pad_d - patch_d) / stride_d + 1;
  int n = channels * height * width * depth;
  unsigned grdSz = (n + NUMTHREADS) - (n % NUMTHREADS);
  hc::extent<1> grdExt(grdSz);
  hc::tiled_extent<1> t_ext = grdExt.tile(NUMTHREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    for (int i = tidx.global[0]; i < (n); i += grdExt[0]) {
      float val = 0;
      int d = i % depth + pad_d;
      int w_index = i / depth;
      int w = w_index % width + pad_w;
      int h_index = w_index / width;
      int h = h_index % height + pad_h;
      int c = h_index / height;
      // compute the start and end of the output
      int d_col_start = (d < patch_d) ? 0 : (d - patch_d) / stride_d + 1;
      int d_col_end = THMin(d / stride_d + 1, depth_col);
      int w_col_start = (w < patch_w) ? 0 : (w - patch_w) / stride_w + 1;
      int w_col_end = THMin(w / stride_w + 1, width_col);
      int h_col_start = (h < patch_h) ? 0 : (h - patch_h) / stride_h + 1;
      int h_col_end = THMin(h / stride_h + 1, height_col);
      int offset = (c * patch_h * patch_w * patch_d + h * patch_w * patch_d + w * patch_d + d) * height_col * width_col * depth_col;
      int coeff_h_col = (1 - stride_h * patch_w * patch_d * height_col) * width_col * depth_col;
      int coeff_w_col = (1 - stride_w * patch_d * height_col * width_col) * depth_col;
      int coeff_d_col = (1 - stride_d * height_col * width_col * depth_col);

      for (int d_col = d_col_start; d_col < d_col_end; ++d_col)
        for (int h_col = h_col_start; h_col < h_col_end; ++h_col) {
          for (int w_col = w_col_start; w_col < w_col_end; ++w_col) {
            val += avData_columns[columnOffset + offset + h_col * coeff_h_col + w_col * coeff_w_col + d_col * coeff_d_col];
          }
        }

      avData_input[inputOffset + i] = val;
    }
  }).wait();
}

/* UpdateOutput routine invoked during Forward Propogation of convolution layer */
/* Computes the output using the current parameter set of the class (VolumetricConvolution) and input.
 * This function returns the result which is stored in the output field.
 */
static int hcnn_VolumetricConvolution_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  // Input
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  // Params:
  int dD = luaT_getfieldcheckint(L, 1, "dW");
  int dW = luaT_getfieldcheckint(L, 1, "dH");
  int dH = luaT_getfieldcheckint(L, 1, "dT");
  int kD = luaT_getfieldcheckint(L, 1, "kW");
  int kW = luaT_getfieldcheckint(L, 1, "kH");
  int kH = luaT_getfieldcheckint(L, 1, "kT");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  int nOutputPlane = luaT_getfieldcheckint(L, 1, "nOutputPlane");
  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", torch_Tensor);
  THHcTensor* bias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "bias", torch_Tensor);
  THHcTensor* columns = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "finput", torch_Tensor);
  THHcTensor* ones = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "fgradInput", torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 4 || input->nDimension == 5, 2, "4D or 5D (batch mode) tensor is expected");
  int batch = 1;

  if (input->nDimension == 4) {
    // Force batch
    batch = 0;
    THHcTensor_resize5d(state, input, 1, input->size[0], input->size[1],
                         input->size[2], input->size[3]);
  }

  long inputWidth   = input->size[3];
  long inputHeight  = input->size[2];
  long inputDepth   = input->size[4];
  long outputWidth  = (inputWidth  - kW) / dW + 1;
  long outputHeight = (inputHeight - kH) / dH + 1;
  long outputDepth  = (inputDepth - kD) / dD + 1;
  // Batch size + input planes
  long batchSize = input->size[0];
  // Resize output
  THHcTensor_resize5d(state, output, batchSize, nOutputPlane, outputDepth,
                       outputHeight, outputWidth);
  // Resize temporary columns
  THHcTensor_resize2d(state, columns, nInputPlane * kD * kW * kH, outputDepth * outputHeight * outputWidth);

  // Define a buffer of ones, for bias accumulation
  // Note: this buffer can be shared with other modules, it only ever gets increased,
  // and always contains ones.
  if (ones->nDimension != 3 || ones->size[0]*ones->size[1]*ones->size[2] < outputDepth * outputHeight * outputWidth) {
    // Resize plane and fill with ones...
    THHcTensor_resize3d(state, ones, outputDepth, outputHeight, outputWidth);
    THHcTensor_fill(state, ones, 1);
  }

  auto avData_input = input->get_device_data(state);
  auto avData_weight = weight->get_device_data(state);
  auto avData_bias = bias->get_device_data(state);
  auto avData_columns = columns->get_device_data(state);
  auto avData_ones = ones->get_device_data(state);
  auto avData_output = output->get_device_data(state);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;

  // For each elt in batch, do:
  for (int elt = 0; elt < batchSize; elt ++) {
    // Do Bias first:
    // M,N,K are dims of matrix A and B
    // (see http://docs.nvidia.com/Hc/cublas/#cublas-lt-t-gt-gemm)
    long m_ = nOutputPlane;
    long n_ = outputDepth * outputHeight * outputWidth;
    long k_ = 1;
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, Trans, NoTrans, n_, m_, k_, 1,
                        avData_ones, k_,
                        avData_bias, k_, 0,
                        avData_output, n_, ones->storageOffset, bias->storageOffset, output->storageOffset + output->stride[0] * elt);
    // Extract columns:
    im3d2col(
      state,
      avData_input, input->storageOffset + input->stride[0] * elt,
      nInputPlane, inputHeight, inputWidth, inputDepth, kH, kW, kD, 0, 0, 0, dH, dW, dD,
      avData_columns, columns->storageOffset
    );
    // M,N,K are dims of matrix A and B
    // (see http://docs.nvidia.com/Hc/cublas/#cublas-lt-t-gt-gemm)
    long m = weight->size[0];
    long n = columns->size[1];
    long k = weight->size[1] * weight->size[2] * weight->size[3] * weight->size[4];
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, NoTrans, NoTrans, n, m, k, 1,
                        avData_columns, n,
                        avData_weight, k, 1,
                        avData_output, n, columns->storageOffset, weight->storageOffset, output->storageOffset + output->stride[0] * elt);
  }

  // Resize output
  if (batch == 0) {
    THHcTensor_resize4d(state, output, nOutputPlane, outputHeight, outputWidth, outputDepth);
    THHcTensor_resize4d(state, input, nInputPlane, inputHeight, inputWidth, inputDepth);
  }

  // return output
  return 1;
}

/* UpdateGradInput routine invoked during Backward Propogation of convolution layer */
/*
 * Computing the gradient of the VolumetricConvolution module with respect to its own input.
 * This is returned in gradInput. Also, the gradInput state variable is updated accordingly
 */
static int hcnn_VolumetricConvolution_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  // Inputs
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  // Params
  int dD = luaT_getfieldcheckint(L, 1, "dW");
  int dW = luaT_getfieldcheckint(L, 1, "dH");
  int dH = luaT_getfieldcheckint(L, 1, "dT");
  int kD = luaT_getfieldcheckint(L, 1, "kW");
  int kW = luaT_getfieldcheckint(L, 1, "kH");
  int kH = luaT_getfieldcheckint(L, 1, "kT");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  int nOutputPlane = luaT_getfieldcheckint(L, 1, "nOutputPlane");
  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", torch_Tensor);
  THHcTensor* gradColumns = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "finput", torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 4 || input->nDimension == 5, 2, "4D or 5D (batch mode) tensor is expected");
  int batch = 1;

  if (input->nDimension == 4) {
    // Force batch
    batch = 0;
    THHcTensor_resize5d(state, input, 1, input->size[0], input->size[1], input->size[2], input->size[3]);
    THHcTensor_resize5d(state, gradOutput, 1, gradOutput->size[0], gradOutput->size[1], gradOutput->size[2], gradOutput->size[3]);
  }

  long inputWidth   = input->size[3];
  long inputHeight  = input->size[2];
  long inputDepth   = input->size[4];
  long outputWidth  = (inputWidth - kW) / dW + 1;
  long outputHeight = (inputHeight - kH) / dH + 1;
  long outputDepth  = (inputDepth - kD) / dD + 1;
  // Batch size + input planes
  long batchSize = input->size[0];
  // Resize output
  THHcTensor_resize5d(state, gradInput, batchSize, nInputPlane, inputDepth, inputHeight, inputWidth);
  // Resize temporary columns
  THHcTensor_resize2d(state, gradColumns, nInputPlane * kW * kH * kD, outputDepth * outputHeight * outputWidth);
  auto avData_gradColumns = gradColumns->get_device_data(state);
  auto avData_gradInput = gradInput->get_device_data(state);
  auto avData_gradOutput = gradOutput->get_device_data(state);
  auto avData_weight = weight->get_device_data(state);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;

  // For each elt in batch, do:
  for (int elt = 0; elt < batchSize; elt ++) {
    // M,N,K are dims of matrix A and B
    // (see http://docs.nvidia.com/Hc/cublas/#cublas-lt-t-gt-gemm)
    long m = weight->size[1] * weight->size[2] * weight->size[3] * weight->size[4];
    long n = gradColumns->size[1];
    long k = weight->size[0];
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, NoTrans, Trans, n, m, k, 1,
                        avData_gradOutput, n,
                        avData_weight, m, 0,
                        avData_gradColumns, n, gradOutput->storageOffset + gradOutput->stride[0] * elt, weight->storageOffset, gradColumns->storageOffset);
    // Unpack columns back into input:
    col2im3d(
      state,
      avData_gradColumns, gradColumns->storageOffset,
      nInputPlane, inputHeight, inputWidth, inputDepth, kH, kW, kD, 0, 0, 0, dH, dW, dD,
      avData_gradInput, gradInput->storageOffset + gradInput->stride[0] * elt
    );
  }

  // Resize output
  if (batch == 0) {
    THHcTensor_resize4d(state, gradOutput, nOutputPlane, outputHeight, outputWidth, outputDepth);
    THHcTensor_resize4d(state, input, nInputPlane, inputHeight, inputWidth, inputDepth);
    THHcTensor_resize4d(state, gradInput, nInputPlane, inputHeight, inputWidth, inputDepth);
  }

  // Return gradInput
  return 1;
}

/* AccGradParameters routine invoked at the end of Back Propogation of convolution layer*/
/*
 * Computing the gradient of the VolumetricConvolution module with respect to its ownparameters.
 * Many modules do not perform this step as they do not have any parameters.
 * The state variable name for the parameters is module dependent.
 * The VolumetricConvolution module is expected to accumulate the gradients with respect to the parameters
 * in some variable.

 * scale is a scale factor that is multiplied with the gradParameters before being accumulated.
*/
static int hcnn_VolumetricConvolution_accGradParameters(lua_State* L) {
  THHcState* state = getGputorchState(L);
  // Inputs
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  // Params
  int dD = luaT_getfieldcheckint(L, 1, "dW");
  int dW = luaT_getfieldcheckint(L, 1, "dH");
  int dH = luaT_getfieldcheckint(L, 1, "dT");
  int kD = luaT_getfieldcheckint(L, 1, "kW");
  int kW = luaT_getfieldcheckint(L, 1, "kH");
  int kH = luaT_getfieldcheckint(L, 1, "kT");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  int nOutputPlane = luaT_getfieldcheckint(L, 1, "nOutputPlane");
  float scale = luaL_optnumber(L, 4, 1);
  THHcTensor* gradWeight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradWeight", torch_Tensor);
  THHcTensor* gradBias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradBias", torch_Tensor);
  THHcTensor* columns = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "finput", torch_Tensor);
  THHcTensor* ones = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "fgradInput", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 4 || input->nDimension == 5, 2, "3D or 4D (batch mode) tensor is expected");
  int batch = 1;

  if (input->nDimension == 4) {
    // Force batch
    batch = 0;
    THHcTensor_resize5d(state, input, 1, input->size[0], input->size[1], input->size[2], input->size[3]);
    THHcTensor_resize5d(state, gradOutput, 1, gradOutput->size[0], gradOutput->size[1], gradOutput->size[2], gradOutput->size[3]);
  }

  long inputWidth   = input->size[3];
  long inputHeight  = input->size[2];
  long inputDepth   = input->size[4];
  long outputWidth  = (inputWidth - kW) / dW + 1;
  long outputHeight = (inputHeight - kH) / dH + 1;
  long outputDepth  = (inputDepth - kD) / dD + 1;
  // Batch size + input planes
  long batchSize = input->size[0];

  // Define a buffer of ones, for bias accumulation
  if (ones->nDimension != 3 || ones->size[0]*ones->size[1]*ones->size[2] < outputDepth * outputHeight * outputWidth) {
    // Resize plane and fill with ones...
    THHcTensor_resize3d(state, ones, outputDepth, outputHeight, outputWidth);
    THHcTensor_fill(state, ones, 1);
  }

  // Resize temporary columns
  THHcTensor_resize2d(state, columns, nInputPlane * kW * kH * kD, outputDepth * outputHeight * outputWidth);
  auto avData_columns = columns->get_device_data(state);
  auto avData_input = input->get_device_data(state);
  auto avData_gradOutput = gradOutput->get_device_data(state);
  auto avData_gradWeight = gradWeight->get_device_data(state);
  auto avData_ones = ones->get_device_data(state);
  auto avData_gradBias = gradBias->get_device_data(state);
  long m_ = nOutputPlane;
  long k_ = outputDepth * outputHeight * outputWidth;
  int lenX = k_;
  int lenY = m_;
  int len_X = (lenX + (NUMTHREADS - 1)) & ~(NUMTHREADS - 1);
  int numBlocks = len_X / NUMTHREADS;
  float* temp_buf;
  THHcCheck(THHcMalloc(state, (void**) &temp_buf, numBlocks * lenY * sizeof(float)));
  hc::extent<1> ext(numBlocks * lenY);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  Hcblaslibrary hcBlas;

  // For each elt in batch, do:
  for (int elt = 0; elt < batchSize; elt ++) {
    // Extract columns:
    im3d2col(
      state,
      avData_input, input->storageOffset + input->stride[0] * elt,
      nInputPlane, inputHeight, inputWidth, inputDepth, kH, kW, kD, 0, 0, 0, dH, dW, dD,
      avData_columns, columns->storageOffset
    );
    // M,N,K are dims of matrix A and B
    // (see http://docs.nvidia.com/Hc/cublas/#cublas-lt-t-gt-gemm)
    long m = gradWeight->size[0];
    long n = gradWeight->size[1] * gradWeight->size[2] * gradWeight->size[3] * gradWeight->size[4];
    long k = columns->size[1];
    // Do GEMM (note: this is a bit confusing because gemm assumes column-major matrices)
    hcBlas.hcblas_sgemm(accl_view, ColMajor, Trans, NoTrans, n, m, k, scale,
                        avData_columns, k,
                        avData_gradOutput, k, 1,
                        avData_gradWeight, n, columns->storageOffset, gradOutput->storageOffset + gradOutput->stride[0] * elt, gradWeight->storageOffset);
    // Do Bias:
    // M,N,K are dims of matrix A and B
    // (see http://docs.nvidia.com/Hc/cublas/#cublas-lt-t-gt-gemm)
    // Do GEMV (note: this is a bit confusing because gemv assumes column-major matrices)
    THHcBlas_gemv(
      state,
      't',
      k_, m_,
      scale,
      avData_gradOutput, gradOutput->storageOffset + gradOutput->stride[0] * elt,
      avData_ones, ones->storageOffset, 1,
      1,
      avData_gradBias, gradBias->storageOffset, 1, temp_buf
    );
  }

  // Resize
  if (batch == 0) {
    THHcTensor_resize4d(state, gradOutput, nOutputPlane, outputHeight, outputWidth, outputDepth);
    THHcTensor_resize4d(state, input, nInputPlane, inputHeight, inputWidth, inputDepth);
  }

  THHcCheck(THHcFree(state, temp_buf));
  // Return nothing
  return 0;
}

static const struct luaL_Reg hcnn_VolumetricConvolution__ [] = {
  {"VolumetricConvolution_updateOutput", hcnn_VolumetricConvolution_updateOutput},
  {"VolumetricConvolution_updateGradInput", hcnn_VolumetricConvolution_updateGradInput},
  {"VolumetricConvolution_accGradParameters", hcnn_VolumetricConvolution_accGradParameters},
  {NULL, NULL}
};

/* Binding the Convolution layer routines with lua stack */
void hcnn_VolumetricConvolution_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_VolumetricConvolution__, "nn");
  lua_pop(L, 1);
}

