/*****************************************************************************************************************
 * Temporal Modules apply to sequences with a one-dimensional relationship
 * (For e.g. sequences of words, phonemes and letters. Strings of some kind).
 *
 * TemporalConvolution : a 1D convolution over an input sequence ;
 *
 * module = nn.TemporalConvolution(inputFrameSize, outputFrameSize, kW, [dW])
 *
 * Functionality: Applies a 1D convolution over an input sequence composed of nInputFrame frames.
 *                The input tensor in forward(input) is expected to be a 2D tensor (nInputFrame x inputFrameSize)
 *                or a 3D tensor (nBatchFrame x nInputFrame x inputFrameSize).
 *
 * The parameters are the following:
 *                inputFrameSize: The input frame size expected in sequences given into forward().
 *                outputFrameSize: The output frame size the convolution layer will produce.
 *                kW: The kernel width of the convolution
 *                dW: The step of the convolution. Default is 1.
 *
 * Note that depending of the size of your kernel, several (of the last) frames of the sequence
 * might be lost. It is up to the user to add proper padding frames in the input sequences.
 * If the input sequence is a 2D tensor of dimension nInputFrame x inputFrameSize,
 * the output sequence will be nOutputFrame x outputFrameSize where
 * nOutputFrame = (nInputFrame - kW) / dW + 1
 *
 * If the input sequence is a 3D tensor of dimension nBatchFrame x nInputFrame x inputFrameSize,
 * the output sequence will be nBatchFrame x nOutputFrame x outputFrameSize.
 * The parameters of the convolution can be found in self.weight
 * (Tensor of size outputFrameSize x (inputFrameSize x kW)) and self.bias (Tensor of size outputFrameSize).
 * The corresponding gradients can be found in self.gradWeight and self.gradBias.
 *
 * For a 2D input, the output value of the layer can be precisely described as:
 *                output[t][i] = bias[i] + sum_j sum_{k=1}^kW weight[i][j][k] * input[dW*(t-1)+k)][j]
 *
 *****************************************************************************************************************/
#include "utils.h"

static int hcnn_TemporalConvolution_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int inputFrameSize = luaT_getfieldcheckint(L, 1, "inputFrameSize");
  int outputFrameSize = luaT_getfieldcheckint(L, 1, "outputFrameSize");
  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", torch_Tensor);
  THHcTensor* bias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "bias", torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* outputWindow, *inputWindow;
  int nInputFrame, nOutputFrame;
  long k, i;
  int dimS = 0; // sequence dimension
  int dimF = 1; // feature dimension
  luaL_argcheck(L, input->nDimension == 2 || input->nDimension == 3, 2, "2D or 3D(batch mode) tensor expected");

  if (input->nDimension == 3) {
    dimS = 1;
    dimF = 2;
  }

  luaL_argcheck(L, input->size[dimF] == inputFrameSize, 2, "invalid input frame size");
  luaL_argcheck(L, input->size[dimS] >= kW, 2, "input sequence smaller than kernel size");
  input = THHcTensor_newContiguous(state, input);
  outputWindow = THHcTensor_new(state);
  inputWindow = THHcTensor_new(state);
  nInputFrame = input->size[dimS];
  nOutputFrame = (nInputFrame - kW) / dW + 1;

  if (input->nDimension == 2) {
    THHcTensor_resize2d(state, output, nOutputFrame, outputFrameSize);

    /* bias first */
    for (k = 0; k < nOutputFrame; k++) {
      THHcTensor_select(state, outputWindow, output, 0, k);
      THHcTensor_copy(state, outputWindow, bias);
    }

    /* ouch */
    for (k = 0; nOutputFrame > 0; k++) {
      long outputFrameStride = (kW - 1) / dW + 1;
      long inputFrameStride = outputFrameStride * dW;
      long nFrame = (nInputFrame - k * dW - kW) / inputFrameStride + 1;
      nOutputFrame -= nFrame;
      THHcTensor_setStorage2d(state, inputWindow, input->storage, input->storageOffset + k * dW * input->size[1],
                               nFrame, inputFrameStride * input->size[1], kW * input->size[1], 1);
      THHcTensor_setStorage2d(state, outputWindow, output->storage, output->storageOffset + k * output->size[1],
                               nFrame, outputFrameStride * output->size[1], output->size[1], 1);
      THHcTensor_transpose(state, weight, NULL, 0, 1);
      THHcTensor_addmm(state, outputWindow, 1, outputWindow, 1, inputWindow, weight);
      THHcTensor_transpose(state, weight, NULL, 0, 1);
    }
  } else {
    THHcTensor* outputSample = THHcTensor_new(state);
    THHcTensor* inputSample = THHcTensor_new(state);
    int nBatchFrame = input->size[0];
    THHcTensor_resize3d(state, output, nBatchFrame, nOutputFrame, outputFrameSize);

    for (i = 0; i < nBatchFrame; i++) {
      THHcTensor_select(state, outputSample, output, 0, i);
      THHcTensor_select(state, inputSample, input, 0, i);
      long nOutputSampleFrame = nOutputFrame;

      /* bias first */
      for (k = 0; k < nOutputFrame; k++) {
        THHcTensor_select(state, outputWindow, outputSample, 0, k);
        THHcTensor_copy(state, outputWindow, bias);
      }

      /* ouch */
      for (k = 0; nOutputSampleFrame > 0; k++) {
        long outputFrameStride = (kW - 1) / dW + 1;
        long inputFrameStride = outputFrameStride * dW;
        long nFrame = (nInputFrame - k * dW - kW) / inputFrameStride + 1;
        nOutputSampleFrame -= nFrame;
        THHcTensor_setStorage2d(state, inputWindow, inputSample->storage,
                                 inputSample->storageOffset + k * dW * inputSample->size[1],
                                 nFrame, inputFrameStride * inputSample->size[1],
                                 kW * inputSample->size[1], 1);
        THHcTensor_setStorage2d(state, outputWindow, outputSample->storage,
                                 outputSample->storageOffset + k * outputSample->size[1], nFrame,
                                 outputFrameStride * outputSample->size[1],
                                 outputSample->size[1], 1);
        THHcTensor_transpose(state, weight, NULL, 0, 1);
        THHcTensor_addmm(state, outputWindow, 1, outputWindow, 1, inputWindow, weight);
        THHcTensor_transpose(state, weight, NULL, 0, 1);
      }
    }

    THHcTensor_free(state, outputSample);
    THHcTensor_free(state, inputSample);
  }

  THHcTensor_free(state, outputWindow);
  THHcTensor_free(state, inputWindow);
  THHcTensor_free(state, input);
  return 1;
}

static int hcnn_TemporalConvolution_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  long nInputFrame;
  long nOutputFrame;
  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  THHcTensor* gradOutputWindow;
  THHcTensor* gradInputWindow;
  long k, i;
  int dimS = 0; // sequence dimension

  if (gradOutput->nDimension == 3) {
    dimS = 1;
  }

  nInputFrame = input->size[dimS];
  nOutputFrame = gradOutput->size[dimS];
  /* Not necessary with partial backprop: */
  gradOutputWindow = THHcTensor_new(state);
  gradInputWindow = THHcTensor_new(state);
  THHcTensor_resizeAs(state, gradInput, input);
  THHcTensor_zero(state, gradInput);

  if (gradOutput->nDimension == 2) {
    /* ouch */
    for (k = 0; nOutputFrame > 0; k++) {
      long outputFrameStride = (kW - 1) / dW + 1;
      long inputFrameStride = outputFrameStride * dW;
      long nFrame = (nInputFrame - k * dW - kW) / inputFrameStride + 1;
      nOutputFrame -= nFrame;
      THHcTensor_setStorage2d(state, gradOutputWindow, gradOutput->storage,
                               gradOutput->storageOffset + k * gradOutput->size[1], nFrame,
                               outputFrameStride * gradOutput->size[1], gradOutput->size[1], 1);
      THHcTensor_setStorage2d(state, gradInputWindow, gradInput->storage,
                               gradInput->storageOffset + k * dW * gradInput->size[1],
                               nFrame, inputFrameStride * gradInput->size[1],
                               kW * gradInput->size[1], 1);
      THHcTensor_addmm(state, gradInputWindow, 1, gradInputWindow, 1, gradOutputWindow, weight);
    }
  } else {
    THHcTensor* gradOutputSample = THHcTensor_new(state);
    THHcTensor* gradInputSample = THHcTensor_new(state);
    long nBatchFrame = input->size[0];

    for (i = 0; i < nBatchFrame; i++) {
      THHcTensor_select(state, gradOutputSample, gradOutput, 0, i);
      THHcTensor_select(state, gradInputSample, gradInput, 0, i);
      long nOutputSampleFrame = nOutputFrame;

      /* ouch */
      for (k = 0; nOutputSampleFrame > 0; k++) {
        long outputFrameStride = (kW - 1) / dW + 1;
        long inputFrameStride = outputFrameStride * dW;
        long nFrame = (nInputFrame - k * dW - kW) / inputFrameStride + 1;
        nOutputSampleFrame -= nFrame;
        THHcTensor_setStorage2d(state, gradOutputWindow, gradOutputSample->storage,
                                 gradOutputSample->storageOffset + k * gradOutputSample->size[1],
                                 nFrame, outputFrameStride * gradOutputSample->size[1],
                                 gradOutputSample->size[1], 1);
        THHcTensor_setStorage2d(state, gradInputWindow, gradInputSample->storage,
                                 gradInputSample->storageOffset + k * dW * gradInputSample->size[1],
                                 nFrame, inputFrameStride * gradInputSample->size[1],
                                 kW * gradInputSample->size[1], 1);
        THHcTensor_addmm(state, gradInputWindow, 1, gradInputWindow, 1, gradOutputWindow, weight);
      }
    }

    THHcTensor_free(state, gradOutputSample);
    THHcTensor_free(state, gradInputSample);
  }

  THHcTensor_free(state, gradOutputWindow);
  THHcTensor_free(state, gradInputWindow);
  return 1;
}

static int hcnn_TemporalConvolution_accGradParameters(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  float scale = luaL_optnumber(L, 4, 1);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  long nInputFrame;
  long nOutputFrame;
  THHcTensor* gradWeight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradWeight", torch_Tensor);
  THHcTensor* gradBias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradBias", torch_Tensor);
  THHcTensor* gradOutputWindow;
  THHcTensor* inputWindow;
  long k, i;
  int dimS = 0; // sequence dimension

  if (gradOutput->nDimension == 3) {
    dimS = 1;
  }

  nInputFrame = input->size[dimS];
  nOutputFrame = gradOutput->size[dimS];
  /* Not necessary with partial backprop: */
  input = THHcTensor_newContiguous(state, input);
  gradOutputWindow = THHcTensor_new(state);
  inputWindow = THHcTensor_new(state);

  if (input->nDimension == 2) {
    /* bias first */
    for (k = 0; k < nOutputFrame; k++) {
      THHcTensor_select(state, gradOutputWindow, gradOutput, 0, k);
      THHcTensor_cadd(state, gradBias, gradBias, scale, gradOutputWindow);
    }

    /* ouch */
    for (k = 0; nOutputFrame > 0; k++) {
      long outputFrameStride = (kW - 1) / dW + 1;
      long inputFrameStride = outputFrameStride * dW;
      long nFrame = (nInputFrame - k * dW - kW) / inputFrameStride + 1;
      nOutputFrame -= nFrame;
      THHcTensor_setStorage2d(state, inputWindow, input->storage,
                               input->storageOffset + k * dW * input->size[1],
                               nFrame, inputFrameStride * input->size[1],
                               kW * input->size[1], 1);
      THHcTensor_setStorage2d(state, gradOutputWindow, gradOutput->storage,
                               gradOutput->storageOffset + k * gradOutput->size[1],
                               nFrame, outputFrameStride * gradOutput->size[1],
                               gradOutput->size[1], 1);
      THHcTensor_transpose(state, gradOutputWindow, NULL, 0, 1);
      THHcTensor_addmm(state, gradWeight, 1, gradWeight, scale, gradOutputWindow, inputWindow);
      THHcTensor_transpose(state, gradOutputWindow, NULL, 0, 1);
    }
  } else {
    THHcTensor* gradOutputSample = THHcTensor_new(state);
    THHcTensor* inputSample = THHcTensor_new(state);
    long nBatchFrame = input->size[0];

    for (i = 0; i < nBatchFrame; i++) {
      THHcTensor_select(state, gradOutputSample, gradOutput, 0, i);
      THHcTensor_select(state, inputSample, input, 0, i);
      long nOutputSampleFrame = nOutputFrame;

      /* bias first */
      for (k = 0; k < nOutputFrame; k++) {
        THHcTensor_select(state, gradOutputWindow, gradOutputSample, 0, k);
        THHcTensor_cadd(state, gradBias, gradBias, scale, gradOutputWindow);
      }

      /* ouch */
      for (k = 0; nOutputSampleFrame > 0; k++) {
        long outputFrameStride = (kW - 1) / dW + 1;
        long inputFrameStride = outputFrameStride * dW;
        long nFrame = (nInputFrame - k * dW - kW) / inputFrameStride + 1;
        nOutputSampleFrame -= nFrame;
        THHcTensor_setStorage2d(state, inputWindow, inputSample->storage,
                                 inputSample->storageOffset + k * dW * inputSample->size[1],
                                 nFrame, inputFrameStride * inputSample->size[1],
                                 kW * inputSample->size[1], 1);
        THHcTensor_setStorage2d(state, gradOutputWindow, gradOutputSample->storage,
                                 gradOutputSample->storageOffset + k * gradOutputSample->size[1],
                                 nFrame, outputFrameStride * gradOutputSample->size[1],
                                 gradOutputSample->size[1], 1);
        THHcTensor_transpose(state, gradOutputWindow, NULL, 0, 1);
        THHcTensor_addmm(state, gradWeight, 1, gradWeight, scale, gradOutputWindow, inputWindow);
        THHcTensor_transpose(state, gradOutputWindow, NULL, 0, 1);
      }
    }

    THHcTensor_free(state, gradOutputSample);
    THHcTensor_free(state, inputSample);
  }

  THHcTensor_free(state, gradOutputWindow);
  THHcTensor_free(state, inputWindow);
  THHcTensor_free(state, input);
  return 1;
}

static const struct luaL_Reg hcnn_TemporalConvolution__ [] = {
  {"TemporalConvolution_updateOutput", hcnn_TemporalConvolution_updateOutput},
  {"TemporalConvolution_updateGradInput", hcnn_TemporalConvolution_updateGradInput},
  {"TemporalConvolution_accGradParameters", hcnn_TemporalConvolution_accGradParameters},
  {NULL, NULL}
};

/* Binding the TemporalConvolution layer routines with lua stack */
void hcnn_TemporalConvolution_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_TemporalConvolution__, "nn");
  lua_pop(L, 1);
}
