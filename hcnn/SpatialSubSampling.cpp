/****************************************************************************************************
 * SpatialSubSampling
 *
 * module = nn.SpatialSubSampling(nInputPlane, kW, kH, [dW], [dH])
 *
 * Functionality: Applies a 2D sub-sampling over an input image composed of
 *                several input planes. The input tensor in forward(input) is expected to be
 *                a 3D tensor (nInputPlane x height x width). The number of output planes will
 *                be the same as nInputPlane.
 *
 * The parameters are the following:
 *                nInputPlane: The number of expected input planes in the image given into forward().
 *                kW: The kernel width of the sub-sampling
 *                kH: The kernel height of the sub-sampling
 *                dW: The step of the sub-sampling in the width dimension. Default is 1.
 *                dH: The step of the sub-sampling in the height dimension. Default is 1.
 *
 * Note that depending of the size of your kernel, several (of the last) columns
 * or rows of the input image might be lost. It is up to the user to add proper padding in images.
 *
 * If the input image is a 3D tensor nInputPlane x height x width,
 * the output image size will be nInputPlane x oheight x owidth where
 *                owidth  = (width  - kW) / dW + 1
 *                oheight = (height - kH) / dH + 1
 *
 * The parameters of the sub-sampling can be found in self.weight (Tensor of size nInputPlane)
 * and self.bias (Tensor of size nInputPlane).
 * The corresponding gradients can be found in self.gradWeight and self.gradBias.
 *
 * The output value of the layer can be precisely described as:
 * output[i][j][k] = bias[k] + weight[k] sum_{s=1}^kW sum_{t=1}^kH input[dW*(i-1)+s)][dH*(j-1)+t][k]
 *
 *****************************************************************************************************/
#include "utils.h"
#define Hc_MAX_THREADS 256

/*
 * Description:
 *    this function subsamples an input 3D tensor along dimensions 1 and 2
 *    3D input, 3D output, 1D weight, 1D bias
 */
void subsample(THHcState* state, float* &avInput, long inOffset, long inpSize,
               float* &avOutput, long outOffset, long outSize,
               float* &avWeight, long weightOffset,
               float* &avBias, long biasOffset,
               int input_n, int input_h, int input_w,
               int kH, int kW, int dH, int dW, int xBlocks) {
  // output size
  int yBlocks = (int)(16L / input_n);
  yBlocks = yBlocks < 1 ? 1 : yBlocks;
  hc::extent<2> grdExt(yBlocks * 8 , xBlocks * 32);
  hc::tiled_extent<2> t_ext = grdExt.tile(8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    long input = inOffset;
    long output = outOffset;
    int weight = weightOffset;
    int bias = biasOffset;
    // iterators
    int xx, yy;
    long output_w = (input_w - kW) / dW + 1;
    long output_h = (input_h - kH) / dH + 1;
    // compute offsets based on thread/block ID
    int o = tidx.tile[1];
    int i = o;
    int k = tidx.tile[1] % input_n;
    int xx_start = tidx.local[1];
    int xx_end = output_w;
    int xx_step = tidx.tile_dim[1];
    int yy_start = tidx.global[0];
    int yy_end = output_h;
    int yy_step = grdExt[0];
    // select input/output plane
    output = output + o * output_w * output_h;
    input = input + i * input_w * input_h;
    // Get the good mask for (k,i) (k out, i in)
    float the_weight = avWeight[weight + k];
    // Initialize to the bias
    float the_bias = avBias[bias + k];

    // For all output pixels...
    for (yy = yy_start; yy < yy_end; yy += yy_step) {
      for (xx = xx_start; xx < xx_end; xx += xx_step) {
        // Compute the mean of the input image...
        long ptr_input = input + yy * dH * input_w + xx * dW;
        long ptr_output = output + yy * output_w + xx;
        float sum = 0;
        int kx, ky;

        for (ky = 0; ky < kH; ky++) {
          for (kx = 0; kx < kW; kx++) {
            if (ptr_input + kx < inpSize)
              sum += avInput[ptr_input + kx];
          }

          ptr_input += input_w; // next input line
        }

        // Update output
        if (ptr_output < outSize)
        avOutput[ptr_output] = the_weight * sum + the_bias;
      }
    }
  }).wait();
}

/*
 * Description:
 *    this function computes the gradWeight from input and gradOutput
 */
void subgradweight(THHcState* state, float* &avInput, long inOffset, long inSize,
                   float* &avGradOutput, long gradOutOffset, long gradOutSize,
                   float* &avGradWeight, long gradWeightOffset, long gradWeightSize,
                   float* &avGradBias, long gradBiasOffset, long gradBiasSize,
                   long Stride_Input, long Stride_Output,
                   int input_n, int input_h, int input_w, int kH, int kW,
                   int dH, int dW, float scale, long sl) {
  // output size
  long output_w = (input_w - kW) / dW + 1;
  long output_h = (input_h - kH) / dH + 1;
  long inputStride = sl * Stride_Input;// inputTensor->stride[0];
  long outputStride = sl * Stride_Output;//gradOutputTensor->stride[0];
  int xBlocks = input_n;
  hc::extent<2> grdExt(8 , xBlocks * 32);
  hc::tiled_extent<2> t_ext = grdExt.tile(8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    // iterators
    int xx, yy;
    // compute offsets based on thread/block ID
    int o = tidx.tile[1];
    int i = o;
    int k = tidx.tile[1] % input_n;

    long input = inOffset + inputStride;
    long gradOutput = gradOutOffset + outputStride;
    int gradWeight = gradWeightOffset;
    int gradBias = gradBiasOffset;

    int xx_start = tidx.local[1];
    int xx_end = output_w;
    int xx_step = tidx.tile_dim[1];

    int yy_start = tidx.local[0];
    int yy_end = output_h;
    int yy_step = tidx.tile_dim[0];

    // select input/output plane
    gradOutput = gradOutput + o * output_w * output_h;
    input = input + i * input_w * input_h;
    // thread ID
    int tid = tidx.tile_dim[1] * tidx.local[0] + tidx.local[1];
    // create array to hold partial sums
    tile_static float sums[Hc_MAX_THREADS];
    sums[tid] = 0;

    // compute partial sums
    for (yy = yy_start; yy < yy_end; yy += yy_step) {
      for (xx = xx_start; xx < xx_end; xx += xx_step) {
        long ptr_input = input + yy * dH * input_w + xx * dW;
        long ptr_gradOutput = gradOutput + yy * output_w + xx;
        float z = avGradOutput[ptr_gradOutput];
        long kx, ky;

        for (ky = 0; ky < kH; ky++) {
          for (kx = 0; kx < kW; kx++) {
            if(ptr_input + kx < inSize)
              sums[tid] += z * avInput[ptr_input + kx];
          }

          ptr_input += input_w;
        }
      }
    }

    tidx.barrier.wait();

    // reduce: accumulate all partial sums to produce final gradWeight
    if ((tidx.local[1] == 0) && (tidx.local[0] == 0)) {
      for (int i = 0; i < tidx.tile_dim[1] * tidx.tile_dim[0]; i++) {
        if(gradWeight + k < gradWeightSize)
          avGradWeight[gradWeight + k] += scale * sums[i];
      }
    }

    tidx.barrier.wait();
    // compute gradBias
    sums[tid] = 0;

    for (int i = tid; i < output_w * output_h; i += (tidx.tile_dim[1] * tidx.tile_dim[0])) {
      if(gradOutput + i < gradOutSize)
        sums[tid] += avGradOutput[gradOutput + i];
    }

    tidx.barrier.wait();

    // reduce gradBias
    if ((tidx.local[1] == 0) && (tidx.local[0] == 0)) {
      for (int i = 0; i < (tidx.tile_dim[1] * tidx.tile_dim[0]); i++) {
        if(gradBias + k < gradBiasSize)
          avGradBias[gradBias + k] += scale * sums[i];
      }
    }
  }).wait();
}

/*
 * Description:
 *    this function computes the gradInput from weight and gradOutput
 */
void subgradinput(THHcState* state, float* &avGradInput, long gradInOffset, long gradInSize,
                  float* &avGradOutput, long gradOutOffset,
                  float* &avWeight, long weightOffset,
                  int input_n, int input_h, int input_w,
                  int kH, int kW, int dH, int dW, int xBlocks) {
  // output size
  int output_w = (input_w - kW) / dW + 1;
  int output_h = (input_h - kH) / dH + 1;
  int yBlocks = (int)(16L / input_n);
  yBlocks = yBlocks < 1 ? 1 : yBlocks;
  hc::extent<2> grdExt(yBlocks * 8 , xBlocks * 32);
  hc::tiled_extent<2> t_ext = grdExt.tile(8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    // iterators
    int xx, yy;
    long gradInput = gradInOffset;
    long gradOutput = gradOutOffset;
    int weight = weightOffset;
    // compute offsets based on thread/block ID
    int o = tidx.tile[1];
    int i = o;
    int k = tidx.tile[1] % input_n;

    int xx_start = tidx.local[1];
    int xx_end = output_w;
    int xx_step = tidx.tile_dim[1];

    int yy_start = tidx.global[0];
    int yy_end = output_h;
    int yy_step = grdExt[0];

    // select input/output plane
    gradOutput = gradOutput + o * output_w * output_h;
    gradInput = gradInput + i * input_w * input_h;
    // get weight
    float the_weight = avWeight[weight + k];

    // compute gradInput
    for (yy = yy_start; yy < yy_end; yy += yy_step) {
      for (xx = xx_start; xx < xx_end; xx += xx_step) {
        long ptr_gradInput = gradInput + yy * dH * input_w + xx * dW;
        long ptr_gradOutput = gradOutput + yy * output_w + xx;
        float z = avGradOutput[ptr_gradOutput] * the_weight;
        int kx, ky;

        for (ky = 0; ky < kH; ky++) {
          for (kx = 0; kx < kW; kx++) {
            if (ptr_gradInput + kx < gradInSize)
              avGradInput[ptr_gradInput + kx] += z;
          }

          ptr_gradInput += input_w;
        }
      }
    }
  }).wait();
}

/*
 * Description:
 *    this function computes the gradInput from weight and gradOutput
 */
void subgradinputAtomic(THHcState* state, float* &avGradInput, long gradInOffset, long gradInSize,
                        float* &avGradOutput, long gradOutOffset,
                        float* &avWeight, long weightOffset,
                        int input_n, int input_h, int input_w,
                        int kH, int kW, int dH, int dW, int xBlocks) {
  // output size
  int output_w = (input_w - kW) / dW + 1;
  int output_h = (input_h - kH) / dH + 1;
  int yBlocks = (int)(16L / input_n);
  yBlocks = yBlocks < 1 ? 1 : yBlocks;
  hc::extent<2> grdExt(yBlocks * 8 , xBlocks * 32);
  hc::tiled_extent<2> t_ext = grdExt.tile(8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    // iterators
    int xx, yy;
    long gradInput = gradInOffset;
    long gradOutput = gradOutOffset;
    int weight = weightOffset;
    // compute offsets based on thread/block ID
    int o = tidx.tile[1];
    int i = o;
    int k = tidx.tile[1] % input_n;

    int xx_start = tidx.local[1];
    int xx_end = output_w;
    int xx_step = tidx.tile_dim[1];

    int yy_start = tidx.global[0];
    int yy_end = output_h;
    int yy_step = grdExt[0];

    // select input/output plane
    gradOutput = gradOutput + o * output_w * output_h;
    gradInput = gradInput + i * input_w * input_h;
    // get weight
    float the_weight = avWeight[weight + k];

    // compute gradInput
    for (yy = yy_start; yy < yy_end; yy += yy_step) {
      for (xx = xx_start; xx < xx_end; xx += xx_step) {
        long ptr_gradInput = gradInput + yy * dH * input_w + xx * dW;
        long ptr_gradOutput = gradOutput + yy * output_w + xx;
        float z = avGradOutput[ptr_gradOutput] * the_weight;
        int kx, ky;

        for (ky = 0; ky < kH; ky++) {
          for (kx = 0; kx < kW; kx++) {
            if (ptr_gradInput + kx < gradInSize)
              hc::atomic_fetch_add(&avGradInput[ptr_gradInput + kx], z);
          }

          ptr_gradInput += input_w;
        }
      }
    }
  }).wait();
}

/* Computes the output using the current parameter set of the class (SpatialSubSampling) and input.
 * This function returns the result which is stored in the output field.
 */
static int hcnn_SpatialSubSampling_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");

  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", "torch.HcTensor");
  THHcTensor* bias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "bias", "torch.HcTensor");
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", "torch.HcTensor");

  auto avWeight = weight->get_device_data(state);
  auto avBias = bias->get_device_data(state);

  luaL_argcheck(L, input->nDimension == 3 || input->nDimension == 4, 2, "3D or 4D (batch) tensor expected");

  if (input->nDimension == 3) {
    long nInputCols = input->size[2];
    long nInputRows = input->size[1];
    long nOutputCols = (nInputCols - kW) / dW + 1;
    long nOutputRows = (nInputRows - kH) / dH + 1;

    luaL_argcheck(L, input->size[0] == nInputPlane, 2, "invalid number of input planes");
    luaL_argcheck(L, nInputCols >= kW && nInputRows >= kH, 2, "input image smaller than kernel size");

    input = THHcTensor_newContiguous(state, input);
    THHcTensor_resize3d(state, output, nInputPlane, nOutputRows, nOutputCols);

    int xBlocks = nInputPlane;
    auto avInput = input->get_device_data(state);
    auto avOutput = output->get_device_data(state);
    subsample (state, avInput, input->storageOffset, input->storage->size,
               avOutput, output->storageOffset, output->storage->size,
               avWeight, weight->storageOffset,
               avBias, bias->storageOffset,
               nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
  } else {
    long nInputCols = input->size[3];
    long nInputRows = input->size[2];
    long nbatch = input->size[0];
    long nOutputCols = (nInputCols - kW) / dW + 1;
    long nOutputRows = (nInputRows - kH) / dH + 1;
    int xBlocks = nInputPlane * nbatch;

    luaL_argcheck(L, input->size[1] == nInputPlane, 2, "invalid number of input planes");
    luaL_argcheck(L, nInputCols >= kW && nInputRows >= kH, 2, "input image smaller than kernel size");

    input = THHcTensor_newContiguous(state, input);
    THHcTensor_resize4d(state, output, nbatch, nInputPlane, nOutputRows, nOutputCols);
    auto avInput = input->get_device_data(state);
    auto avOutput = output->get_device_data(state);
    subsample(state, avInput, input->storageOffset, input->storage->size,
              avOutput, output->storageOffset, output->storage->size,
              avWeight, weight->storageOffset,
              avBias, bias->storageOffset,
              nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
  }

  // clean
  THHcTensor_free(state, input);
  return 1;
}

/*
 * Computing the gradient of the SpatialSubSampling module with respect to its own input.
 * This is returned in gradInput. Also, the gradInput state variable is updated accordingly
 */
static int hcnn_SpatialSubSampling_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  // Solved by adding atomic operations
  //luaL_argcheck(L, dW == kW, 1, "dW and kW must be equal (this will be fixed soon)");
  //luaL_argcheck(L, dH == kH, 1, "dH and kH must be equal (this will be fixed soon)");
  THHcTensor* weight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "weight", "torch.HcTensor");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", "torch.HcTensor");

  auto avGradOutput = gradOutput->get_device_data(state);
  auto avWeight = weight->get_device_data(state);
  THHcTensor_resizeAs(state, gradInput, input);
  THHcTensor_zero(state, gradInput);
  auto avGradInput = gradInput->get_device_data(state);

  if (input->nDimension == 3) {
    long nInputCols = input->size[2];
    long nInputRows = input->size[1];
    int xBlocks = nInputPlane;

    subgradinput (state, avGradInput, gradInput->storageOffset, gradInput->storage->size,
                  avGradOutput, gradOutput->storageOffset,
                  avWeight, weight->storageOffset,
                  nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
  } else {
    long nInputCols = input->size[3];
    long nInputRows = input->size[2];
    long nbatch = input->size[0];
    int xBlocks = nInputPlane * nbatch;

    // run updateGradInput kernel
    if (kH == dH && kW == dW) {
      subgradinput (state, avGradInput, gradInput->storageOffset, gradInput->storage->size,
                    avGradOutput, gradOutput->storageOffset,
                    avWeight, weight->storageOffset,
                    nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
    } else {
      subgradinputAtomic (state, avGradInput, gradInput->storageOffset, gradInput->storage->size, 
                          avGradOutput, gradOutput->storageOffset,
                          avWeight, weight->storageOffset,
                          nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
    }
  }

  return 1;
}

/*
 * Computing the gradient of the SpatialSubSampling module with respect to its ownparameters.
 * Many modules do not perform this step as they do not have any parameters.
 * The state variable name for the parameters is module dependent.
 * The SpatialSubSampling module is expected to accumulate the gradients with respect to the parameters
 * in some variable.

 * scale is a scale factor that is multiplied with the gradParameters before being accumulated.
*/
static int hcnn_SpatialSubSampling_accGradParameters(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  int nInputPlane = luaT_getfieldcheckint(L, 1, "nInputPlane");
  float scale = luaL_optnumber(L, 4, 1);

  // Solved by adding atomic operations
  //luaL_argcheck(L, dW == kW, 1, "dW and kW must be equal (this will be fixed soon)");
  //luaL_argcheck(L, dH == kH, 1, "dH and kH must be equal (this will be fixed soon)");
  THHcTensor* gradWeight = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradWeight", "torch.HcTensor");
  THHcTensor* gradBias = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradBias", "torch.HcTensor");

  auto avGradWeight = gradWeight->get_device_data(state);
  auto avGradBias = gradBias->get_device_data(state);
  auto avGradOutput = gradOutput->get_device_data(state);
  input = THHcTensor_newContiguous(state, input);
  auto avInput = input->get_device_data(state);

  if (input->nDimension == 3) {
    long nInputCols = input->size[2];
    long nInputRows = input->size[1];
    long sl = 0;

    // run gradweight kernel
    subgradweight (state, avInput, input->storageOffset, input->storage->size,
                   avGradOutput, gradOutput->storageOffset, gradOutput->storage->size,
                   avGradWeight, gradWeight->storageOffset, gradWeight->storage->size,
                   avGradBias, gradBias->storageOffset, gradBias->storage->size, 0, 0,
                   nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, scale, sl);
  } else {
    long nInputCols = input->size[3];
    long nInputRows = input->size[2];
    long nbatch = input->size[0];

    // run gradweight kernel
    long sl;
    for (sl = 0; sl < nbatch; sl++) {
      subgradweight (state, avInput, input->storageOffset, input->storage->size,
                     avGradOutput, gradOutput->storageOffset, gradOutput->storage->size,
                     avGradWeight, gradWeight->storageOffset, gradWeight->storage->size,
                     avGradBias, gradBias->storageOffset, gradBias->storage->size,
                     input->stride[0], gradOutput->stride[0],
                     nInputPlane, nInputRows, nInputCols,
                     kH, kW, dH, dW, scale, sl);
    }
  }

  // clean
  THHcTensor_free(state, input);
  return 0;
}

static const struct luaL_Reg hcnn_SpatialSubSampling__ [] = {
  {"SpatialSubSampling_updateOutput", hcnn_SpatialSubSampling_updateOutput},
  {"SpatialSubSampling_updateGradInput", hcnn_SpatialSubSampling_updateGradInput},
  {"SpatialSubSampling_accGradParameters", hcnn_SpatialSubSampling_accGradParameters},
  {NULL, NULL}
};

/* Register SpatialSubSampling Hc Implementation with lua stack*/
void hcnn_SpatialSubSampling_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_SpatialSubSampling__, "nn");
  lua_pop(L, 1);
}

#undef Hc_MAX_THREADS
