/*****************************************************************
 * HardTanh
 *
 * Applies the HardTanh function element-wise to the input Tensor,
 * thus outputting a Tensor of the same dimension.
 *
 * HardTanh is defined as:
 *
 * f(x) = 1, if x > 1,
 * f(x) = -1, if x < -1,
 * f(x) = x, otherwise.
 *
 *****************************************************************/
#include "utils.h"
struct hardtanhupdateOutput_functor {
  hardtanhupdateOutput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input) const __attribute__((hc, cpu)) {
    if (input < -1) {
      return -1;
    } else if (input <= 1) {
      return input;
    } else {
      return 1;
    }
  }
};

static int hcnn_HardTanh_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, hardtanhupdateOutput_functor());
  THHcTensor_free(state, input);
  return 1;
}

struct hardtanhupdateGradInput_functor {
  hardtanhupdateGradInput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input, const float& gradOutput) const __attribute__((hc, cpu)) {
    if (input < -1 || input > 1) {
      return 0;
    } else {
      return gradOutput;
    }
  }
};

static int hcnn_HardTanh_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, input);
  transform(state, input, gradOutput, gradInput, hardtanhupdateGradInput_functor());
  THHcTensor_free(state, gradOutput);
  THHcTensor_free(state, input);
  return 1;
}

static const struct luaL_Reg hcnn_HardTanh__ [] = {
  {"HardTanh_updateOutput", hcnn_HardTanh_updateOutput},
  {"HardTanh_updateGradInput", hcnn_HardTanh_updateGradInput},
  {NULL, NULL}
};

/* Binding the HardTanh layer routines with lua stack */
void hcnn_HardTanh_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_HardTanh__, "nn");
  lua_pop(L, 1);
}
