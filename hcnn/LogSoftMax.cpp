/******************************************************************
 * LogSoftMax
 *
 * Applies the LogSoftmax function to an n-dimensional input Tensor.
 *
 * LogSoftmax is defined as
 * f_i(x) = log(1/a exp(x_i)), where a = sum_j exp(x_j).
 *
 *******************************************************************/

#define MINUS_LOG_THRESHOLD -18.42
#define LOGSOFTMAX_THREADS 256
#include "utils.h"
#include "amp_math.h"

void hcnn_LogSoftMax_updateOutput_kernel(THHcState* state, float* &avOutput, long outOffset,
    float* &avInp, long inOffset,
    int nframe, int dim) {
  hc::extent<1> grdExt(nframe * LOGSOFTMAX_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(LOGSOFTMAX_THREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[LOGSOFTMAX_THREADS + 1];
    int k = tidx.tile[0];
    unsigned int i_start = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];
    // max?
    buffer[i_start] = -FLT_MAX;

    for (int i = i_start; i < i_end; i += i_step) {
      float z = avInp[inOffset + k * dim + i];

      if(buffer[i_start] < z) {
        buffer[i_start] = z;
      }
    }

    // reduce
    for (unsigned int stride = i_step >> 1; stride > 0; stride >>= 1) {
      tidx.barrier.wait();

      if ((i_start < stride) && (buffer[i_start] < buffer[i_start + stride])) {
        buffer[i_start] = buffer[i_start + stride];
      }
    }

    if (i_start == 0) {
      float max_k = -FLT_MAX;

      if(max_k < buffer[0]) {
        max_k = buffer[0];
      }

      buffer[LOGSOFTMAX_THREADS] = max_k;
    }

    tidx.barrier.wait();
    // logadd?
    float max_k = buffer[LOGSOFTMAX_THREADS];
    buffer[i_start] = 0;

    for (int i = i_start; i < i_end; i += i_step) {
      buffer[i_start] += hc::fast_math::expf(avInp[inOffset + k * dim + i] - max_k);
    }

    // reduce
    for (unsigned int stride = i_step >> 1; stride > 0; stride >>= 1) {
      tidx.barrier.wait();

      if (i_start < stride) {
        buffer[i_start] += buffer[i_start + stride];
      }
    }

    if (i_start == 0) {
      buffer[LOGSOFTMAX_THREADS] = max_k + hc::fast_math::logf(buffer[0]);
    }

    tidx.barrier.wait();
    // logsoftmax
    float logsum_k = buffer[LOGSOFTMAX_THREADS];

    for (int i = i_start; i < i_end; i += i_step) {
      avOutput[outOffset + k * dim + i] = avInp[inOffset + k * dim + i] - logsum_k;
    }
  }).wait();
}

void hcnn_LogSoftMax_updateGradInput_kernel(THHcState* state, float* &avGradInput, long gradInOffset,
    float* &avOutput, long outOffset,
    float* &avGradOutput, long gradOutOffset,
    int nframe, int dim) {
  hc::extent<1> grdExt(nframe * LOGSOFTMAX_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(LOGSOFTMAX_THREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[LOGSOFTMAX_THREADS];
    int k = tidx.tile[0];
    float* gradInput_k = avGradInput + gradInOffset;
    gradInput_k += k * dim;
    float* output_k = avOutput + outOffset;
    output_k += k * dim;
    float* gradOutput_k = avGradOutput + gradOutOffset;
    gradOutput_k += k * dim;
    unsigned int tx = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];
    // sum?
    buffer[tx] = 0;

    for (int i = tx; i < i_end; i += i_step) {
      buffer[tx] += gradOutput_k[gradOutOffset + i];
    }

    // reduce
    for (unsigned int stride = tidx.tile_dim[0] >> 1; stride > 0; stride >>= 1) {
      tidx.barrier.wait();

      if (tx < stride) {
        buffer[tx] += buffer[tx + stride];
      }
    }

    tidx.barrier.wait();
    float sum_k = buffer[0];

    for (int i = tx; i < i_end; i += i_step) {
      gradInput_k[gradInOffset + i] = gradOutput_k[gradOutOffset + i] - hc::fast_math::expf(output_k[outOffset + i]) * sum_k;
    }
  }).wait();
}

static int hcnn_LogSoftMax_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  auto avOutput = output->get_device_data(state);
  auto avInput = input->get_device_data(state);

  if (input->nDimension == 1) {
    hcnn_LogSoftMax_updateOutput_kernel(state, avOutput, output->storageOffset,
                                         avInput, input->storageOffset,
                                         1, input->size[0]);
  } else if (input->nDimension == 2) {
    hcnn_LogSoftMax_updateOutput_kernel(state, avOutput, output->storageOffset,
                                         avInput, input->storageOffset,
                                         input->size[0], input->size[1]);
  } else {
    THError("vector or matrix expected");
  }

  THHcTensor_free(state, input);
  return 1;
}

static int hcnn_LogSoftMax_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  output = THHcTensor_newContiguous(state, output);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, output);
  auto avGradInput = gradInput->get_device_data(state);
  auto avGradOutput = gradOutput->get_device_data(state);
  auto avOutput = output->get_device_data(state);

  if (gradInput->nDimension == 1) {
    hcnn_LogSoftMax_updateGradInput_kernel(state, avGradInput, gradInput->storageOffset,
                                            avOutput, output->storageOffset,
                                            avGradOutput, gradOutput->storageOffset,
                                            1, gradInput->size[0]);
  } else if (gradInput->nDimension == 2) {
    hcnn_LogSoftMax_updateGradInput_kernel (state, avGradInput, gradInput->storageOffset,
        avOutput, output->storageOffset,
        avGradOutput, gradOutput->storageOffset,
        gradInput->size[0], gradInput->size[1]);
  } else {
    THError("vector or matrix expected");
  }

  THHcTensor_free(state, output);
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_LogSoftMax__ [] = {
  {"LogSoftMax_updateOutput", hcnn_LogSoftMax_updateOutput},
  {"LogSoftMax_updateGradInput", hcnn_LogSoftMax_updateGradInput},
  {NULL, NULL}
};

/* Binding the LogSoftMax layer routines with lua stack */
void hcnn_LogSoftMax_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_LogSoftMax__, "nn");
  lua_pop(L, 1);
}
