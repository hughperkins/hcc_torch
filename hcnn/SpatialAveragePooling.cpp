/************************************************************************************************
 * SpatialAveragePooling
 *
 * module = nn.SpatialAveragePooling(kW, kH [, dW, dH])
 *
 * Functionality : Applies 2D average-pooling operation in kWxkH regions by step size dWxdH steps.
 *                 The number of output features is equal to the number of input planes.
 *
 *************************************************************************************************/
#include "utils.h"
#define Hc_MAX_THREADS 256

/*
 * Description:
 *    this function avg-pools an input 3D tensor along dimensions 1 and 2
 *    3D input, 3D output
 */
void subsample(THHcState* state, float* &avInput, long inOffset, long inpSize,
               float* &avOutput, long outOffset, long outSize,
               int input_n, int input_h, int input_w, 
               int kH, int kW, int dH, int dW, int xBlocks) {
  int yBlocks = (int)(16L / input_n);
  yBlocks = yBlocks < 1 ? 1 : yBlocks;
  hc::extent<2> grdExt(yBlocks * 8 , xBlocks * 32);
  hc::tiled_extent<2> t_ext = grdExt.tile(8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    // iterators
    int xx, yy;
    // output size
    long output_w = (input_w - kW) / dW + 1;
    long output_h = (input_h - kH) / dH + 1;
    // compute offsets based on thread/block ID
    int o = tidx.tile[1];
    int i = o;
    int xx_start = tidx.local[1];
    int xx_end = output_w;
    int xx_step = tidx.tile_dim[1];
    int yy_start = tidx.global[0];
    int yy_end = output_h;
    int yy_step = grdExt[0];
    // select input/output plane
    long output = o * output_w * output_h;
    long input = i * input_w * input_h;

    // For all output pixels...
    for(yy = yy_start; yy < yy_end; yy += yy_step) {
      for(xx = xx_start; xx < xx_end; xx += xx_step) {
        // Compute the mean of the input image...
        long ptr_input = input + yy * dH * input_w + xx * dW;
        long ptr_output = output + yy * output_w + xx;
        float sum = 0;
        int kx, ky;

        for(ky = 0; ky < kH; ky++) {
          for(kx = 0; kx < kW; kx++) {
            if (inOffset + ptr_input + kx < inpSize)
              sum += avInput[inOffset + ptr_input + kx];
          }

          ptr_input += input_w; // next input line
        }

        // Update output
        if (outOffset + ptr_output < outSize)
          avOutput[outOffset + ptr_output] = (sum / float(kW * kH));
      }
    }
  }).wait();
}

/*
 * Description:
 *    this function computes the gradInput from gradOutput
 */
void subgradinput(THHcState* state, float* &avGradInput, long gradInOffset, long gradInpSize,
                  float* &avGradOutput, long gradOutOffset,
                  int input_n, int input_h, int input_w, int kH, int kW, int dH, int dW, int xBlocks) {
  int yBlocks = (int)(16L / input_n);
  yBlocks = yBlocks < 1 ? 1 : yBlocks;
  hc::extent<2> grdExt(yBlocks * 8 , xBlocks * 32);
  hc::tiled_extent<2> t_ext = grdExt.tile(8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    // iterators
    int xx, yy;
    // output size
    long output_w = (input_w - kW) / dW + 1;
    long output_h = (input_h - kH) / dH + 1;
    // compute offsets based on thread/block ID
    int o = tidx.tile[1];
    int i = o;
    int xx_start = tidx.local[1];
    int xx_end = output_w;
    int xx_step = tidx.tile_dim[1];
    int yy_start = tidx.global[0];
    int yy_end = output_h;
    int yy_step = grdExt[0];
    // select input/output plane
    long gradOutput = o * output_w * output_h;
    long gradInput = i * input_w * input_h;

    // compute gradInput
    for(yy = yy_start; yy < yy_end; yy += yy_step) {
      for(xx = xx_start; xx < xx_end; xx += xx_step) {
        long ptr_gradInput = gradInput + yy * dH * input_w + xx * dW;
        long ptr_gradOutput = gradOutput + yy * output_w + xx;
        float z = avGradOutput[gradOutOffset + ptr_gradOutput];
        int kx, ky;

        for(ky = 0; ky < kH; ky++) {
          for(kx = 0; kx < kW; kx++) {
            if (gradInOffset + ptr_gradInput + kx < gradInpSize)
              avGradInput[gradInOffset + ptr_gradInput + kx] += (z / float(kW * kH));
          }

          ptr_gradInput += input_w;
        }
      }
    }
  }).wait();
}

/*
 * Description:
 *    this function computes the gradInput from gradOutput
 *    but with an atomic accumulation. It is needed to be done so
 *    for cases of kH != dH and kW != dW
 */
void subgradinputAtomic(THHcState* state, float* &avGradInput, long gradInOffset, long gradInpSize,
                        float* &avGradOutput, long gradOutOffset,
                        int input_n, int input_h, int input_w, int kH, int kW, int dH, int dW, int xBlocks) {
  int yBlocks = (int)(16L / input_n);
  yBlocks = yBlocks < 1 ? 1 : yBlocks;
  hc::extent<2> grdExt(yBlocks * 8 , xBlocks * 32);
  hc::tiled_extent<2> t_ext = grdExt.tile(8, 32);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<2> tidx) __attribute__((hc, cpu)) {
    // iterators
    int xx, yy;
    // output size
    long output_w = (input_w - kW) / dW + 1;
    long output_h = (input_h - kH) / dH + 1;
    // compute offsets based on thread/block ID
    int o = tidx.tile[1];
    int i = o;
    int xx_start = tidx.local[1];
    int xx_end = output_w;
    int xx_step = tidx.tile_dim[1];
    int yy_start = tidx.global[0];
    int yy_end = output_h;
    int yy_step = grdExt[0];
    // select input/output plane
    long gradOutput = o * output_w * output_h;
    long gradInput = i * input_w * input_h;

    // compute gradInput
    for(yy = yy_start; yy < yy_end; yy += yy_step) {
      for(xx = xx_start; xx < xx_end; xx += xx_step) {
        long ptr_gradInput = gradInput + yy * dH * input_w + xx * dW;
        long ptr_gradOutput = gradOutput + yy * output_w + xx;
        float z = avGradOutput[gradOutOffset + ptr_gradOutput];
        int kx, ky;

        for(ky = 0; ky < kH; ky++) {
          for(kx = 0; kx < kW; kx++) {
            if (gradInOffset + ptr_gradInput + kx < gradInpSize)
              hc::atomic_fetch_add(&avGradInput[gradInOffset + ptr_gradInput + kx], (z / float(kW * kH)));
          }

          ptr_gradInput += input_w;
        }
      }
    }
  }).wait();
}

/* Computes the output using the current parameter set of the class (SpatialAveragePooling) and input.
 * This function returns the result which is stored in the output field.
 */
static int hcnn_SpatialAveragePooling_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  luaL_argcheck(L, input->nDimension == 3 || input->nDimension == 4, 2, "3D or 4D (batch) tensor expected");

  if (input->nDimension == 3) {
    long nInputCols = input->size[2];
    long nInputRows = input->size[1];
    long nOutputCols = (nInputCols - kW) / dW + 1;
    long nOutputRows = (nInputRows - kH) / dH + 1;
    long nInputPlane = input->size[0];
    luaL_argcheck(L, nInputCols >= kW && nInputRows >= kH, 2, "input image smaller than kernel size");
    input = THHcTensor_newContiguous(state, input);
    THHcTensor_resize3d(state, output, nInputPlane, nOutputRows, nOutputCols);
    int xBlocks = nInputPlane;
    auto avInput = input->get_device_data(state);
    auto avOutput = output->get_device_data(state);
    // run subsample kernel
    subsample (state, avInput, input->storageOffset, input->storage->size, avOutput, output->storageOffset, output->storage->size,
               nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
  } else {
    long nInputCols = input->size[3];
    long nInputRows = input->size[2];
    long nbatch = input->size[0];
    long nOutputCols = (nInputCols - kW) / dW + 1;
    long nOutputRows = (nInputRows - kH) / dH + 1;
    long nInputPlane = input->size[1];
    luaL_argcheck(L, nInputCols >= kW && nInputRows >= kH, 2, "input image smaller than kernel size");
    input = THHcTensor_newContiguous(state, input);
    THHcTensor_resize4d(state, output, nbatch, nInputPlane, nOutputRows, nOutputCols);
    int xBlocks = nInputPlane * nbatch;
    auto avInput = input->get_device_data(state);
    auto avOutput = output->get_device_data(state);
    // run subsample kernel
    subsample (state, avInput, input->storageOffset, input->storage->size, avOutput, output->storageOffset, output->storage->size,
               nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
  }

  // clean
  THHcTensor_free(state, input);
  return 1;
}

/*
 * Computing the gradient of the SpatialAveragePooling module with respect to its own input.
 * This is returned in gradInput. Also, the gradInput state variable is updated accordingly
 */
static int hcnn_SpatialAveragePooling_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int kW = luaT_getfieldcheckint(L, 1, "kW");
  int kH = luaT_getfieldcheckint(L, 1, "kH");
  int dW = luaT_getfieldcheckint(L, 1, "dW");
  int dH = luaT_getfieldcheckint(L, 1, "dH");
  // Solve by adding atomic operations
  //luaL_argcheck(L, dW == kW, 1, "dW and kW must be equal (this will be fixed soon)");
  //luaL_argcheck(L, dH == kH, 1, "dH and kH must be equal (this will be fixed soon)");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);

  if (input->nDimension == 3) {
    long nInputCols = input->size[2];
    long nInputRows = input->size[1];
    long nInputPlane = input->size[0];
    THHcTensor_resizeAs(state, gradInput, input);
    THHcTensor_zero(state, gradInput);
    int xBlocks = nInputPlane;
    auto avGradInput = gradInput->get_device_data(state);
    auto avGradOutput = gradOutput->get_device_data(state);

    // run updateGradInput kernel
    if (kH == dH && kW == dW) {
      subgradinput(state, avGradInput, gradInput->storageOffset, gradInput->storage->size,
                   avGradOutput, gradOutput->storageOffset,
                   nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
    } else {
      subgradinputAtomic(state, avGradInput, gradInput->storageOffset, gradInput->storage->size,
                         avGradOutput, gradOutput->storageOffset,
                         nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
    }
  } else {
    long nInputCols = input->size[3];
    long nInputRows = input->size[2];
    long nInputPlane = input->size[1];
    long nbatch = input->size[0];
    THHcTensor_resizeAs(state, gradInput, input);
    THHcTensor_zero(state, gradInput);
    int xBlocks = nInputPlane * nbatch;
    auto avGradInput = gradInput->get_device_data(state);
    auto avGradOutput = gradOutput->get_device_data(state);

    // run updateGradInput kernel
    if (kH == dH && kW == dW) {
      subgradinput(state, avGradInput, gradInput->storageOffset, gradInput->storage->size,
                   avGradOutput, gradOutput->storageOffset,
                   nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
    } else {
      subgradinputAtomic(state, avGradInput, gradInput->storageOffset, gradInput->storage->size,
                         avGradOutput, gradOutput->storageOffset,
                         nInputPlane, nInputRows, nInputCols, kH, kW, dH, dW, xBlocks);
    }
  }

  return 1;
}

static const struct luaL_Reg hcnn_SpatialAveragePooling__ [] = {
  {"SpatialAveragePooling_updateOutput", hcnn_SpatialAveragePooling_updateOutput},
  {"SpatialAveragePooling_updateGradInput", hcnn_SpatialAveragePooling_updateGradInput},
  {NULL, NULL}
};

/* Register SpatialAveragePooling Hc Implementation with lua stack*/
void hcnn_SpatialAveragePooling_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_SpatialAveragePooling__, "nn");
  lua_pop(L, 1);
}
