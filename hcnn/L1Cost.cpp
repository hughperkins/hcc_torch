#include "utils.h"
#include "amp_math.h"
#include "THHcFunctorOpns.h"

struct l1cost_functor {
  l1cost_functor() __attribute__((hc, cpu)) {}
  float operator()(float x, float y) const __attribute__((hc, cpu)) {
    return hc::fast_math::fabs(x) + hc::fast_math::fabs(y);
  }
};

static int hcnn_L1Cost_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  float sum = reduce<float>(state, input, 0.0, l1cost_functor());
  THHcTensor_free(state, input);
  lua_pushnumber(L, sum);
  lua_setfield(L, 1, "output");
  lua_pushnumber(L, sum);
  return 1;
}

struct l1cost_updateGradInput_functor {
  l1cost_updateGradInput_functor() __attribute__((hc, cpu)) {}
  float operator()(float x) const __attribute__((hc, cpu)) {
    if(x > 0) {
      return 1;
    } else if(x < 0) {
      return -1;
    } else {
      return 0;
    }
  }
};

static int hcnn_L1Cost_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, gradInput, input);
  transform(state, input, gradInput, l1cost_updateGradInput_functor());
  THHcTensor_free(state, input);
  return 1;
}

static const struct luaL_Reg hcnn_L1Cost__ [] = {
  {"L1Cost_updateOutput", hcnn_L1Cost_updateOutput},
  {"L1Cost_updateGradInput", hcnn_L1Cost_updateGradInput},
  {NULL, NULL}
};

/* Binding the L1Cost layer routines with lua stack */
void hcnn_L1Cost_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_L1Cost__, "nn");
  lua_pop(L, 1);
}
