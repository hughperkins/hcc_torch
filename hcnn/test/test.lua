local hcnntest = {}
local precision_forward = 1e-4
local precision_backward = 1e-2
local nloop = 1
local times = {}

local runtests = false
require 'nn'

if not hctorch then
  require 'hctorch'
  runtests=true
end

--e.g.: th -lhcnn -e "nn.testhc{'copies'}"

function hcnntest.copies()
   -- test vector
   local t = torch.HcTensor(100,10)

   -- simple copy
   t:normal()
   local t2 = t:clone()
   mytester:asserteq( t:add(-1,t2):abs():max(), 0, 'simple copy')

   -- transpose copy
   t:normal()
   local t3 = t:transpose(1,2)
   local t4 = t3:clone()
   mytester:asserteq( t3:add(-1,t4):abs():max(), 0, 'transpose copy')

   -- unfold copy
   t:normal()
   local t5 = t:unfold(2,5,1)
   local t6 = t5:clone()
   mytester:asserteq( t5:add(-1,t6):abs():max(), 0, 'transpose copy')

   -- host copy
   t = torch.FloatTensor(100,10)
   t:normal()
   local tc = t:hc()
   tc = tc:transpose(1,2)
   local t2 = tc:float()
   mytester:asserteq(t:transpose(1,2):add(-1,t2):abs():max(), 0, 'host copy, plus transpoe')
end

function hcnntest.Tanh_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Tanh forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.Tanh()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Tanh():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Tanh_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Tanh.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.Tanh()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Abs_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Abs forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.Abs()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Abs():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Abs_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Abs.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.Abs()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.Abs():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Sigmoid_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Sigmoid forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.Sigmoid()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Sigmoid():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

--W
function hcnntest.Sigmoid_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Sigmoid.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.Sigmoid()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Threshold_forward()
   local size = math.random(1,100)
   local thres = torch.uniform(-1,1)
   local val = torch.uniform(-1,1)

   local tm = {}
   local title = string.format('Threshold forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.Threshold(thres,val)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = sconv:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Threshold_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Threshold.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.Threshold()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Sqrt_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Sqrt forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size):abs()
   local sconv = nn.Sqrt()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Sqrt():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Sqrt_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Sqrt.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size):abs()
   local gradOutput = torch.randn(size)
   local sconv = nn.Sqrt()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

--W
function hcnntest.Square_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Square forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.Square()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Square():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Square_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Square.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.Square()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Max_forward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Max forward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local sconv = nn.Max(2)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Max(2):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')

   local error = gconv.indices:float() - sconv.indices
   mytester:assertlt(error:abs():max(), 1e-8, 'error on indices ')
end

function hcnntest.Max_backward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Max.backward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local gradOutput = torch.randn(size1)
   local sconv = nn.Max(2)
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Min_forward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Min forward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local sconv = nn.Min(2)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Min(2):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')

   local error = gconv.indices:float() - sconv.indices
   mytester:assertlt(error:abs():max(), 1e-8, 'error on indices ')
end

function hcnntest.Min_backward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Min.backward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local gradOutput = torch.randn(size1)
   local sconv = nn.Min(2)
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Sum_forward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Sum forward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local sconv = nn.Sum(2)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Sum(2):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Sum_backward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Sum.backward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local gradOutput = torch.randn(size1)
   local sconv = nn.Sum(2)
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.Mean_forward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Mean forward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local sconv = nn.Mean(2)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Mean(2):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Mean_backward()
   local size1 = math.random(1,1000)
   local size2 = math.random(2,100)

   local tm = {}
   local title = string.format('Mean.backward %dx%d', size1, size2)
   times[title] = tm

   local input = torch.randn(size1,size2)
   local gradOutput = torch.randn(size1)
   local sconv = nn.Mean(2)
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialConvolutionMM_forward_single()
   local from = math.random(1,32)
   local to = math.random(1,8) * 8
   local ki = math.random(3,15)
   local kj = math.random(3,15)
   local si = math.random(1,3)
   local sj = math.random(1,3)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local padW = math.random(0,1)
   local padH = math.random(0,1)
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_batch()
   local bs = math.random(1,4) * 4
   local from = math.random(1,32)
   local to = math.random(1,8) * 8
   local ki = math.random(3,15)
   local kj = math.random(3,15)
   local si = math.random(1,3)
   local sj = math.random(1,3)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local padW = math.random(0,1)
   local padH = math.random(0,1)
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_backward_single()
   local from = math.random(1,32)
   local to = math.random(1,8) * 8
   local ki = math.random(3,15)
   local kj = math.random(3,15)
   local si = math.random(1,3)
   local sj = math.random(1,3)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local padW = math.random(0,1)
   local padH = math.random(0,1)
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_batch()
   local bs = math.random(1,4) * 4
   local from = math.random(1,32)
   local to = math.random(1,8) * 8
   local ki = math.random(3,15)
   local kj = math.random(3,15)
   local si = math.random(1,3)
   local sj = math.random(1,3)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local padW = math.random(0,1)
   local padH = math.random(0,1)
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialSubSampling_forward()
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(2,4)
   local sj = math.random(2,4)
   local outi = math.random(32,256)
   local outj = math.random(32,256)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.forward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialSubSampling_forward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(2,4)
   local sj = math.random(2,4)
   local outi = math.random(32,256)
   local outj = math.random(32,256)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialSubSampling_backward()
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(2,4)
   local sj = math.random(2,4)
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.backward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialSubSampling_backward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(2,4)
   local sj = math.random(2,4)
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialMaxPooling_forward()
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(1,4)
   local sj = math.random(1,4)
   local outi = math.random(32,256)
   local outj = math.random(32,256)
   local padi = math.random(0,ki/2-1)
   local padj = math.random(0,kj/2-1)
   local ini = (outi-1)*si+ki - padi*2
   local inj = (outj-1)*sj+kj - padj*2
   local ceil_mode = math.random(0,1) == 1

   local tm = {}
   local title = string.format('SpatialMaxPooling.forward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj)
   if ceil_mode then sconv:ceil() end
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj):hc()
   if ceil_mode then gconv:ceil() end
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
   local error_ind = gconv.indices:float() - sconv.indices
   mytester:asserteq(error_ind:max(), 0, 'error on indices (forward) ')
end

function hcnntest.SpatialMaxPooling_forward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(2,4)
   local sj = math.random(2,4)
   local outi = math.random(32,256)
   local outj = math.random(32,256)
   local padi = math.random(0,ki/2-1)
   local padj = math.random(0,kj/2-1)
   local ini = (outi-1)*si+ki - padi*2
   local inj = (outj-1)*sj+kj - padj*2
   local ceil_mode = math.random(0,1) == 1

   local tm = {}
   local title = string.format('SpatialMaxPooling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj)
   if ceil_mode then sconv:ceil() end
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj):hc()
   if ceil_mode then gconv:ceil() end
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialMaxPooling_backward()
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(1,4)
   local sj = math.random(1,4)
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local padi = math.random(0,ki/2-1)
   local padj = math.random(0,kj/2-1)
   local ini = (outi-1)*si+ki - padi*2
   local inj = (outj-1)*sj+kj - padj*2
   local ceil_mode = math.random(0,1) == 1

   local tm = {}
   local title = string.format('SpatialMaxPooling.backward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj)
   if ceil_mode then sconv:ceil() end
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj):hc()
   if ceil_mode then gconv:ceil() end
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialMaxPooling_backward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   -- enforce testing non-atomic kernel (dW == kW) and (dH == kH)
   local si = ki
   local sj = kj
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local padi = math.random(0,ki/2-1)
   local padj = math.random(0,kj/2-1)
   local ini = (outi-1)*si+ki - padi*2
   local inj = (outj-1)*sj+kj - padj*2
   local ceil_mode = math.random(0,1) == 1

   local tm = {}
   local title = string.format('SpatialMaxPooling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj)
   if ceil_mode then sconv:ceil() end
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj,padi,padj):hc()
   if ceil_mode then gconv:ceil() end
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

-- TOOD: need to implement atomicAdd for float
function hcnntest.SpatialMaxPooling_backward_batch_atomic()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   -- enforce that kW ~= dW or kH ~= dH (which trigers the atomic kernel)
   local si = ki + ((math.random(0,1) == 1) and -math.random(1,ki-1) or math.random(1,2))
   local sj = kj + ((math.random(0,1) == 1) and  -math.random(1,kj-1) or math.random(1,2))
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.backward %dx%dx%dx%d o %dx%d (%dx%d) -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, si, sj, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialAveragePooling_forward()
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(32,256)
   local outj = math.random(32,256)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAveragePooling_forward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(32,256)
   local outj = math.random(32,256)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAveragePooling_backward()
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.backward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialAveragePooling_backward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end
-- TODO: to implement
--[[function hcnntest.SpatialAdaptiveMaxPooling_forward()
   local from = math.random(1,64)
   local to = from
   local outi = math.random(2,64)
   local outj = math.random(2,64)
   local ini = math.random(10,256)
   local inj = math.random(10,256)

   local tm = {}
   local title = string.format('SpatialAdaptiveMaxPooling.forward %dx%dx%d -> %dx%dx%d',
                               from, inj, ini, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialAdaptiveMaxPooling(outi,outj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAdaptiveMaxPooling(outi,outj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
   local error_ind = gconv.indices:float() - sconv.indices
   mytester:asserteq(error_ind:max(), 0, 'error on indices (forward) ')
end

function hcnntest.SpatialAdaptiveMaxPooling_forward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local outi = math.random(2,64)
   local outj = math.random(2,64)
   local ini = math.random(10,256)
   local inj = math.random(10,256)

   local tm = {}
   local title = string.format('SpatialAdaptiveMaxPooling.forward %dx%dx%dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialAdaptiveMaxPooling(outi,outj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAdaptiveMaxPooling(outi,outj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAdaptiveMaxPooling_backward()
   local from = math.random(1,64)
   local to = from
   local outi = math.random(2,64)
   local outj = math.random(2,64)
   local ini = math.random(10,256)
   local inj = math.random(10,256)

   local tm = {}
   local title = string.format('SpatialAdaptiveMaxPooling.backward %dx%dx%d -> %dx%dx%d',
                               from, inj, ini, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialAdaptiveMaxPooling(outi,outj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialAdaptiveMaxPooling(outi,outj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialAdaptiveMaxPooling_backward_batch()
   local bs = math.random(4,10)
   local from = math.random(1,64)
   local to = from
   local outi = math.random(2,64)
   local outj = math.random(2,64)
   local ini = math.random(10,256)
   local inj = math.random(10,256)

   local tm = {}
   local title = string.format('SpatialAdaptiveMaxPooling.backward %dx%dx%dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, bs, to, outj, outi)
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialAdaptiveMaxPooling(outi,outj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialAdaptiveMaxPooling(outi,outj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end]]--

function hcnntest.SpatialLPPooling_forward()
   local from = math.random(1,64)
   local to = from
   local pnorm = 2
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = ki
   local sj = kj
   local outi = math.random(32,256)
   local outj = math.random(32,256)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialLPPooling.forward (P=2 only) %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialLPPooling(from,pnorm,ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialLPPooling(from,pnorm,ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialLPPooling_backward()
   local from = math.random(1,64)
   local to = from
   local pnorm = 2
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = ki
   local sj = kj
   local outi = math.random(32,64)
   local outj = math.random(32,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialLPPooling.backward (P=2 only) %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialLPPooling(from,pnorm,ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.mse()
   for sizeAverage = 0, 1 do
      local size = math.random(3000,5000)
      local input = torch.randn(size,1,1)
      local target = torch.randn(size)
      local mod = nn.MSECriterion(sizeAverage == 1)

      local tm = {}
      local title = string.format('MSECriterion sizeAverage %d, %d ', sizeAverage, size)
      times[title] = tm

      local a = torch.Timer()
      local fout = mod:forward(input,target)
      local fgin = mod:backward(input,target):clone()
      tm.cpu = a:time().real

      local cinput = input:hc()
      local ctarget = target:hc()
      local cmod = nn.MSECriterion(sizeAverage == 1):hc()
      a:reset()
      local cout = cmod:forward(cinput,ctarget)
      local cgin = cmod:backward(cinput,ctarget)
      hctorch.synchronize()
      tm.hc = a:time().real

      local tm2 = {}
      local title = string.format('MSECriterion2 sizeAverage %d, %d ',sizeAverage, size)
      times[title] = tm2
      tm2.cpu = tm.cpu
      local cinput2 = input:hc()
      local ctarget2 = target:hc()
      local cmod2 = nn.MSECriterion(sizeAverage == 1):hc()
      a:reset()
      local cout2 = cinput2.nn.MSECriterion_updateOutput2(cmod,cinput2,ctarget2)
      local cgin2 = cinput2.nn.MSECriterion_updateGradInput2(cmod,cinput2,ctarget2)
      hctorch.synchronize()
      tm2.hc = a:time().real

      mytester:assertlt(math.abs(fout-cout), precision_forward, 'error  on output')
      local gerr = cgin:float() - fgin
      mytester:assertlt(gerr:abs():max(), precision_forward, 'error  on gradInput')

      mytester:assertlt(math.abs(fout-cout2), precision_forward, 'error on output - 2')
      local gerr2 = cgin2:float() - fgin
      mytester:assertlt(gerr2:abs():max(), precision_forward, 'error on gradInput -2')
   end
end

function hcnntest.distkldiv()
   for sizeAverage = 0, 1 do
      local size = math.random(3000,5000)
      local input = torch.randn(size,1,1)
      local target = torch.randn(size)
      local mod = nn.DistKLDivCriterion(sizeAverage == 1)

      local tm = {}
      local title = string.format('DistKLDivCriterion sizeAverage %d, %d ',sizeAverage,size)
      times[title] = tm

      local a = torch.Timer()
      local fout = mod:forward(input,target)
      local fgin = mod:backward(input,target):clone()
      tm.cpu = a:time().real

      local cinput = input:hc()
      local ctarget = target:hc()
      local cmod = nn.DistKLDivCriterion(sizeAverage == 1):hc()
      a:reset()
      local cout = cmod:forward(cinput,ctarget)
      local cgin = cmod:backward(cinput,ctarget)
      hctorch.synchronize()
      tm.hc = a:time().real

      mytester:assertlt(math.abs(fout-cout), precision_forward, 'error  on output')
      local gerr = cgin:float() - fgin
      mytester:assertlt(gerr:abs():max(), precision_backward, 'error  on gradInput')
   end
end

function hcnntest.SoftMax_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('SoftMax forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.SoftMax()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SoftMax():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SoftMax_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('SoftMax.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.SoftMax()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.LogSoftMax_forward()
   local size = math.random(1,256)

   local tm = {}
   local title = string.format('LogSoftMax forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.LogSoftMax()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.LogSoftMax():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward*10, 'error on state (forward) ')
end

function hcnntest.LogSoftMax_backward()
   local size = math.random(1,256)

   local tm = {}
   local title = string.format('LogSoftMax.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.LogSoftMax()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.LogSoftMax_forward_batch()
   local size = math.random(1,256)
   local bs = math.random(32,256)

   local tm = {}
   local title = string.format('LogSoftMax forward batch %d x %d -> %d x %d', bs, size, bs, size)
   times[title] = tm

   local input = torch.randn(bs, size)
   local sconv = nn.LogSoftMax()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.LogSoftMax():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward*10, 'error on state (forward) ')
end

function hcnntest.LogSoftMax_backward_batch()
   local size = math.random(1,256)
   local bs = math.random(32,256)

   local tm = {}
   local title = string.format('LogSoftMax.backward batch %d x %d -> %d x %d', bs, size, bs, size)
   times[title] = tm

   local input = torch.randn(bs, size)
   local gradOutput = torch.randn(bs, size)
   local sconv = nn.LogSoftMax()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.TemporalConvolution_forward()
   local from = math.random(1,64) -- inputFrameSize
   local to = math.random(1,64) -- outputFrameSize
   local ki = math.random(3,15) -- kernelWidth (kW)
   local si = math.random(1,2) -- stepSize (dW)
   local outi = math.random(1,256) -- nOutputFrame
   local ini = (outi-1)*si+ki -- nInputFrame

   local tm = {}
   local title = string.format('TemporalConvolution.forward %dx%d o %d -> %dx%d [s: %d]',
                               from, ini, ki, to, outi, si)
   times[title] = tm

   local input = torch.randn(ini,from)
   local sconv = nn.TemporalConvolution(from,to,ki,si)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.TemporalConvolution(from,to,ki,si):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.TemporalConvolution_forward_batch()
   local bs = math.random(4,16)
   local from = math.random(1,64)
   local to = math.random(1,64)
   local ki = math.random(3,15)
   local si = math.random(1,2)
   local outi = math.random(1,256)
   local ini = (outi-1)*si+ki

   local tm = {}
   local title = string.format('TemporalConvolution.forward %dx%dx%d o %d -> %dx%dx%d [s: %d]',
                               bs, from, ini, ki, bs, to, outi, si)
   times[title] = tm

   local input = torch.randn(bs,ini,from)
   local sconv = nn.TemporalConvolution(from,to,ki,si)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.TemporalConvolution(from,to,ki,si):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.TemporalConvolution_backward()
  local from = math.random(1,64)
   local to = math.random(1,64)
   local ki = math.random(3,15)
   local si = math.random(1,2)
   local outi = math.random(1,256)
   local ini = (outi-1)*si+ki

   local tm = {}
   local title = string.format('TemporalConvolution.backward %dx%d o %d -> %dx%d',
                               from, ini, ki, to, outi)

   times[title] = tm

   local input = torch.randn(ini,from)
   local gradOutput = torch.randn(outi,to)
   local sconv = nn.TemporalConvolution(from,to,ki,si)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.TemporalConvolution(from,to,ki,si):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.TemporalConvolution_backward_batch()
   local bs = math.random(4,16)
   local from = math.random(1,64)
   local to = math.random(1,64)
   local ki = math.random(3,15)
   local si = math.random(1,2)
   local outi = math.random(1,256)
   local ini = (outi-1)*si+ki

   local tm = {}
   local title = string.format('TemporalConvolution.backward %dx%dx%d o %d -> %dx%dx%d',
                               bs, from, ini, ki, bs, to, outi)
   times[title] = tm

   local input = torch.randn(bs,ini,from)
   local gradOutput = torch.randn(bs,outi,to)
   local sconv = nn.TemporalConvolution(from,to,ki,si)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.TemporalConvolution(from,to,ki,si):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc= gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.Dropout()
   local p = 0.2 --prob of droping out a neuron
   local input = torch.HcTensor(1000):fill((1-p))
   local module = nn.Dropout(p)
   module:hc()
   -- version 2
   local output = module:forward(input)
   mytester:assert(math.abs(output:mean() - (1-p)) < 0.05, 'dropout output')
   local gradInput = module:backward(input, input)
   mytester:assert(math.abs(gradInput:mean() - (1-p)) < 0.05, 'dropout gradInput')
   -- version 1 (old nnx version)
   local input = input:fill(1)
   local module = nn.Dropout(p,true)
   module:hc()
   local output = module:forward(input)
   mytester:assert(math.abs(output:mean() - (1-p)) < 0.05, 'dropout output')
   local gradInput = module:backward(input, input)
   mytester:assert(math.abs(gradInput:mean() - (1-p)) < 0.05, 'dropout gradInput')
end

function hcnntest.Dropout_forward()
   local size = math.random(1,200)

   local tm = {}
   local title = string.format('Dropout forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.Dropout()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Dropout():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

end


function hcnntest.Exp_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Exp forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.Exp()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.Exp():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.Exp_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('Exp.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.Exp()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end


function hcnntest.SoftPlus_forward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('SoftPlus forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.SoftPlus()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SoftPlus():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SoftPlus_backward()
   local size = math.random(1,100)

   local tm = {}
   local title = string.format('SoftPlus.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.SoftPlus()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialUpSamplingNearest_forward()
   local f = torch.random(3, 15)
   local h = torch.random(3, 15)
   local w = torch.random(3, 15)
   local scale = torch.random(2,5)

   local tm = {}
   local title = string.format('SpatialUpSamplingNearest.forward %dx%dx%d -> %dx%dx%d',
                               f, h, w, f, h*scale, w*scale)
   times[title] = tm

   local input = torch.randn(f, h, w)
   local sconv = nn.SpatialUpSamplingNearest(scale)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = sconv:clone():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialUpSamplingNearest_forward_batch()
   local nbatch = torch.random(3, 15)
   local f = torch.random(3, 15)
   local h = torch.random(3, 15)
   local w = torch.random(3, 15)
   local scale = torch.random(2,5)

   local tm = {}
   local title = string.format('SpatialUpSamplingNearest.forward %dx%dx%dx%d -> %dx%dx%dx%d',
                               nbatch, f, h, w, nbatch, f, h*scale, w*scale)
   times[title] = tm

   local input = torch.randn(nbatch, f, h, w)
   local sconv = nn.SpatialUpSamplingNearest(scale)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = sconv:clone():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')

end

function hcnntest.SpatialUpSamplingNearest_backward()
   local f = torch.random(3, 15)
   local h = torch.random(3, 15)
   local w = torch.random(3, 15)
   local scale = torch.random(2,5)

   local tm = {}
   local title = string.format('SpatialUpSamplingNearest.backward %dx%dx%d -> %dx%dx%d',
                               f, h, w, f, h*scale, w*scale)
   times[title] = tm

   local input = torch.randn(f, h, w)
   local gradOutput = torch.randn(f, h*scale, w*scale)
   local sconv = nn.SpatialUpSamplingNearest(scale)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialUpSamplingNearest_backward_batch()
   local nbatch = torch.random(3, 15)
   local f = torch.random(3, 15)
   local h = torch.random(3, 15)
   local w = torch.random(3, 15)
   local scale = torch.random(2,5)

   local tm = {}
   local title = string.format('SpatialUpSamplingNearest.backward %dx%dx%dx%d -> %dx%dx%dx%d',
                               nbatch, f, h, w, nbatch, f, h*scale, w*scale)
   times[title] = tm

   local input = torch.randn(nbatch, f, h, w)
   local gradOutput = torch.randn(nbatch, f, h*scale, w*scale)
   local sconv = nn.SpatialUpSamplingNearest(scale)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.l1cost()
   local size = math.random(300,500)
   local input = torch.randn(size)
   local mod = nn.L1Cost()
   local tm = {}
   local title = string.format('L1Cost %d ',size)
   times[title] = tm
   local a = torch.Timer()
   local fout = mod:forward(input)
   local fgin = mod:backward(input):clone()
   tm.cpu = a:time().real
   local cinput = input:hc()
   local cmod = nn.L1Cost():hc()
   a:reset()
   local cout = cmod:forward(cinput)
   local cgin = cmod:backward(cinput)
   hctorch.synchronize()
   tm.hc = a:time().real
   mytester:assertlt(math.abs(fout-cout), precision_forward, 'error on output')
   local gerr = cgin:float() - fgin
   mytester:assertlt(gerr:abs():max(), precision_forward, 'error on gradInput')
end

function hcnntest.ClassNLLCriterionSingleTarget()
   local size = math.random(3000,5000)
   local input = torch.randn(size)
   local target = 1
   local mod = nn.ClassNLLCriterion()

   local tm = {}
   local title = string.format('ClassNLLCriterionSingleTarget %d ',size)
   times[title] = tm

   local a = torch.Timer()
   local fout = mod:forward(input, target)
   local fgin = mod:backward(input, target):clone()
   tm.cpu = a:time().real

   local cinput = input:hc()
   local ctarget = torch.HcTensor(1):fill(target)
   local cmod = nn.ClassNLLCriterion():hc()
   a:reset()
   local cout = cmod:forward(cinput,ctarget)
   local cgin = cmod:backward(cinput,ctarget)
   hctorch.synchronize()
   tm.hc = a:time().real

   mytester:assertlt(
       math.abs(fout-cout), precision_forward, 'error  on output')
   local gerr = cgin:float() - fgin
   mytester:assertlt(gerr:abs():max(), precision_forward, 'error  on gradInput')
end

function hcnntest.ClassNLLCriterionMultipleTarget()
   local size = math.random(3000,5000)
   local input = torch.randn(size, size)
   local target = torch.randperm(size)
   local mod = nn.ClassNLLCriterion()

   local tm = {}
   local title = string.format('ClassNLLCriterionMultiTarget %d ',size)
   times[title] = tm

   local a = torch.Timer()
   local fout = mod:forward(input, target)
   local fgin = mod:backward(input, target):clone()
   tm.cpu = a:time().real

   local cinput = input:hc()
   local ctarget = target:hc()
   local cmod = nn.ClassNLLCriterion():hc()
   a:reset()
   local cout = cmod:forward(cinput,ctarget)
   local cgin = cmod:backward(cinput,ctarget)
   hctorch.synchronize()
   tm.hc = a:time().real

   mytester:assertlt(
       math.abs(fout-cout), precision_forward, 'error on output')

   local gerr = cgin:float() - fgin
   mytester:assertlt(gerr:abs():max(), precision_forward, 'error  on gradInput')
end




-- more cases
function _testLabelCriterionLayer(net)
  collectgarbage()
--  print('testlabelcrtierionlayer')
  N = 8
  in_size = 10
  
  local input = torch.Tensor(N, in_size):uniform() - 0.5
  local target = torch.multinomial(torch.range(1,in_size), N, true):float()

  local output = net:forward(input, target)

  local netCl = net:clone():hc()
  local inputCl = input:clone():hc()
  local targetCl = target:clone():hc()
  local outputCl = netCl:forward(inputCl, targetCl)

  --assertFloatNear(output, outputCl)

  local gradInput = net:backward(input, target)
  local gradInputCl = netCl:backward(inputCl, targetCl)

  --luaunit.assertEquals(gradInput, gradInputCl:double())
  local gerr = gradInputCl:float() - gradInput
   mytester:assertlt(gerr:abs():max(), precision_backward, 'error  on gradInput')
  collectgarbage()
end

function hcnntest.test_ClassNLLCriterion()
  _testLabelCriterionLayer(nn.ClassNLLCriterion())
end

function _testVectorLayer(net, in_size, out_size)
  collectgarbage()
  N = 32
  if in_size == nil then
    in_size = net.weight:size(2)
  end
  if out_size == nil then
    out_size = net.weight:size(1)
  end
--  print('net', net)
  local netCl = net:clone():hc()

--  local net = nn.Sigmoid()
  local input = torch.Tensor(N, in_size):uniform() - 0.5
  local output = net:forward(input)
--  print('output\n', output)

  local inputCl = input:clone():hc()
  local outputCl = netCl:forward(inputCl)
--  print('outputCl\n', outputCl)

  --luaunit.assertEquals(output, outputCl:double())

  local gradOutput = torch.Tensor(N, out_size):uniform() - 0.5
  local gradInput = net:backward(input, gradOutput)
--  print('gradInput\n', gradInput)

  local gradOutputCl = gradOutput:clone():hc()
  local gradInputCl = netCl:backward(inputCl, gradOutputCl)
--  print('gradInputcl\n', gradInputCl)

  --luaunit.assertEquals(gradInput, gradInputCl:double())
  local gerr = gradInputCl:float() - gradInput
   mytester:assertlt(gerr:abs():max(), precision_backward, 'error  on gradInput')
  collectgarbage()
end

function hcnntest.test_linear()
  _testVectorLayer(nn.Linear(4,3))
end

function hcnntest.test_tanh()
  _testVectorLayer(nn.Tanh(), 4, 4)
end

function hcnntest.test_sigmoid()
  _testVectorLayer(nn.Sigmoid(), 4, 4)
end

function hcnntest.test_relu()
  _testVectorLayer(nn.ReLU(), 4, 4)
end

function hcnntest.test_LogSoftMax()
  _testVectorLayer(nn.LogSoftMax(), 4 , 4)
end


function _test4dLayer(net, inPlanes, inSize, outPlanes, outSize, debug)
  collectgarbage()
--  print('net', net)
  local batchSize = 8
--  local numPlanes = 32
  if debug ~= nil then
    batchSize = 1
--    numPlanes = 2
  end
  local input = torch.Tensor(batchSize, inPlanes, inSize, inSize):uniform() - 0.5
  local gradOutput = torch.Tensor(batchSize, outPlanes, outSize, outSize):uniform() - 0.5
  local netCl = net:clone():hc()

  local output = net:forward(input)

  local inputCl = input:clone():hc()
  local outputCl = netCl:forward(inputCl)

  --luaunit.assertEquals(output, outputCl:double())

  local gradInput = net:backward(input, gradOutput)

  local gradOutputCl = gradOutput:clone():hc()
  local gradInputCl = netCl:backward(inputCl, gradOutputCl)

  --luaunit.assertEquals(gradInput, gradInputCl:double())
  local gerr = gradInputCl:float() - gradInput
   mytester:assertlt(gerr:abs():max(), precision_backward, 'error  on gradInput')
  collectgarbage()
end

function hcnntest.test_SpatialMaxPooling()
  _test4dLayer(nn.SpatialMaxPooling(2,2,2,2), 32, 32, 32, 16)
  _test4dLayer(nn.SpatialMaxPooling(3,3,3,3), 32, 48, 32, 16)
end

function hcnntest.testSigmoidv2()
  _test4dLayer(nn.Sigmoid(), 32, 32, 32, 32)
end

function hcnntest.testIdentity()
  _test4dLayer(nn.Identity(), 32, 32, 32, 32)
end

function hcnntest.testTanhv2()
  _test4dLayer(nn.Tanh(), 32, 32, 32, 32)
end

function _testTableLayer(net)
  collectgarbage()
  N = 5
--  print('net', net)

  local netCl = net:clone():hc()

  local num_tables = 2
  local in_size = 5
  local out_size = in_size
  local t1 = torch.Tensor(N, in_size):uniform() * 2 - 1.0
  local t2 = torch.Tensor(N, in_size):uniform() * 2 - 1.0
--  print('t1\n', t1)
--  print('t2\n', t2)

  local input = {t1, t2}
  local inputCl = {t1:hc(), t2:hc()}

--  print('output\n', input)

  local output = net:forward(input)
--  print('output\n', output)

--  local t1Cl = t1:clone():hc()
--  local t2Cl = t2:clone():hc()
  local outputCl = netCl:forward(inputCl)
--  print('outputCl\n', outputCl)

--  luaunit.assertEquals(output, outputCl:double())

  local gradOutput = torch.Tensor(N, out_size):uniform() * 2 - 0.5
  local gradInput = net:backward(input, gradOutput)
--  print('gradInput\n', gradInput[1], gradInput[2])

  local gradOutputCl = gradOutput:clone():hc()
  local gradInputCl = netCl:backward(inputCl, gradOutputCl)
--  print('gradInputcl\n', gradInputCl[1], gradInputCl[2])

  for i=1,num_tables do
    --luaunit.assertEquals(gradInput[i], gradInputCl[i]:double())
    local gerr = gradInputCl[i]:float() - gradInput[i]
   mytester:assertlt(gerr:abs():max(), precision_backward, 'error  on gradInput')
  end
  collectgarbage()
end

function hcnntest.testCMulTable()
  _testTableLayer(nn.CMulTable())
end

function hcnntest.testCAddTable()
  _testTableLayer(nn.CAddTable())
end

function _testNarrow(net)
  collectgarbage()
  N = 2
--  print('net', net)
  in_size = 12

  local input =torch.Tensor(N, in_size):uniform() * 2 - 1.0
  local inputCl = input:clone():hc()
  local netCl = net:clone():hc()

  local output = net:forward(input)
--  print('output\n', output)

  local outputCl = netCl:forward(inputCl)

--  print('outputCl\n', outputCl)

--  luaunit.assertEquals(output, outputCl:double())

  -- local gradOutput = torch.Tensor(N, out_size):uniform() * 2 - 0.5
  local gradOutput = output:clone() * 3 + 0.1
  local gradInput = net:backward(input, gradOutput)

  local gradOutputCl = gradOutput:clone():hc()
  local gradInputCl = netCl:backward(inputCl, gradOutputCl)

--  print('gradInput\n', gradInput)
--  print('gradInputCl\n', gradInputCl)

  --luaunit.assertEquals(gradInput, gradInputCl:double())
  local gerr = gradInputCl:float() - gradInput
   mytester:assertlt(gerr:abs():max(), precision_backward, 'error  on gradInput')
  collectgarbage()
end

function hcnntest.testNarrow()
  _testNarrow(nn.Narrow(2, 2, 5))
end

--add cudnn testcase
function hcnntest.SpatialConvolution_forward_batch()
   local bs = math.random(1,32)
   local from = math.random(1,32)
   local to = math.random(1,64)
   local ki = math.random(1,15)
   local kj = math.random(1,15)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local input = torch.randn(bs,from,inj,ini):hc()
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj):hc()
   local groundtruth = sconv:forward(input)
   hctorch.synchronize()
   local gconv = nn.SpatialConvolution(from,to,ki,kj,si,sj):hc()
   gconv.weight:copy(sconv.weight)
   gconv.bias:copy(sconv.bias)
   local reshc = gconv:forward(input)
   hctorch.synchronize()
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end


function hcnntest.SpatialConvolution_backward_batch()
   local bs = math.random(1,32)
   local from = math.random(1,32)
   local to = math.random(1,64)
   local ki = math.random(1,15)
   local kj = math.random(1,15)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local scale = math.random()

   local input = torch.randn(bs,from,inj,ini):hc()
   local gradOutput = torch.randn(bs,to,outj,outi):hc()
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj):hc()
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput, scale)
   hctorch.synchronize()
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias

   local gconv = nn.SpatialConvolution(from,to,ki,kj,si,sj):hc()
   gconv.weight:copy(sconv.weight)
   gconv.bias:copy(sconv.bias)
   gconv:forward(input)

   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')

   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput, scale)
   hctorch.synchronize()
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias

   local error = reshc:float() - groundgrad:float()
   local werror = weighthc:float() - groundweight:float()
   local berror = biashc:float() - groundbias:float()

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolution_forward_single()
   local from = math.random(1,32)
   local to = math.random(1,64)
   local ki = math.random(1,15)
   local kj = math.random(1,15)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local input = torch.randn(from,inj,ini):hc()
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj):hc()
   local groundtruth = sconv:forward(input)
   hctorch.synchronize()
   local gconv = nn.SpatialConvolution(from,to,ki,kj,si,sj):hc()
   gconv.weight:copy(sconv.weight)
   gconv.bias:copy(sconv.bias)
   local reshc = gconv:forward(input)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 3, 'error in dimension')
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward,
                     'error on state (forward) ')
end


function hcnntest.SpatialConvolution_backward_single()
   local from = math.random(1,32)
   local to = math.random(1,64)
   local ki = math.random(1,15)
   local kj = math.random(1,15)
   local si = math.random(1,ki)
   local sj = math.random(1,kj)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local input = torch.randn(from,inj,ini):hc()
   local gradOutput = torch.randn(to,outj,outi):hc()
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj):hc()
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias

   local gconv = nn.SpatialConvolution(from,to,ki,kj,si,sj):hc()
   gconv.weight:copy(sconv.weight)
   gconv.bias:copy(sconv.bias)
   gconv:forward(input)

   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')

   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 3, 'error in dimension')
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias

   local error = reshc:float() - groundgrad:float()
   local werror = weighthc:float() - groundweight:float()
   local berror = biashc:float() - groundbias:float()

   mytester:assertlt(error:abs():max(), precision_backward,
                     'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward,
                     'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward,
                     'error on bias (backward) ')
end

function hcnntest.VolumetricConvolution_forward_single()
   local from = math.random(1,16)
   local to = math.random(1,16)
   local ki = math.random(3,5)
   local kj = math.random(3,5)
   local kk = math.random(3,5)
   local si = math.random(1,ki-1)
   local sj = math.random(1,kj-1)
   local sk = math.random(1,kk-1)
   local outi = math.random(1,17)
   local outj = math.random(1,17)
   local outk = math.random(1,5)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local ink = (outk-1)*sk+kk
   local input = torch.randn(from,ink,inj,ini):hc()
   local sconv = nn.VolumetricConvolution(from,to,kk,ki,kj,sk,si,sj):float()
   local groundtruth = sconv:forward(input:float())
   hctorch.synchronize()
   local gconv = nn.VolumetricConvolution(from,to,kk,ki,kj,sk,si,sj):hc()
   gconv.weight:copy(sconv.weight)
   gconv.bias:copy(sconv.bias)
   local reshc = gconv:forward(input)
   hctorch.synchronize()
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward,
                     'error on state (forward) ')
end

function hcnntest.VolumetricConvolution_backward_single()
   local from = math.random(1,16)
   local to = math.random(1,16)
   local ki = math.random(3,5)
   local kj = math.random(3,5)
   local kk = math.random(3,5)
   local si = math.random(1,ki-1)
   local sj = math.random(1,kj-1)
   local sk = math.random(1,kk-1)
   local outi = math.random(1,17)
   local outj = math.random(1,17)
   local outk = math.random(1,5)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local ink = (outk-1)*sk+kk
   local input = torch.randn(from,ink,inj,ini):hc()
   local gradOutput = torch.randn(to,outk,outj,outi):hc()
   local sconv = nn.VolumetricConvolution(from,to,kk,ki,kj,sk,si,sj):float()
   sconv:forward(input:float())
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input:float(), gradOutput:float())
   hctorch.synchronize()
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias

   local gconv = nn.VolumetricConvolution(from,to,kk,ki,kj,sk,si,sj):hc()
   gconv.weight:copy(sconv.weight)
   gconv.bias:copy(sconv.bias)
   gconv:forward(input)
   hctorch.synchronize()

   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')

   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   hctorch.synchronize()

   mytester:asserteq(reshc:dim(), 4, 'error in dimension')
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias

   local error = reshc:float() - groundgrad:float()
   local werror = weighthc:float() - groundweight:float()
   local berror = biashc:float() - groundbias:float()

   mytester:assertlt(error:abs():max(), precision_backward,
                     'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward,
                     'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward,
                     'error on bias (backward) ')

end

function hcnntest.SpatialMaxPooling_batch()
   local bs = math.random(1,32)
   local from = math.random(1,32)
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = ki
   local sj = kj
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local input = torch.randn(bs,from,inj,ini):hc()
   local gradOutput = torch.randn(bs,from,outj,outi):hc()

   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   local groundtruth = sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')
   local reshc = gconv:forward(input)
   local resgrad = gconv:backward(input, gradOutput)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 4, 'error in dimension')
   mytester:asserteq(resgrad:dim(), 4, 'error in dimension')
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialMaxPooling_single()
   local from = math.random(1,32)
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = ki
   local sj = kj
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local input = torch.randn(from,inj,ini):hc()
   local gradOutput = torch.randn(from,outj,outi):hc()

   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   local groundtruth = sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   local _ = gconv:forward(input)
   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')
   local reshc = gconv:forward(input)
   local resgrad = gconv:backward(input, gradOutput)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 3, 'error in dimension')
   mytester:asserteq(resgrad:dim(), 3, 'error in dimension')
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward,
                     'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   mytester:assertlt(error:abs():max(), precision_backward,
                     'error on state (backward) ')
end

function hcnntest.SpatialAveragePooling_batch()
   local bs = math.random(1,32)
   local from = math.random(1,32)
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(2,4)
   local sj = math.random(2,4)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local input = torch.randn(bs,from,inj,ini):hc()
   local gradOutput = torch.randn(bs,from,outj,outi):hc()

   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local groundtruth = sconv:forward(input):clone()
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')
   local reshc = gconv:forward(input)
   local resgrad = gconv:backward(input, gradOutput)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 4, 'error in dimension')
   mytester:asserteq(resgrad:dim(), 4, 'error in dimension')
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialAveragePooling_single()
   local from = math.random(1,32)
   local ki = math.random(2,4)
   local kj = math.random(2,4)
   local si = math.random(2,4)
   local sj = math.random(2,4)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj
   local input = torch.randn(from,inj,ini):hc()
   local gradOutput = torch.randn(from,outj,outi):hc()

   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local groundtruth = sconv:forward(input):clone()
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local _ = gconv:forward(input)
   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')
   local reshc = gconv:forward(input)
   local resgrad = gconv:backward(input, gradOutput)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 3, 'error in dimension')
   mytester:asserteq(resgrad:dim(), 3, 'error in dimension')
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward,
                     'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   mytester:assertlt(error:abs():max(), precision_backward,
                     'error on state (backward) ')
end

local function nonlinSingle(nonlin)
   local from = math.random(1,32)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = outi
   local inj = outj
   local input = torch.randn(from,inj,ini):hc()
   local gradOutput = torch.randn(from,outj,outi):hc()

   local sconv = nn[nonlin]():hc()
   local groundtruth = sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   -- 50% prob to choose inplace or out-of-place
   local inplace = false
   if math.random(0,1) == 1 then
      inplace = true
   end
   local gconv = nn[nonlin](inplace):hc()
   local input__ = input:clone()
   local _ = gconv:forward(input__)

   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')

   local input__ = input:clone()
   local gradOutput__ = gradOutput:clone()
   local reshc = gconv:forward(input__)
   local resgrad = gconv:backward(input__, gradOutput__)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 3, 'error in dimension')
   mytester:asserteq(resgrad:dim(), 3, 'error in dimension')
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward,
                     'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   mytester:assertlt(error:abs():max(), precision_backward,
                     'error on state (backward) ')
end

function nonlinBatch(nonlin)
   local bs = math.random(1,32)
   local from = math.random(1,32)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = outi
   local inj = outj
   local input = torch.randn(bs,from,inj,ini):hc()
   local gradOutput = torch.randn(bs,from,outj,outi):hc()

   local sconv = nn[nonlin]():hc()
   local groundtruth = sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   -- 50% prob to choose inplace or out-of-place
   local inplace = false
   if math.random(0,1) == 1 then
      inplace = true
   end
   local gconv = nn[nonlin](inplace):hc()
   local input__ = input:clone()
   local reshc = gconv:forward(input__)

   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')

   local input__ = input:clone()
   local gradOutput__ = gradOutput:clone()
   local reshc = gconv:forward(input__)
   local resgrad = gconv:backward(input__, gradOutput__)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 4, 'error in dimension')
   mytester:asserteq(resgrad:dim(), 4, 'error in dimension')
   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(), precision_forward,
                     'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   mytester:assertlt(error:abs():max(), precision_backward,
                     'error on state (backward) ')
end

function hcnntest.ReLU_single()
   nonlinSingle('ReLU')
end

function hcnntest.ReLU_batch()
   nonlinBatch('ReLU')
end

function hcnntest.Tanh_single()
   nonlinSingle('Tanh')
end

function hcnntest.Tanh_batch()
   nonlinBatch('Tanh')
end

function hcnntest.Sigmoid_single()
   nonlinSingle('Sigmoid')
end

function hcnntest.Sigmoid_batch()
   nonlinBatch('Sigmoid')
end

function hcnntest.SoftMax_single()
   local sz = math.random(1,64)
   local input = torch.randn(sz):hc()
   local gradOutput = torch.randn(sz):hc()

   local sconv = nn.SoftMax():hc()
   local groundtruth = sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   local gconv = nn.SoftMax():hc()
   local _ = gconv:forward(input)

   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')

   local reshc = gconv:forward(input)
   local resgrad = gconv:backward(input, gradOutput)
   hctorch.synchronize()
   local error = reshc:float() - groundtruth:float()
   local errmax = error:abs():max()
   if (errmax ~= errmax) then
      local state = {}
      state.input = input
      state.gradOutput = gradOutput
      state.reshc = reshc
      state.resgrad = resgrad
      state.groundtruth = groundtruth
      state.groundgrad = groundgrad
      print(#input)
      torch.save('badSoftMax.t7', state)
      print(#input)
   end
   mytester:assertlt(errmax, precision_forward,
                     'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   errmax = error:abs():max()
   if (errmax ~= errmax) then
      local state = {}
      state.input = input
      state.gradOutput = gradOutput
      state.reshc = reshc
      state.resgrad = resgrad
      state.groundtruth = groundtruth
      state.groundgrad = groundgrad
      print(#input)
      torch.save('badSoftMax.t7', state)
      print(#input)
   end
   mytester:assertlt(errmax, precision_backward,
                     'error on state (backward) ')
end

-- TODO: Need to support non-vector and non-matrix
--[[function hcnntest.SoftMax_batch()
   local bs = math.random(1,32)
   local from = math.random(1,32)
   local outi = math.random(1,64)
   local outj = math.random(1,64)
   local ini = outi
   local inj = outj
   local input = torch.randn(bs,from,inj,ini):hc()
   local gradOutput = torch.randn(bs,from,outj,outi):hc()

   local sconv = nn.SoftMax():hc()
   local groundtruth = sconv:forward(input:view(bs,-1))
   local groundgrad = sconv:backward(input, gradOutput)
   hctorch.synchronize()
   local gconv = nn.SoftMax():hc()
   local reshc = gconv:forward(input)

   -- serialize and deserialize
   torch.save('modelTemp.t7', gconv)
   gconv = torch.load('modelTemp.t7')

   local reshc = gconv:forward(input)
   local resgrad = gconv:backward(input, gradOutput)
   hctorch.synchronize()
   mytester:asserteq(reshc:dim(), 4, 'error in dimension')
   mytester:asserteq(resgrad:dim(), 4, 'error in dimension')

   local error = reshc:float() - groundtruth:float()
   mytester:assertlt(error:abs():max(),
                     precision_forward, 'error on state (forward) ')
   error = resgrad:float() - groundgrad:float()
   mytester:assertlt(error:abs():max(),
                     precision_backward, 'error on state (backward) ')
end]]--



function nn.testhc(tests)
   local oldtype = torch.getdefaulttensortype()
   torch.setdefaulttensortype('torch.FloatTensor')
   math.randomseed(os.time())
   mytester = torch.Tester()
   mytester:add(hcnntest)
   mytester:run(tests)
   torch.setdefaulttensortype(oldtype)
   --[[print ''
   print ' ------------------------------------------------------------------------------------------------'
   print '|  Module                                                                          |  Speedup    |'
   print ' ------------------------------------------------------------------------------------------------'
   for module,tm in pairs(times) do
      local str = string.format('| %-80s | %4.2f        |', module, (tm.cpu / (tm.hc or 1e6)))
      print(str)
   end
   print ' ------------------------------------------------------------------------------------------------']]--
end

if runtests then 
  require 'hcnn'
  nn.testhc()
end
