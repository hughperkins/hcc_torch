#include "utils.h"
#include "amp_math.h"

struct thresholdupdateOutput_functor {
  const double threshold;
  const double val;

  thresholdupdateOutput_functor(double threshold_, double val_) __attribute__((hc, cpu)): threshold(threshold_), val(val_) {}

  float operator()(const float& input) const __attribute__((hc, cpu)) {
    return (input > threshold) ? input : val;
  }
};

static int hcnn_Threshold_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  double val = luaT_getfieldchecknumber(L, 1, "val");
  double threshold = luaT_getfieldchecknumber(L, 1, "threshold");
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, thresholdupdateOutput_functor(threshold, val));
  THHcTensor_free(state, input);
  return 1;
}

struct thresholdupdateGradInput_functor {
  const double threshold;
  const double val;

  thresholdupdateGradInput_functor(double threshold_, double val_) __attribute__((hc, cpu)) : threshold(threshold_), val(val_) {}

  float operator()(const float& input, const float& gradOutput) const __attribute__((hc, cpu)) {
    return (input > threshold) ? gradOutput : 0;
  }
};

static int hcnn_Threshold_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  double val = luaT_getfieldchecknumber(L, 1, "val");
  double threshold = luaT_getfieldchecknumber(L, 1, "threshold");
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, gradInput, output);
  transform(state, input, gradOutput, gradInput, thresholdupdateGradInput_functor(threshold, val));
  THHcTensor_free(state, input);
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_Threshold__ [] = {
  {"Threshold_updateOutput", hcnn_Threshold_updateOutput},
  {"Threshold_updateGradInput", hcnn_Threshold_updateGradInput},
  {NULL, NULL}
};

/* Binding the Threshold layer routines with lua stack */
void hcnn_Threshold_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Threshold__, "nn");
  lua_pop(L, 1);
}

