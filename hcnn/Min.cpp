/***********************************************************************
 * Min
 *
 * module = nn.Min(dimension)
 * Functionality : Applies a min operation over dimension dimension.
 *                 Hence, if an nxpxq Tensor was given as input,
 *                 and dimension = 2 then an nxq matrix would be output.
 *
 ***********************************************************************/
#include "utils.h"
void min_output(THHcState* state, float* &avInp, long inpOffset,
                float* &avOut, long outOffset,
                float* &avInD, long indOffset,
                unsigned int inpSz, unsigned int outSz,
                unsigned int indSz, long nrows, long ncols, unsigned int numBlocks) {
  hc::extent<1> grdExt(numBlocks * 256);
  hc::tiled_extent<1> t_ext = grdExt.tile(256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    long o = tidx.global[0];
    
    if (o >= nrows) {
      return;
    }
    
    long i = o * ncols;
    // compute min:
    float min = avInp[inpOffset + i];
    long argmin = 0;
    long ii;
    
    for (ii = 1; ii < ncols; ii++) {
      float val = avInp[inpOffset + ii + i];
      
      if (val < min) {
        min = val;
        argmin = ii;
      }
    }
    
    // store
    avOut[outOffset + o] = min;
    avInD[indOffset + o] = (float) argmin + 1;
  }).wait();
}

void min_gradInput(THHcState* state, float* &avInp, long inpOffset,
                   float* &avOut, long outOffset,
                   float* &avInD, long indOffset,
                   unsigned int inputSz, unsigned int outSz,
                   unsigned int indSz, long nrows, long ncols, unsigned int numBlocks) {
  hc::extent<1> grdExt(numBlocks * 256);
  hc::tiled_extent<1> t_ext = grdExt.tile(256);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [ = ] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    long o = tidx.global[0];
    
    if (o >= nrows) {
      return;
    }
    
    // input offset:
    long i = o * ncols;
    // bprop min gradient:
    long idx = (long)avInD[indOffset + o] - 1;
    avInp[inpOffset + i + idx] = avOut[outOffset + o];
  }).wait();
}

static int hcnn_Min_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  int dimension = luaT_getfieldcheckint(L, 1, "dimension") - 1;
  THHcTensor* indices = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "indices", torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  luaL_argcheck(L, dimension >= 0 && dimension < input->nDimension, 2, "dimension out of range");
  luaL_argcheck(L, dimension == input->nDimension - 1, 2, "only supported dimension is innermost (Hc kernel only)");
  input = THHcTensor_newContiguous(state, input);
  THLongStorage* dim = THLongStorage_newWithSize(input->nDimension);
  long i;
  
  for (i = 0; i < input->nDimension; i++) {
    dim->data[i] = input->size[i];
  }
  
  dim->data[dimension] = 1;
  THHcTensor_resize(state, output, dim, NULL);
  THHcTensor_resize(state, indices, dim, NULL);
  THLongStorage_free(dim);
  long nrows = THHcTensor_nElement(state, output);
  long ncols = input->size[dimension];
  // blocks & threads:
  long nthreads = 256;
  long nblocks = ceil((float)nrows / nthreads);
  auto avInput = input->get_device_data(state);
  auto avOutput = output->get_device_data(state);
  auto avIndices = indices->get_device_data(state);
  // kernel:
  min_output(state, avInput, input->storageOffset,
             avOutput, output->storageOffset,
             avIndices, indices->storageOffset,
             THHcTensor_nElement(state, input), THHcTensor_nElement(state, output),
             THHcTensor_nElement(state, indices), nrows, ncols, nblocks);
  // final cut:
  THHcTensor_free(state, input);
  THHcTensor_select(state, output, NULL, dimension, 0);
  return 1;
}

static int hcnn_Min_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* indices = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "indices", torch_Tensor);
  int dimension  = luaT_getfieldcheckint(L, 1, "dimension") - 1;
  THHcTensor* gradInput  = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  THHcTensor_resizeAs(state, gradInput, input);
  THHcTensor_zero(state, gradInput);
  long nrows = THHcTensor_nElement(state, gradOutput);
  long ncols = gradInput->size[dimension];
  // blocks & threads:
  long nthreads = 256;
  long nblocks = ceil((float)nrows / nthreads);
  auto avGradInput = gradInput->get_device_data(state);
  auto avGradOutput = gradOutput->get_device_data(state);
  auto avIndices = indices->get_device_data(state);
  // kernel:
  min_gradInput(state, avGradInput, gradInput->storageOffset,
                avGradOutput, gradOutput->storageOffset,
                avIndices, indices->storageOffset,
                THHcTensor_nElement(state, gradInput),
                THHcTensor_nElement(state, gradOutput), THHcTensor_nElement(state, indices),
                nrows, ncols, nblocks);
  return 1;
}

static const struct luaL_Reg hcnn_Min__ [] = {
  {"Min_updateOutput", hcnn_Min_updateOutput},
  {"Min_updateGradInput", hcnn_Min_updateGradInput},
  {NULL, NULL}
};

/* Binding the Min layer routines with lua stack */
void hcnn_Min_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Min__, "nn");
  lua_pop(L, 1);
}
