/****************************************************************************
 * Exp
 *
 * module = nn.Exp()
 * Functionality : Applies the exp function element-wise to the input Tensor,
 *                 thus outputting a Tensor of the same dimension.
 *
 ****************************************************************************/
#include "utils.h"
#include "amp_math.h"

struct expupdateOutput_functor {
  expupdateOutput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input) const __attribute__((hc, cpu)) {
    return hc::fast_math::exp(input);
  }
};

static int hcnn_Exp_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, expupdateOutput_functor());
  THHcTensor_free(state, input);
  return 1;
}

struct expupdateGradInput_functor {
  expupdateGradInput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& output, const float& gradOutput) const __attribute__((hc, cpu)) {
    return gradOutput * output;
  }
};

static int hcnn_Exp_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, output);
  transform(state, output, gradOutput, gradInput, expupdateGradInput_functor());
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_Exp__ [] = {
  {"Exp_updateOutput", hcnn_Exp_updateOutput},
  {"Exp_updateGradInput", hcnn_Exp_updateGradInput},
  {NULL, NULL}
};

/* Binding the Exp layer routines with lua stack */
void hcnn_Exp_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Exp__, "nn");
  lua_pop(L, 1);
}
