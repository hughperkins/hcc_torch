#include "utils.h"

#define STR(x) #x
#define EXPAND_STR(x) STR(x)
THHcState* getGputorchState(lua_State* L) {
  lua_getglobal(L, EXPAND_STR(Torch_TARGET));
  lua_getfield(L, -1, "getState");
  lua_call(L, 0, 1);
  THHcState* state = (THHcState*) lua_touserdata(L, -1);
  assert (state && "state is invalid!");
  lua_pop(L, 2);
  return state;
}
