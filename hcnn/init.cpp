#include "luaT.h"
#include "THHc.h"
#include "THLogAdd.h" /* DEBUG: WTF */
#include "utils.h"

#define glue2(x,y) x ## y
#define glue(x,y) glue2(x,y)
#define exported_libname(x, y, z) glue(x, y)(z)

LUA_EXTERNC DLL_EXPORT int exported_libname(luaopen_lib, Nn_TARGET, lua_State* L);
int exported_libname(luaopen_lib, Nn_TARGET, lua_State* L) {
  hcnn_Tanh_init(L);
  hcnn_Sigmoid_init(L);
  hcnn_Max_init(L);
  hcnn_Min_init(L);
  hcnn_HardTanh_init(L);
  hcnn_L1Cost_init(L);
  hcnn_LogSoftMax_init(L);
  hcnn_SoftMax_init(L);
  hcnn_TemporalConvolution_init(L);
  hcnn_SpatialConvolutionMM_init(L);
  hcnn_SpatialMaxPooling_init(L);
  hcnn_SpatialSubSampling_init(L);
  hcnn_MultiMarginCriterion_init(L);
  hcnn_Square_init(L);
  hcnn_Sqrt_init(L);
  hcnn_Threshold_init(L);
  hcnn_MSECriterion_init(L);
  hcnn_AbsCriterion_init(L);
  hcnn_DistKLDivCriterion_init(L);
  hcnn_Abs_init(L);
  hcnn_SoftPlus_init(L);
  hcnn_Exp_init(L);
  hcnn_SpatialUpSamplingNearest_init(L);
  hcnn_SpatialAveragePooling_init(L);
  hcnn_ClassNLLCriterion_init(L);
  hcnn_VolumetricConvolution_init(L);
  return 1;
}
