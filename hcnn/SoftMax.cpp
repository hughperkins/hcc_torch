/*********************************************************************************************
 * SoftMax
 *
 * Applies the Softmax function to an n-dimensional input Tensor, rescaling them
 * so that the elements of the n-dimensional output Tensor lie in the range (0,1) and sum to 1.
 *
 * Softmax is defined as
 * f_i(x) = exp(x_i-shift) / sum_j exp(x_j-shift), where shift = max_i x_i.
 *
 **********************************************************************************************/
#include "utils.h"
#define MINUS_LOG_THRESHOLD -18.42
#define SOFTMAX_THREADS 256

void hcnn_SoftMax_updateOutput_kernel(THHcState* state, float* &avOutput, long outOffset,
                                       float*&avInp, long inpOffset,
                                       int nframe, int dim) {
  hc::extent<1> grdExt(nframe * SOFTMAX_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(SOFTMAX_THREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1>& tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[SOFTMAX_THREADS + 1];
    int k = tidx.tile[0];
    float* input_k = avInp + inpOffset;
    input_k += k * dim;
    float* output_k = avOutput + outOffset;
    output_k += k;
    int i_start = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];
    // max?
    buffer[i_start] = -FLT_MAX;

    for (int i = i_start; i < i_end; i += i_step) {
      float z = input_k[i];

      if(buffer[i_start] < z) {
        buffer[i_start] = z;
      }
    }

    tidx.barrier.wait();

    // reduce
    if (i_start == 0) {
      float max_k = -FLT_MAX;

      for (int i = 0; i < i_step; i++) {
        if(max_k < buffer[i]) {
          max_k = buffer[i];
        }
      }

      buffer[SOFTMAX_THREADS] = max_k;
    }

    tidx.barrier.wait();
    // sum?
    float max_k = buffer[SOFTMAX_THREADS];
    buffer[i_start] = 0;

    for (int i = i_start; i < i_end; i += i_step) {
      float z = hc::fast_math::expf(input_k[i] - max_k);
      buffer[i_start] += z;
      output_k[i] = z;
    }

    tidx.barrier.wait();

    // reduce
    if (i_start == 0) {
      float sum_k = 0;

      for (int i = 0; i < i_step; i++) {
        sum_k += buffer[i];
      }

      buffer[SOFTMAX_THREADS] = sum_k;
    }

    tidx.barrier.wait();
    // softmax
    float sum_k = buffer[SOFTMAX_THREADS];

    for (int i = i_start; i < i_end; i += i_step) {
      output_k[i] = output_k[i] / sum_k;
    }
  }).wait();
}


void hcnn_SoftMax_updateGradInput_kernel(THHcState* state, float* &avGradInput, long gradInOffset,
    float* &avOutput, long outOffset,
    float* &avGradOutput, long gradOutOffset,
    int nframe, int dim) {
  hc::extent<1> grdExt(nframe * SOFTMAX_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(SOFTMAX_THREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[SOFTMAX_THREADS];
    int k = tidx.tile[0];
    float* gradInput_k = avGradInput + gradInOffset;
    gradInput_k += k * dim;
    float* output_k = avOutput + outOffset;
    output_k += k * dim;
    float* gradOutput_k = avGradOutput + gradOutOffset;
    gradOutput_k += k * dim;
    int i_start = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];
    // sum?
    buffer[i_start] = 0;

    for (int i = i_start; i < i_end; i += i_step) {
      buffer[i_start] += gradOutput_k[i] * output_k[i];
    }

    tidx.barrier.wait();

    // reduce
    if (i_start == 0) {
      float sum_k = 0;

      for (int i = 0; i < i_step; i++) {
        sum_k += buffer[i];
      }

      buffer[0] = sum_k;
    }

    tidx.barrier.wait();
    float sum_k = buffer[0];

    for (int i = i_start; i < i_end; i += i_step) {
      gradInput_k[i] = output_k[i] * (gradOutput_k[i] - sum_k);
    }
  }).wait();
}

static int hcnn_SoftMax_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  auto avInput = input->get_device_data(state);
  auto avOutput = output->get_device_data(state);

  if (input->nDimension == 1) {
    hcnn_SoftMax_updateOutput_kernel(state, avOutput, output->storageOffset,
                                      avInput, input->storageOffset,
                                      1, input->size[0]);
  } else if (input->nDimension == 2) {
    hcnn_SoftMax_updateOutput_kernel(state, avOutput, output->storageOffset,
                                      avInput, input->storageOffset,
                                      input->size[0], input->size[1]);
  } else {
    THError("vector or matrix expected");
  }

  THHcTensor_free(state, input);
  return 1;
}


static int hcnn_SoftMax_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  output = THHcTensor_newContiguous(state, output);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, output);
  auto avGradInput = gradInput->get_device_data(state);
  auto avOutput = output->get_device_data(state);
  auto avGradOutput = gradOutput->get_device_data(state);

  if (gradInput->nDimension == 1) {
    hcnn_SoftMax_updateGradInput_kernel(state, avGradInput, gradInput->storageOffset,
                                         avOutput, output->storageOffset,
                                         avGradOutput, gradOutput->storageOffset,
                                         1, gradInput->size[0]);
  } else if (gradInput->nDimension == 2) {
    hcnn_SoftMax_updateGradInput_kernel(state, avGradInput, gradInput->storageOffset,
                                         avOutput, output->storageOffset,
                                         avGradOutput, gradOutput->storageOffset,
                                         gradInput->size[0], gradInput->size[1]);
  } else {
    THError("vector or matrix expected");
  }

  THHcTensor_free(state, gradOutput);
  THHcTensor_free(state, output);
  return 1;
}

static const struct luaL_Reg hcnn_SoftMax__ [] = {
  {"SoftMax_updateOutput", hcnn_SoftMax_updateOutput},
  {"SoftMax_updateGradInput", hcnn_SoftMax_updateGradInput},
  {NULL, NULL}
};

/* Binding the SoftMax layer routines with lua stack */
void hcnn_SoftMax_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_SoftMax__, "nn");
  lua_pop(L, 1);
}
