/********************************************************************************************
 * MSECriterion
 *
 * criterion = nn.MSECriterion()
 * Functionality: Creates a criterion that measures the mean squared error between
 *                n elements in the input x and output y:  the Mean Squared Error
 *                criterion is generallyused for regression;
 *
 * loss(x, y) = 1/n \sum |x_i - y_i|^2 .
 * If x and y are d-dimensional Tensors with a total of n elements,
 * the sum operation still operates over all the elements, and divides by n.
 * The two Tensors must have the same number of elements (but their sizes might be different).
 *
 *********************************************************************************************/

#include<numeric>
#include "utils.h"
#include "amp_math.h"
#include "THHcFunctorOpns.h"

/* UpdateOutput routine invoked during Forward Propogation of convolution layer
 *
 * Computes the output using the current parameter set of the class (MSECriterion) and input.
 * This function returns the result which is stored in the output field.
 */

static int hcnn_MSECriterion_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  luaL_argcheck(L, THHcTensor_nElement(state, input) == THHcTensor_nElement(state, target),
                2, "input and target need to have the same number of elements");
  long size = THHcTensor_nElement(state, input);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  float sum = InnerProduct_plus_mse(state,  input, target);

  if(sizeAverage) {
    sum /= size;
  }

  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  lua_pushnumber(L, sum);
  lua_setfield(L, 1, "output");
  lua_pushnumber(L, sum);
  return 1;
}

/* UpdateGradInput routine invoked during Backward Propogation of convolution layer
 *
 * Computing the gradient of the MSECriterion module with respect to its own input.
 * This is returned in gradInput. Also, the gradInput state variable is updated accordingly
 */

static int hcnn_MSECriterion_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  luaL_argcheck(L, THHcTensor_nElement(state, input) == THHcTensor_nElement(state, target), 2,
                "input and target need to have the same number of elements");
  long size = THHcTensor_nElement(state, input);
  float norm = (sizeAverage ? 2. / size : 2.);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  THHcTensor_resizeAs(state, gradInput, input);
  transform_mse(state, input, target, gradInput, norm);
  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  return 1;
}

#define MSECRITERION_THREADS 128

void hcnn_MSECriterion_updateOutput_kernel(THHcState* state, float* &avOutput, long outOffset,
    float* &avInp, long inpOffset,
    float* &avTarget, long targetOffset,
    int nframe, int dim, int sizeAverage) {
  hc::extent<1> grdExt(MSECRITERION_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(MSECRITERION_THREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    tile_static float buffer[MSECRITERION_THREADS];
    float* input_k = avInp + inpOffset;
    float* target_k = avTarget + targetOffset;
    int k = tidx.tile[0];
    input_k += k * dim;
    target_k += k * dim;
    int i_start = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];
    // mse
    buffer[i_start] = 0;

    for (int i = i_start; i < i_end; i += i_step) {
      float z = input_k[i] - target_k[i];
      buffer[i_start] += z * z;
    }

    tidx.barrier.wait();

    //reduce
    if (i_start == 0) {
      avOutput[outOffset] = 0;

      for (int i = 0; i < i_step; i++) {
        avOutput[outOffset] += buffer[i];
      }

      if (sizeAverage) {
        avOutput[outOffset] /= dim;
      }
    }
  }).wait();
}

void hcnn_MSECriterion_updateGradInput_kernel(THHcState* state, float* &avGradInput, long gradInOffset,
    float* &avInp, long inpOffset,
    float* &avTarget, long targetOffset,
    float norm, int nframe, int dim) {
  hc::extent<1> grdExt(MSECRITERION_THREADS);
  hc::tiled_extent<1> t_ext = grdExt.tile(MSECRITERION_THREADS);
  THHcDeviceState* device_state = state->deviceState;
  hc::accelerator_view accl_view = device_state->get_current_accelerator_view();
  hc::parallel_for_each(accl_view, t_ext, [=] (hc::tiled_index<1> tidx) __attribute__((hc, cpu)) {
    float* input_k = avInp + inpOffset;
    float* target_k = avTarget + targetOffset;
    float* gradInput_k = avGradInput + gradInOffset;
    int k = tidx.tile[0];
    gradInput_k += k * dim;
    input_k += k * dim;
    target_k += k * dim;
    int i_start = tidx.local[0];
    int i_end = dim;
    int i_step = tidx.tile_dim[0];

    // gradInput
    for (int i = i_start; i < i_end; i += i_step) {
      gradInput_k[i] = norm * (input_k[i] - target_k[i]);
    }
  }).wait();
}

static int hcnn_MSECriterion_updateOutput2(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  long size = THHcTensor_nElement(state, input);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  THHcStorage* output = THHcStorage_newWithSize(state, 1);
  float* pavOutput = output->data;
  auto avInput = input->get_device_data(state);
  auto avTarget = target->get_device_data(state);
  //Since there is no storageOffset for THHcStorage the 2nd Argument is set to 0
  hcnn_MSECriterion_updateOutput_kernel(state, pavOutput, 0,
                                         avInput, input->storageOffset,
                                         avTarget, target->storageOffset,
                                         1, size, sizeAverage);
  lua_pushnumber(L, THHcStorage_get(state, output, 0));
  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  THHcStorage_free(state, output);
  lua_pushstring(L, "output");
  lua_pushvalue(L, -2);
  lua_rawset(L, 1);
  return 1;
}

static int hcnn_MSECriterion_updateGradInput2(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* target = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  int sizeAverage = luaT_getfieldcheckboolean(L, 1, "sizeAverage");
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  long size = THHcTensor_nElement(state, input);
  float norm = (sizeAverage ? 2. / size : 2.);
  input = THHcTensor_newContiguous(state, input);
  target = THHcTensor_newContiguous(state, target);
  THHcTensor_resizeAs(state, gradInput, input);
  auto avGradInput = gradInput->get_device_data(state);
  auto avInput = input->get_device_data(state);
  auto avTarget = target->get_device_data(state);
  hcnn_MSECriterion_updateGradInput_kernel(state, avGradInput, gradInput->storageOffset,
      avInput, input->storageOffset,
      avTarget, target->storageOffset,
      norm, 1, size);
  THHcTensor_free(state, input);
  THHcTensor_free(state, target);
  return 1;
}

static const struct luaL_Reg hcnn_MSECriterion__ [] = {
  {"MSECriterion_updateOutput", hcnn_MSECriterion_updateOutput},
  {"MSECriterion_updateGradInput", hcnn_MSECriterion_updateGradInput},
  {"MSECriterion_updateOutput2", hcnn_MSECriterion_updateOutput2},
  {"MSECriterion_updateGradInput2", hcnn_MSECriterion_updateGradInput2},
  {NULL, NULL}
};

/* Binding the MSECriterion layer routines with lua stack */
void hcnn_MSECriterion_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_MSECriterion__, "nn");
  lua_pop(L, 1);
}
