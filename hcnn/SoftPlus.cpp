/*************************************************************************
 *SoftPlus
 *
 * Applies the SoftPlus function to an n-dimensioanl input Tensor.
 * Can be used to constrain the output of a machine to always be positive.
 *
SoftPlus is defined as f_i(x) = 1/beta * log(1 + exp(beta * x_i)).
 *
 **************************************************************************/
#include "utils.h"
#include "amp_math.h"
struct softPlusupdateOutput_functor {
  float threshold;
  float beta;
  softPlusupdateOutput_functor(float threshold_, float beta_) __attribute__((hc, cpu)) : threshold(threshold_), beta(beta_) {}
  softPlusupdateOutput_functor(const softPlusupdateOutput_functor& other) __attribute__((hc, cpu))
    : threshold(other.threshold), beta(other.beta) {}
  float operator()(const float& input) const __attribute__((hc, cpu)) {
    float betain = beta * input;
    return ((betain) > threshold) ? input : (1 / beta) * hc::precise_math::log1p(hc::fast_math::exp(betain));
  }
  softPlusupdateOutput_functor& operator = (const softPlusupdateOutput_functor&other) __attribute__((hc, cpu)) {
    threshold = other.threshold;
    beta = other.beta;
    return *this;
  }
};

static int hcnn_SoftPlus_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  float beta = luaT_getfieldchecknumber(L, 1, "beta");
  float threshold = luaT_getfieldchecknumber(L, 1, "threshold");
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, softPlusupdateOutput_functor(threshold, beta));
  THHcTensor_free(state, input);
  return 1;
}

struct softPlusupdateGradInput_functor {
  float threshold;
  float beta;
  softPlusupdateGradInput_functor(float threshold_, float beta_) __attribute__((hc, cpu)): threshold(threshold_), beta(beta_) {}
  softPlusupdateGradInput_functor(const softPlusupdateGradInput_functor& other) __attribute__((hc, cpu))
    : threshold(other.threshold), beta(other.beta) {}
  float operator()(const float& output, const float& gradOutput) const __attribute__((hc, cpu)) {
    float betaout = beta * output;
    float exp_bo = hc::fast_math::exp(betaout);
    return ((betaout) > threshold) ? gradOutput : gradOutput * (exp_bo - 1) / exp_bo;
  }
  softPlusupdateGradInput_functor& operator = (const softPlusupdateGradInput_functor&other) __attribute__((hc, cpu)) {
    threshold = other.threshold;
    beta = other.beta;
    return *this;
  }
};

static int hcnn_SoftPlus_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  float beta = luaT_getfieldchecknumber(L, 1, "beta");
  float threshold = luaT_getfieldchecknumber(L, 1, "threshold");
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, output);
  transform(state, output, gradOutput, gradInput, softPlusupdateGradInput_functor(threshold, beta));
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_SoftPlus__ [] = {
  {"SoftPlus_updateOutput", hcnn_SoftPlus_updateOutput},
  {"SoftPlus_updateGradInput", hcnn_SoftPlus_updateGradInput},
  {NULL, NULL}
};

/* Binding the SoftPlus layer routines with lua stack */
void hcnn_SoftPlus_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_SoftPlus__, "nn");
  lua_pop(L, 1);
}

