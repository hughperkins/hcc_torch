/**************************************************************
 * Tanh
 *
 * Applies the Tanh function element-wise to the input Tensor,
 * thus outputting a Tensor of the same dimension.
 *
 ***************************************************************/
#include "utils.h"
#include "amp_math.h"

struct tanhupdateOutput_functor {
  tanhupdateOutput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& input) const __attribute__((hc, cpu)) {
    return hc::fast_math::tanh(input);
  }
};

static int hcnn_Tanh_updateOutput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* input = (THHcTensor*)luaT_checkudata(L, 2, torch_Tensor);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  input = THHcTensor_newContiguous(state, input);
  THHcTensor_resizeAs(state, output, input);
  transform(state, input, output, tanhupdateOutput_functor());
  THHcTensor_free(state, input);
  return 1;
}

struct tanhupdateGradInput_functor {
  tanhupdateGradInput_functor() __attribute__((hc, cpu)) {}
  float operator()(const float& output, const float& gradOutput) const __attribute__((hc, cpu)) {
    return gradOutput * (1 - output * output);
  }
};

static int hcnn_Tanh_updateGradInput(lua_State* L) {
  THHcState* state = getGputorchState(L);
  THHcTensor* output = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "output", torch_Tensor);
  THHcTensor* gradOutput = (THHcTensor*)luaT_checkudata(L, 3, torch_Tensor);
  THHcTensor* gradInput = (THHcTensor*)luaT_getfieldcheckudata(L, 1, "gradInput", torch_Tensor);
  gradOutput = THHcTensor_newContiguous(state, gradOutput);
  THHcTensor_resizeAs(state, gradInput, output);
  transform(state, output, gradOutput, gradInput, tanhupdateGradInput_functor());
  THHcTensor_free(state, gradOutput);
  return 1;
}

static const struct luaL_Reg hcnn_Tanh__ [] = {
  {"Tanh_updateOutput", hcnn_Tanh_updateOutput},
  {"Tanh_updateGradInput", hcnn_Tanh_updateGradInput},
  {NULL, NULL}
};

/* Binding the Tanh layer routines with lua stack */
void hcnn_Tanh_init(lua_State* L) {
  luaT_pushmetatable(L, torch_Tensor);
  luaT_registeratname(L, hcnn_Tanh__, "nn");
  lua_pop(L, 1);
}
