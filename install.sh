# Single script to install Torch-dependencies and MCW torch packages
BASEDIR=${PWD}

TORCHDIR=~/torch

function usage
{
    echo "usage:  [-r|--reinstall ] [-u|--remove]  [-h|--help]"
    echo "-r
        Reinstall torch7, its dependencies and HC GPU package(hctorch,hcnn,etc)"
    echo "-u
        Remove torch7, its dependencies and HC GPU package(hctorch,hcnn,etc)"
    echo "-h
        View usage"
}
reinstall=false
uninstall=false
while [ "$1" != "" ]; do
    case $1 in
        -u | --remove )       uninstall=true
                                ;;
        -r | --reinstall )    reinstall=true
                                ;;
        -cuda )               cubkn=true
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if $uninstall; then
  rm -rdf $TORCHDIR
  
  echo "Unistall sucessfully!"
  exit 0
fi

# default hcc path
export HCPLUSPLUSROOT="/opt/rocm/hcc"
# Sanity check
# Check if HC++ is installed or $MCWHCCBUILD (build) is set
if [ -d "$HCPLUSPLUSROOT" ]; then
  echo "Found HC++ in $HCPLUSPLUSROOT"
elif [ -z $MCWHCCBUILD ]; then
  echo "HC++ Compiler not found. Please install it."
  echo "Or use export MCWHCCBUILD=<path-to-AMP-build-folder>";
  exit 1
else
  if [ -e $MCWHCCBUILD/compiler/bin/clang ]; then
   echo "$MCWHCCBUILD/compiler/bin/clang found";
  else
    echo "$MCWHCCBUILD is not a valid AMP build folder";
    echo "Use export MCWHCCBUILD=<path-to-AMP-build-folder>";
    exit 1
  fi
fi

# Check torch directory
if $reinstall;then
  rm -rdf $TORCHDIR
fi
if [ -d "$TORCHDIR" ]; then
  echo "Found $TORCHDIR";
  # Already a git repository
  if [ -d "$TORCHDIR/.git" ]; then
    echo "Found $TORCHDIR/.git";
    # Get upstream
    cd $TORCHDIR && git fetch --all
  else
    rm -rf $TORCHDIR
    git clone https://github.com/torch/distro.git $TORCHDIR --recursive
  fi
else
  # clone the torch dependencies on root folder
  git clone https://github.com/torch/distro.git $TORCHDIR --recursive
fi
# Check if LD_LIBRARY_PATH contains torch lib path
PREPEND_SHELL_PROFILE=yes
if echo $LD_LIBRARY_PATH | grep -q "/torch/install/lib" ; then
  PREPEND_SHELL_PROFILE=no
fi
echo "prepend to shell profile: $PREPEND_SHELL_PROFILE"
# Freeze the coomit to a stable commit version
cd $TORCHDIR && git checkout -q 502c90b757c8c0cd66cd689078d5991bdd75b0b0

#Export torch distro package
export TORCHDISTROPATH=$TORCHDIR

# Use Lua version
export TORCH_LUA_VERSION=LUA51

#INSTALL Torch distro
#DO NOT add to shell profile multiple times
cd $TORCHDISTROPATH && echo $PREPEND_SHELL_PROFILE | ./install.sh

# Remove duplicated paths introduced by torch-activate in $PATH, $LD_LIBRARY_PATH, $DLD_LIBRARY_PATH
RC_FILE=0
if [[ $(echo $SHELL | grep bash) ]]; then
    RC_FILE=$HOME/.bashrc
elif [[ $(echo $SHELL | grep zsh) ]]; then
    RC_FILE=$HOME/.zshrc
else
    echo "

Non-standard shell $SHELL detected. You might want to
add the following lines to your shell profile:

. $TORCHDIR/bin/torch-activate
"
fi

echo "$RC_FILE found"
if [ $PREPEND_SHELL_PROFILE == "yes" ] && [ -f $RC_FILE ] ; then
  # Setup torch path in current shell
  . $TORCHDIR/install/bin/torch-activate
  echo "
export PATH=$(echo $PATH | tr ':' '\n' | sort -u | tr '\n' ':')
export LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | tr ':' '\n' | sort -u | tr '\n' ':')
export DYLD_LIBRARY_PATH=$(echo $DYLD_LIBRARY_PATH | tr ':' '\n' | sort -u | tr '\n' ':')" >>$RC_FILE
fi


#install MCW TORCH PACKAGES

##if $reinstall; then
removebuild="rm -rdf build"
###fi

#force fresh build 
cd $BASEDIR/mcw_nn && $removebuild && $TORCHDISTROPATH/install/bin/luarocks make rocks/nn-scm-1.rockspec
cd $BASEDIR/hctorch && $removebuild
  $TORCHDISTROPATH/install/bin/luarocks make rocks/cutorch-scm-1.rockspec
cd $BASEDIR/hctorch && $removebuild
  $TORCHDISTROPATH/install/bin/luarocks make rocks/hctorch-scm-1.rockspec
cd $BASEDIR/hcnn && $removebuild
  $TORCHDISTROPATH/install/bin/luarocks make rocks/cunn-scm-1.rockspec
cd $BASEDIR/hcnn && $removebuild
  $TORCHDISTROPATH/install/bin/luarocks make rocks/hcnn-scm-1.rockspec


echo "All done"


#
