-- Copyright 2004-present Facebook. All Rights Reserved.


require('hctorch')

local withDevice = hctorch.withDevice

local hc_local_copy_buffers = {}

--[[
`nn.AbstractParallel` is the base class for modules controlling
data/model-parallel behaviour in Torch.

The key concept is that data/model-parallelism _splits_ along a
dimension, and this class controls the distribution of input and
merging of output along this dimension.

To extend this class, override `_distributeInput` as appropriate.

See `nn.DataParallel` and `nn.ModelParallel` for examples of usage.
]]
local AbstractParallel, parent = torch.class('nn.AbstractParallel',
                                             'nn.Container')

function AbstractParallel:__init(dimension)
    if not dimension then
        error "must specify a dimension!"
    end
    parent.__init(self)
    self.modules = {}
    self.hc_assignments = {}
    self.size = torch.LongStorage()
    self.dimension = dimension
    self.container_hcid = hctorch.getDevice()

    self.input_hc = {}  -- inputs for each hc
    self.gradOutput_hc = {} -- inputs for each hc
    self.gradInput_hc = {} -- gradInput for each hc
end

function AbstractParallel:_freeCaches()
    self.input_hc = {}
    self.gradOutput_hc = {}
    self.gradInput_hc = {}
end

--[[
This function yields the Hc id for the module to be added.

It can be used for load balancing. It assumes all Hcs are available.
]]
function AbstractParallel:nextHc()
    local hcid = #self.hc_assignments % hctorch.getDeviceCount() + 1
    return hcid
end

function AbstractParallel._getBuffer()
    local device = hctorch.getDevice()
    if not hc_local_copy_buffers[device] then
        hc_local_copy_buffers[device] = torch.HcTensor()
    end
    return hc_local_copy_buffers[device]
end


function AbstractParallel:add(module, hcid)
    table.insert(self.modules, module)
    local hcid = hcid or self:nextHc()
    table.insert(self.hc_assignments, hcid)
    return self
end

function AbstractParallel:get(index)
    return self.modules[index]
end

--[[
Asynchronous copy from dest to source.

Use with caution; there needs to be some sort of external synchronization to
prevent source from being modified after this copy is enqueued.
]]
function AbstractParallel:hcSend(dest, source)
    assert(torch.typename(dest) == 'torch.HcTensor')
    assert(torch.typename(source) == 'torch.HcTensor')
    local dest_hcid = dest:getDevice()
    local source_hcid = source:getDevice()
    if source_hcid == dest_hcid then
        -- if both tensors are on the same hc normal copy works
        withDevice(dest_hcid, function()
            dest:copy(source)
        end)
        return
    end
    -- if both tensors are contiguous copy across hcs works
    if source:isContiguous() and dest:isContiguous() then
        withDevice(dest_hcid, function() dest:copy(source) end)
        return
    end

    local tmp_source = source
    if not source:isContiguous() then
        withDevice(source_hcid, function()
                       self:_getBuffer():resizeAs(source)
                       self:_getBuffer():copy(source)
                       tmp_source = self:_getBuffer()
        end)
    end

    withDevice(dest_hcid, function()
                   -- if destination is not contiguous copy across hcs does not
                   -- work; we need to first copy the source to the dest hc
                   if not dest:isContiguous() then
                       self:_getBuffer():resizeAs(tmp_source)
                       self:_getBuffer():copy(tmp_source)
                       dest:copy(self:_getBuffer())
                   else
                       if false then
                           -- Whoops! TODO t5112851
                           dest:lazyCopy(tmp_source)
                       else
                           dest:copy(tmp_source)
                       end
                   end

    end)
end

function AbstractParallel:updateOutput(input)
    local container_hcid = hctorch.getDevice()
    local outs = {}

    -- distribute the input to Hcs
    self:_distributeInput(input)

    -- update output for each module.
    for i, module in ipairs(self.modules) do
        local hcid = self.hc_assignments[i]
        withDevice(hcid, function()
                       assert(self.input_hc[hcid]:getDevice() ==
                                  self.hc_assignments[hcid])
                       outs[i] = module:updateOutput(self.input_hc[hcid])
        end)
    end

    -- find the size of the merged output.
    assert(container_hcid == self.hc_assignments[1])
    assert(outs[1].getDevice and
           (outs[1]:getDevice() == 0 or
            outs[1]:getDevice() == container_hcid))
    self.size:resize(outs[1]:dim()):copy(outs[1]:size())
    for i=2,#outs do
            self.size[self.dimension] =
                self.size[self.dimension] + outs[i]:size(self.dimension)
    end

    -- merge (concatenate) the outputs
    self.output:resize(self.size)
    local offset = 1
    for i=1,#outs do
        local outputDim = outs[i]:size(self.dimension)
        local output_narrowed =
            self.output:narrow(self.dimension, offset, outputDim)
        self:hcSend(output_narrowed, outs[i])
        offset = offset + outputDim
    end

    return self.output
end

function AbstractParallel:_distributeGradOutput(_input, gradOutput)
    local container_hcid = hctorch.getDevice()

    -- distribute gradOutput chunks to modules
    local offset = 1
    for i,module in ipairs(self.modules) do
        local hcid = self.hc_assignments[i]
        withDevice(hcid, function()
            local currentOutput = module.output

            -- get the gradOutput chunk for this module
            local currentGradOutput =
            gradOutput:narrow(self.dimension, offset,
                              currentOutput:size(self.dimension))

            self.gradOutput_hc[i] = self.gradOutput_hc[i] or torch.HcTensor()
            self.gradOutput_hc[i]:resizeAs(currentGradOutput)
            if hcid == container_hcid then
                self.gradOutput_hc[i]:copy(currentGradOutput)
            else
                -- copy gradoutput chunk to module's hc
                self:hcSend(self.gradOutput_hc[i], currentGradOutput)
            end

            offset = offset + currentOutput:size(self.dimension)
        end)
    end
end

function AbstractParallel:updateGradInput(_input, gradOutput)
   error('Not implemented')
end

function AbstractParallel:_mixGrads()
end

function AbstractParallel:accGradParameters(_input, _gradOutput, scale)
    scale = scale or 1
    for i,module in ipairs(self.modules) do
        local hcid = self.hc_assignments[i]
        withDevice(hcid, function()
            module:accGradParameters(self.input_hc[hcid],
            self.gradOutput_hc[i],
            scale)
        end)
    end
    -- Combine gradients for data parallel models
    self:_mixGrads()
end

function AbstractParallel:accUpdateGradParameters(_input, _gradOutput, lr)
    for i,module in ipairs(self.modules) do
        local hcid = self.hc_assignments[i]
        withDevice(hcid, function()
            module:accUpdateGradParameters(self.input_hc[hcid], self.gradOutput_hc[i], lr)
        end)
    end
end

function AbstractParallel:zeroGradParameters()
    for i,module in ipairs(self.modules) do
        withDevice(self.hc_assignments[i], function()
            module:zeroGradParameters()
        end)
    end
end

function AbstractParallel:updateParameters(learningRate)
    for i,module in ipairs(self.modules) do
        withDevice(self.hc_assignments[i], function()
            module:updateParameters(learningRate)
        end)
    end
end

function AbstractParallel:share(mlp,...)
    error("Share is not supported for the AbstractParallel layer.")
end

function AbstractParallel:clone()
    local clone = parent.clone(self)
    clone:hc()
    return clone
end

function AbstractParallel:reset(stdv)
    for i,module in ipairs(self.modules) do
        withDevice(self.hc_assignments[i], function()
            self.modules[i]:reset(stdv)
        end)
    end
end


