require "hctorch"
require "nn"
require "paths"

paths.dofile('fb_dependencies_ported/AbstractParallel.lua')
paths.dofile('fb_dependencies_ported/ModelParallel.lua')
paths.dofile('fb_dependencies_ported/DataParallel.lua')
paths.dofile('fb_dependencies_ported/Optim.lua')

function nn.Module:getParametersByDevice()
    local n_dev = hctorch.getDeviceCount()
    local d2weights = {} -- Device => { tensor1, tensor2, ..., tensorN }
    local d2grads   = {} -- Device => { tensor1, tensor2, ..., tensorN }

    local function tensor_to_dev(tensor)
        local tnm = torch.typename(tensor)
        if tnm == 'torch.HcTensor' then
            return tensor:getDevice()
        end
        return 0
    end

    local params, grads = self:parameters()
    assert(#params == #grads)
    -- Herd each tensor into appropriate row of weights,grads
    for i = 1,#params do
        local p = params[i]
        local g = grads[i]
        local d = tensor_to_dev(p)
        if d ~= tensor_to_dev(g) then
            error(("Improbable module; params,grads on devices %d,%d"):
                  format(d, tensor_to_dev(g)))
        end
        if not d2weights[d] then
            d2weights[d] = {}
            d2grads[d] = {}
        end
        table.insert(d2weights[d], p)
        table.insert(d2grads[d], g)
    end

    local function gather(dev, params, grads)
        if not params or #params == 0 then
            return nil
        end
        if dev == 0 then
            return nn.Module._gather(params), nn.Module._gather(grads)
        end
        return hctorch.withDevice(dev,
            function() return nn.Module._gather(params),
                              nn.Module._gather(grads)
        end)
    end

    local ret_params = { }
    local ret_grads = { }
    for d = 0,n_dev do -- sic
        ret_params[d], ret_grads[d] = gather(d, d2weights[d], d2grads[d])
    end

    return ret_params, ret_grads
end


