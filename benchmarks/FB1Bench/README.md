-- Copyright 2014-present Advanced Micro Devices. All Rights Reserved

-- Copyright 2014-present Multicoreware. All Rights Reserved

-- Copyright 2004-present Facebook. All Rights Reserved.

A. Requirements:
================
     Having mcw_nn, hctorch and hcnn torch packages installed. (Refer the repo's wiki for installation of the same)
	 

B. Steps to run the bench (In a nutshell):
==========================================
1. Download the benchmark
   Via Browser : click [here](https://multicorewareinc.egnyte.com/dl/3W4m2yQW0m)  
   Via Commandline : wget https://multicorewareinc.egnyte.com/dd/3W4m2yQW0m/imagenet-barebones.tar.gz

2. tar -xvf imagenet-barebones.tar.gz

3. cd imagenet-barebones

4. th -i runMetaData.lua

5. exit Torch prompt

6. th -lhcnn -i main.lua


C. Detailed Explanation 	   
=======================

##a. Running the Training Script


This package contains an implementation of an ImageNet classifier using Torch7. This is the barebones script for training a Convolutional Network on Imagenet dataset.

The script comes with a micro subset of the full Imagenet data. It is in the directory ./data/

In order to run the script you need to do the following:

      Step 1: Install Torch7 distro packages. Along with that also install torch packages viz mcw\_nn, hctorch and hcnn on your machine.

      Step 2: Run the metadata generation script (which assumes raw training data files in the directory ./data/train/)          

      $ th -i runMetaData.lua

      This will generate a bunch of metadata files in the ./data/ directory which consists of information, such as, path of the raw data file etc.

      Step 3: Run the training script on AMD GPU

      $ th -i main.lua

           OR

      Step 3: Run the training script on CPU
     
      -- Change the value of config.main.type variable in the config.lua file to

      config.main.type = 'torch.FloatTensor'

      Then run the following command

      $ th -i main.lua


Running on GPU assumes that have a working installation of GPU on your machine. In addition it assumes that torch is configured with GPU (see
wiki page for further details).

## b. Expected Output
=====================

The training script logs key metrics to the console at regular intervals.
The following values are reported:

      1. nbatch: Reflects the total number of training batches processed so far. This index increases steadily over the course of training.

      2. rat: The learning rate. This parameter feeds into the learning algorithm and basically determines how large a step the gradient descent performs
         per iteration. In this training script, the learning rate is held constant. More elaborate training schemes changes this value (typically
         decreasing as traning progresses.
     
      3. err: The classification error. This is the number of miscategorized inputs per batch divided by the total number of images tested (i.e. batch size).
         This is expected to fall monotonically over the course of training. In this script with the provided small set of training data, this number
         will go to 0.0 fairly quickly due to the network overfitting its
         responses to the small data set.
         
      4. obj: The objective-function's output for this batch. As training progresses, this values is minimized. The value should decrease monotonically over
         the course of the training run. You will see some fluctuations for both
         the err and the obj. This is because they are computed independently for
         for each mini-batch.
         
      5. dat: This is the time (in seconds) required to load the data for the training batch. fpp: This is the time (in seconds) required to do a forward pass through the
         network. In other words, this is the time it takes to classify the image.

      6. bpp: This is the time (in seconds) required to do a backward pass through the network. Backward passes compute gradients and are thus expected to take substantially longer than the forward pass.
      
      7. upd: This is the time (in seconds) required to updated the networks weights making one step in the direction of the accumulated gradients of the batch.

At a lower frequency, the training script performs a test to evaluate the effectiveness of the training procedure using a different data set from the
training data set. It outputs the following values:

      1. n: The number of samples seen during the evaluation phase. This index
         increases steadily over the course of evaluation phase.
  
      2. e: The accumulated classification error over all the samples seen so
         far during the evaluation phase.
         
      3. l: The accumulated value of the objective function's output over all
         the samples seen so far during the evaluation phase.
         err: As described above.
         obj: As described above.
         fpp: As described above.
         acc: Time (in seconds) required to accumulate the error and loss values(e and l).










