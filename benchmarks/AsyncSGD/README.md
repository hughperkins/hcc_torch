#Training an Object Classifier in Torch-7 on multiple GPUs over [ImageNet](http://image-net.org/download-images) using AsynSGD algorithm to harness the efficacy of Multiple-GPUs

## A. Introduction
In this concise example we showcase:

- train [AlexNet](http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks) or [Overfeat](http://arxiv.org/abs/1312.6229) on ImageNet

- showcase hcnn backend

- use asychronouse SGD a downpour like algorithm to speedup training over multiple GPUs. The current implementation hosts the CPU as parameter server.


## B. File Description
- `opts.lua` - all the command-line options and description
- `data.lua` - contains the logic to create K threads for parallel data-loading.
- `donkey.lua` - contains the data-loading logic and details. It is run by each data-loader thread. random image cropping, generating 10-crops etc. are in here.
- `dataset.lua` - a general purpose data loader, mostly derived from [here: imagenetloader.torch](https://github.com/soumith/imagenetloader.torch). That repo has docs and more examples of using this loader.
- `main-AsynSGD.lua` - the main procedure file of Asynchonous SGD. 
- `Optim-AsynSGD.lua` - the SGD procedure file of Asynchronous SGD. It is call by main-AsynSGD.lua.
- `model-AsyncSGD.lua` - creates AlexNet or Overfeat model and criterion.


## C. Requirements

- Download Imagenet-12 dataset from http://image-net.org/download-images . It has 1000 classes and 1.2 million images.

- Having mcw_nn, hctorch and hcnn torch packages installed. (Refer the repo's wiki for installation of the same)



## D. Data processing 

(The contents of this section is borrowed from [here](https://github.com/facebook/fbcunn/tree/master/examples/imagenet))

**The images dont need to be preprocessed or packaged in any database.** 

It is preferred to keep the dataset on an [SSD](http://en.wikipedia.org/wiki/Solid-state_drive) but we have used the data loader comfortably over NFS without loss in speed.

We just use a simple convention: SubFolderName == ClassName. So, for example: if you have classes {cat,dog}, cat images go into the folder dataset/cat and dog images go into 

dataset/dog

The training images for imagenet are already in appropriate subfolders (like n07579787, n07880968). You need to get the validation groundtruth and move the validation images 

into appropriate subfolders. To do this, download ILSVRC2012_img_train.tar ILSVRC2012_img_val.tar and follow the below steps:

### a. Extract train data

    mkdir train && mv ILSVRC2012_img_train.tar train/ && cd train
	
    tar -xvf ILSVRC2012_img_train.tar && rm -f ILSVRC2012_img_train.tar
	
    find . -name "*.tar" | while read NAME ; do mkdir -p "${NAME%.tar}"; tar -xvf "${NAME}" -C "${NAME%.tar}"; rm -f "${NAME}"; done
	
### b. Extract validation data

    cd ../ && mkdir val && mv ILSVRC2012_img_val.tar val/ && cd val && tar -xvf ILSVRC2012_img_val.tar

    wget -qO- https://raw.githubusercontent.com/soumith/imagenetloader.torch/master/valprep.sh | bash


Now you are all set!

If your imagenet dataset is on HDD or a slow SSD, run this command to resize all the images such that the smaller dimension is 256 and the aspect ratio is intact.

This helps with loading the data from disk faster.


      find . -name "*.JPEG" | xargs -I {} convert {} -resize "256^>" {}
      

## E. Install Torch-dependencies and MCW torch packages

Refer the repo's wiki for installation of the same


## F. Clone AsyncSGD benchmark

     git clone https://bitbucket.org/multicoreware/asynchronoussgd.git

     cd asynchronoussgd

     git checkout hcnn-backend-cleancode


## G. GPU Training

The training script come with following options (some of them are Hyperparameters for the Network) that can be set by the user. This can be listed by running the script with the flag --help

    $ th main-AsynSGD.lua --help

    Torch-7 Imagenet Training script
    Options:
    
    -cache       subdirectory in which to save/log experiments [/home/mcw/SSD/imagenet_runs_asyncsgd]
    -data        Home of ImageNet dataset [/home/mcw/SSD/imagenet-256]
    -manualSeed  Manually set RNG seed [2]
    -GPU         Default preferred GPU [1]
    -nGPU        Number of GPUs to use by default [1]
    -backend     Options: hcnn-AsynSGD [hcnn-AsynSGD]
    -nDonkeys    number of donkeys to initialize (data loading threads) [2]
    -nEpochs     Number of total epochs to run [60]
    -epochSize   Number of batches per epoch [10000]
    -epochNumber Manual epoch number (useful on restarts) [1]
    -batchSize   mini-batch size (1 = pure stochastic) [128]
    -LR          learning rate; if set, overrides default LR/WD recipe [0]
    -momentum    momentum [0.9]
    -weightDecay weight decay [0.0005]
    -netType     Options: alexnet | overfeat [alexnet]
    -retrain     provide path to model to retrain with [none]
    -optimState  provide path to an optimState to reload from [none]
    -nPush       Number of batches for pushing parameters [101]

Each of these parameters have default values set. To Modify a particular option we just need to add the appropriate flag

For example,  to change number of target GPUs 
  
   th main-AsncSGD.lua --nGPU #(target\_number\_of\_GPUs)


To begin the training, simply need to run main-AsyncSGD.lua

By default, the script runs 1-GPU AlexNet with the hcnn backend and 2 data-loader threads.

```bash
th main-AsynSGD.lua -data [imagenet-folder with train and val folders]
```

For 2-GPU AlexNet + hcnn, you can run it this way:

```bash
th main-AsynSGD.lua -data [imagenet-folder with train and val folders] -nGPU 2 -backend hcnn -netType alexnet
```
You can also alternatively train OverFeat using this following command:

```bash
th main-AsynSGD.lua -data [imagenet-folder with train and val folders] -netType overfeat
```

For multi-GPU overfeat (let's say 2-GPUs)

```
th main-AsynSGD.lua -data [imagenet-folder with train and val folders] -netType overfeat -nGPU 2
```

The training script prints the current Top-1 and Top-5 error in % as well as the objective loss at every mini-batch.

A hard-coded learning rate schedule is used so that AlexNet converges to an error of 42.5% at the end of 53 epochs.

At the end of every epoch, the model is saved to disk (as model_[xx].t7 where xx is the epoch number).

You can reload this model into torch at any time using torch.load

```lua
model = torch.load('model_10.t7') -- loading back a saved model
```

## H. Brief Description of Algorithm

The main components and their functions

     a. Parameter File: It is a local file that stores the parameters. Different GPUs can fetch parameters from the parameter file at the beginning of a new iteration of training and update the new difference to parameter file.

     b. CPU: It creates thread pool and schedules mini batches to pool threads asynchronously.

     c. GPU: It trains batches separately, make difference of the new & old parameters and updates the parameter difference to the parameter file

     d. Data loader: It is a procedure in the main thread. It loads a mini batch to a GPU when the GPU is available for training.

     e. Data set: It is the local files that keeps the data batches. A batch of data is loaded by a data loader at one time.
     
     
![20150511-Architecture-of-AsynSGD-algorithm.png](https://bitbucket.org/repo/x6X8nB/images/2491410530-20150511-Architecture-of-AsynSGD-algorithm.png)

## I. Execution Work Flow

As is shown in the figure. The execution path for a GPU say GPU1 are as follows:

1. CPU creates a thread pool. The number of threads in this pool is equal to the GPU number. e.g 2 threads for 2 GPUs. Each of the pool thread will initialize a 
   
    model for itself including layers and initial parameters.
    
2. GPU1 saves the initial random parameters to parameter file.

3. Data loader1 loads a batch of data and sends it to GPU1.

4. GPU1 begins training as soon as it got a mini batch.g

5. If batchnumber % nPush == 0 then

    a. GPU1 makes the difference of the new and old parameters like: dW = W' - W
    
    b. GPU1 loads parameters from the parameter file to a table params 
    
    c. Add dW to params like: params = params + dW
    
    d. Save params to the parameter file
    
6. If batchnumber % nFetch == 0 then

    a. GPU1 load params from the parameter file
    
    b. Replace the current model’s parameters with loaded params

GPU1 loops with steps (3), (4), (5) and (6) .
Step (5) and (6) are not necessary every time. Only the training iterations of a GPU reach the preset value (nFetch or nPush) the GPU will do fetch or push job.
The other GPUs do the similar jobs at the same time in asynchronous parallel.
All the GPUs work separately without any synchronous.

