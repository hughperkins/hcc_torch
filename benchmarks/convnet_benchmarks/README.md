Install torch-7 (Refer the repos wiki for installation of the same)

For layerwise benchmarks (table in frontpage with L1,L2,L3,L4,L5)
Run the benchmark using:
```bash
th layerwise_benchmarks/benchmark.lua
```
Run the multiGPU test using:
```bash
th layerwise_benchmarks/multigpu.lua
```

For imagenet-winners benchmarks
Run the benchmark using:
```bash
th imagenet_winners/benchmark.lua
```
