#!/bin/sh

iter=1
NofIter=1000

while [ $iter -le $NofIter ]
do
   # hctorch test
   th -lhctorch -e "hctorch.test()"
  
   # hcnn test
   th -lhcnn -e "nn.testhc()"

   iter=`expr $iter + 1`
done
