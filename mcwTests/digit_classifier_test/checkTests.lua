require 'nn'

function check_tensor_elements(tensor, name)
   local storage = tensor:storage()
   for i = 1, storage:size() do
      if (storage[i] ~= storage[i]) or 
         (storage[i] <= -math.huge or storage[i] >= math.huge) then
         if name ~= nil then print("") print("Layer: " .. name) end
         print("FAIL: check_tensor_elements. Tensor has nan/inf elements")
         print("Err: Index:" .. i .. ", " .. storage[i])
         return false
      end
   end
   return true
end

function check_number(num, name)
   if (num ~= num) or (num <= -math.huge or num >= math.huge) then 
      if name ~= nil then print("") print("Layer: " .. name) end
      print("FAIL: check_number : Val=" .. num)
      return false
   end
   return true
end

local function compare_tensors(Tensor1, Tensor2)
   if Tensor1 == nil or Tensor2 == nil then 
      print("FAIL: compare_tensors")
      print("Empty tensor")  
      return false 
   end
   local errAccept = 1e-4

   local size1 = Tensor1:size()
   local size2 = Tensor2:size()

   if size1:size() ~= size2:size() then
      print("FAIL: compare_tensors")
      print("Dimension mismatch")  
      return false
   end

   for i=1, size1:size() do
      if size1[i] ~= size2[i] then
         print("FAIL: compare_tensors")
         print("Size mismatch")  
         return false
      end
   end

   local storage1 = Tensor1:storage()
   local storage2 = Tensor2:storage()

   for i = 1, storage1:size() do
      if math.abs(storage1[i] - storage2[i]) > errAccept then
         print("Err: Index:" .. i .. ", " .. storage1[i] .. " and " .. storage2[i]  .. " are not same")
         return false
      end
   end
   return true
end

local function compare_results(name1, name2)
   if name1 == nil or name2 == nil then return false end
   print(name1 .. " VS " .. name2)
   local errAccept = 1e-4

   output1 = torch.load(paths.concat(defaultDir, name1 .. '-output.t7'))
   output2 = torch.load(paths.concat(defaultDir, name2 .. '-output.t7'))

   local storage1 = output1:storage()
   local storage2 = output2:storage()

   if storage1:size() ~= storage2:size() then print("Size mismatch") return false end
   for i = 1, storage1:size() do
      if math.abs(storage1[i] - storage2[i]) > errAccept then
         print("Err: Index:" .. i .. ", " .. storage1[i] .. " and " .. storage2[i]  .. " are not same")
         return false
      end
   end
   return true
end
