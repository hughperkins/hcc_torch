require 'nn'

paths.dofile('checkTests.lua')

function forward_section(model, input)
   local output
   if defaultType == "torch.CudaTensor" then
      model = model:cuda()
      input = input:cuda()
      output = model:forward(input)
      --torch.save(paths.concat(defaultDir, 'cuda-output.t7'), output1:float())
   elseif defaultType == "torch.HcTensor" then
      model = model:hc()
      input = input:hc()
      output = model:forward(input)
      --torch.save(paths.concat(defaultDir, 'amp-output.t7'), output1:float())
   else
      output = model:forward(input)
      --torch.save(paths.concat(defaultDir, 'cpu-output.t7'), output1:float())
   end
   return output
end

function forward_criterion(model, input, labels)
   local objective
   if defaultType == "torch.CudaTensor" then
      model = model:cuda()
      input = input:cuda()
      labels = labels:cuda()
      objective = model:forward(input, labels)
      --torch.save(paths.concat(defaultDir, 'cuda-output.t7'), output1:float())
   elseif defaultType == "torch.HcTensor" then
      model = model:hc()
      input = input:hc()
      labels = labels:hc()
      objective = model:forward(input, labels)
      --torch.save(paths.concat(defaultDir, 'amp-output.t7'), output1:float())
   else
      objective = model:forward(input, labels)
      --torch.save(paths.concat(defaultDir, 'cpu-output.t7'), output1:float())
   end
   return objective
end

function backward_criterion(model, input, labels)
   local gradOutput
   if defaultType == "torch.CudaTensor" then
      model = model:cuda()
      input = input:cuda()
      labels = labels:cuda()
      gradOutput = model:backward(input, labels)
      --torch.save(paths.concat(defaultDir, 'cuda-output.t7'), output1:float())
   elseif defaultType == "torch.HcTensor" then
      model = model:hc()
      input = input:hc()
      labels = labels:hc()
      gradOutput = model:backward(input, labels)
      --torch.save(paths.concat(defaultDir, 'amp-output.t7'), output1:float())
   else
      gradOutput = model:backward(input, labels)
      --torch.save(paths.concat(defaultDir, 'cpu-output.t7'), output1:float())
   end
   return gradOutput
end

function backward_section(model, input, err)
   local diff
   if defaultType == "torch.CudaTensor" then
      model = model:cuda()
      input = input:cuda()
      diff = model:backward(input, err)
      --torch.save(paths.concat(defaultDir, 'cuda-output.t7'), output1:float())
   elseif defaultType == "torch.HcTensor" then
      model = model:hc()
      input = input:hc()
      diff = model:backward(input, err)
      --torch.save(paths.concat(defaultDir, 'amp-output.t7'), output1:float())
   else
      diff = model:backward(input, err)
      --torch.save(paths.concat(defaultDir, 'cpu-output.t7'), output1:float())
   end
   return diff
end

function testLayers(model, input, criterion, target)
   local layers = {}

   for i=1, model:size() do
      layers[i] = model:get(i)
   end

   output = input
   if opt.propagation == "complete" then
      for i=1, model:size() do
         output = forward_section(layers[i], output)
         if not(check_tensor_elements(output)) then return false end
      end
      f = forward_criterion(criterion, output, target)
      if not(check_number(f, "Criterion_forward")) then return false end

      df_do = backward_criterion(criterion, output, target)
      if not(check_tensor_elements(df_do, "Criterion_backward")) then return false end
      prevOutput = input
      for i=1, model:size() do
         groundGrad = backward_section(model, prevOutput, df_do)
         if not(check_tensor_elements(groundGrad)) then return false end
         prevOutput = groundGrad
      end
   elseif opt.propagation == "forward" then
      for i=1, model:size() do
         output = forward_section(layers[i], output)
         if not(check_tensor_elements(output)) then return false end
      end
      f = forward_criterion(criterion, output, target)
      if not(check_number(f, "Criterion_forward")) then return false end
   elseif opt.propagation == "backward" then
      output = model:forward(input)
      f = forward_criterion(criterion, output, target)

      df_do = backward_criterion(criterion, output, target)
      if not(check_tensor_elements(df_do, "Criterion_backward")) then return false end
      prevOutput = input
      for i=1, model:size() do
         groundGrad = backward_section(model, prevOutput, df_do)
         if not(check_tensor_elements(groundGrad)) then return false end
         prevOutput = groundGrad
      end
   else
      print("Invalid option " .. opt.propagation .. " for propagation")
      print("Propagation could be 'complete' or 'forward' or 'backward'")
      return false
   end

   return true
end

function testModel(model, input, criterion, target)
   if opt.propagation == "complete" then
      output = forward_section(model, input)
      if not(check_tensor_elements(output, "model_forward")) then return false end

      f = forward_criterion(criterion, output, target)
      if not(check_number(f, "Criterion_forward")) then return false end

      df_do = backward_criterion(criterion, output, target)
      if not(check_tensor_elements(df_do, "Criterion_backward")) then return false end

      groundGrad = backward_section(model, input, df_do)
      if not(check_tensor_elements(groundGrad, "model_backward")) then return false end
   elseif opt.propagation == "forward" then
      output = forward_section(model, input)
      if not(check_tensor_elements(output, "model_forward")) then return false end

      f = forward_criterion(criterion, output, target)
      if not(check_number(f, "Criterion_forward")) then return false end
   elseif opt.propagation == "backward" then
      output = forward_section(model, input)
      f = forward_criterion(criterion, output, target)

      df_do = backward_criterion(criterion, output, target)
      if not(check_tensor_elements(df_do, "Criterion_backward")) then return false end

      groundGrad = backward_section(model, input, df_do)
      if not(check_tensor_elements(groundGrad, "model_backward")) then return false end
   else
      print("Invalid option " .. opt.propagation .. " for propagation")
      print("Propagation could be 'complete' or 'forward' or 'backward'")
   end

   return true
end

function mcwTest.digit_classifier_test()
   local title = 'digit_classifier_test'
   result[title] = 'FAIL'

   --Layers of digit-classifier for convnet model
   model = nn.Sequential()
   model:add(nn.SpatialConvolutionMM(1, 32, 5, 5))
   model:add(nn.Tanh())
   model:add(nn.SpatialMaxPooling(3, 3, 3, 3))
   model:add(nn.SpatialConvolutionMM(32, 64, 5, 5))
   model:add(nn.Tanh())
   model:add(nn.SpatialMaxPooling(2, 2, 2, 2))
   model:add(nn.Reshape(64*2*2))
   model:add(nn.Linear(64*2*2, 200))
   model:add(nn.Tanh())
   model:add(nn.Linear(200, 10))
   model:add(nn.LogSoftMax())

   criterion = nn.ClassNLLCriterion()

   input = torch.Tensor():rand(4,1,32,32)
   targets = torch.Tensor():rand(10)

   local res = false
   if opt.test == "model" then
      res = testModel(model, input, criterion, targets)
   else
      res = testLayers(model, input, criterion, targets)
   end

   if res then result[title] = 'PASS' end
end

function mcwTest.barebones_test()
   local title = 'barebones_test'
   result[title] = 'FAIL'
   res = {}

   local layer1 = nn.SpatialConvolutionMM(3,4,11,11,1,1)
   local input = torch.Tensor():rand(32,3,221,221)

   output = forward_section(layer1, input)
   res[1] = check_tensor_elements(output, "SpatialConvolutionMM")

   local layer2 = nn.SpatialMaxPooling(26,26)
   output = forward_section(layer2, output)
   res[2] = check_tensor_elements(output, "SpatialMaxPooling")

   local layer3 = nn.Reshape(4 * 8 * 8)
   output = forward_section(layer3, output)
   res[3] = check_tensor_elements(output, "Reshape")

   local layer4 = nn.Linear(4 * 8 * 8, 1000)
   output = forward_section(layer4, output)
   res[4] = check_tensor_elements(output, "Linear")

   local layer5 = nn.LogSoftMax()
   output = forward_section(layer5, output)
   res[5] = check_tensor_elements(output, "LogSoftMax")

   local layer6 = nn.ClassNLLCriterion()
   local labels = torch.Tensor():rand(32)
   loss = forward_criterion(layer6, output, labels)
   res[6] = (loss == loss)

   if res[1] and  res[2] and res[3] and res[4] and res[5] and res[6] then result[title] = 'PASS' end
end

