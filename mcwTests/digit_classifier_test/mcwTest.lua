--[[
This script has few tests on different layers of different benchmarks.
It checks the correctness of each layer of different demos and benchmarks.
It can also compare the results across the platforms. (cunn VS hcnn VS nn)

MULTICOREWARE PRIVATE LIMITED
]]--

--NOTE: Still under construction

require 'nn'
require 'paths' 

mcwTest = {}
precision_forward = 1e-4
precision_backward = 1e-4
result = {}

-------------------------------------------------------------------------------------
-- parse command-line options
-------------------------------------------------------------------------------------
opt = lapp[[
   --save          (default "results")      subdirectory to save logs
   --backend       (default "amp")          backend cuda | amp | cpu 
   --device        (default 1)              device on which the tests run
   --seed          (default 123)            manual seed to set
   --test          (default "model")        model | layer
   --propagation   (default "complete")     complete | forward | backward
   --store         (default false)          store the test outputs in disk
]]
-------------------------------------------------------------------------------------

local seed = opt.seed
local device = opt.device
torch.manualSeed(seed)

os.execute('mkdir -p ' .. opt.save)
defaultDir = paths.concat(os.getenv('PWD'), opt.save)

defaultType=""
if opt.backend == 'cuda' then
   print("CUDA backend")
   defaultType = "torch.CudaTensor"

   require 'cunn'
   cutorch.setDevice(device)
   cutorch.manualSeed(seed)
elseif opt.backend == 'amp' then
   print("AMP backend")
   defaultType = "torch.HcTensor"

   require 'hcnn'
   hctorch.setDevice(device)
   hctorch.manualSeed(seed)
else
   print("CPU backend")
   defaultType = "torch.FloatTensor"
end

paths.dofile('test.lua')

function mcwTests(tests)
   mcwTester = torch.Tester()
   mcwTester:add(mcwTest)
   mcwTester:run(tests)

   print ''
   print ' ----------------------------------------------------------------------------------------------'
   print '|  Test name                                                                       | Result   |'
   print ' ----------------------------------------------------------------------------------------------'
   for module,res in pairs(result) do
      local str = string.format('| %-80s | %-4s     |', module, res)
      print(str)
   end
   print ' ----------------------------------------------------------------------------------------------'
end

mcwTests("digit_classifier_test")
