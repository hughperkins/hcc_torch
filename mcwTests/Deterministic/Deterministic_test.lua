local hcnntest = {}
local precision_forward = 1e-4
local precision_backward = 1e-2
local nloop = 1
local times = {}

local runtests = false
require 'nn'

if not hctorch then
  require 'hctorch'
  runtests=true
end

function hcnntest.SpatialAveragePooling_forward1()
   local from = 37
   local to = from
   local ki = 4
   local kj = 2
   local si = 1
   local sj = 1
   local outi = 181
   local outj = 79
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
							-- 37    80   184  2   4   37  79    181 
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAveragePooling_forward2()
   local from = 17
   local to = from
   local ki = 3
   local kj = 4
   local si = 2
   local sj = 2
   local outi = 197
   local outj = 191
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
							 --17    384  395  4   3   17  191   197
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAveragePooling_forward3()
   local from = 3
   local to = from
   local ki = 4
   local kj = 4
   local si = 4
   local sj = 4
   local outi = 152
   local outj = 101
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
							 --3     404  457  4   4   3   101   152
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAveragePooling_forward_batch1()
   local bs = 4
   local from = 32
   local to = from
   local ki = 4
   local kj = 4
   local si = 1
   local sj = 2
   local outi = 154
   local outj = 172
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --4   32    346  157  4   4   4   32  172   154
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAveragePooling_forward_batch2()
   local bs = 8
   local from = 64
   local to = from
   local ki = 4
   local kj = 4
   local si = 4
   local sj = 1
   local outi = 108
   local outj = 48
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --8   64    51   432  4   4   8   64  48    108
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialAveragePooling_forward_batch3()
   local bs = 4
   local from = 56
   local to = from
   local ki = 3
   local kj = 3
   local si = 3
   local sj = 3
   local outi = 125
   local outj = 71
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --4   56    213  375  3   3   4   56  71    125
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialMaxPooling_forward()
   local from = 35
   local to = from
   local ki = 2
   local kj = 3
   local si = 4
   local sj = 3
   local outi = 206
   local outj = 107
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.forward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
							 --35    321  822  3   2   35  107   206
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
   local error_ind = gconv.indices:float() - sconv.indices
   mytester:asserteq(error_ind:max(), 0, 'error on indices (forward) ')
end

function hcnntest.SpatialMaxPooling_forward_batch1()
   local bs = 6
   local from = 32
   local to = from
   local ki = 4
   local kj = 2
   local si = 3
   local sj = 4
   local outi = 249
   local outj = 162
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --6   32    646  748  2   4   6   32  162   249
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialMaxPooling_forward_batch2()
   local bs = 10
   local from = 60
   local to = from
   local ki = 2
   local kj = 3
   local si = 3
   local sj = 4
   local outi = 53
   local outj = 186
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --10  60    743  158  3   2   10  60  186   53
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_single1()
   local from = 13
   local to = 40
   local ki = 3
   local kj = 13
   local si = 1
   local sj = 3
   local outi = 58
   local outj = 41
   local padW = 1
   local padH = 1
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --13    131  58   13  3   40  41    58    3   1   1     1
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_single2()
   local from = 24
   local to = 8
   local ki = 3
   local kj = 4
   local si = 3
   local sj = 3
   local outi = 19
   local outj = 35
   local padW = 1
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --24    106  55   4   3   8   35    19    3   3   0     1
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_single3()
   local from = 15
   local to = 64
   local ki = 5
   local kj = 11
   local si = 2
   local sj = 3
   local outi = 52
   local outj = 2
   local padW = 0
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --15    14   107  11   5  64   2     52    3   2   0     0
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_batch1()
   local bs = 8
   local from = 14
   local to = 16
   local ki = 8
   local kj = 7
   local si = 3
   local sj = 3
   local outi = 12
   local outj = 2
   local padW = 1
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --8   14    10   39   7   8   8   16  2     12    3   3   0     1
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_batch2()
   local bs = 4
   local from = 27
   local to = 24
   local ki = 6
   local kj = 4
   local si = 1
   local sj = 1
   local outi = 17
   local outj = 51
   local padW = 1
   local padH = 1
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --4   27    52   20   4   6   4   24  51    17    1   1   1     1
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_batch3()
   local bs = 4
   local from = 6
   local to = 56
   local ki = 8
   local kj = 12
   local si = 2
   local sj = 2
   local outi = 44
   local outj = 40
   local padW = 1
   local padH = 1
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --4   6     88   92   12  8   4   56  40    44    2   2   1     1
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_batch4()
   local bs = 8
   local from = 8
   local to = 16
   local ki = 9
   local kj = 12
   local si = 2
   local sj = 3
   local outi = 36
   local outj = 61
   local padW = 0
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --8   8     192  79   12  9   8   16  61    36    3   2   0     0
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialConvolutionMM_forward_batch5()
   local bs = 4
   local from = 6
   local to = 65
   local ki = 5
   local kj = 3
   local si = 3
   local sj = 2
   local outi = 50
   local outj = 16
   local padW = 1
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --4    6     48  101  3    5  4   56   16    50    3   2   0     1
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialUpSamplingNearest_forward_batch1()
   local nbatch = 9
   local f = 12
   local h = 11
   local w = 8
   local scale = 4

   local tm = {}
   local title = string.format('SpatialUpSamplingNearest.forward %dx%dx%dx%d -> %dx%dx%dx%d',
                               nbatch, f, h, w, nbatch, f, h*scale, w*scale)
							 --9       12 11 8  9       12 44       32  
   times[title] = tm

   local input = torch.randn(nbatch, f, h, w)
   local sconv = nn.SpatialUpSamplingNearest(scale)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = sconv:clone():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')

end

function hcnntest.SpatialUpSamplingNearest_forward_batch2()
   local nbatch = 6
   local f = 12
   local h = 7
   local w = 11
   local scale = 3

   local tm = {}
   local title = string.format('SpatialUpSamplingNearest.forward %dx%dx%dx%d -> %dx%dx%dx%d',
                               nbatch, f, h, w, nbatch, f, h*scale, w*scale)
							 --6       12 7  11 6       12  21      33
   times[title] = tm

   local input = torch.randn(nbatch, f, h, w)
   local sconv = nn.SpatialUpSamplingNearest(scale)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = sconv:clone():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')

end

function hcnntest.SpatialSubSampling_forward_batch1()
   local bs = 8
   local from = 38
   local to = from
   local ki = 4
   local kj = 2
   local si = 4
   local sj = 4
   local outi = 153
   local outj = 108
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --8   38    430  612  2   4   8   38  108   153
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialSubSampling_forward_batch2()
   local bs = 5
   local from = 33
   local to = from
   local ki = 2
   local kj = 4
   local si = 3
   local sj = 4
   local outi = 217
   local outj = 170
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.forward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --5   33    680  650  4   2   5   33  170   217 
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward, 'error on state (forward) ')
end

function hcnntest.SpatialMaxPooling_backward_batch1()
   local bs = 7
   local from = 35
   local to = from
   local ki = 4
   local kj = 3
   -- enforce testing non-atomic kernel (dW == kW) and (dH == kH)
   local si = ki
   local sj = kj
   local outi = 32
   local outj = 36
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --7   35    108  128  3   4   7   35  36    32
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialMaxPooling_backward_batch2()
   local bs = 8
   local from = 63
   local to = from
   local ki = 4
   local kj = 2
   -- enforce testing non-atomic kernel (dW == kW) and (dH == kH)
   local si = ki
   local sj = kj
   local outi = 34
   local outj = 58
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --8   63    116  136  2   4   8   63  58    34
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialMaxPooling_backward_batch3()
   local bs = 9
   local from = 14
   local to = from
   local ki = 2
   local kj = 4
   -- enforce testing non-atomic kernel (dW == kW) and (dH == kH)
   local si = ki
   local sj = kj
   local outi = 34
   local outj = 42
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialMaxPooling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --9   14    168  68   4   2   9   14  42    34
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialMaxPooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialMaxPooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialAveragePooling_backward_batch1()
   local bs = 8
   local from = 47
   local to = from
   local ki = 3
   local kj = 3
   local si = 1
   local sj = 3
   local outi = 53
   local outj = 49
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --8   47    147  55   3    3  8   47  49    53
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialAveragePooling_backward_batch2()
   local bs = 4
   local from = 61
   local to = from
   local ki = 2
   local kj = 4
   local si = 2
   local sj = 3
   local outi = 56
   local outj = 55
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialAveragePooling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --4   61    166  112  4   2   4   61  55    56
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialAveragePooling(ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialAveragePooling(ki,kj,si,sj):hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_single1()
   local from = 32
   local to = 40
   local ki = 4
   local kj = 15
   local si = 2
   local sj = 2
   local outi = 5
   local outj = 2
   local padW = 1
   local padH = 1
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --32    15   10   15  4   40   2    5     2    2  1     1
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_single2()
   local from = 15
   local to = 24
   local ki = 14
   local kj = 11
   local si = 1
   local sj = 3
   local outi = 23
   local outj = 64
   local padW = 1
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --15    200  34   11  14  24   64   23    3    1   0    1
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_single3()
   local from = 15
   local to = 24
   local ki = 5
   local kj = 11
   local si = 2
   local sj = 3
   local outi = 52
   local outj = 2
   local padW = 0
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --15    14   107  11  5   64  2     52     3  2   0      0
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_single4()
   local from = 20
   local to = 56
   local ki = 6
   local kj = 8
   local si = 1
   local sj = 3
   local outi = 14
   local outj = 49
   local padW = 1
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --20    152  17   8   6   56  49     14   3    1   0    1
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_single5()
   local from = 17
   local to = 24
   local ki = 14
   local kj = 13
   local si = 1
   local sj = 3
   local outi = 60
   local outj = 42
   local padW = 0
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%d o %dx%d -> %dx%dx%d [s: %dx%d] [p: %dx%d]',
                               from, inj, ini, kj, ki, to, outj, outi, sj, si, padH, padW)
							 --17    136  73   13  14  24  42     60    3   1   0    0
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_batch1()
   local bs = 4
   local from = 7
   local to = 48
   local ki = 6
   local kj = 8
   local si = 1
   local sj = 1
   local outi = 60
   local outj = 58
   local padW = 1
   local padH = 1
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --4   7     63   63   8   6   4    48  58    60    1   1   1     1
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

--Test Fails: Need to Debug.
--[[function hcnntest.SpatialConvolutionMM_backward_batch2()
   local bs = 12
   local from = 6
   local to = 16
   local ki = 13
   local kj = 9
   local si = 1
   local sj = 2
   local outi = 54
   local outj = 51
   local padW = 0
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
			     --12  6     109   66  9   13  12  16  51    54     2   1   0    0
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end]]--

function hcnntest.SpatialConvolutionMM_backward_batch3()
   local bs = 16
   local from = 5
   local to = 32
   local ki = 4
   local kj = 8
   local si = 1
   local sj = 3
   local outi = 30
   local outj = 18
   local padW = 0
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --16  5     59   33   8    4  16  32  18    30    3   1    0    0
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_batch4()
   local bs = 4
   local from = 6
   local to = 56
   local ki = 5
   local kj = 3
   local si = 2
   local sj = 3
   local outi = 50
   local outj = 16
   local padW = 1
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --4   6     48   101  3    5  4   56  16    50    3    2    0     1
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialConvolutionMM_backward_batch5()
   local bs = 8
   local from = 19
   local to = 64
   local ki = 10
   local kj = 14
   local si = 2
   local sj = 3
   local outi = 48
   local outj = 15
   local padW = 1
   local padH = 0
   local ini = (outi-1)*si+ki-padW*2
   local inj = (outj-1)*sj+kj-padH*2

   local tm = {}
   local title = string.format('SpatialConvolutionMM.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d [s: %dx%d] [p: %dx%d]',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi, sj, si, padH, padW)
							 --8   19    56   102  14  10  8   64  15     48    3    2   0    1	
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialConvolutionMM(from,to,ki,kj,si,sj,padW,padH):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialSubSampling_backward1()
   local from = 30
   local to = from
   local ki = 3
   local kj = 3
   local si = 4
   local sj = 4
   local outi = 39
   local outj = 36
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.backward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
							 --30    143  155  3   3   30   36    39
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialSubSampling_backward2()
   local from = 14
   local to = from
   local ki = 2
   local kj = 3
   local si = 3
   local sj = 2
   local outi = 43
   local outj = 63
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.backward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
							 --14    127  128  3    2  14   63    43
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialSubSampling_backward3()
   local from = 52
   local to = from
   local ki = 4
   local kj = 2
   local si = 2
   local sj = 4
   local outi = 62
   local outj = 38
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.backward %dx%dx%d o %dx%d -> %dx%dx%d',
                               from, inj, ini, kj, ki, to, outj, outi)
							 --52    150  126  2   4   52   38   62
   times[title] = tm

   local input = torch.randn(from,inj,ini)
   local gradOutput = torch.randn(to,outj,outi)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.SpatialSubSampling_backward_batch()
   local bs = 9
   local from = 23
   local to = from
   local ki = 3
   local kj = 4
   local si = 4
   local sj = 2
   local outi = 63
   local outj = 43
   local ini = (outi-1)*si+ki
   local inj = (outj-1)*sj+kj

   local tm = {}
   local title = string.format('SpatialSubSampling.backward %dx%dx%dx%d o %dx%d -> %dx%dx%dx%d',
                               bs, from, inj, ini, kj, ki, bs, to, outj, outi)
							 --9    23    88  251  4    3  9   23   43    63 
   times[title] = tm

   local input = torch.randn(bs,from,inj,ini)
   local gradOutput = torch.randn(bs,to,outj,outi)
   local sconv = nn.SpatialSubSampling(from,ki,kj,si,sj)
   sconv:forward(input)
   sconv:zeroGradParameters()
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      sconv:zeroGradParameters()
      groundgrad = sconv:backward(input, gradOutput)
   end
   local groundweight = sconv.gradWeight
   local groundbias = sconv.gradBias
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = nn.SpatialSubSampling(from,ki,kj,si,sj):hc()
   gconv.weight = sconv.weight:hc()
   gconv.bias = sconv.bias:hc()
   gconv:forward(input)
   gconv:zeroGradParameters()
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      gconv:zeroGradParameters()
      reshc = gconv:backward(input, gradOutput)
   end
   local weighthc = gconv.gradWeight
   local biashc = gconv.gradBias
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad
   local werror = weighthc:float() - groundweight
   local berror = biashc:float() - groundbias

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
   mytester:assertlt(werror:abs():max(), precision_backward, 'error on weight (backward) ')
   mytester:assertlt(berror:abs():max(), precision_backward, 'error on bias (backward) ')
end

function hcnntest.ClassNLLCriterionMultipleTarget1()
   local size = 4525
   local input = torch.randn(size, size)
   local target = torch.randperm(size)
   local mod = nn.ClassNLLCriterion()

   local tm = {}
   local title = string.format('ClassNLLCriterionMultiTarget %d ',size)
   times[title] = tm

   local a = torch.Timer()
   local fout = mod:forward(input, target)
   local fgin = mod:backward(input, target):clone()
   tm.cpu = a:time().real

   local cinput = input:hc()
   local ctarget = target:hc()
   local cmod = nn.ClassNLLCriterion():hc()
   a:reset()
   local cout = cmod:forward(cinput,ctarget)
   local cgin = cmod:backward(cinput,ctarget)
   hctorch.synchronize()
   tm.hc = a:time().real

   mytester:assertlt(
       math.abs(fout-cout), precision_forward, 'error on output')

   local gerr = cgin:float() - fgin
   mytester:assertlt(gerr:abs():max(), precision_forward, 'error  on gradInput')
end

function hcnntest.ClassNLLCriterionMultipleTarget2()
   local size = 3622
   local input = torch.randn(size, size)
   local target = torch.randperm(size)
   local mod = nn.ClassNLLCriterion()

   local tm = {}
   local title = string.format('ClassNLLCriterionMultiTarget %d ',size)
   times[title] = tm

   local a = torch.Timer()
   local fout = mod:forward(input, target)
   local fgin = mod:backward(input, target):clone()
   tm.cpu = a:time().real

   local cinput = input:hc()
   local ctarget = target:hc()
   local cmod = nn.ClassNLLCriterion():hc()
   a:reset()
   local cout = cmod:forward(cinput,ctarget)
   local cgin = cmod:backward(cinput,ctarget)
   hctorch.synchronize()
   tm.hc = a:time().real

   mytester:assertlt(
       math.abs(fout-cout), precision_forward, 'error on output')

   local gerr = cgin:float() - fgin
   mytester:assertlt(gerr:abs():max(), precision_forward, 'error  on gradInput')
end

function hcnntest.LogSoftMax_forward1()
   local size = 224

   local tm = {}
   local title = string.format('LogSoftMax forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.LogSoftMax()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.LogSoftMax():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward*10, 'error on state (forward) ')
end

function hcnntest.LogSoftMax_forward2()
   local size = 140

   local tm = {}
   local title = string.format('LogSoftMax forward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local sconv = nn.LogSoftMax()
   local groundtruth = sconv:forward(input)
   local a = torch.Timer()
   for i = 1,nloop do
      groundtruth = sconv:forward(input)
   end
   tm.cpu = a:time().real

   input = input:hc()
   local gconv = nn.LogSoftMax():hc()
   local reshc = gconv:forward(input)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:forward(input)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundtruth
   mytester:assertlt(error:abs():max(), precision_forward*10, 'error on state (forward) ')
end

function hcnntest.LogSoftMax_backward()
   local size = 34

   local tm = {}
   local title = string.format('LogSoftMax.backward %d -> %d', size, size)
   times[title] = tm

   local input = torch.randn(size)
   local gradOutput = torch.randn(size)
   local sconv = nn.LogSoftMax()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function hcnntest.LogSoftMax_backward_batch()
   local size = 178
   local bs = 163

   local tm = {}
   local title = string.format('LogSoftMax.backward batch %d x %d -> %d x %d', bs, size, bs, size)
   times[title] = tm

   local input = torch.randn(bs, size)
   local gradOutput = torch.randn(bs, size)
   local sconv = nn.LogSoftMax()
   sconv:forward(input)
   local groundgrad = sconv:backward(input, gradOutput)
   local a = torch.Timer()
   for i = 1,nloop do
      groundgrad = sconv:backward(input, gradOutput)
   end
   tm.cpu = a:time().real

   input = input:hc()
   gradOutput = gradOutput:hc()
   local gconv = sconv:clone():hc()
   gconv:forward(input)
   local reshc = gconv:backward(input, gradOutput)
   a:reset()
   for i = 1,nloop do
      reshc = gconv:backward(input, gradOutput)
   end
   hctorch.synchronize()
   tm.hc = a:time().real

   local error = reshc:float() - groundgrad

   mytester:assertlt(error:abs():max(), precision_backward, 'error on state (backward) ')
end

function deterministic_test(tests)
   local oldtype = torch.getdefaulttensortype()
   torch.setdefaulttensortype('torch.FloatTensor')
   math.randomseed(os.time())
   mytester = torch.Tester()
   mytester:add(hcnntest)
   mytester:run(tests)
   torch.setdefaulttensortype(oldtype)
   print ''
   print ' ------------------------------------------------------------------------------------------------'
   print '|  Module                                                                          |  Speedup    |'
   print ' ------------------------------------------------------------------------------------------------'
   for module,tm in pairs(times) do
      local str = string.format('| %-80s | %4.2f        |', module, (tm.cpu / (tm.hc or 1e6)))
      print(str)
   end
   print ' ------------------------------------------------------------------------------------------------'
end

if runtests then 
  require 'hcnn'
  deterministic_test()
end
