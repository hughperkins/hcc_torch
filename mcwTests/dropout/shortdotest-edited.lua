-- simple test
require 'nn'
--cpu = true
amd = true
--cuda = true
--if not amd and not cuda then print('MUST SET EITHER amd or cuda to true: exiting') os.exit() end
seed = 1

-- for s9150
if amd then
    require 'hctorch'
    require 'hcnn'
end

-- for cuda
if cuda then
    require 'cutorch'
    require 'cunn'
end


print('set device, seeds')
devicenum = devicenum or 1
if amd then  hctorch.setDevice(devicenum) end
if cuda then cutorch.setDevice(devicenum)  end

--set the random seed
torch.manualSeed(seed)
if amd  then hctorch.manualSeed(seed) end
if cuda then cutorch.manualSeed(seed)  end

-- build helper functions
function outputSize(cols, rows, filter, pool)
    local size = nil
    local wout = 0
    local hout = 0
    if cols and rows and filter and pool then
        local width        = cols         or nil
        local height       = rows         or nil
        local numfilters   = filter.fout  or nil
        local filterw      = filter.kw    or nil
        local filterh      = filter.kh    or nil
        local filterstepw  = filter.stepw or 1
        local filtersteph  = filter.steph or 1
        local filterpad    = filter.pad   or 0
        local poolw        = pool.kw      or nil
        local poolh        = pool.kh      or nil
        local poolstepw    = pool.stepw   or 1
        local poolsteph    = pool.steph   or 1

        if width and height and numfilters and filterw and filterh and filterstepw and filtersteph and filterpad and poolw and poolh and poolstepw and poolsteph then

            width  = width  + 2*filterpad
            height = height + 2*filterpad
            wout   = math.floor(((width  - filterw+1) / filterstepw) / poolstepw)
            hout   = math.floor(((height - filterh+1) / filtersteph) / poolsteph)
            size   = wout * hout * numfilters
        end
    end
    size = size or 0
    return size, wout, hout
end

print('create the net')
netsize = 100
net = nn.Sequential()
net:add(nn.Linear(netsize, netsize))
net:add(nn.Dropout(0.5))

if amd  then net = net:hc()  end
if cuda then net = net:cuda() end

print('create inputs of', netsize)
inputs = torch.range(1,netsize)

if amd  then inputs = inputs:hc()  end
if cuda then inputs = inputs:cuda() end

print('forward pass')
outputs = net:forward(inputs)

-- create file name, save relu outputs
filestr = 'cpu_relu_outputs_short'
if amd  then filestr = 'dropout/amd_relu_outputs_short'  end
if cuda then filestr = 'cuda_relu_outputs_short' end
--print('save layer 2 - ReLU - output to torch file as FloatTensor', filestr)

if amd or cuda then 
   ro = net.modules[2].output:float()
else
   ro = net.modules[2].output
end
--print(ro)

torch.save(filestr, ro)

function isEqual(name1, name2)
    name1 = name1 or 'cuda_relu_outputs_short'
    name2 = name2 or 'dropout/amd_relu_outputs_short'
    print('using ', name1, name2)
    t1 = torch.load(name1)
    t2 = torch.load(name2)

    maxerror = 1e4
    local result = true
    for i = 1, t1:size(1) do
        v1 = (t1[i]*maxerror)
        v2 = (t2[i]*maxerror)
        --if v1 ~= v2 then print(string.format('index %i, t1[%i] %f, v1 %f, t2[%i] %f, v2 %f', i, i, t1[i], v1, i, t2[i], v2)) result = false break end
        if math.abs(t1[i] - t2[i]) > 1e-4 then print(string.format('index %i, t1[%i] %f, v1 %f, t2[%i] %f, v2 %f', i, i, t1[i], v1, i, t2[i], v2)) result = false end
    end
    return result
end

--compare cuda and amd
if not isEqual('dropout/cuda_relu_outputs_short', 'dropout/amd_relu_outputs_short') then
  print ('FAILED: cuda and amd result is not equal')
else
  print ('PASS: cuda and amd result is equal')
end
