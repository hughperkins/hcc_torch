require 'nn'

local function outputSize(cols, rows, filter, pool)

    local size = nil
    local wout = 0
    local hout = 0
    if cols and rows and filter and pool then
        local width        = cols         or nil
        local height       = rows         or nil
        local numfilters   = filter.fout  or nil
        local filterw      = filter.kw    or nil
        local filterh      = filter.kh    or nil
        local filterstepw  = filter.stepw or 1
        local filtersteph  = filter.steph or 1
        local filterpad    = filter.pad   or 0
        local poolw        = pool.kw      or nil
        local poolh        = pool.kh      or nil
        local poolstepw    = pool.stepw   or 1
        local poolsteph    = pool.steph   or 1

        if width and height and numfilters and filterw and filterh and filterstepw and filtersteph and filterpad and poolw and poolh and poolstepw and poolsteph then

            width  = width  + 2*filterpad
            height = height + 2*filterpad
            wout   = math.floor(((width  - filterw+1) / filterstepw) / poolstepw)
            hout   = math.floor(((height - filterh+1) / filtersteph) / poolsteph)
            size   = wout * hout * numfilters
        end
    end
    if size == nil then verbosep("outputSize() invalid args", cols, rows, filter, pool) end
    size = size or 0
    return size, wout, hout
end

--local type = "torch.FLoatTensor"    --Check CPU
local type = "torch.HcTensor"      --Check Hc
--local type = "torch.CudaTensor"     --check CUDA

print('do requires')
torch.manualSeed(1)
if type == "torch.CudaTensor" then

   require 'cunn'
   cutorch.manualSeed(1)
   cutorch.setDevice(1)
elseif type == "torch.HcTensor" then

   require 'hcnn'
   hctorch.manualSeed(1)
   hctorch.setDevice(1)

end

f1_1 = {fin=1, fout=1, kw=15,    kh=15}
p1   = {kw=2,  kh=2,    stepw=2,  steph=2}
s1Out = outputSize(40, 120, f1_1, p1)

print('creating the model, inputs, labels')
model = nn.Sequential()
model:add(nn.SpatialConvolutionMM(f1_1.fin, f1_1.fout, f1_1.kw, f1_1.kh))
model:add(nn.SpatialBatchNormalization(f1_1.fout))
model:add(nn.SpatialMaxPooling(p1.kw, p1.kh, p1.stepw , p1.steph))
model:add(nn.ReLU())
model:add(nn.View(s1Out))
model:add(nn.Linear(s1Out, 5))
loss = nn.MSECriterion()

-- create 1 batch of 16 input and labels
inputs = torch.range(1,4800):view(1, 1, 40, 120)
inputs = torch.expand(inputs, 16, 1, 40, 120)
labels = torch.range(1,5):view(1,5)
labels = torch.expand(labels, 16, 5)

print('do casts')
if type == "torch.CudaTensor" then

   model:cuda()
   loss:cuda()
   inputs = inputs:cuda()
   labels = labels:cuda()

elseif type == "torch.HcTensor" then

   model:hc()
   loss:hc()
   inputs = inputs:hc()
   labels = labels:hc()

end

print('test: fwd')
for i = 1, 10000 do

    --io.write('*')
    outputs = model:forward(inputs):type(type)
    err     = loss:forward(outputs, labels) * 16
    df_dWs  = loss:backward(outputs, labels)
    model:backward(inputs, df_dWs)

end
print ('test PASS')
