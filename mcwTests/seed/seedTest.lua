-- test seed
require 'nn'
require 'hctorch'
require 'hcnn'

tolerance = 1e-4
local function isEqual(a, b, tolerance, ...)
   if a == nil and b == nil then return true end
   if a == nil and b ~= nil then return false end
   if a ~= nil and b == nil then return false end
   if torch.type(b) ~= torch.type(a) then
      --[[
        TODO: memory leak for original b is lost and won't be released (At least 1 Hc memory allocation)
             type(a) is torch.FloatTensor , type(b) is torch.HcTensor
      ]]--
      b = b:typeAs(a) -- TODO: remove the need for this (a-b doesnt work for bytetensor, hctensor pairs)
   end
   local diff = a-b
   tolerance = tolerance or 0.000001
   if type(a) == 'number' then
      return math.abs(diff) < tolerance
   else
      if torch.type(diff) ~= 'torch.FloatTensor' then
         diff = diff:float() -- TODO: remove the need for this (byteTensor and abs)
      end
      return diff:abs():max() < tolerance
   end
end


--compare cuda and amd
seed = 1
hctorch.manualSeed(seed)
hc_seed_1 = torch.HcTensor():rand(2,4)
cuda_seed_1_rand2X4 = torch.Tensor({{0.2921,0.2327,0.6960,0.2313},{0.7935,0.6467,0.5798,0.7534}})
if not isEqual(cuda_seed_1_rand2X4, hc_seed_1, tolerance) then
  print ('',hc_seed_1)
  print ('!!!!!!!!!!!!!!FAILED: cuda and amd result is not equal, seed = ', seed)
else
  print ('PASS: cuda and amd result is equal')
end

