cd mcw_nn
rm -rf build/
luarocks make rocks/nn-scm-1.rockspec
th -lnn -e "nn.test()"
cd ../hctorch
rm -rf build/
luarocks make rocks/hctorch-scm-1.rockspec
th -lhctorch -e "hctorch.test()"
cd ../hcnn
rm -rf build/
luarocks make rocks/hcnn-scm-1.rockspec
th -lhcnn -e "nn.testhc()"
