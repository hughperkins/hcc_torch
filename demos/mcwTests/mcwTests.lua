require 'nn'

--Choose the type based on backend
local defaultType = "torch.FloatTensor"  
--local defaultType = "torch.CudaTensor"
--local defaultType = "torch.HcTensor" 
local seed = 12345
local device = 1

torch.manualSeed(seed)
if defaultType == "torch.CudaTensor" then
   require 'cunn'
   cutorch.setDevice(device)
   cutorch.manualSeed(seed)
elseif defaultType == "torch.HcTensor" then
   require 'hcnn'
   hctorch.setDevice(device)
   hctorch.manualSeed(seed)
end

local defaultDir = paths.concat(os.getenv('PWD'))

local model = nn.SpatialConvolutionMM(1,32,5,5)
local input = torch.Tensor(4,1,32,32)

if defaultType == "torch.CudaTensor" then
   print("CUDA backend")
   model = model:cuda()
   input = input:cuda()
   local output = model:forward(input)
   torch.save(paths.concat(defaultDir, 'cuda-output.t7'), output:float())
elseif defaultType == "torch.HcTensor" then
   print("AMP backend")
   model = model:hc()
   input = input:hc()
   local output = model:forward(input)
   torch.save(paths.concat(defaultDir, 'amp-output.t7'), output:float())
else
   print("CPU backend")
   local output = model:forward(input)
   torch.save(paths.concat(defaultDir, 'cpu-output.t7'), output:float())
end

local function compare_results(name1, name2)
   if name1 == nil or name2 == nil then return end
   print(name1 .. " VS " .. name2)
   local errAccept = 1e-4

   output1 = torch.load(paths.concat(defaultDir, name1 .. '-output.t7'))
   output2 = torch.load(paths.concat(defaultDir, name2 .. '-output.t7'))

   local storage1 = output1:storage()
   local storage2 = output2:storage()

   if storage1:size() ~= storage2:size() then print("Size mismatch") print("TEST FAILED") return end 
   for i = 1, storage1:size() do
      if math.abs(storage1[i] - storage2[i]) > errAccept then
         print("Err: Index:" .. i .. ", " .. storage1[i] .. " and " .. storage2[i]  .. " are not same")
         print("TEST FAILED")
         return
      end
   end
   print("TEST PASSED")
end

--Enable any of these tests to compare the results
--compare_results('cpu', 'cuda')
--compare_results('cpu', 'amp')
--compare_results('cuda', 'amp')
